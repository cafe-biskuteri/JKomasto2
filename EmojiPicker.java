
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.Box;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.event.DocumentListener;
import javax.swing.event.DocumentEvent;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import cafe.biskuteri.hinoki.Tree;
import cafe.biskuteri.hinoki.DSVTokeniser;

class
EmojiPicker extends JFrame
implements ImageAPI2.Listener {

    private PriorityExecutor
    emojiThreads;

    private DirectoryCache
    emojiCache;

//   -  -%-  -

    private EmojiPickerComponent
    display;

    private LinkedHashMap<String, Emoji>
    emojis,
    unicodeEmojis;

    private EmojiPicker.Listener
    listener;

//  ---%-@-%---

    public void
    addEmoji(Emoji emoji)
    {
        assert emoji != null;
        assert emoji.string != null;
        assert emoji.label != null;
        assert emoji.keywords != null;
        emojis.put(emoji.string, emoji);
    }

    public boolean
    hasEmoji(Emoji emoji)
    {
        assert emoji != null;
        assert emoji.string != null;

        return emojis.containsKey(emoji.string);
        /*
        * For the purposes of EmojiPicker, the emoji string
        * (for a remote emoji, the shortcode flanked by colons)
        * is the unique ID for the emoji. A remote emoji's
        * shortcode itself, at time of writing the value of
        * emoji.label, is not acceptable because we might
        * want a different printable name for an emoji. So the
        * string that's pasted into a text pane is what's up.
        */
    }

    public void
    beginLoadingRemoteEmojis()
    {
        for (Emoji emoji: emojis.values())
        {
            if (emoji.glyph != null) continue;
            if (emoji.glyphResolver != null) continue;

            assert emoji.urlr != null;

            emoji.glyphResolver = this;
            emojiThreads.execute(ImageAPI2.fetch(
                ImageAPI2.URLs.remote(emoji.urlr),
                "emoji:" + emoji.string, -1, this));
        }
    }

    public void
    addEmojisFromCache()
    {
        if (emojiCache == null) return;

        for (String key: emojiCache.keySet())
        {
            String emojiString = key;
            String shortcode = emojiString
                .substring(1, emojiString.length() - 1);

            Image glyph; try
            {
                glyph = emojiCache.get(
                    key, new ImageIODirectoryCacheHandler());
                // (未) Make this async. As in, don't call #get here.
            }
            catch (IOException eIo) { continue; }

            Emoji emoji = new Emoji();
            emoji.string = emojiString;
            emoji.label = shortcode;
            emoji.keywords = new String[] { shortcode };
            emoji.glyph = glyph;
            addEmoji(emoji);
        }
    }

    public void
    refreshEmojis()
    {
        List<Emoji> emojis = new ArrayList<>();
        emojis.addAll(this.emojis.values());
        emojis.addAll(unicodeEmojis.values());
        display.setEmojis(emojis);
    }

    public void
    setFont(Font font)
    {
        super.setFont(font);

        boolean constructed = display != null;
        if (!constructed) return;

        display.setFont(font);
    }

    public void
    setListener(EmojiPicker.Listener listener)
    {
        this.listener = listener;
    }

//   -  -%-  -

    public void
    emojiPicked(Emoji emoji)
    {
        if (listener != null) listener.emojiPicked(emoji);
    }

    public void
    imageReady(String name, Image image)
    {
        if (name.equals("spritesheet"))
        {
            loadUnicodeEmojis(image, "emojis.dsv");
            refreshEmojis();
        }

        if (name.startsWith("emoji:"))
        {
            name = name.substring("emoji:".length());
            
            Emoji emoji = emojis.get(name);
            assert emoji != null;
            emoji.glyph = image;
            emoji.glyphResolver = null;

            display.refresh(emoji.string);
            addEmojiToCache(emoji);
        }
    }

    private void
    addEmojiToCache(Emoji emoji)
    {
        if (emojiCache == null) return;

        try
        {
            emojiCache.add(
                emoji.string,
                emoji.glyph,
                new ImageIODirectoryCacheHandler());
        }
        catch (IOException eIo)
        {
            // Forget it.
            eIo.printStackTrace();
        }
    }

//  ---%-@-%---

    public interface
    Listener {

        public void
        emojiPicked(Emoji emoji);

    }

//  ---%-@-%---

    public static void
    main(String... args)
    throws IOException
    {
        convertEmojiTestToDSV();
    }

    public static void
    convertEmojiTestToDSV()
    throws IOException
    {
        BufferedReader r; InputStream is;
        is = EmojiPicker.class.getResourceAsStream("emoji-test.txt");
        r = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Font font = new Font("Firefox Emoji", Font.PLAIN, 12);

        String group = null, subgroup = null;
        String line; while ((line = r.readLine()) != null)
        {
            if (line.startsWith("# group: "))
            {
                group = line.substring("# group: ".length());
                continue;
            }
            if (line.startsWith("# subgroup: "))
            {
                subgroup = line.substring("# subgroup: ".length());
                continue;
            }
            if (line.matches("^ *$") || line.startsWith("#"))
            {
                continue;
            }

            String[] f1 = line.split(";", 2);
            String[] f2 = f1[1].split("#", 2);
            f1[0] = f1[0].trim();
            f2[0] = f2[0].trim();
            f2[1] = f2[1].trim().replaceAll("^.*E[^ ]* ", "");

            String[] codep = f1[0].split(" ");
            int[] pcodep = new int[codep.length];
            for (int o = 0; o < codep.length; ++o) try
            {
                pcodep[o] = Integer.parseInt(codep[o], 16);
            }
            catch (NumberFormatException eNf)
            {
                assert false;
            }

            String string = new String(pcodep, 0, pcodep.length);
            String status = f2[0];
            String description = f2[1].replaceAll(":", "\\\\:");
            // Why did we have to escape the backslash twice..?
            // The second argument is not a regex..
            String[] words = f2[1].replaceAll(":", "").split(" ");

            if (!status.equals("fully-qualified")) continue;
            if (font.canDisplayUpTo(string) == 0) continue;

            System.out.print(string);
            System.out.print(":" + description);
            System.out.print(":[" + group);
            System.out.print(":" + subgroup);
            for (String word: words) System.out.print(":" + word);
            System.out.println("]");
        }
    }

//  ---%-@-%---

    public static class
    Emoji {

        public String
        string,
        label;

        public Image
        glyph;

        public String[]
        keywords;

        public String
        urlr;

        public Object
        glyphResolver;

    }

//  ---%-@-%---

    EmojiPicker(PriorityExecutor emojiThreads, DirectoryCache emojiCache)
    {
        this.emojiThreads = emojiThreads;
        this.emojiCache = emojiCache;

        emojis = new LinkedHashMap<>();
        unicodeEmojis = new LinkedHashMap<>();
        listener = null;

        display = new EmojiPickerComponent(this, emojiThreads);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setContentPane(display);
        setSize(320, 640);
        setLocationByPlatform(true);
        setTitle(EnglishStrings.get("emojis title"));

        setFont(new Font("VL Gothic", Font.PLAIN, 15));
        /*
        * (知) Always set on the window first. For lightweight
        * components, their becoming shown gets a default set font
        * from the L&F. But for windows, it will respect the font
        * you had set during construction when it becomes shown.
        */

        loadUnicodeEmojis();
    }

    private void
    loadUnicodeEmojis()
    {
        ImageAPI2.fetch(
            ImageAPI2.URLs.local("emojis"),
            "spritesheet", 0, this).run();
    }

    private void
    loadUnicodeEmojis(Image spritesheet, String emojiListFilepath)
    {
        assert spritesheet != null;
        int emojiSize = 32;

        Reader r = resourceFileToReader(emojiListFilepath);
        if (r == null) return;

        DSVTokeniser.Options o = new DSVTokeniser.Options();
        int x = 0, y = 0, w = spritesheet.getWidth(null);
        Tree<String> line; while (true) try
        {
            line = DSVTokeniser.tokenise(r, o);
            if (line.get(0).value.equals(o.endOfStreamValue))
                break;

            String string = line.get(0).value;
            String label = line.get(1).value;

            Tree<String> tKeywords = line.get(2);
            String[] keywords = new String[tKeywords.size()];
            for (int ko = 0; ko < tKeywords.size(); ++ko)
                keywords[ko] = tKeywords.get(ko).value;

            Image glyph = ImageAPI2.ImageOps.subimage(
                spritesheet, x, y, emojiSize, emojiSize);

            Emoji addee = new Emoji();
            addee.string = string;
            addee.label = label;
            addee.keywords = keywords;
            addee.glyph = glyph;
            unicodeEmojis.put(addee.string, addee);

            if ((x += emojiSize) >= w)
            {
                y += emojiSize;
                x = 0;
            }
        }
        catch (IOException eIo) { break; }
    }

//   -  -%-  -

    private static InputStreamReader
    resourceFileToReader(String filepath)
    {
        try
        {
            return new InputStreamReader(
                EmojiPicker.class.getResourceAsStream(filepath),
                "UTF-8");
        }
        catch (IOException eIo)
        {
            return null;
        }
    }

}

class
EmojiPickerComponent extends JPanel
implements
    DocumentListener, MouseListener, MouseMotionListener,
    ImageAPI2.Listener {

    private EmojiPicker
    primaire;

    private PriorityExecutor
    emojiThreads;

//   -  -%-  -

    private JScrollPane
    scroll;

    private ImageComponent
    preview;

    private Image
    glyph, scaledGlyph;

    private JTextField
    search;

    private Box
    listing;

    private HashMap<String, EmojiComponent>
    emojiComponents;

    private EmojiComponent
    lastHoveredEmoji;

    private Dimension
    nsz;

//  ---%-@-%---

    public void
    setEmojis(List<EmojiPicker.Emoji> emojis)
    {
        assert emojis != null;

        Font font = getFont();

        listing.removeAll();
        for (EmojiPicker.Emoji emoji: emojis)
        {
            EmojiComponent c = new EmojiComponent(
                emoji, emojiThreads);
            c.setFont(font);
            c.addMouseListener(this);
            c.addMouseMotionListener(this);
            listing.add(c);
            emojiComponents.put(c.getEmoji().string, c);
        }
    }

    public void
    filter(String substring)
    {
        assert substring != null;
        boolean go = !substring.isEmpty();

        if (go) for (Component uc: listing.getComponents())
        {
            EmojiComponent c = (EmojiComponent)uc;
            EmojiPicker.Emoji emoji = c.getEmoji();

            boolean match = emoji.string.indexOf(substring) != -1;
            for (String keyword: emoji.keywords)
                match |= keyword.indexOf(substring) != -1;

            c.setVisible(match);
        }
        else for (Component uc: listing.getComponents())
        {
            EmojiComponent c = (EmojiComponent)uc;
            c.setVisible(true);
        }
    }

    public void
    setFont(Font font)
    {
        super.setFont(font);

        boolean constructed = listing != null;
        if (!constructed) return;

        String name = font.getName();
        int baseSize = font.getSize();
        Font large = new Font(name, Font.ITALIC, baseSize * 14/12);
        Font small = new Font(name, Font.PLAIN, baseSize);

        search.setFont(large);

        for (Component child: listing.getComponents())
        {
            assert child instanceof EmojiComponent;
            child.setFont(small);
        }
    }

    public void
    refresh(String emojiString)
    {
        EmojiComponent c = emojiComponents.get(emojiString);
        if (c == null) return;
        c.refreshEmoji();
    }

//   -  -%-  -

    public void
    mouseMoved(MouseEvent eM)
    {
        assert eM.getSource() instanceof EmojiComponent;
        EmojiComponent src = (EmojiComponent)eM.getSource();
        if (src == lastHoveredEmoji) return;
        EmojiPicker.Emoji emoji = src.getEmoji();

        scaledGlyph = null;
        preview.repaint();

        if (emoji.glyph != null)
        {
            Dimension nsz = ImageAPI2.ImageOps.containFit(
                emoji.glyph.getWidth(null),
                emoji.glyph.getHeight(null),
                preview.getWidth(), preview.getHeight());

            emojiThreads.execute(ImageAPI2.scale(
                emoji.glyph, nsz.width, nsz.height,
                null, 1, this));
        }

        lastHoveredEmoji = src;
    }

    public void
    mouseClicked(MouseEvent eM)
    {
        assert eM.getSource() instanceof EmojiComponent;
        EmojiComponent src = (EmojiComponent)eM.getSource();
        EmojiPicker.Emoji emoji = src.getEmoji();
        primaire.emojiPicked(emoji);
    }

    public void
    mouseEntered(MouseEvent eM)
    {
        setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

    public void
    mouseExited(MouseEvent eM)
    {
        setCursor(null);
    }

    public void
    insertUpdate(DocumentEvent eD) { filter(search.getText()); }

    public void
    removeUpdate(DocumentEvent eD) { filter(search.getText()); }

    public void
    imageReady(String name, Image image)
    {
        scaledGlyph = image;
        preview.repaint();
    }


    public void
    mousePressed(MouseEvent eM) { }

    public void
    mouseReleased(MouseEvent eM) { }

    public void
    mouseDragged(MouseEvent eM) { }

    public void
    changedUpdate(DocumentEvent eD) { }

//  ---%-@-%---

    public class
    ImageComponent extends JComponent {

        protected void
        paintComponent(Graphics g)
        {
            g.clearRect(0, 0, getWidth(), getHeight());
            if (scaledGlyph != null)
            {
                int sw = scaledGlyph.getWidth(null);
                int sh = scaledGlyph.getHeight(null);
                int x = (getWidth() - sw) / 2;
                int y = (getHeight() - sh) / 2;
                g.drawImage(scaledGlyph, x, y, sw, sh, null);
            }
        }

    }

//  ---%-@-%---

    EmojiPickerComponent(
        EmojiPicker primaire, PriorityExecutor emojiThreads)
    {
        this.primaire = primaire;
        this.emojiThreads = emojiThreads;

        nsz = new Dimension();

        search = new JTextField();
        search.getDocument().addDocumentListener(this);

        JLabel searchLabel = new JLabel(
            EnglishStrings.get("emojis search"));
        searchLabel.setLabelFor(search);

        JPanel topRight = new JPanel();
        topRight.setLayout(new BorderLayout());
        topRight.add(searchLabel, BorderLayout.NORTH);
        topRight.add(search);

        preview = new ImageComponent();
        preview.setPreferredSize(new Dimension(48, 48));

        JPanel top = new JPanel();
        top.setLayout(new BorderLayout(8, 0));
        top.add(preview, BorderLayout.WEST);
        top.add(topRight);

        emojiComponents = new HashMap<>();

        listing = Box.createVerticalBox();
        scroll = new JScrollPane(
            listing,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );
        scroll.getVerticalScrollBar().setUnitIncrement(32);
        scroll.setBorder(null);

        setLayout(new BorderLayout(0, 8));
        add(top, BorderLayout.NORTH);
        add(scroll);
    }

}

class
EmojiComponent extends AbstractButton
implements ImageAPI2.Listener {

    private EmojiPicker.Emoji
    emoji;

    private PriorityExecutor
    emojiThreads;

//   -  -%-  -

    private Image
    scaledGlyph;

    private JLabel
    label;

    private Rectangle
    glyphArea;

    private Dimension
    nsz;

//  ---%-@-%---

    public EmojiPicker.Emoji
    getEmoji() { return emoji; }

    public void
    refreshEmoji() { considerRescale(); }

//   -  -%-  -

    public Dimension
    getPreferredSize()
    {
        Dimension lsz = label.getPreferredSize();
        int h = Math.max(getHeight(), lsz.height);
        return new Dimension(h + lsz.width, h);
    }

    public Dimension
    getMaximumSize()
    {
        Dimension lsz = label.getPreferredSize();
        int h = Math.max(getHeight(), lsz.height);
        return new Dimension(Integer.MAX_VALUE, h);
    }

    private void
    considerRescale()
    {
        boolean spurious = !isDisplayable() || !isVisible();
        if (emoji.glyph != null && !spurious)
        {
            ImageAPI2.ImageOps.containFit(
                nsz,
                emoji.glyph.getWidth(null),
                emoji.glyph.getHeight(null),
                glyphArea.width, glyphArea.height);

            boolean rescale = true;
            if (scaledGlyph != null)
            {
                rescale =
                    scaledGlyph.getWidth(null) != nsz.width
                    || scaledGlyph.getHeight(null) != nsz.height;
            }

            boolean rescaling = scaledGlyph == emoji.glyph;
            if (rescale && !rescaling)
            {
                scaledGlyph = emoji.glyph;
                emojiThreads.execute(ImageAPI2.scale(
                    emoji.glyph, nsz.width, nsz.height,
                    null, 1, this));
            }
        }
    }

    public void
    doLayout()
    {
        int w = getWidth(), h = getHeight();
        int x0 = h;
        int x1 = x0 + h;
        int x2 = x1 + h;
        int x3 = w - h;

        glyphArea.setBounds(x0, 0, x1 - x0, h);
        considerRescale();
        
        label.setLocation(x2, 0);
        label.setSize(x3 - x2, h);
    }

    protected void
    paintComponent(Graphics g)
    {
        if (scaledGlyph != null)
        {
            ImageApi.containFit(
                nsz,
                scaledGlyph.getWidth(null),
                scaledGlyph.getHeight(null),
                glyphArea.width, glyphArea.height);

            g.drawImage(
                scaledGlyph,
                glyphArea.x + (glyphArea.width - nsz.width) / 2,
                glyphArea.y + (glyphArea.height - nsz.height) / 2,
                nsz.width,
                nsz.height, null);
        }
    }

    public void
    setFont(Font font)
    {
        super.setFont(font);

        boolean constructed = label != null;
        if (!constructed) return;

        label.setFont(font);
    }

    public void
    imageReady(String name, Image image)
    {
        scaledGlyph = image;
        repaint();
    }

//  ---%-@-%---

    EmojiComponent(
        EmojiPicker.Emoji emoji, PriorityExecutor emojiThreads)
    {
        assert emoji != null;
        assert emoji.string != null;
        assert emoji.keywords != null;
        assert emojiThreads != null;
        this.emoji = emoji;
        this.emojiThreads = emojiThreads;

        label = new JLabel(emoji.string);
        label.setText(emoji.label);

        glyphArea = new Rectangle();
        nsz = new Dimension();

        setLayout(null);
        add(label);

        setToolTipText(emoji.string);
    }

}
