
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.Image;
import java.net.URL;
import java.net.MalformedURLException;
import cafe.biskuteri.hinoki.Tree;

class
ImageWindow extends JFrame
implements MouseListener, ImageFetcher.Listener {

    private ImageFetcher
    fetcher;

    private Attachment[]
    attachments;

    private int
    offset;

//   -  -%-  -

    private ImageComponent
    display;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    use(Attachment[] attachments, boolean hidden)
    {
        assert attachments != null;

        if (hidden) attachments = new Attachment[0];
        /*
        * A bit crude, but to do it properly means always
        * showing a hidden description regardless of which
        * image we are in. Would need some more code.
        */

        synchronized (this)
        {
            if (this.attachments != null)
            {
                for (Attachment a: this.attachments)
                    if (a.imageResolver == this)
                        a.imageResolver = null;
            }

            this.attachments = attachments;

            if (attachments.length == 0)
            {
                display.setImage(null);
                String s = JapaneseStrings.get("image none");
                display.setDescription(s);
                display.setShowDescription(true);
                display.setNext(null);
                display.setPrev(null);
                display.repaint();
                return;
            }

            display.setShowDescription(false);
            toImage(offset = 0);

            for (int o = 0; o < attachments.length; ++o)
            {
                Attachment a = attachments[o];
                if (a.image != null) continue;
                if (a.imageResolver != null) continue;

                String name = attachments.hashCode() + ":" + o;
                a.imageResolver = this;
                fetcher.fetch(a.url, name, 2, this);
            }
        }
    }

    public void
    toNextImage()
    {
        synchronized (this)
        {
            if (attachments.length == 0) return;
            assert offset < attachments.length - 1;
            toImage(offset + 1);
        }
    }

    public void
    toPrevImage()
    {
        synchronized (this)
        {
            if (attachments.length == 0) return;
            assert offset > 0;
            toImage(offset - 1);
        }
    }

    public void
    toImage(int offset)
    {
        int last = attachments.length  - 1;
        assert offset >= 0;
        assert offset < attachments.length;

        this.offset = offset;

        Attachment prev, curr, next;
        curr = attachments[offset];
        next = offset < last ? attachments[offset + 1] : null;
        prev = offset > 0 ? attachments[offset - 1] : null;

        display.setImage(curr.image);
        display.setNext(next != null ? next.image : null);
        display.setPrev(prev != null ? prev.image : null);
        String s1 =
            curr.description != null
                ? curr.description + "\n\n"
                : "";
        String s2 =
            JapaneseStrings.get("image type template")
                .replaceAll("<>", curr.type);
        display.setDescription(s1 + s2);

        display.repaint();
    }

    public void
    resetFocus() { display.resetFocus(); }

//   -  -%-  -

    public void
    imageReady(String name, Image image)
    {
        String[] fields = name.split(":");
        assert fields.length > 1;
        int o = -1; try {
            o = Integer.parseInt(fields[1]);
        }
        catch (NumberFormatException eNf) {
            assert false;
        }

        if (name.equals(attachments.hashCode()))
        {
            assert o >= 0 && o < attachments.length;
            assert attachments[o].imageResolver == this;
            attachments[o].image = image;
            attachments[o].imageResolver = null;
            if (o == this.offset) toImage(this.offset);
        }
    }

    public void
    mouseClicked(MouseEvent eM)
    {
        assert eM.getSource() == display;

        display.setCursor(WAIT);

        if (eM.getButton() == MouseEvent.BUTTON3)
            dispose();

        display.setCursor(null);
    }

    public void
    mousePressed(MouseEvent eM) { }

    public void
    mouseReleased(MouseEvent eM) { }

    public void
    mouseEntered(MouseEvent eM) { }

    public void
    mouseExited(MouseEvent eM) { }

//  ---%-@-%---

    ImageWindow(JKomasto primaire)
    {
        fetcher = primaire.getImageFetcher();

        display = new ImageComponent(this, fetcher);
        setContentPane(display);
        display.addMouseListener(this);

        setIconImage(primaire.getProgramIcon());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(600, 600);

        addKeyListener(new SharedKeyShortcuts(null, true));

        use(new Attachment[0], false);
    }

}

class
Centrer extends JPanel {

    public void
    setMargin(int top, int left, int bottom, int right)
    {
        setBorder(
            BorderFactory.createEmptyBorder(
                top, left, bottom, right));
    }

//  ---%-@-%---

    public
    Centrer(Component c)
    {
        setOpaque(false);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JPanel middle = new JPanel();
        middle.setOpaque(false);
        middle.setLayout(new BoxLayout(middle, BoxLayout.X_AXIS));
        boolean limited = c.getPreferredSize().width != 0;
        if (limited) middle.add(Box.createHorizontalGlue());
        middle.add(c);
        if (limited) middle.add(Box.createHorizontalGlue());

        add(Box.createVerticalGlue());
        add(middle);
        add(Box.createVerticalGlue());
    }

}

class
ImageComponent extends JPanel
implements
    ActionListener,
    MouseListener, MouseMotionListener,
    MouseWheelListener, KeyListener,
    ImageFetcher.Listener {

    private ImageWindow
    primaire;

    private ImageFetcher
    fetcher;

    private Image
    image, prevImage, nextImage;

//   -  -%-  -

    private Image
    scaledImage;

    private boolean
    rescaling;

    private Painter
    view;

    private RichTextPane4
    description;

    private Centrer
    descriptionWrapper;

    private JPanel
    buttonArea;

    private JButton
    prev, next, toggleScaled, toggleDescription;

    private boolean
    scaleImage, showDescription;

    private int
    xOffset, yOffset, zoomLevel;

    private int
    dragX, dragY, xPOffset, yPOffset;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    setImage(Image image)
    {
        if (scaledImage != null & scaledImage != this.image)
            scaledImage.flush();

        this.image = image;
        scaledImage = null;
        xOffset = yOffset = xPOffset = yPOffset = 0;
        zoomLevel = 100;
        view.considerRescale();
    }

    public void
    setPrev(Image image)
    {
        prev.setEnabled(image != null);
        prev.setText(image == null ? "◁" : "");
        prev.setIcon(image == null ? null : new ImageIcon(image));
        /*
        * (未) It is known that the default buttons aren't that
        * good for our purpose, so once the dust settles,
        * maybe create a custom component.
        */
    }

    public void
    setNext(Image image)
    {
        next.setEnabled(image != null);
        next.setText(image == null ? "▷": "");
        next.setIcon(image == null ? null : new ImageIcon(image));
    }

    public void
    setDescription(String s)
    {
        String html = htmlEscape(s);
        try
        {
            description.setText(BasicHTMLParser.parse(html));
        }
        catch (InvalidHTMLException eIh)
        {
            s = "(invalid HTML)";
            description.setText(new Tree<>("text", s));
        }
    }

    public void
    resetFocus()
    {
        toggleScaled.requestFocusInWindow();
    }

    public void
    setScaleImage(boolean scaleImage)
    {
        this.scaleImage = scaleImage;
        String k = "image " + (scaleImage ? "unscale" : "scale");
        toggleScaled.setText(JapaneseStrings.get(k));
        setImage(this.image);
        repaint();
    }

    public void
    setShowDescription(boolean showDescription)
    {
        this.showDescription = showDescription;
        remove(descriptionWrapper);
        remove(view);
        if (showDescription)
        {
            add(descriptionWrapper, BorderLayout.CENTER);
            String s = JapaneseStrings.get("image hide desc");
            toggleDescription.setText(s);
        }
        else
        {
            add(view, BorderLayout.CENTER);
            String s = JapaneseStrings.get("image show desc");
            toggleDescription.setText(s);
        }
        repaint();
    }

//   -  -%-  -

    public void
    imageReady(String name, Image scaled)
    {
        this.scaledImage = scaled;
        rescaling = false;
        repaint();
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();

        setCursor(WAIT);
        if (src == prev) primaire.toPrevImage();
        if (src == next) primaire.toNextImage();
        if (src == toggleScaled) setScaleImage(!scaleImage);
        if (src == toggleDescription)
            setShowDescription(!showDescription);
        setCursor(null);
    }

    public void
    mousePressed(MouseEvent eM)
    {
        dragX = eM.getX();
        dragY = eM.getY();
    }

    public void
    mouseDragged(MouseEvent eM)
    {
        int dx = eM.getX() - dragX;
        int dy = eM.getY() - dragY;
        xPOffset = dx;
        yPOffset = dy;
        repaint();
    }

    public void
    mouseReleased(MouseEvent eM)
    {
        xOffset += xPOffset;
        yOffset += yPOffset;
        xPOffset = yPOffset = 0;
    }

    public void
    mouseWheelMoved(MouseWheelEvent eMw)
    {
        setCursor(WAIT);

        zoomLevel += 10 * -eMw.getUnitsToScroll();
        if (zoomLevel < 20) zoomLevel = 20;
        if (zoomLevel > 400) zoomLevel = 400;
        scaledImage = null;
        view.considerRescale();
        repaint();

        setCursor(null);
    }

    public void
    keyPressed(KeyEvent eK)
    {
        int code = eK.getKeyCode();

        setCursor(WAIT);

        if (eK.isShiftDown())
        {
            if (code == KeyEvent.VK_LEFT)
            {
                boolean ok = prev.isEnabled();
                if (ok) primaire.toPrevImage();
            }
            if (code == KeyEvent.VK_RIGHT)
            {
                boolean ok = next.isEnabled();
                if (ok) primaire.toNextImage();
            }
        }
        else
        {
            if (code == KeyEvent.VK_UP)
            {
                yOffset += getHeight() * 1/16;
            }
            if (code == KeyEvent.VK_DOWN)
            {
                yOffset -= getHeight() * 1/16;
            }
            if (code == KeyEvent.VK_LEFT)
            {
                xOffset += getWidth() * 1/16;
            }
            if (code == KeyEvent.VK_RIGHT)
            {
                xOffset -= getWidth() * 1/16;
            }
            if (code == KeyEvent.VK_S)
            {
                setScaleImage(!scaleImage);
            }
            if (code == KeyEvent.VK_D)
            {
                setShowDescription(!showDescription);
            }
        }

        setCursor(null);
        repaint();
    }

    public void
    keyTyped(KeyEvent eK)
    {
        char c = eK.getKeyChar();

        setCursor(WAIT);
        if (c == '+')
        {
            zoomLevel += 20;
            if (zoomLevel > 400) zoomLevel = 400;
            scaledImage = null;
            view.considerRescale();
            repaint();
        }
        if (c == '-')
        {
            zoomLevel -= 20;
            if (zoomLevel < 20) zoomLevel = 20;
            scaledImage = null;
            view.considerRescale();
            repaint();
        }
        setCursor(null);
    }


    public void
    mouseEntered(MouseEvent eM) { }

    public void
    mouseExited(MouseEvent eM) { }

    public void
    mouseMoved(MouseEvent eM) { }

    public void
    mouseClicked(MouseEvent eM) { }

    public void
    keyReleased(KeyEvent eK) { }

//   -  -%-  -

    private static String
    htmlEscape(String string)
    {
        return string
            .replaceAll("&", "&amp;")
            .replaceAll("<", "&lt;")
            .replaceAll(">", "&gt;");
    }

//  ---%-@-%---

    private class
    Painter extends JPanel {

        protected void
        paintComponent(Graphics g)
        {
            if (scaledImage == null) return;
            int w = getWidth(), h = getHeight();
            int ws = scaledImage.getWidth(null);
            int hs = scaledImage.getHeight(null);

            int x = ((w - ws) / 2) + xOffset + xPOffset;
            int y = ((h - hs) / 2) + yOffset + yPOffset;
            g.drawImage(scaledImage, x, y, ws, hs, null);
        }

        public void
        considerRescale()
        {
            if (image != null)
            {
                int w = getWidth(), h = getHeight();
                if (!isDisplayable()) return;

                int iw = image.getWidth(null);
                int ih = image.getHeight(null);
                if (iw == -1 || ih == -1) return;

                Dimension b = new Dimension(iw, ih);
                if (scaleImage)
                {
                    b = ImageApi.containFit(iw, ih, w, h);
                }
                b.width = b.width * zoomLevel / 100;
                b.height = b.height * zoomLevel / 100;

                boolean rescale = true;
                if (scaledImage != null)
                {
                    iw = scaledImage.getWidth(null);
                    ih = scaledImage.getHeight(null);
                    rescale = iw != b.width || ih != b.height;
                }

                if (rescale && !rescaling)
                {
                    rescaling = true;
                    fetcher.scale(
                        image, b.width, b.height,
                        null, 1, ImageComponent.this);
                }
            }
        }

        public void
        doLayout() { considerRescale(); }

    }

//  ---%-@-%---

    ImageComponent(ImageWindow primaire, ImageFetcher fetcher)
    {
        this.primaire = primaire;
        this.fetcher = fetcher;

        Dimension BUTTON_SIZE = new Dimension(48, 48);

        setOpaque(false);
        scaleImage = true;
        showDescription = false;
        zoomLevel = 100;

        String s1 = JapaneseStrings.get("image unscale");
        String s2 = JapaneseStrings.get("image show desc");
        prev = new JButton();
        next = new JButton();
        toggleScaled = new JButton(s1);
        toggleDescription = new JButton(s2);
        prev.setPreferredSize(BUTTON_SIZE);
        next.setPreferredSize(BUTTON_SIZE);
        prev.addActionListener(this);
        next.addActionListener(this);
        toggleScaled.addActionListener(this);
        toggleDescription.addActionListener(this);
        prev.addKeyListener(this);
        next.addKeyListener(this);
        toggleScaled.addKeyListener(this);
        toggleDescription.addKeyListener(this);

        buttonArea = new JPanel();
        buttonArea.setOpaque(false);
        buttonArea.add(prev);
        buttonArea.add(toggleScaled);
        buttonArea.add(toggleDescription);
        buttonArea.add(next);

        view = new Painter();

        description = new RichTextPane4(fetcher);
        description.setMinimumSize(new Dimension(32, 32));
        description.setPreferredSize(new Dimension(0, 0));
        /*
        * RichTextPane4 is very sensitive to the text it gets
        * and its current width. If you let layout manager give
        * it a zero width, assertion failures will happen. I don't
        * have the energy to go back into that complex system
        * to try to defend against that..
        *
        * For components without a preferred size though, it will
        * return the minimum size if you call getPreferredSize.
        * We're relying on RichTextPane4's usual preferred size
        * calculation in Centrer above, so restore the default
        * 0,0 size that we are expecting.
        */

        descriptionWrapper = new Centrer(description);
        descriptionWrapper.setMargin(32, 32, 32, 32);

        setPrev(null);
        setNext(null);

        setLayout(new BorderLayout());
        add(buttonArea, BorderLayout.SOUTH);
        add(view, BorderLayout.CENTER);

        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
    }

}
