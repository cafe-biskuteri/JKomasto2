
import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.net.*;

class
ImageFetcherTest
implements ImageFetcher.Listener {

    private ImageFetcher
    fetcher;

//  ---%-@-%---

    public void
    imageReady(String name, Image image)
    {
        boolean scaled = name.startsWith("scaled");
        if (scaled)
        {
            final int DISPOSE = JFrame.DISPOSE_ON_CLOSE;
            Component c = new JLabel(new ImageIcon(image));
    
            JFrame mainframe = new JFrame(name.substring(6));
            mainframe.add(c);
            mainframe.pack();
            mainframe.setDefaultCloseOperation(DISPOSE);
            mainframe.setLocationByPlatform(true);
            mainframe.setVisible(true);
            mainframe.toFront();
    
            synchronized (this) {
                this.notify();
            }
        }
        else
        {
            fetcher.scale(
                image, 800, 600,
                "scaled" + name, 0, this);
        }
    }

//  ---%-@-%---

    public static void
    main(String... args)
    throws IOException, MalformedURLException
    {
        String[] urls = new String[] {
            "https://deadinside.sfo2.cdn.digitaloceanspaces.com/accounts/avatars/106/943/068/059/029/316/original/659401804c7329bf.png",
            "https://deadinside.sfo2.cdn.digitaloceanspaces.com/accounts/headers/106/943/068/059/029/316/original/fa4bb6b1ea52c77d.jpg",
            "https://deadinside.sfo2.cdn.digitaloceanspaces.com/cache/accounts/avatars/000/151/638/original/ad6ec6f7ec52f8be.png",
            "https://deadinside.sfo2.cdn.digitaloceanspaces.com/site_uploads/files/000/000/001/@1x/e4fabcc10e998cb2.png"
        };

        String tmp = System.getProperty("java.io.tmpdir");
        File tmpdir = new File(tmp, "imagefetcher");
        tmpdir.mkdir();
        tmpdir.deleteOnExit();

        ImageFetcherTest instance = new ImageFetcherTest();
        instance.fetcher = new ImageFetcher(tmpdir);

        for (String urlr: urls)
        {
            String path = new URL(urlr).getPath();
            String name = new File(path).getName();
            instance.fetcher.fetch(urlr, name, 1, instance);
        }

        synchronized (instance) {
            try { instance.wait(); }
            catch (InterruptedException eIt) { }
        }
    }

}
