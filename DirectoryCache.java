
import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.awt.Image;
import javax.imageio.ImageIO;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Base64;

class
DirectoryCache {

    private String
    directoryName;

//  ---%-@-%---

    public <Item> void
    add(String name, Item item, Handler<Item> writer)
    throws IOException
    {
        OutputStream os = new FileOutputStream(
            directoryName
            + File.separatorChar
            + makeSafeFilename(name));
        writer.write(item, os);
        os.close();
    }

    public <Item> Item
    get(String name, Handler<Item> reader)
    throws IOException
    {
        InputStream is = new FileInputStream(
            directoryName
            + File.separatorChar
            + makeSafeFilename(name));
        Item item = reader.read(is);
        is.close();
        return item;
    }

    public Set<String>
    keySet()
    {
        try
        {
            File[] listing = new File(directoryName).listFiles();
            if (listing == null)
                throw new IOException(
                    "Could not list files in directory");

            Set<String> returnee = new HashSet<>();
            for (File file: listing) try
            {
                String safeFilename = file.getName();
                String name = decodeSafeFilename(safeFilename);
                returnee.add(name);
            }
            catch (IllegalArgumentException eIa)
            {
                // Probably random file in the directory.
                continue;
            }
            return returnee;
        }
        catch (IOException eIo)
        {
            /*
            * If the directory is gone, implies the user deleted
            * it. This class is not designed to work once something
            * like that happens..
            */
            assert false;
            return Collections.emptySet();
        }
    }

//   -  -%-  -

    private static String
    makeSafeFilename(String string)
    {
        return toBase64(string);
    }

    private static String
    decodeSafeFilename(String filename)
    throws IllegalArgumentException
    {
        return fromBase64(filename);
    }

    private static String
    toBase64(String string)
    {
        Base64.Encoder enc = Base64.getUrlEncoder();
        return enc.encodeToString(string.getBytes());
    }

    private static String
    fromBase64(String base64)
    throws IllegalArgumentException
    {
        Base64.Decoder dec = Base64.getUrlDecoder();
        return new String(dec.decode(base64));
    }

//  ---%-@-%---

    public interface
    Handler<Item> {

        public void
        write(Item item, OutputStream os)
        throws IOException;

        public Item
        read(InputStream is)
        throws IOException;

    }

//  ---%-@-%---

    public
    DirectoryCache(String directoryName)
    throws IOException
    {
        this.directoryName = directoryName;

        File dir = new File(directoryName);
        if (!dir.exists() && !dir.mkdir())
            throw new IOException(
                "Couldn't create directory: " + directoryName);
    }

}

class
ImageIODirectoryCacheHandler
implements DirectoryCache.Handler<Image> {

    public void
    write(Image image, OutputStream os)
    throws IOException
    {
        ImageIO.write(
            ImageAPI2.ImageOps.toBufferedImage(image), "png", os);
    }

    public Image
    read(InputStream is)
    throws IOException { return ImageIO.read(is); }

}
