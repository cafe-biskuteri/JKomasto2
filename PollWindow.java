
import javax.swing.JFrame;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.JToggleButton;
import javax.swing.JScrollPane;
import javax.swing.Box;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Scrollable;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Container;
import java.awt.Insets;
import java.awt.Graphics;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.Arrays;
import java.io.IOException;
import java.time.ZonedDateTime;
import cafe.biskuteri.hinoki.Tree;

class
PollWindow extends JFrame
implements Scalable {

    private JKomasto
    primaire;

    private MastodonApi
    api;

    private Poll
    poll;

//   -  -%-  -

    private PollVotingComponent
    votingDisplay;

    private PollResultsComponent
    resultsDisplay;

    private int
    scale;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    use(Poll poll)
    {
        this.poll = poll;

        boolean[] picks = new boolean[poll.options.length];
        if (poll.ownVotes != null)
            for (int index: poll.ownVotes)
                picks[index - 1] = true;
        votingDisplay.setOptions(poll.options, poll.multiOption);
        votingDisplay.setPicks(picks);

        resultsDisplay.setValues(poll.voteCounts, poll.options);

        boolean ended =
            poll.expiryDateTime != null
            && ZonedDateTime.now().isAfter(poll.expiryDateTime);

        showResults(ended || poll.ownVotes != null);
    }

    public void
    showResults(boolean showResults)
    {
        if (showResults) {
            remove(votingDisplay);
            add(resultsDisplay);
        }
        else {
            remove(resultsDisplay);
            add(votingDisplay);
        }
        revalidate();
    }

//   -  -%-  -

    public void
    submissionRequested(int[] picks)
    {
        assert poll != null;
        assert picks != null;
        assert picks.length > 0;
        for (int index: picks)
            assert index >= 0 && index <= poll.options.length;

        votingDisplay.setCursor(WAIT);

        RequestOutcome r = api.voteInPoll(poll.id, picks);
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                this,
                "Attempt to vote in poll failed..."
                + "\n" + r.exception.getMessage());

            votingDisplay.setCursor(null);
            return;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                this,
                "Attempt to vote in poll failed..."
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP code: " + r.httpCode + ")");

            votingDisplay.setCursor(null);
            return;
        }

        /*
        * Mutate the poll to incorporate our new votes. Better
        * than refetching the poll, I think, although refetching
        * is usually what we do in this client.
        * Does the endpoint return something interesting?
        */
        for (int index: picks)
        {
            // Assume every index was voted for the first time.
            ++poll.voteCounts[index - 1];
        }
        use(poll);

        votingDisplay.setCursor(null);
        showResults(true);
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        votingDisplay.setScale(scale);
        resultsDisplay.setScale(scale);

        boolean disposed = !isDisplayable();
        pack(); if (disposed) dispose();
    }

//  ---%-@-%---

    PollWindow(JKomasto primaire)
    {
        this.primaire = primaire;
        api = primaire.getMastodonApi();
        ImageFetcher fetcher = primaire.getImageFetcher();

        votingDisplay = new PollVotingComponent(this, fetcher);
        resultsDisplay = new PollResultsComponent(this, fetcher);
        add(resultsDisplay);

        setIconImage(primaire.getProgramIcon());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setScale(20);
        addMouseWheelListener(new MouseWheelScaler(1, 6));
    }

}

class
PollVotingComponent extends JComponent
implements ActionListener, ImageFetcher.Listener {

    private PollWindow
    primaire;

    private ImageFetcher
    fetcher;

    private boolean
    multiOption;

//   -  -%-  -

    private Container
    options;

    private JButton
    submit;

    private int
    scale;

//   -  -%-  -

    private static Image
    backgroundImage;

//  ---%-@-%---

    public void
    setOptions(String[] htmls, boolean multiOption)
    {
        options.removeAll();
        for (String html: htmls)
        {
            PollOptionComponent p = new PollOptionComponent(this);
            p.setLabel(html);
            p.setScale(scale);

            if (html != htmls[0])
                options.add(Box.createVerticalStrut(6));
            options.add(p);
        }
        options.add(Box.createVerticalGlue());
        revalidate();

        this.multiOption = multiOption;
    }

    public void
    setPicks(boolean[] picks)
    {
        int i = 0; for (Component uc: options.getComponents())
        {
            if (!(uc instanceof PollOptionComponent)) continue;
            ((PollOptionComponent)uc).setPicked(picks[++i - 1]);
        }
    }

    public void
    setEmojis(Map<String, Image> emojis)
    {
        for (Component uc: options.getComponents())
        {
            if (!(uc instanceof PollOptionComponent)) continue;
            ((PollOptionComponent)uc).setEmojis(emojis);
            // Are we really given emojis for the poll en masse
            // rather than for each option?
        }
    }

//   -  -%-  -

    private int[]
    getPicks()
    {
        int[] returnee = new int[options.getComponentCount()];
        int oReturnee = -1, iOption = 0;
        for (Component uc: options.getComponents())
        {
            if (!(uc instanceof PollOptionComponent)) continue;
            PollOptionComponent c = (PollOptionComponent)uc;
            ++iOption;

            if (c.isPicked()) returnee[++oReturnee] = iOption;
        }

        returnee = Arrays.copyOf(returnee, 1 + oReturnee);
        return returnee;
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        if (eA.getSource() == submit)
        {
            int[] picks = getPicks();
            if (picks.length == 0) return;
            primaire.submissionRequested(picks);
        }
    }

    public void
    optionChanged(PollOptionComponent c, boolean picked)
    {
        if (!multiOption)
        for (Component uc2: options.getComponents())
        {
            if (!(uc2 instanceof PollOptionComponent)) continue;
            PollOptionComponent c2 = (PollOptionComponent)uc2;
            c2.setPicked(c2 == c && picked);
        }
    }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        int w = 330 * scale/20;
        int h = 440 * scale/20;
        setPreferredSize(new Dimension(w, h));

        int om = 12 * scale/20;
        int im = 18 * scale/20;
        Border b1 = 
            BorderFactory.createEmptyBorder(om, om, om, om);
        Border b2 = 
            BorderFactory.createEmptyBorder(im, im, 0, im);
        Border b3 = 
            BorderFactory.createEmptyBorder(0, im, im, im);

        setBorder(b1);
        ((JScrollPane)getComponent(0)).setBorder(b2);
        ((Box)getComponent(1)).setBorder(b3);

        for (Component c: options.getComponents())
        {
            if (!(c instanceof PollOptionComponent)) continue;
            ((PollOptionComponent)c).setScale(scale);
        }
    }

    public void
    doLayout()
    {
        Insets insets = getInsets();
        int xs = insets.left, xe = getWidth() - insets.right;
        int ys = insets.top, ye = getHeight() - insets.bottom;
        int w = xe - xs, h = ye - ys;

        int y2 = h - getComponent(1).getPreferredSize().height;
        int y1 = y2 - (h * 1/25);

        getComponent(0).setBounds(xs, ys, w, y1 - ys);
        getComponent(1).setBounds(xs, ys + y2, w, h - y2);
    }

    protected void
    paintComponent(Graphics g)
    {
        paintBackground(g);
    }

    private void
    paintBackground(Graphics g)
    {
        if (backgroundImage == null) return;
        Insets insets = getInsets();
        int w = getWidth(), h = getHeight();

        int iw = backgroundImage.getWidth(null);
        int ih = backgroundImage.getHeight(null);
        assert iw % 3 == 0;
        int tw = iw / 3;

        // Paint background first.
        int sx = iw * 5/6;
        int sy = ih * 4/5;
        int sw = tw / 2;
        int sh = tw / 2;
        for (int y = 0; y < h; y += sw)
        for (int x = 0; x < w; x += sw)
        {
            g.drawImage(
                backgroundImage,
                x, y, x + sw, y + sh,
                sx, sy, sx + sw, sy + sh, null);
        }

        // Paint centre.
        int left = insets.left + (tw / 2);
        int top = insets.top + (tw / 2);
        int right = w - insets.right - (tw / 2);
        int bottom = h - insets.bottom - (tw / 2);
        sx = iw * 1/6;
        sy = ih * 1/5;
        sw = tw;
        sh = tw;
        for (int y = top; y < bottom; y += sw)
        for (int x = left; x < right; x += sw)
        {
            int sw2 = Math.min(sw, right - x);
            int sh2 = Math.min(sh, bottom - y);
            g.drawImage(
                backgroundImage,
                x, y, x + sw2, y + sh2,
                sx, sy, sx + sw2, sy + sh2, null);
        }

        // Paint left and right fillers.
        top = insets.top + sw;
        bottom = h - insets.bottom - tw;
        left = insets.left;
        right = w - insets.right;
        int sx1 = iw * 2/6;
        int sx2 = iw * 3/6;
        sy = ih * 4/5;
        sw = tw / 2;
        sh = tw / 2;
        for (int y = top; y < bottom; y += sh)
        {
            g.drawImage(
                backgroundImage,
                left, y, left + sw, y + sh,
                sx1, sy, sx1 + sw, sy + sh, null);
            g.drawImage(
                backgroundImage,
                right - sw, y, right, y + sh,
                sx2, sy, sx2 + sw, sy + sh, null);
        }

        // Paint left and right edges.
        sx1 = iw * 4/6;
        sx2 = iw * 5/6;
        sy = ih * 1/5;
        sw = tw / 2;
        sh = tw;
        int iy = top + ((bottom - top) % sh) / 2;
        for (int y = iy; y < bottom - sh; y += sh)
        {
            g.drawImage(
                backgroundImage,
                left, y, left + sw, y + sh,
                sx1, sy, sx1 + sw, sy + sh, null);
            g.drawImage(
                backgroundImage,
                right - sw, y, right, y + sh,
                sx2, sy, sx2 + sw, sy + sh, null);
        }

        // Paint top and bottom fillers.
        left = insets.left + tw;
        right = w - insets.right - tw;
        top = insets.top;
        bottom = h - insets.bottom;
        sx1 = iw * 0/6;
        sx2 = iw * 1/6;
        sy = ih * 4/5;
        sw = tw / 2;
        sh = tw / 2;
        for (int x = left; x < right; x += sw)
        {
            g.drawImage(
                backgroundImage,
                x, top, x + sw, top + sh,
                sx1, sy, sx1 + sw, sy + sh, null);
            g.drawImage(
                backgroundImage,
                x, bottom - sh, x + sw, bottom,
                sx2, sy, sx2 + sw, sy + sh, null);
        }

        // Paint top and bottom edges.
        sx = iw * 2/3;
        int sy1 = ih * 0/5;
        int sy2 = ih * 3/5;
        sw = tw;
        sh = tw / 2;
        int ix = left + ((right - left) % sw) / 2;
        for (int x = ix; x < right - sw; x += sw)
        {
            g.drawImage(
                backgroundImage,
                x, top, x + sw, top + sh,
                sx, sy1, sx + sw, sy1 + sh, null);
            g.drawImage(
                backgroundImage,
                x, bottom - sh, x + sw, bottom,
                sx, sy2, sx + sw, sy2 + sh, null);
        }

        // Paint corners.
        left = insets.left;
        right = w - insets.right;
        top = insets.top;
        bottom = h - insets.bottom;
        sx1 = iw * 0/3;
        sx2 = iw * 1/3;
        sy1 = ih * 0/5;
        sy2 = ih * 2/5;
        sw = tw;
        sh = tw;
        g.drawImage(
            backgroundImage,
            left, top, left + sw, top + sh,
            sx1, sy1, sx1 + sw, sy1 + sh, null);
        g.drawImage(
            backgroundImage,
            right - sw, top, right, top + sh,
            sx2, sy1, sx2 + sw, sy1 + sh, null);
        g.drawImage(
            backgroundImage,
            left, bottom - sh, left + sw, bottom,
            sx1, sy2, sx1 + sw, sy2 + sh, null);
        g.drawImage(
            backgroundImage,
            right - sw, bottom - sh, right, bottom,
            sx2, sy2, sx2 + sw, sy2 + sh, null);
    }

    public void
    imageReady(String name, Image image)
    {
        if (name.equals("bg"))
        {
            backgroundImage = image;
            repaint();
        }
    }

//  ---%-@-%---

    class
    VerticalContainer extends JComponent
    implements Scrollable {

        public boolean
        getScrollableTracksViewportHeight() { return false; }

        public boolean
        getScrollableTracksViewportWidth() { return true; }

        public Dimension
        getPreferredScrollableViewportSize()
        {
            return getPreferredSize();
        }

        public int
        getScrollableBlockIncrement(
            Rectangle visibleRect, int orientation, int direction)
        {
            return visibleRect.height / 2;
        }

        public int
        getScrollableUnitIncrement(
            Rectangle visibleRect, int orientation, int direction)
        {
            return visibleRect.height / 8;
        }

//      -=%=-

        VerticalContainer()
        {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        }

    }

//  ---%-@-%---

    PollVotingComponent(PollWindow primaire, ImageFetcher fetcher)
    {
        this.primaire = primaire;
        this.fetcher = fetcher;

        options = new VerticalContainer();

        JScrollPane optionsScroll = new JScrollPane(
            options,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        optionsScroll.setBorder(null);
        optionsScroll.setOpaque(false);
        optionsScroll.getViewport().setOpaque(false);

        submit = new JButton("Submit choice!");
        int BOLD_ITALIC = Font.BOLD | Font.ITALIC;
        submit.setFont(new Font("Dialog", Font.BOLD, 14));
        submit.addActionListener(this);

        Box pollActions = Box.createHorizontalBox();
        pollActions.add(Box.createHorizontalGlue());
        pollActions.add(submit);

        setLayout(null);
        add(optionsScroll);
        add(pollActions);

        if (backgroundImage == null)
        {
            String path = "graphics/pollWindow.png";
            String urlr = getClass().getResource(path).toString();
            fetcher.fetch(urlr, "bg", 2, this);
        }
    }

}

class
PollOptionComponent extends JComponent
implements ActionListener {

    private PollVotingComponent
    primaire;

    private boolean
    picked;

//   -  -%-  -

    private JToggleButton
    checkbox;

    private RichTextPane4
    label;

//  ---%-@-%---

    public boolean
    isPicked() { return checkbox.isSelected(); }

    public void
    setPicked(boolean picked)
    {
        this.picked = picked;
        checkbox.setText(picked ? "☑" : "☐");
    }

    public void
    setLabel(String html)
    {
        try
        {
            label.setText(BasicHTMLParser.parse(html));
        }
        catch (InvalidHTMLException eIh)
        {
            String s = "(invalid HTML)";
            label.setText(new Tree<>("text", s));
        }
    }

    public void
    setEmojis(Map<String, Image> emojis)
    {
        label.setEmojis(emojis);
    }

//   -  -%-  -

    public void
    setScale(int scale)
    {
        String fname = "Dialog";
        Font small = new Font(fname, Font.PLAIN, 14 * scale/20);
        Font large = new Font(fname, Font.BOLD, 20 * scale/20);
        label.setFont(small);
        checkbox.setFont(large);

        FontMetrics fm = getFontMetrics(small);
        int lineHeight = fm.getHeight();
        int w = Integer.MAX_VALUE;
        int h = lineHeight * 4;
        setPreferredSize(new Dimension(w, h));

        ((BorderLayout)getLayout()).setHgap(8 * scale/20);
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        if (eA.getSource() == checkbox)
        {
            setPicked(checkbox.isSelected());
            primaire.optionChanged(this, picked);
        }
    }

//  ---%-@-%---

    PollOptionComponent(PollVotingComponent primaire)
    {
        this.primaire = primaire;

        checkbox = new JToggleButton("☐");
        checkbox.setBorderPainted(false);
        checkbox.setContentAreaFilled(false);
        checkbox.setMargin(new Insets(0, 0, 0, 0));
        checkbox.addActionListener(this);

        label = new RichTextPane4(null);
        label.setMaxDisplayLines(3);

        setLayout(new BorderLayout());
        add(checkbox, BorderLayout.WEST);
        add(label);
    }

}

class
PollResultsComponent extends JComponent
implements ImageFetcher.Listener {

    private PollWindow
    primaire;

    private ImageFetcher
    fetcher;

//   -  -%-  -

    private int
    scale;

//   -  -%-  -

    private static Image
    backgroundImage;

//  ---%-@-%---

    public void
    setValues(int[] values, String[] labels)
    {
        assert values != null;
        assert labels != null;
        assert values.length == labels.length;

        int totalValue = 0;
        for (int value: values)
        {
            assert value >= 0;
            totalValue += value;
        }

        int pipUnit = Math.max(1, totalValue / 20);

        removeAll();
        for (int o = 0; o < values.length; ++o)
        {
            PollResultComponent c =
                new PollResultComponent(fetcher);
            c.setTotalValue(totalValue);
            c.setPipUnit(pipUnit);
            c.setValue(values[o]);
            c.setLabel(labels[o]);
            c.setScale(scale);
            add(c);
        }
    }

//   -  -%-  -

    public void
    setScale(int scale)
    {
        this.scale = scale;

        int w = 330 * scale/20;
        int h = 440 * scale/20;
        setPreferredSize(new Dimension(w, h));

        int m = 12 * scale/20;
        setBorder(BorderFactory.createEmptyBorder(m, m, m, m));

        for (Component uc: getComponents())
        {
            assert uc instanceof PollResultComponent;
            ((PollResultComponent)uc).setScale(scale);
        }
    }

    protected void
    paintComponent(Graphics g)
    {
        paintBackground(g);
    }

    private void
    paintBackground(Graphics g)
    {
        if (backgroundImage == null) return;
        Insets insets = getInsets();
        int w = getWidth(), h = getHeight();

        int iw = backgroundImage.getWidth(null);
        int ih = backgroundImage.getHeight(null);
        assert iw % 3 == 0;
        int tw = iw / 3;

        // Paint background first.
        int sx = iw * 5/6;
        int sy = ih * 4/5;
        int sw = tw / 2;
        int sh = tw / 2;
        for (int y = 0; y < h; y += sw)
        for (int x = 0; x < w; x += sw)
        {
            g.drawImage(
                backgroundImage,
                x, y, x + sw, y + sh,
                sx, sy, sx + sw, sy + sh, null);
        }

        // Paint centre.
        int left = insets.left + (tw / 2);
        int top = insets.top + (tw / 2);
        int right = w - insets.right - (tw / 2);
        int bottom = h - insets.bottom - (tw / 2);
        sx = iw * 1/6;
        sy = ih * 1/5;
        sw = tw;
        sh = tw;
        for (int y = top; y < bottom; y += sw)
        for (int x = left; x < right; x += sw)
        {
            int sw2 = Math.min(sw, right - x);
            int sh2 = Math.min(sh, bottom - y);
            g.drawImage(
                backgroundImage,
                x, y, x + sw2, y + sh2,
                sx, sy, sx + sw2, sy + sh2, null);
        }

        // Paint left and right fillers.
        top = insets.top + sw;
        bottom = h - insets.bottom - tw;
        left = insets.left;
        right = w - insets.right;
        int sx1 = iw * 2/6;
        int sx2 = iw * 3/6;
        sy = ih * 4/5;
        sw = tw / 2;
        sh = tw / 2;
        for (int y = top; y < bottom; y += sh)
        {
            g.drawImage(
                backgroundImage,
                left, y, left + sw, y + sh,
                sx1, sy, sx1 + sw, sy + sh, null);
            g.drawImage(
                backgroundImage,
                right - sw, y, right, y + sh,
                sx2, sy, sx2 + sw, sy + sh, null);
        }

        // Paint left and right edges.
        sx1 = iw * 4/6;
        sx2 = iw * 5/6;
        sy = ih * 1/5;
        sw = tw / 2;
        sh = tw;
        int iy = top + ((bottom - top) % sh) / 2;
        for (int y = iy; y < bottom - sh; y += sh)
        {
            g.drawImage(
                backgroundImage,
                left, y, left + sw, y + sh,
                sx1, sy, sx1 + sw, sy + sh, null);
            g.drawImage(
                backgroundImage,
                right - sw, y, right, y + sh,
                sx2, sy, sx2 + sw, sy + sh, null);
        }

        // Paint top and bottom fillers.
        left = insets.left + tw;
        right = w - insets.right - tw;
        top = insets.top;
        bottom = h - insets.bottom;
        sx1 = iw * 0/6;
        sx2 = iw * 1/6;
        sy = ih * 4/5;
        sw = tw / 2;
        sh = tw / 2;
        for (int x = left; x < right; x += sw)
        {
            g.drawImage(
                backgroundImage,
                x, top, x + sw, top + sh,
                sx1, sy, sx1 + sw, sy + sh, null);
            g.drawImage(
                backgroundImage,
                x, bottom - sh, x + sw, bottom,
                sx2, sy, sx2 + sw, sy + sh, null);
        }

        // Paint top and bottom edges.
        sx = iw * 2/3;
        int sy1 = ih * 0/5;
        int sy2 = ih * 3/5;
        sw = tw;
        sh = tw / 2;
        int ix = left + ((right - left) % sw) / 2;
        for (int x = ix; x < right - sw; x += sw)
        {
            g.drawImage(
                backgroundImage,
                x, top, x + sw, top + sh,
                sx, sy1, sx + sw, sy1 + sh, null);
            g.drawImage(
                backgroundImage,
                x, bottom - sh, x + sw, bottom,
                sx, sy2, sx + sw, sy2 + sh, null);
        }

        // Paint corners.
        left = insets.left;
        right = w - insets.right;
        top = insets.top;
        bottom = h - insets.bottom;
        sx1 = iw * 0/3;
        sx2 = iw * 1/3;
        sy1 = ih * 0/5;
        sy2 = ih * 2/5;
        sw = tw;
        sh = tw;
        g.drawImage(
            backgroundImage,
            left, top, left + sw, top + sh,
            sx1, sy1, sx1 + sw, sy1 + sh, null);
        g.drawImage(
            backgroundImage,
            right - sw, top, right, top + sh,
            sx2, sy1, sx2 + sw, sy1 + sh, null);
        g.drawImage(
            backgroundImage,
            left, bottom - sh, left + sw, bottom,
            sx1, sy2, sx1 + sw, sy2 + sh, null);
        g.drawImage(
            backgroundImage,
            right - sw, bottom - sh, right, bottom,
            sx2, sy2, sx2 + sw, sy2 + sh, null);
    }

    public void
    imageReady(String name, Image image)
    {
        if (name.equals("bg"))
        {
            backgroundImage = image;
            repaint();
        }
    }

//  ---%-@-%---

    PollResultsComponent(PollWindow primaire, ImageFetcher fetcher)
    {
        this.primaire = primaire;
        this.fetcher = fetcher;

        if (backgroundImage == null)
        {
            String path = "graphics/pollWindow.png";
            String urlr = getClass().getResource(path).toString();
            fetcher.fetch(urlr, "bg", 2, this);
        }

        setLayout(new GridLayout(-1, 2));
    }

}

class
PollResultComponent extends JComponent
implements ImageFetcher.Listener {

    private ImageFetcher
    fetcher;

    private int
    value, totalValue, pipUnit;

    private Image
    backgroundImage;

//   -  -%-  -

    private RichTextPane4
    label;

    private Dimension
    pipSize;

    private int
    scale;

    private Image
    scaledPipImage;

//   -  -%-  -

    private static Image
    pipImage;

//  ---%-@-%---

    public void
    setValue(int n) { value = n; }

    public void
    setTotalValue(int n) { totalValue = n; }

    public void
    setPipUnit(int n) { pipUnit = n; }

    public void
    setLabel(String n) { label.setText(new Tree<>("text", n)); }

//   -  -%-  -

    protected void
    paintComponent(Graphics g)
    {
        paintPips(g);
    }

    private void
    paintPips(Graphics g)
    {
        if (pipSize == null) return;

        Image pipImage =
            scaledPipImage != null
                ? scaledPipImage : this.pipImage;
        if (pipImage == null) return;

        int pipCount = value / pipUnit;
        int xs = getInsets().left;
        int ys = getHeight() / 2;

        for (int i = 1; i <= pipCount; ++i)
        {
            int xo = (i - 1) % 10;
            int yo = (i - 1) / 10;
            int x = xs + (xo * pipSize.width * 5/4);
            int y = ys + (yo * pipSize.height * 5/4);
            g.drawImage(
                pipImage, x, y,
                pipSize.width, pipSize.height, null);
        }
    }

    public void
    doLayout()
    {
        Insets insets = getInsets();
        int w = getWidth(), h = getHeight();
        
        label.setBounds(
            insets.left, insets.top,
            w - insets.right - insets.left, (h / 2) - insets.top);

        if (pipImage != null)
        {
            int columnWidth =
                (w - insets.right - insets.left) * 2/25;
            int rowHeight =
                ((h - insets.bottom) - (h / 2)) * 2/5;
            // 2/25 = (5/4)*10 ... 2/5 = (5/4)*2
            ImageApi.containFit(
                pipSize,
                pipImage.getWidth(null), pipImage.getHeight(null),
                columnWidth, rowHeight);

            scaledPipImage = null;
            if (pipSize.width > 0 && pipSize.height > 0)
                fetcher.scale(
                    pipImage, pipSize.width, pipSize.height,
                    "sc", 1, this);
        }
    }

    public void
    imageReady(String name, Image image)
    {
        if (name.equals("sc"))
        {
            scaledPipImage = image;
            repaint();
        }
        else if (name.equals("pip"))
        {
            pipImage = image;
            doLayout();
            repaint();
        }
    }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        int m = 18 * scale/20;
        setBorder(BorderFactory.createEmptyBorder(m, m, m, m));

        String fname = "Dialog";
        Font text = new Font(fname, Font.PLAIN, 15 * scale/20);
        label.setFont(text);
    }

//  ---%-@-%---

    PollResultComponent(ImageFetcher fetcher)
    {
        this.fetcher = fetcher;

        pipSize = new Dimension();
        if (pipImage == null)
        {
            String path = "graphics/pollPip.png";
            String urlr = getClass().getResource(path).toString();
            fetcher.fetch(urlr, "pip", 2, this);
        }

        label = new RichTextPane4(null);

        add(label);
    }

}
