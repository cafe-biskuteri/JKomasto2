
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.Frame;
import java.awt.Component;
import javax.swing.SwingUtilities;

class
SharedKeyShortcuts
implements KeyListener, KeyEventDispatcher {

    private JKomasto
    primaire;

//  ---%-@-%---

    public void
    keyPressed(KeyEvent eK)
    {
        assert eK.getSource() instanceof Component;
        Component src = (Component)eK.getSource();
        int code = eK.getKeyCode();

        if (code == KeyEvent.VK_F11)
        {
            Window tlc = SwingUtilities.getWindowAncestor(src);
            toggleMaximised(tlc);
        }
        if (code == KeyEvent.VK_F12)
        {
            new TimelineWindow(primaire).setVisible(true);
        }
    }

    public boolean
    dispatchKeyEvent(KeyEvent eK)
    {
        if (eK.isConsumed()) return false;
        if (!(eK.getSource() instanceof Component)) return false;

        KeyboardFocusManager kfm =
            KeyboardFocusManager
                .getCurrentKeyboardFocusManager();
        Component src = (Component)eK.getSource();

        if (eK.getKeyText(eK.getKeyCode()).equals("NumPad +")) {
            eK.setKeyCode(KeyEvent.VK_SPACE);
            eK.setKeyChar(' ');
            // You can't truly mutate a key event into something
            // else, but changing its code is sufficient for JButton.
        }

        Window tlc = SwingUtilities.getWindowAncestor(src);
        if (hasKeyListener(tlc, this))
        for (KeyListener l: tlc.getKeyListeners())
        switch (eK.getID())
        {
            case KeyEvent.KEY_PRESSED: l.keyPressed(eK); break;
            case KeyEvent.KEY_RELEASED: l.keyReleased(eK); break;
            case KeyEvent.KEY_TYPED: l.keyTyped(eK); break;
        }

        return false;

        /*
        * This was quite tricky!
        *
        * Firstly, be sure to think through how a key event
        * gets dispatched, and what you should do when you present
        * it to a listener other than one on its source.
        *
        * In this case, I wanted the event to continue to its source
        * if we the key listener did not handle it. So I forwarded
        * the event to the top-level container without consuming it.
        * So two components are receiving the event - regardless of
        * if the top-level container handled it, the event will
        * continue being dispatched to the source.
        *
        * Secondly, what method do we use to present the event to
        * the top-level container. Docs already told us that we
        * shouldn't use tlc.dispatchEvent because that will
        * recursively enter the KeyEventDispatcher pipeline.
        * Surprisingly, kfm.redispatchEvent is the wrong answer
        * as well. After we present it to the top-level container,
        * we return false and expect KeyboardFocusManager to forward
        * the event to the source. However, the way redispatchEvent
        * works to stop recursion, the event is tagged as invalid -
        * not for the pipeline it seems, cause key events do get
        * dispatched to the source and we are able to type in text
        * fields and stuff - but /somewhere/, because focus traversal
        * keys no longer work. KeyboardFocusManager itself seems to
        * handle focus traversal, and it refuses to perform it if
        * redispatchEvent had been called for the key event. It's
        * rather strange behaviour but.
        *
        * Anyways therefore, I dispatch the event manually to the
        * top-level container's key listeners, except it's not like,
        * dispatch, just presenting. The key event should otherwise
        * behave identically to if we as key event dispatcher
        * weren't present.
        */
    }

    public void
    keyReleased(KeyEvent eK) { }

    public void
    keyTyped(KeyEvent eK) { }

//   -  -%-  -

    private static void
    toggleMaximised(Window window)
    {
        if (!(window instanceof Frame)) return;
        // Technically we can do something, but I'll ignore.

        int state = ((Frame)window).getExtendedState();

        state ^= Frame.MAXIMIZED_BOTH;
        ((Frame)window).setExtendedState(state);
        window.requestFocus();
    }

    private static boolean
    hasKeyListener(Component component, KeyListener listener)
    {
        if (component == null) return false;
        if (listener == null) return false;

        for (KeyListener l: component.getKeyListeners())
            if (l == listener) return true;

        return false;
    }

//  ---%-@-%---

    SharedKeyShortcuts(JKomasto primaire, boolean becomeDispatcher)
    {
        this.primaire = primaire;
        if (becomeDispatcher) becomeDispatcher();
    }

    public void
    becomeDispatcher()
    {
        KeyboardFocusManager kfm;
        kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        kfm.removeKeyEventDispatcher(this);
        kfm.addKeyEventDispatcher(this);
    }

}
