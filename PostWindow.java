
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JComponent;
import javax.swing.Box;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.JOptionPane;
import javax.swing.MenuSelectionManager;
import javax.swing.MenuElement;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Component;
import java.awt.Color;
import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.AWTKeyStroke;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import cafe.biskuteri.hinoki.Tree;


class
PostWindow extends JFrame
implements Scalable, ImageFetcher.Listener {

    private JKomasto
    primaire;

    private MastodonApi
    api;

    private ImageFetcher
    fetcher;

    private Post
    post;

//   -  -%-  -

    private PostComponent
    display;

    private PlaceholderComponent
    malformedPlaceholder,
    hiddenPlaceholder;

    private int
    scale;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    use(Post post, boolean hidden)
    {
        if (post != null) assert post.boostedPost == null;
        // Caller has to do the unwrapping.

        synchronized (this)
        {
            Component initialContentPane = getContentPane();
            initialContentPane.setCursor(WAIT);

            if (this.post != null)
            {
                if (this.post.author.avatarResolver == this)
                    this.post.author.avatarResolver = null;

                for (Attachment a: post.attachments)
                    if (a.imageResolver == this)
                        a.imageResolver = null;
            }

            this.post = post;
            if (post == null)
            {
                setContentPane(malformedPlaceholder);
                revalidate();
            }
            else if (hidden)
            {
                setContentPane(hiddenPlaceholder);
                revalidate();
            }
            else
            {
                syncDisplayToPost();
                setContentPane(display);
                revalidate();
                display.resetFocus();
            }

            initialContentPane.setCursor(null);
        }
    }

    public void
    openAuthorProfile()
    {
        synchronized (this)
        {
            display.setCursor(WAIT);

            ProfileWindow w = new ProfileWindow(primaire);
            w.use(post.author);
            w.setLocationRelativeTo(this);
            w.setVisible(true);

            display.setCursor(null);
        }
    }

    public void
    favourite(boolean favourited)
    {
        synchronized (this)
        {
            display.setCursor(WAIT);
            display.setFavouriteBoostEnabled(false);
            display.paintImmediately(display.getBounds());

            RequestOutcome r =
                api.setPostFavourited(post.id, favourited);
            if (r.exception != null)
            {
                JOptionPane.showMessageDialog(
                    PostWindow.this,
                    "Tried to favourite post, failed.."
                    + "\n" + r.exception.getClass()
                    + ": " + r.exception.getMessage()
                );
            }
            else if (r.errorEntity != null)
            {
                JOptionPane.showMessageDialog(
                    PostWindow.this,
                    "Tried to favourite post, failed.."
                    + "\n" + r.errorEntity.get("error").value
                    + "\n(HTTP error code: " + r.httpCode + ")"
                );
            }
            else
            {
                post.favourited = favourited;
            }

            display.setCursor(null);
            display.setFavourited(post.favourited);
            display.setFavouriteBoostEnabled(true);
            display.repaint();
        }
    }

    public void
    boost(boolean boosted)
    {
        synchronized (this)
        {
            display.setCursor(WAIT);
            display.setFavouriteBoostEnabled(false);
            display.paintImmediately(display.getBounds());

            RequestOutcome r =
                api.setPostBoosted(post.id, boosted);
            if (r.exception != null)
            {
                JOptionPane.showMessageDialog(
                    PostWindow.this,
                    "Tried to boost post, failed.."
                    + "\n" + r.exception.getClass()
                    + ": " + r.exception.getMessage()
                );
            }
            else if (r.errorEntity != null)
            {
                JOptionPane.showMessageDialog(
                    PostWindow.this,
                    "Tried to boost post, failed.."
                    + "\n" + r.errorEntity.get("error").value
                    + "\n(HTTP error code: " + r.httpCode + ")"
                );
            }
            else
            {
                post.boosted = boosted;
            }

            display.setBoosted(post.boosted);
            display.setCursor(null);
            display.setFavouriteBoostEnabled(true);
            display.repaint();
        }
    }

    public void
    bookmark(boolean bookmarked)
    {
        synchronized (this)
        {
            display.setCursor(WAIT);
            display.setReplyMiscEnabled(false);
            display.paintImmediately(display.getBounds());

            RequestOutcome r =
                api.setPostBookmarked(post.id, bookmarked);
            if (r.exception != null)
            {
                JOptionPane.showMessageDialog(
                    PostWindow.this,
                    "Tried to bookmark post, failed.."
                    + "\n" + r.exception.getClass()
                    + ": " + r.exception.getMessage()
                );
            }
            else if (r.errorEntity != null)
            {
                JOptionPane.showMessageDialog(
                    PostWindow.this,
                    "Tried to bookmark post, failed.."
                    + "\n" + r.errorEntity.get("error").value
                    + "\n(HTTP error code: " + r.httpCode + ")"
                );
            }
            else
            {
                post.bookmarked = bookmarked;
            }

            display.setCursor(null);
            display.setReplyMiscEnabled(true);
            display.repaint();
        }
    }

    public void
    reply()
    {
        synchronized (this)
        {
            display.setCursor(WAIT);

            Composition c = Composition.reply(
                post,
                api.getAccountDetails().get("acct").value);

            ComposeWindow w = new ComposeWindow(primaire);
            w.use(c);
            w.setLocation(getX(), getY() + 100);
            w.setVisible(true);

            display.setCursor(null);
        }
    }

    public void
    openMedia()
    {
        synchronized (this)
        {
            display.setCursor(WAIT);

            String s = JapaneseStrings.get("post media title");
            ImageWindow w = new ImageWindow(primaire);
            w.setTitle(s + " - " + this.getTitle() + " - JKomasto");
            w.setIconImage(this.getIconImage());
            w.use(post.attachments, false);
            w.setLocationRelativeTo(null);
            w.setVisible(true);
            w.resetFocus();

            display.setCursor(null);
        }
    }

    public void
    deletePost(boolean redraft)
    {
        synchronized (this)
        {
            display.setCursor(WAIT);
            display.setDeleteEnabled(false);
            display.paintImmediately(display.getBounds());

            String[] s = new String[] {
                JapaneseStrings.get("post confirm delete no"),
                JapaneseStrings.get("post confirm delete yes"),
                JapaneseStrings.get("post confirm delete"),
                JapaneseStrings.get("post confirm redraft"),
                JapaneseStrings.get("post confirm delete title")
            };

            JOptionPane dialog = new JOptionPane();
            dialog.setMessageType(JOptionPane.WARNING_MESSAGE);
            dialog.setMessage(redraft ? s[3] : s[2]);
            dialog.setOptionType(JOptionPane.DEFAULT_OPTION);
            dialog.setOptions(new String[] { s[1], s[0] });
            dialog.setInitialValue(s[0]);
            dialog.createDialog(this, s[4]).setVisible(true);
            if (!dialog.getValue().equals(s[1]))
            {
                display.setCursor(null);
                display.setDeleteEnabled(true);
                display.paintImmediately(display.getBounds());
                return;
            }

            RequestOutcome r = api.deletePost(post.id);
            if (r.exception != null)
            {
                JOptionPane.showMessageDialog(
                    PostWindow.this,
                    "Failed to delete post.."
                    + "\n" + r.exception.getMessage()
                );
            }
            else if (r.errorEntity != null)
            {
                JOptionPane.showMessageDialog(
                    PostWindow.this,
                    "Failed to delete post.."
                    + "\n" + r.errorEntity.get("error").value
                    + "\n(HTTP code: " + r.httpCode + ")"
                );
            }
            else if (redraft)
            {
                Composition c = Composition.recover(r.entity);
                ComposeWindow w = new ComposeWindow(primaire);
                w.use(c);
                w.setLocation(getX(), getY() + 100);
                w.setVisible(true);
            }

            display.setCursor(null);
            display.setDeleteEnabled(true);
            display.repaint();
            /*
            * I've opted to keep showing the post even when it
            * is deleted. We can have a post flag and disable the
            * actions button as well, but I'm very used to and
            * have no problems with leaving the post window open
            * (important for auto view) and having no indicator.
            */
        }
    }

    public void
    copyPostId()
    {
        synchronized (this)
        {
            ClipboardApi.serve(post.id);
        }
    }

    public void
    copyPostLink()
    {
        synchronized (this)
        {
            ClipboardApi.serve(post.uri);
        }
    }

    public void
    openThread()
    {
        assert post != null;

        synchronized (this)
        {
            PostThread thread = getPostThread(api, this, post);
            if (thread == null) return;

            for (Post post: thread.posts)
                if (post.id.equals(this.post.id))
                    use(post, false);
        }
    }

    public void
    moveWithinThread(int direction)
    {
        assert post != null;
        if (post.thread == null) return;

        if (direction == 1)
        {
            Post parent = getParent(post.thread, post);
            if (parent != null) use(parent, false);
        }
        if (direction == 2)
        {
            Post parent = getParent(post.thread, post);
            if (parent == null) return;
            List<Post> siblings = getChildren(post.thread, parent);
            int o = siblings.indexOf(post);
            if (o > 0) use(siblings.get(o - 1), false);
        }
        if (direction == 3)
        {
            Post parent = getParent(post.thread, post);
            if (parent == null) return;
            List<Post> siblings = getChildren(post.thread, parent);
            int o = siblings.indexOf(post);
            if (o < siblings.size() - 1) use(siblings.get(o + 1), false);
        }
        if (direction == 4)
        {
            List<Post> children = getChildren(post.thread, post);
            if (!children.isEmpty()) use(children.get(0), false);
        }
    }

    public void
    openPoll()
    {
        assert post.poll != null;

        PollWindow w = new PollWindow(primaire);
        w.use(post.poll);
        w.setTitle("Poll - " + post.id + " - JKomasto");
        w.setLocationRelativeTo(this);
        w.setVisible(true);
    }

    public void
    profileLinkClicked(String link)
    {
        String id = null;
        for (String[] mapping: post.mentions)
        {
            String url = mapping[0];
            String id2 = mapping[2];
            if (url.equals(link)) id = id2;
        }
        assert id != null;

        ClipboardApi.serve("@" + id);
    }

    public void
    plainLinkClicked(String link)
    {
        ClipboardApi.serve(link);
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        display.setScale(scale);

        String fname = "MotoyaLMaru";
        int fsz = 16 * scale/20;
        Font large = new Font(fname, Font.PLAIN, fsz * 14/12);
        malformedPlaceholder.setFont(large);
        hiddenPlaceholder.setFont(large);

        boolean isDisposed = !isDisplayable();
        int w = 400 * scale/20;
        int h = 300 * scale/20;
        Dimension sz = new Dimension(w, h);
        getContentPane().setPreferredSize(sz);
        pack(); if (isDisposed) dispose();
    }

//   -  -%-  -

    private void
    syncDisplayToPost()
    {
        assert post != null;

        display.setAuthorName(post.author.name);
        display.setAuthorId(post.author.id);

        String oid = api.getAccountDetails().get("id").value;
        String aid = post.author.numId;
        display.setDeleteEnabled(aid.equals(oid));

        boolean gif = post.author.avatarUrl.endsWith(".gif");
        display.setAuthorAvatar(post.author.avatar, gif);
        if (post.author.avatar == null
            && post.author.avatarResolver == null)
        {
            String name = post.hashCode() + ":a";
            post.author.avatarResolver = this;
            fetcher.fetch(
                post.author.avatarUrl, name, 4, this);
        }

        display.setDate(post.date);
        display.setTime(post.time);

        String flags = "";
        switch (post.visibility)
        {
            case PUBLIC: flags += "P"; break;
            case UNLISTED: flags += "U"; break;
            case FOLLOWERS: flags += "F"; break;
            case MENTIONED: flags += "M"; break;
            default: assert false;
        }
        if (post.inThread) flags += " t";
        if (post.hasReplies) flags += " r";
        if (post.edited) flags += " e";
        if (post.attachments.length > 0) flags += " a";
        if (post.poll != null) flags += " q";
        boolean described = true;
        for (Attachment a: post.attachments)
            described &= a.description != null;
        if (!described) flags += " N";

        display.setFlags(flags);

        display.setEmojiUrls(post.emojiUrls);

        display.setHtml(post.text);
        display.setFavourited(post.favourited);
        display.setBoosted(post.boosted);
        display.setBookmarked(post.bookmarked);

        display.setMediaPreview(null);
        if (post.attachments.length > 0)
        {
            display.setMediaPreview(post.attachments[0].image);

            int o = -1;
            for (Attachment a: post.attachments)
            {
                if (!a.type.equals("image")) continue;
                if (a.image != null) continue;
                if (a.imageResolver != null) continue;

                String name = post.hashCode() + ":m:" + ++o;
                a.imageResolver = this;
                fetcher.fetch(a.url, name, 4, this);
            }
        }

        display.setThreadPosition(getThreadPosition(post));

        display.setPollEnabled(post.poll != null);

        post.resolveApproximateText();
        String s = post.approximateText;
        s = s.replaceAll("^(@[^ ]+ )+", "");
        s = limitToASCII(s);
        this.setTitle(s + " - JKomasto");
        // Regarding putting all of the approximate text -
        // KDE is handling this wonderfully, but should we
        // avoid it? On X, that's a pretty big window atom.
    }

    public void
    imageReady(String name, Image image)
    {
        String[] fields = name.split(":");
        assert fields.length > 1;
        if (!fields[0].equals("" + post.hashCode())) return;

        int o = -1; if (fields.length == 3) try {
            o = Integer.parseInt(fields[2]);
        }
        catch (NumberFormatException eNf) {
            assert false;
        }

        if (fields[1].equals("a"))
        {
            assert image.getHeight(null) != -1;
            post.author.avatar = image;
            post.author.avatarResolver = null;

            boolean gif = post.author.avatarUrl.endsWith(".gif");
            display.setAuthorAvatar(image, gif);
        }
        else if (fields[1].equals("m"))
        {
            assert image.getHeight(null) != -1;
            assert o != -1 && o < post.attachments.length;

            Attachment a = post.attachments[o];
            a.image = image;
            a.imageResolver = null;

            if (o == 0) display.setMediaPreview(image);
            // Any ImageWindow currently looking at the
            // attachments won't be notified, the user
            // will have to reopen.
        }
    }

//  -  -%-  -

    private static PostThread
    getPostThread(MastodonApi api, Component c, Post post)
    {
        assert post != null;

        RequestOutcome r; do
        {
            RequestOutcome r1, r2;

            // Start by getting post context.
            r1 = r = api.getPostContext(post.id);
            if (r1.exception != null) break;
            if (r1.errorEntity != null) break;

            // Mastodon incorrectly includes in the ancestors
            // list posts that aren't reachable from the post
            // we give it. Go through ancestors and find the
            // farthest back ancestor that's reachable.
            Tree<String> top =
                PostThread.getHighestReachableAncestor(
                    post, r1.entity);

            // Resolve descendants of top of thread.
            Tree<String> descendants;
            if (top == null)
            {
                // If no reachable ancestors, current post is the
                // top of thread, but we didn't have the entity
                // for it yet. Let's fetch it now.
                r2 = r = api.getSpecificPost(post.id);
                if (r2.exception != null) break;
                if (r2.errorEntity != null) break;

                top = r2.entity;
                descendants = r1.entity.get("descendants");
            }
            else
            {
                // We need descendants of the top of thread,
                // which may have been missing from r1.entity.
                r2 = r = api.getPostContext(top.get("id").value);
                if (r2.exception != null) break;
                if (r2.errorEntity != null) break;

                descendants = r2.entity.get("descendants");
            }

            try
            {
                Tree<String> context = new Tree<>();
                top.key = "top";
                context.add(top);
                context.add(descendants);
                return new PostThread(context);
            }
            catch (MalformedEntityException eMu)
            {
                JOptionPane.showMessageDialog(
                    c,
                    "Failed to fetch post context...."
                    + "\nEntity of current post is malformed..!");
                return null;
            }
        }
        while (false);

        // If ended up here, broke out due to failed request.
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                c,
                "Failed to fetch post context...."
                + "\n" + r.exception.getMessage());
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                c,
                "Failed to fetch post context...."
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP code: " + r.httpCode + ")");
        }
        else assert false;
        return null;
    }

    public static String
    getThreadPosition(Post post)
    {
        assert post != null;
        if (post.thread == null) return "";

        List<Post> siblings = getSiblings(post.thread, post);

        StringBuilder b = new StringBuilder();
        b.append(getAncestorCount(post.thread, post));
        b.append("/");
        b.append(1 + siblings.indexOf(post));
        b.append("/");
        b.append(siblings.size());
        b.append("/");
        b.append(getChildren(post.thread, post).size());
        return b.toString();
    }

    public static List<Post>
    getChildren(PostThread thread, Post parent)
    {
        List<Post> returnee = new ArrayList<>();
        for (Post post: thread.posts)
            if (parent.id.equals(post.parentId))
                returnee.add(post);
        return returnee;
    }

    public static Post
    getParent(PostThread thread, Post child)
    {
        for (Post post: thread.posts)
            if (post.id.equals(child.parentId))
                return post;
        return null;
    }

    public static List<Post>
    getSiblings(PostThread thread, Post post)
    {
        Post parent = getParent(thread, post);
        if (parent == null)
        {
            List<Post> returnee = new ArrayList<>();
            returnee.add(post);
            return returnee;
        }
        else return getChildren(thread, parent);
    }

    public static int
    getAncestorCount(PostThread thread, Post post)
    {
        Post parent = getParent(thread, post);
        if (parent == null) return 0;
        else return 1 + getAncestorCount(thread, parent);
    }

//   -    -%-     -

    private static String
    limitToASCII(String s)
    {
        try
        {
            return new String(s.getBytes("US-ASCII"), "US-ASCII");
        }
        catch (UnsupportedEncodingException eUe)
        {
            assert false;
            return null;
        }
    }

//  ---%-@-%---

    PostWindow(JKomasto primaire)
    {
        this.primaire = primaire;
        Settings settings = primaire.getSettings();
        api = primaire.getMastodonApi();
        fetcher = primaire.getImageFetcher();
        
        String[] fnames = settings.richTextPaneFontNames;
        display = new PostComponent(this, fetcher, fnames);
        // PostComponent needs fetcher for RoundButton and
        // TwoToggleButton construction.

        String s1 = "There is no post currently opened.";
        malformedPlaceholder = new PlaceholderComponent();
        malformedPlaceholder.setText(s1);

        String s2 = "This post is hidden (at your request).";
        hiddenPlaceholder = new PlaceholderComponent();
        hiddenPlaceholder.setText(s2);

        setIconImage(primaire.getProgramIcon());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setScale(20);

        addMouseWheelListener(new MouseWheelScaler(2, 6));
        addKeyListener(new SharedKeyShortcuts(primaire, true));
        addFocusTraversalKeys(this);

        use(null, false);
    }

//   -  -%-  -

    private static void
    addFocusTraversalKeys(Container c)
    {
        final int FW, BW, KUP, KDOWN;
        FW = KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS;
        BW = KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS;
        KUP = KeyEvent.VK_KP_UP;
        KDOWN = KeyEvent.VK_KP_DOWN;
        /*
        * (知) I fought with Swing for 2 hours trying to get
        * the miscMenu receiving focus when it is open
        * while the focus keys were the directional keys.
        * I gave up because it's not an important feature.
        * Basically, setting your own tab order is quite a
        * difficult task...
        *
        * Leaving the focus keys as keypad ones instead
        * allows JPopupMenu to do its mysterious default
        * behaviour of eating the focus when the directional
        * keys are pressed.
        */

        AWTKeyStroke up, down;
        up = AWTKeyStroke.getAWTKeyStroke(KUP, 0);
        down = AWTKeyStroke.getAWTKeyStroke(KDOWN, 0);

        Set<AWTKeyStroke> keystrokes = new HashSet<>();
        keystrokes.addAll(c.getFocusTraversalKeys(FW));
        keystrokes.add(down);
        c.setFocusTraversalKeys(FW, keystrokes);

        keystrokes = new HashSet<>();
        keystrokes.addAll(c.getFocusTraversalKeys(BW));
        keystrokes.add(up);
        c.setFocusTraversalKeys(BW, keystrokes);
    }

}



class
PostComponent extends JPanel
implements
    ActionListener, KeyListener,
    Scalable, ImageFetcher.Listener, ImageAPI2.Listener,
    RichTextPane4.LinkListener {

    private PostWindow
    primaire;

    private ImageFetcher
    fetcher;

    private String[]
    fontNames;

//   -  -%-  -

    private RichTextPane4
    authorName,
    body;

    private JLabel
    authorId, time, date;

    private TwoToggleButton
    favouriteBoost,
    replyMisc,
    nextPrev;

    private RoundButton
    profile,
    media;

    private JPopupMenu
    miscMenu;

    private JMenuItem
    openThread,
    openPoll,
    bookmarkPost,
    copyPostId,
    copyPostLink,
    deletePost,
    redraftPost;

    private Box
    right;

    private JLabel
    flagDisplay,
    threadPosition;

    private int
    scale;

    private List<BorderLayout>
    borderLayouts;

    private List<AdjustableStrut>
    struts;

    private JComponent
    bottom,
    buttons;

    private Map<String, Image>
    loadingEmojis;

    private int
    unloadedEmojiCount;

//   -  -%-  -

    private static Image
    backgroundImage;

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR),
    HAND = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR),
    CROSSHAIR =
        Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);

//  ---%-@-%---

    public void
    setAuthorName(String n)
    {
        try
        {
            authorName.setText(BasicHTMLParser.parse(n));
        }
        catch (InvalidHTMLException eIh)
        {
            n = "(invalid HTML)";
            authorName.setText(new Tree<>("text", n));
        }
    }

    public void
    setAuthorId(String n) { authorId.setText(n); }

    public void
    setAuthorAvatar(Image n, boolean gif)
    {
        profile.setImage(n, gif);
    }

    public void
    setDate(String n) { date.setText(n); }

    public void
    setTime(String n) { time.setText(n); }

    public void
    setFlags(String n)
    {
        String[] verbose = new String[] {
            "P", "Public",
            "U", "Unlisted",
            "F", "Followers",
            "M", "Mentioned",
            "t", ", in thread",
            "r", ", replies",
            "e", ", edited",
            "a", ", attachment",
            "q", ", poll",
            "N", ", not described"
        };
        StringBuilder b = new StringBuilder();
        for (String flag: n.split(" "))
            for (int o = 0; o < verbose.length; o += 2)
                if (flag.equals(verbose[o]))
                    b.append(verbose[o + 1]);
        b.append("   ");  // Extra space for cursive font.

        flagDisplay.setText(b.toString());
    }

    public void
    setEmojiUrls(String[][] n)
    {
        loadingEmojis = new HashMap<>();
        unloadedEmojiCount = n.length;

        String p = System.identityHashCode(loadingEmojis) + "<><>";
        /*
        * (知) We have to use the default hash code here because we're
        * using this for identifying the hash map, we can't have it
        * change everytime we add an entry.
        */
        for (String[] entry: n)
            fetcher.fetch(entry[1], p + entry[0], 1, this);
    }

    public void
    setHtml(String n)
    {
        try
        {
            body.setText(BasicHTMLParser.parse(n));
        }
        catch (InvalidHTMLException eIh)
        {
            n = "(invalid HTML)";
            body.setText(new Tree<>("text", n));
        }
    }

    public void
    setFavourited(boolean a)
    {
        //favouriteBoost.removeActionListener(this);
        favouriteBoost.setPrimaryToggled(a);
        //favouriteBoost.addActionListener(this);
    }

    public void
    setBoosted(boolean a)
    {
        //favouriteBoost.removeActionListener(this);
        favouriteBoost.setSecondaryToggled(a);
        //favouriteBoost.addActionListener(this);
    }

    public void
    setBookmarked(boolean a)
    {
        bookmarkPost.setText(
            a
                ? JapaneseStrings.get("post unbookmark")
                : JapaneseStrings.get("post bookmark")
        );
    }

    public void
    setFavouriteBoostEnabled(boolean a)
    {
        favouriteBoost.setEnabled(a);
    }

    public void
    setReplyMiscEnabled(boolean a) { replyMisc.setEnabled(a); }

    public void
    setDeleteEnabled(boolean a)
    {
        deletePost.setEnabled(a);
        redraftPost.setEnabled(a);
    }

    public void
    setMediaPreview(Image n) { media.setImage(n, false); }

    public void
    setThreadPosition(String n)
    {
        threadPosition.setText(n + "  ");
        // Extra space for cursive font.
    }

    public void
    setPollEnabled(boolean a) { openPoll.setEnabled(a); }

    public void
    resetFocus()
    {
        media.requestFocusInWindow();
    }

    public int
    getScale()
    {
        return authorId.getFont().getSize();
    }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        String fname1 = "MotoyaLMaru";
        String fname2 = "TeXGyreChorus";
        int fsz = 16 * scale/20;
        Font small = new Font(fname1, Font.PLAIN, fsz);
        Font large = new Font(fname1, Font.PLAIN, fsz * 18/14);
        Font fancy = new Font(fname2, Font.PLAIN, fsz);

        authorId.setFont(small);
        date.setFont(small);

        authorName.setFont(large);
        time.setFont(large);

        body.setFonts(makeFonts(fsz * 16/14, fontNames));

        flagDisplay.setFont(fancy);
        threadPosition.setFont(fancy);

        setFont(miscMenu, small);

        borderLayouts.get(0).setHgap(8 * scale/20);
        borderLayouts.get(1).setHgap(8 * scale/20);
        borderLayouts.get(3).setHgap(8 * scale/20);
        borderLayouts.get(2).setVgap(2 * scale/20);
        borderLayouts.get(3).setVgap(2 * scale/20);

        struts.get(0).setSizing(0, 8 * scale/20);
        struts.get(1).setSizing(0, 8 * scale/20);
        struts.get(2).setSizing(0, 8 * scale/20);
        struts.get(3).setSizing(0, 8 * scale/20);
        struts.get(4).setSizing(0, 2 * scale/20);
        struts.get(5).setSizing(0, 6 * scale/20);
        buttons.setMaximumSize(buttons.getPreferredSize());

        int mo = 10 * (20 + (scale - 20)/2)/20;
        int py = 2 * scale/20;
        int px = 8 * scale/20;
        int uw = 1 * scale/20;  // So like, increase at scale > 40.
        Color tg = new Color(0, 0, 0, 25);
        Border margin =
            BorderFactory.createEmptyBorder(mo, mo, mo - py, mo);
        Border upper =
            BorderFactory.createMatteBorder(uw, 0, 0, 0, tg);
        Border padding =
            BorderFactory.createEmptyBorder(py, px, py, px);
        Border bbottom =
            BorderFactory.createCompoundBorder(upper, padding);
        bottom.setBorder(bbottom);
        setBorder(margin);
    }

//   -  -%-  -

    public void
    imageReady(String name, Image image)
    {
        if (name.equals("common:backgroundImage"))
        {
            backgroundImage = image;
            return;
        }

        String[] fields = name.split("<><>");
        assert fields.length == 2;
        if (loadingEmojis == null) return;
        String expected = "" + System.identityHashCode(loadingEmojis);
        if (!fields[0].equals(expected)) return;

        loadingEmojis.put(fields[1], image);
        if (--unloadedEmojiCount == 0)
        {
            body.setEmojis(loadingEmojis);
            loadingEmojis = null;
        }
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        Component src = (Component)eA.getSource();
        String command = eA.getActionCommand();

        setCursor(WAIT);

        if (src == profile)
        {
            primaire.openAuthorProfile();
        }

        if (src == favouriteBoost)
        {
            if (command.equals("favouriteOn"))
                primaire.favourite(true);
            if (command.equals("favouriteOff"))
                primaire.favourite(false);
            if (command.equals("boostOn"))
                primaire.boost(true);
            if (command.equals("boostOff"))
                primaire.boost(false);
        }

        if (src == replyMisc)
        {
            if (miscMenu.isVisible())
            {
                Component sel = getSelected(miscMenu);
                if (sel != null)
                {
                    assert sel instanceof JMenuItem;
                    ((JMenuItem)sel).doClick();
                }
            }
            else if (command.startsWith("reply"))
            {
                primaire.reply();
            }
            else if (command.startsWith("misc"))
            {
                int rx = replyMisc.getWidth() / 2;
                int ry = replyMisc.getHeight() - miscMenu.getHeight();
                miscMenu.show(replyMisc, rx, ry);
            }
        }
        else miscMenu.setVisible(false);

        if (src == nextPrev)
        {
            if (command.startsWith("next"))
                body.nextPage();
            else if (command.startsWith("prev"))
                body.previousPage();
            else assert false;
            // First time an interactive element
            // doesn't call something in primaire..
        }

        if (src == media) primaire.openMedia();

        if (src == openThread)
        {
            primaire.openThread();
            //replyMisc.requestFocusInWindow();
        }

        if (src == openPoll)
        {
            assert openPoll.isEnabled();
            primaire.openPoll();
        }

        if (src == bookmarkPost)
        {
            String s1 = bookmarkPost.getText();
            String s2 = JapaneseStrings.get("post bookmark");
            boolean bookmark = s1.equals(s2);
            primaire.bookmark(bookmark);
        }

        if (src == copyPostId) primaire.copyPostId();
        if (src == copyPostLink) primaire.copyPostLink();
        if (src == deletePost) primaire.deletePost(false);
        if (src == redraftPost) primaire.deletePost(true);

        setCursor(null);
    }

    public void
    keyPressed(KeyEvent eK)
    {
        int code = eK.getKeyCode();
        Component src = (Component)eK.getSource();

        setCursor(WAIT);

        if (code == KeyEvent.VK_I) {
            primaire.moveWithinThread(1);
            src.requestFocusInWindow();
        }
        if (code == KeyEvent.VK_K) {
            primaire.moveWithinThread(4);
            src.requestFocusInWindow();
        }
        if (code == KeyEvent.VK_J) {
            primaire.moveWithinThread(2);
            src.requestFocusInWindow();
        }
        if (code == KeyEvent.VK_L) {
            primaire.moveWithinThread(3);
            src.requestFocusInWindow();
        }

        setCursor(null);
    }

    public void
    linkClicked(String link, int linkClass)
    {
        setCursor(WAIT);

        if (linkClass == 1) primaire.profileLinkClicked(link);
        else primaire.plainLinkClicked(link);

        setCursor(null);
    }

    public Cursor
    getCursorForLink(String link, int linkClass)
    {
        if (linkClass == 0) return HAND;
        else if (linkClass == 1) return CROSSHAIR;
        else return null;
    }

    protected void
    paintComponent(Graphics g)
    {
        if (backgroundImage == null)
        {
            g.clearRect(0, 0, getWidth(), getHeight());
        }
        else
        {
            int tw = backgroundImage.getWidth(this);
            int th = backgroundImage.getHeight(this);
            if (tw != -1)
            for (int y = 0; y < getHeight(); y += th)
            for (int x = 0; x < getWidth(); x += tw)
            {
                g.drawImage(backgroundImage, x, y, this);
            }
        }
    }

    public void
    keyTyped(KeyEvent eK) { }

    public void
    keyReleased(KeyEvent eK) { }

//   -  -%-  -

    private static Component
    getSelected(JPopupMenu menu)
    {
        MenuElement[] sel =
            MenuSelectionManager.defaultManager()
                .getSelectedPath();
        /*
        * (知) For some reason, the selection model of the
        * JPopupMenu doesn't do anything. So we have to
        * consult this apparently global menu manager.
        */
        for (int o = 0; o < sel.length - 1; ++o)
        {
            if (sel[o] == menu)
                return sel[o + 1].getComponent();
        }
        return null;
    }

    private static void
    setFont(MenuElement me, Font font)
    {
        me.getComponent().setFont(font);
        for (MenuElement mse: me.getSubElements())
            setFont(mse, font);
    }

//  ---%-@-%---

    PostComponent(
        PostWindow primaire,
        ImageFetcher fetcher,
        String[] fontNames)
    {
        this.primaire = primaire;
        this.fetcher = fetcher;
        this.fontNames = fontNames;

        if (backgroundImage == null) loadCommonImages();

        profile = new RoundButton(fetcher);
        favouriteBoost = new TwoToggleButton("favourite", "boost");
        replyMisc = new TwoToggleButton("reply", "misc");
        nextPrev = new TwoToggleButton("next", "prev");
        media = new RoundButton(fetcher);
        //profile.getAccessibleContext()
            //.setAccessibleName("Open author profile");
        favouriteBoost.getAccessibleContext()
            .setAccessibleName("Favourite / boost");
        replyMisc.getAccessibleContext()
            .setAccessibleName("Reply / misc");
        nextPrev.getAccessibleContext()
            .setAccessibleName("Scroll contents down / up");
        //media.getAccessibleContext()
            //.setAccessibleName("Open post attachments");
        profile.addActionListener(this);
        favouriteBoost.addActionListener(this);
        replyMisc.addActionListener(this);
        nextPrev.addActionListener(this);
        media.addActionListener(this);
        profile.addKeyListener(this);
        favouriteBoost.addKeyListener(this);
        replyMisc.addKeyListener(this);
        nextPrev.addKeyListener(this);
        media.addKeyListener(this);

        borderLayouts = new ArrayList<>();
        borderLayouts.add(new BorderLayout());
        borderLayouts.add(new BorderLayout());
        borderLayouts.add(new BorderLayout());
        borderLayouts.add(new BorderLayout());

        struts = new ArrayList<>();
        struts.add(new AdjustableStrut());
        struts.add(new AdjustableStrut());
        struts.add(new AdjustableStrut());
        struts.add(new AdjustableStrut());
        struts.add(new AdjustableStrut());
        struts.add(new AdjustableStrut());

        String[] s = new String[] {
            JapaneseStrings.get("post open replies"),
            JapaneseStrings.get("post open poll"),
            JapaneseStrings.get("post bookmark"),
            JapaneseStrings.get("post copy id"),
            JapaneseStrings.get("post copy link"),
            JapaneseStrings.get("post delete"),
            JapaneseStrings.get("post redraft") };

        openThread = new JMenuItem(s[0]);
        openPoll = new JMenuItem(s[1]);
        bookmarkPost = new JMenuItem(s[2]);
        copyPostId = new JMenuItem(s[3]);
        copyPostLink = new JMenuItem(s[4]);
        deletePost = new JMenuItem(s[5]);
        redraftPost = new JMenuItem(s[6]);
        openThread.addActionListener(this);
        openPoll.addActionListener(this);
        bookmarkPost.addActionListener(this);
        copyPostId.addActionListener(this);
        copyPostLink.addActionListener(this);
        deletePost.addActionListener(this);
        redraftPost.addActionListener(this);
        miscMenu = new JPopupMenu();
        miscMenu.add(openThread);
        miscMenu.add(openPoll);
        miscMenu.add(new JSeparator());
        miscMenu.add(bookmarkPost);
        miscMenu.add(new JSeparator());
        miscMenu.add(copyPostId);
        miscMenu.add(copyPostLink);
        miscMenu.add(new JSeparator());
        miscMenu.add(deletePost);
        miscMenu.add(new JSeparator());
        miscMenu.add(redraftPost);

        buttons = Box.createVerticalBox();
        buttons.setOpaque(false);
        buttons.add(profile);
        buttons.add(struts.get(0));
        buttons.add(favouriteBoost);
        buttons.add(struts.get(1));
        buttons.add(replyMisc);
        buttons.add(struts.get(2));
        buttons.add(nextPrev);
        buttons.add(struts.get(3));
        buttons.add(media);
        Box left = Box.createVerticalBox();
        left.add(buttons);

        authorId = new JLabel();
        authorName = new RichTextPane4(fetcher);
        authorName.setFocusable(false);
        authorName.setMaxDisplayLines(1);
        time = new JLabel();
        date = new JLabel();

        JPanel top1 = new JPanel();
        top1.setOpaque(false);
        top1.setLayout(borderLayouts.get(0));
        top1.add(authorId);
        top1.add(date, BorderLayout.EAST);
        JPanel top2 = new JPanel();
        top2.setOpaque(false);
        top2.setLayout(borderLayouts.get(1));
        top2.add(authorName);
        top2.add(time, BorderLayout.EAST);
        Box top = Box.createVerticalBox();
        top.add(top1);
        top.add(struts.get(4));
        top.add(top2);
        top.add(struts.get(5));

        body = new RichTextPane4(fetcher);
        body.addLinkListener(this);

        flagDisplay = new JLabel();
        threadPosition = new JLabel();

        bottom = Box.createHorizontalBox();
        bottom.add(flagDisplay);
        bottom.add(Box.createHorizontalGlue());
        bottom.add(threadPosition);

        JPanel centre = new JPanel();
        centre.setOpaque(false);
        centre.setLayout(borderLayouts.get(2));
        centre.add(top, BorderLayout.NORTH);
        centre.add(body);

        setLayout(borderLayouts.get(3));
        add(left, BorderLayout.WEST);
        add(centre);
        add(bottom, BorderLayout.SOUTH);
    }

//   -  -%-  -

    private void
    loadCommonImages()
    {
        ImageAPI2
            .fetch(
                ImageAPI2.URLs.local("postWindow3"),
                "common:backgroundImage", 0, this)
            .run();
    }

//   -  -%-  -

    private static List<Font>
    makeFonts(int pointSize, String[] fontNames)
    {
        assert fontNames != null;

        List<Font> returnee = new ArrayList<>();
        for (String name: fontNames)
            returnee.add(new Font(name, Font.PLAIN, pointSize));

        returnee.add(new Font("Dialog", Font.PLAIN, pointSize));

        return returnee;
    }

}


class
PlaceholderComponent extends JPanel
implements ImageAPI2.Listener {

    private JLabel
    text;

    private Image
    backgroundImage;

//  ---%-@-%---

    public void
    setText(String text)
    {
        this.text.setText(text);
    }

    public void
    setFont(Font font)
    {
        super.setFont(font);
        //if (text != null) text.setFont(font);
        if (text != null) text.setFont(null);
    }

//   -    -%-     -

    protected void
    paintComponent(Graphics g)
    {
        if (backgroundImage == null)
        {
            g.clearRect(0, 0, getWidth(), getHeight());
        }
        else
        {
            int tw = backgroundImage.getWidth(this);
            int th = backgroundImage.getHeight(this);
            if (tw != -1)
            for (int y = 0; y < getHeight(); y += th)
            for (int x = 0; x < getWidth(); x += tw)
            {
                g.drawImage(backgroundImage, x, y, this);
            }
        }
    }

//  ---%-@-%---

    PlaceholderComponent()
    {
        text = new JLabel("", JLabel.CENTER);

        setLayout(new BorderLayout());
        add(text, BorderLayout.CENTER);

        ImageAPI2
            .fetch(
                ImageAPI2.URLs.local("postWindow3"),
                "backgroundImage", 0, this)
            .run();
    }

//   -  -%-  -

    public void
    imageReady(String name, Image image)
    {
        if (name.equals("backgroundImage"))
            backgroundImage = image;
    }

}
