# Instructions for running

This project depends on 'Hinoki' and 'Java API For Processing JSON'.

- Hinoki is another project [available from this Gitlab group](https://gitlab.com/cafe-biskuteri/Hinoki).

- Java API For Processing JSON (JSR-353) should be provided by your package manager.
Under Debian, the package name is ['libjsonp-java'](https://packages.debian.org/stable/java/libjsonp-java), which provides the '/usr/share/java/javax.json.jar'.

Run the JKomasto class with the two added to the classpath:
```
CLASSPATH=".:(path to Hinoki):(path to libjsonp JAR)"
javac -cp "$CLASSPATH" JKomasto2.java
java -cp "$CLASSPATH" JKomasto2
```
In the personal run script, Hinoki is in the same parent directory as JKomasto2, allowing a hardcoded "../Hinoki".
