
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import cafe.biskuteri.hinoki.DSVTokeniser;
import cafe.biskuteri.hinoki.Tree;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

class
SettingsApi {

    public static void
    setDefaultSettings(Settings settings)
    {
        settings.richTextPaneFontNames = new String[] {
            //"Victor Mono",
            "Quicksand",
            "02UtsukushiMincho",
            "VL PGothic",
            //"TexGyreHeros",
            "Ume Gothic C4",
            "Firefox Emoji"
        };

        settings.defaultVisibility = PostVisibility.UNLISTED;

        settings.speechCommand = new String[] {
            "spd-say", "<>" };
        
        settings.imageCommand = new String[] {
            "display", "<>" };
        
        settings.videoCommand = new String[] {
            "mpv", "<>" };
        
        settings.audioCommand = new String[] {
            "mpv", "<>" };

        try
        {
            validate(settings);
        }
        catch (IllegalArgumentException eIa)
        {
            assert false;
        }
    }

    public static void
    loadSettingsFromCache(Settings settings)
    throws IOException
    {
        Settings returnee = new Settings();

        FileReader r = new FileReader(getSettingsPath());
        DSVTokeniser.Options to = new DSVTokeniser.Options();

        List<Tree<String>> rows = new ArrayList<>();
        Tree<String> t = new Tree<String>(null, "");
        while (!t.value.equals(to.endOfStreamValue))
        {
            t = DSVTokeniser.tokenise(r, to);
            rows.add(t);
        }

        if (rows.size() >= 1)
            returnee.richTextPaneFontNames =
                toStringArray(rows.get(0));
    
        if (rows.size() >= 2)
            returnee.defaultVisibility = toEnumByName(
                rows.get(1).get(0).value.toUpperCase(),
                PostVisibility.class);

        if (rows.size() >= 3)
            returnee.speechCommand = toStringArray(rows.get(2));

        if (rows.size() >= 4)
            returnee.imageCommand = toStringArray(rows.get(3));

        if (rows.size() >= 5)
            returnee.videoCommand = toStringArray(rows.get(4));

        if (rows.size() >= 6)
            returnee.audioCommand = toStringArray(rows.get(5));

        r.close();
        try
        {
            validate(returnee);
            copyInto(settings, returnee);
        }
        catch (IllegalArgumentException eIa)
        {
            throw new IOException("cache invalid format");
        }
    }

    public static void
    saveSettingsIntoCache(Settings settings)
    throws IOException
    {
        try
        {
            validate(settings);
        }
        catch (IllegalArgumentException eIa)
        {
            assert false;
        }

        FileWriter w = new FileWriter(getSettingsPath());

        w.write(toDSV(settings.richTextPaneFontNames) + "\n");

        w.write(settings.defaultVisibility.name() + "\n");

        w.write(toDSV(settings.speechCommand) + "\n");

        w.write(toDSV(settings.imageCommand) + "\n");

        w.write(toDSV(settings.videoCommand) + "\n");

        w.write(toDSV(settings.audioCommand) + "\n");

        w.close();
    }

//  -=- --- -%- --- -=-

    private static void
    validate(Settings settings)
    throws IllegalArgumentException
    {
        boolean ok = true;

        ok &= settings != null;

        ok &= validateStringArray(settings.richTextPaneFontNames);

        ok &= settings.defaultVisibility != null;

        ok &= validateCommand(settings.speechCommand);

        ok &= validateCommand(settings.imageCommand);

        ok &= validateCommand(settings.videoCommand);

        ok &= validateCommand(settings.audioCommand);

        if (!ok) throw new IllegalArgumentException();
    }

    private static boolean
    validateStringArray(String[] array)
    {
        if (array == null) return false;

        boolean allFilled = true;
        for (String item: array)
            allFilled &= item != null && !item.isEmpty();

        return allFilled;
    }

    private static boolean
    validateCommand(String[] command)
    {
        if (command == null) return true;

        boolean hasSubstitution = false;
        for (String argument: command)
        {
            if (argument == null) return false;
            if (argument.isEmpty()) return false;
            hasSubstitution |= argument.equals("<>");
        }

        return hasSubstitution;
    }

//   -      -%-      -

    private static String
    getSettingsPath()
    {
        String userHome = System.getProperty("user.home");
        String osName = System.getProperty("os.name");
        boolean isWindows = osName.contains("Windows");
        boolean isUnix = !isWindows;
        String configDir = isWindows ? "AppData/Local" : ".config";
        String filename = "jkomasto.settings.dsv";
        String path = userHome + "/" + configDir + "/" + filename;
        return path;
    }

//   -      -%-      -

    private static void
    copyInto(Settings dest, Settings src)
    {
        dest.richTextPaneFontNames = src.richTextPaneFontNames;

        dest.defaultVisibility = src.defaultVisibility;

        dest.speechCommand = clone(src.speechCommand);

        dest.imageCommand = clone(src.imageCommand);

        dest.videoCommand = clone(src.videoCommand);

        dest.audioCommand = clone(src.audioCommand);
    }

//   -      -%-      -

    private static String[]
    toStringArray(Tree<String> tree)
    {
        if (tree.size() == 1 && tree.get(0).value.isEmpty())
            return null;

        String[] returnee = new String[tree.size()];
        for (int o = 0; o < returnee.length; ++o)
            returnee[o] = tree.get(o).value;
        return returnee;
    }

    private static String
    toDSV(String[] array)
    {
        if (array == null) return "";

        StringBuilder returnee = new StringBuilder();
        for (String string: array)
        {
            returnee.append(string.replaceAll(":", "\\:"));
            returnee.append(":");
        }
        returnee.deleteCharAt(returnee.length() - 1);
        return returnee.toString();
    }

//   -      -%-      -

    private static <T> T[]
    clone(T[] array)
    {
        return Arrays.copyOf(array, array.length);
    }

    private static <T> T
    toEnumByName(String name, Class<T> enumClass)
    {
        for (T enumConstant: enumClass.getEnumConstants())
            if (name.equals(((Enum)enumConstant).name()))
                return enumConstant;
        return null;
    }

}