
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import java.awt.Font;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.FontMetrics;
import java.awt.Cursor;
import java.awt.Insets;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.EnumMap;
import java.io.IOException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import cafe.biskuteri.hinoki.Tree;

class
NotificationsWindow extends JFrame
implements Scalable, WindowListener {

    private JKomasto
    primaire;

    private MastodonApi
    api;

    private TimelineWindowUpdater
    windowUpdater;

    private ImageFetcher
    fetcher;

    private volatile List<Notification>
    notifications;

//   -    -%-     -

    private NotificationsComponent
    display;

    private boolean
    showingLatest;

    private int
    scale;

//   -    -%-     -

    private static EnumMap<NotificationType, Image>
    NOTIF_ICONS = new EnumMap<>(NotificationType.class);

    private static int
    ROW_COUNT = NotificationsComponent.ROW_COUNT;

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    refresh()
    {
        synchronized (this)
        {
            display.setCursor(WAIT);

            String firstId = showingLatest ? null : getFirstId();
            
            notifications = fetchPage(firstId, null);
            List<Boolean> hidden = getHidden(notifications);
            // By principle, parent tells display whether
            // items are hidden.
            display.showNotifications(notifications, hidden);

            display.setCursor(null);
        }
    }

    public void
    showLatestPage()
    {
        synchronized (this)
        {
            display.setCursor(WAIT);

            notifications = fetchPage(null, null);
            showingLatest = true;
            List<Boolean> hidden = getHidden(notifications);
            display.showNotifications(notifications, hidden);

            display.setCursor(null);
        }
    }

    public void
    showPrevPage()
    {
        synchronized (this)
        {
            display.setCursor(WAIT);

            assert !notifications.isEmpty();

            notifications = fetchPage(null, getFirstId());
            if (notifications.size() < ROW_COUNT)
            {
                showLatestPage();
                return;
            }

            showingLatest = false;
            List<Boolean> hidden = getHidden(notifications);
            display.showNotifications(notifications, hidden);

            display.setCursor(null);
        }
    }

    public void
    showNextPage()
    {
        synchronized (this)
        {
            assert !notifications.isEmpty();

            display.setCursor(WAIT);

            notifications = fetchPage(getLastId(), null);
            showingLatest = false;
            List<Boolean> hidden = getHidden(notifications);
            display.showNotifications(notifications, hidden);

            display.setCursor(null);
        }
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        display.setScale(scale);

        boolean isDisposed = !isDisplayable();
        int w = 280 * scale/20;
        int h = 320 * scale/20;
        Dimension sz = new Dimension(w, h);
        display.setPreferredSize(sz);
        pack(); if (isDisposed) dispose();
    }

//   -    -%-     -

    private String
    getFirstId()
    {
        for (Notification n: notifications)
            if (n != null) return n.id;
        return null;
    }

    private String
    getLastId()
    {
        for (int o = notifications.size() - 1; o >= 0; --o) {
            Notification n = notifications.get(o);
            if (n != null) return n.id;
        }
        return null;
    }

    private List<Notification>
    fetchPage(String maxId, String minId)
    {
        RequestOutcome r =
            api.getNotifications(ROW_COUNT, maxId, minId);
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                this,
                "Failed to fetch notifications page..."
                + "\n" + r.exception.getMessage()
            );
            return null;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                this,
                "Failed to fetch notifications page..."
                + "\n" + r.errorEntity.get("error").value
                + "(HTTP code: " + r.httpCode + ")"
            );
            return null;
        }

        List<Notification> returnee = new ArrayList<>();
        for (Tree<String> entity: r.entity) try
        {
            Notification n = new Notification(entity);
            n.resolvePostText();
            returnee.add(n);
        }
        catch (MalformedEntityException eMe)
        {
            returnee.add(null);
        }
        return returnee;
    }

    private List<Boolean>
    getHidden(List<Notification> notifications)
    {
        List<Boolean> returnee = new ArrayList<>();
        for (Notification n: notifications)
        {
            if (n == null || n.post == null) returnee.add(false);
            else returnee.add(api.isHidden(n.post.id));
        }
        return returnee;
    }

    public void
    notificationOpened(int index)
    {
        assert index <= notifications.size();
        Notification n = notifications.get(index - 1);
        if (n.post == null) return;

        PostWindow w = new PostWindow(primaire);
        w.use(n.post, false);
        w.setLocation(getX() + (getWidth() * 9/8), getY());
        w.setVisible(true);
    }


    public void
    notificationActorOpened(int index)
    {
        assert index <= notifications.size();
        Notification n = notifications.get(index - 1);
        assert n.actor != null;

        ProfileWindow w = new ProfileWindow(primaire);
        w.use(n.actor);
        w.setLocation(getX() + (getWidth() * 9/8), getY());
        w.setVisible(true);
    }

//   -  -%-  -

    public void
    windowOpened(WindowEvent eW)
    {
        windowUpdater.add(this);
    }

    public void
    windowClosed(WindowEvent eW)
    {
        windowUpdater.remove(this);
    }


    public void
    windowClosing(WindowEvent eW) { }

    public void
    windowDeiconified(WindowEvent eW) { }

    public void
    windowIconified(WindowEvent eW) { }

    public void
    windowActivated(WindowEvent eW) { }

    public void
    windowDeactivated(WindowEvent eW) { }

//  ---%-@-%---

    NotificationsWindow(JKomasto primaire)
    {
        super(
            JapaneseStrings.get("notifications title")
            + " - JKomasto"
        );
        this.primaire = primaire;
        api = primaire.getMastodonApi();
        windowUpdater = primaire.getWindowUpdater();
        fetcher = primaire.getImageFetcher();

        populateNotificationIcons();

        notifications = new ArrayList<>();

        display = new NotificationsComponent(this, fetcher);
        display.setNotificationIcons(NOTIF_ICONS);
        setContentPane(display);

        setIconImage(primaire.getProgramIcon());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setScale(20);

        showLatestPage();
        // Matching TimelineWindow's behaviour for now.

        this.addWindowListener(this);
        addMouseWheelListener(new MouseWheelScaler(1, 10));
        addKeyListener(new SharedKeyShortcuts(primaire, true));
    }

//   -  -%-  -

    private void
    populateNotificationIcons()
    {
        if (!NOTIF_ICONS.isEmpty()) return;
        
        NOTIF_ICONS.put(
            NotificationType.BOOST,
            ImageApi.local("boostNotification"));
        NOTIF_ICONS.put(
            NotificationType.FAVOURITE,
            ImageApi.local("favouriteNotification"));

        for (Image image: NOTIF_ICONS.values())
            ImageApi.awaitImage(image);
    }

}

class
NotificationsComponent extends JPanel
implements Scalable, ActionListener, MouseListener {

    private NotificationsWindow
    primaire;

    private EnumMap<NotificationType, Image>
    notifIcons;

//   -    -%-     -

    private JButton
    latest, prev, next;

    private AdjustableStrut
    strut;

    private JComponent
    centre;

    private List<NotificationComponent>
    rows;

    private int
    scale;

//   -    -%-     -

    static final int
    ROW_COUNT = 10;

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR),
    HAND = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

//  ---%-@-%---

    public void
    showNotifications(
        List<Notification> notifications, List<Boolean> hidden)
    {
        assert hidden.size() == notifications.size();
        int count = Math.min(notifications.size(), rows.size());
        for (int o = 0; o < count; ++o)
        {
            Notification n = notifications.get(o);
            NotificationComponent c = rows.get(o);

            if (n == null)
            {
                c.setMalformed(true);
                c.setHidden(false);
                c.revalidate();
                continue;
            }

            c.setMalformed(false);
            c.setHidden(hidden.get(o));

            c.setName(n.actor.name);
            if (notifIcons != null)
                c.setIcon(notifIcons.get(n.type));
            else
                c.setIcon(null);
            String type = n.type.name().toLowerCase();
            c.setType(JapaneseStrings.get("notifications " + type));

            c.setEmphasis(
                n.type == NotificationType.MENTION ||
                n.type == NotificationType.FOLLOW ||
                n.type == NotificationType.FOLLOWREQ);
            if (n.postText == null) c.setText("");
            else c.setText(n.postText.replaceAll("^(@[^ ]+ )+", ""));

            c.revalidate();
        }
        for (int o = count; o < rows.size(); ++o)
        {
            rows.get(o).reset();
        }
    }

    public void
    setNotificationIcons(
        EnumMap<NotificationType, Image> notifIcons)
    {
        this.notifIcons = notifIcons;
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        int m = 8 * scale/20;
        Border b1 = BorderFactory.createEmptyBorder(m, m, m, m);
        Border b2 = BorderFactory.createLoweredSoftBevelBorder();
        centre.setBorder(b2);
        this.setBorder(b1);

        String fname = "VL PGothic";
        Font large = new Font(fname, Font.BOLD, 14 * scale/20);
        Font small = new Font(fname, Font.BOLD, 10 * scale/20);
        latest.setFont(small);
        next.setFont(large);
        prev.setFont(large);

        strut.setSizing(8 * scale/20, 0);

        ((BorderLayout)getLayout()).setVgap(8 * scale/20);

        for (NotificationComponent c: rows)
            c.setScale(scale);
    }

//   -    -%-     -

    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();
        setCursor(WAIT);
        if (src == latest) primaire.showLatestPage();
        if (src == prev) primaire.showPrevPage();
        if (src == next) primaire.showNextPage();
        setCursor(null);
    }

    public void
    mouseClicked(MouseEvent eM)
    {
        assert eM.getSource() instanceof NotificationComponent;
        int offset = rows.indexOf(eM.getSource());
        assert offset != -1;

        setCursor(WAIT);

        if (eM.getButton() == MouseEvent.BUTTON1)
            primaire.notificationOpened(offset + 1);
        else if (eM.getButton() == MouseEvent.BUTTON3)
            primaire.notificationActorOpened(offset + 1);

        setCursor(null);
    }

    public void
    mouseEntered(MouseEvent eM)
    {
        assert eM.getSource() instanceof NotificationComponent;
        NotificationComponent c =
            (NotificationComponent)eM.getSource();

        c.setCursor(HAND);
    }

    public void
    mouseExited(MouseEvent eM)
    {
        assert eM.getSource() instanceof NotificationComponent;
        NotificationComponent c =
            (NotificationComponent)eM.getSource();

        c.setCursor(null);
    }

    public void
    mousePressed(MouseEvent eM)
    {
        assert eM.getSource() instanceof NotificationComponent;
        NotificationComponent c =
            (NotificationComponent)eM.getSource();
        boolean hover = c.getMousePosition() != null;

        c.setCursor(WAIT);
        c.setPressed(true);
        c.setCursor(hover ? HAND : null);
    }

    public void
    mouseReleased(MouseEvent eM)
    {
        assert eM.getSource() instanceof NotificationComponent;
        NotificationComponent c =
            (NotificationComponent)eM.getSource();
        boolean hover = c.getMousePosition() != null;

        c.setCursor(WAIT);
        c.setPressed(false);
        c.setCursor(hover ? HAND : null);
    }

//  ---%-@-%---

    NotificationsComponent(
        NotificationsWindow primaire, ImageFetcher fetcher)
    {
        this.primaire = primaire;

        String key = "TabbedPane.tabAreaBackground";
        Color bg1 = javax.swing.UIManager.getColor(key);

        latest = new JButton("|<");
        prev = new JButton("<");
        next = new JButton(">");
        latest.setToolTipText(JapaneseStrings.get("flip newest"));
        prev.setToolTipText(JapaneseStrings.get("flip newer"));
        next.setToolTipText(JapaneseStrings.get("flip older"));
        latest.addActionListener(this);
        prev.addActionListener(this);
        next.addActionListener(this);

        Dimension msz = latest.getMaximumSize();
        msz.height = Integer.MAX_VALUE;
        latest.setMaximumSize(msz);

        Insets margin = latest.getMargin();
        margin.left = 4;
        margin.right = 3;
        latest.setMargin(margin);

        centre = new JPanel();
        centre.setLayout(new BoxLayout(centre, BoxLayout.Y_AXIS));

        rows = new ArrayList<>();
        for (int n = ROW_COUNT; n > 0; --n)
        {
            NotificationComponent c =
                new NotificationComponent(fetcher);
            c.addMouseListener(this);
            rows.add(c);
            centre.add(c);
        }

        strut = new AdjustableStrut();
        Box bottom = Box.createHorizontalBox();
        bottom.add(Box.createGlue());
        bottom.add(latest);
        bottom.add(prev);
        bottom.add(strut);
        bottom.add(next);

        setLayout(new BorderLayout());
        add(centre);
        add(bottom, BorderLayout.SOUTH);

        setBackground(bg1);
    }

}

class
NotificationComponent extends JComponent
implements Scalable, ImageFetcher.Listener {

    private ImageFetcher
    fetcher;

    private Image
    icon;

//   -  -%-  -

    private JLabel
    type;

    private ImageComponent
    iconDisplay;

    private Image
    scaledIcon;

    private JLabel
    name, text;

    private int
    typeMaxWidth;

    private JLabel
    malformedText, hiddenText;

    private boolean
    malformed, hidden;

    private boolean
    emphasis;

    private int
    scale;

    private boolean
    pressed;

//  ---%-@-%---

    public void
    setType(String n) { type.setText(n); }

    public String
    getType() { return type.getText(); }

    public void
    setIcon(Image icon)
    {
        if (scaledIcon != null && scaledIcon != this.icon)
            scaledIcon.flush();

        this.icon = icon;
        scaledIcon = null;
        doLayout();
        repaint();
    }

    public void
    setName(String n) { name.setText(n); }

    public void
    setText(String n) { text.setText(n); }

    public void
    setMalformed(boolean n) { malformed = n; }

    public void
    setHidden(boolean n) { hidden = n; }

    public void
    setEmphasis(boolean n)
    {
        emphasis = n;

        Color c1 = getForeground();
        Color c2 = c1.brighter().brighter().brighter();
        name.setForeground(emphasis ? c1 : c2);
        type.setForeground(emphasis ? c1 : c2);
        text.setForeground(emphasis ? c1 : c2);
    }

    public void
    setPressed(boolean n)
    {
        pressed = n;

        boolean isLast =
            getParent().getComponent
                (getParent().getComponentCount() - 1) == this;
        Color c = new Color(0, 0, 0, 25);
        int s = isLast ? 0 : 1 * getScale()/20;
        Border b1 = BorderFactory.createMatteBorder(0, 0, s, 0, c);
        Border b2 = BorderFactory.createLoweredSoftBevelBorder();
        setBorder(pressed ? b2 : b1);
    }

    public void
    reset()
    {
        setType(" ");
        setIcon(null);
        setName(" ");
        setText(" ");
        hidden = false;
        emphasis = true;
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        String fname = "Ume Hy Gothic";
        int fsz = 16 * scale/20;
        Font f1 = new Font(fname, Font.PLAIN, fsz);
        Font f2 = new Font(fname, Font.PLAIN, fsz * 11/14);
        Font f3 = new Font(fname, Font.ITALIC, fsz );
        name.setFont(f1);
        type.setFont(f2);
        text.setFont(f3);
        hiddenText.setFont(f2);

        FontMetrics fm = getFontMetrics(f2);
        typeMaxWidth = 0;
        NotificationType[] types =
            NotificationType.class.getEnumConstants();
        for (NotificationType t: types)
        {
            String sf = t.name().toLowerCase();
            String s = JapaneseStrings.get("notifications " + sf);
            int typeWidth = fm.stringWidth(s);
            if (typeWidth > typeMaxWidth) typeMaxWidth = typeWidth;
            /*
            * I feel like I don't like this that much, because we
            * repeat it for every component on every resize, and
            * fittin to the max width (and changing the strings so
            * that they'd be more equal) is.. Like, I like the
            * percentage solution better, and just adjusting it to
            * have the right width for the majority of notifications.
            *
            * Moreover, it was already usable without the label being
            * readable because we have icons, that's what it was for.
            * I don't think we should prioritise the label much
            * especially when the English ones will be long.
            */
        }

        setPressed(pressed);
        doLayout();
    }

//   -    -%-     -

    public void
    imageReady(String name, Image image)
    {
        scaledIcon = image;
        //iconDisplay.repaint();
        repaint();
    }

    public void
    doLayout()
    {
        int w = getWidth(), h = getHeight();

        int x0 = w * 0/20;
        int x2 = w * 10/20;
        int x1 = x2 - typeMaxWidth - w * 1/20;
        int x3 = w * 12/20;
        int x4 = w * 20/20;

        name.setLocation(x0 + 4, 0);
        name.setSize((x1 - 4) - (x0 + 4), h);
        name.setVisible(!malformed && !hidden);

        type.setLocation(x1 + 4, 0);
        type.setSize((x2 - 1) - (x1 + 4), h);
        type.setVisible(!malformed && !hidden);

        iconDisplay.setLocation((x2 + 1), 2);
        iconDisplay.setSize((x3 - 4) - (x2 + 1), (h - 2) - 2);
        iconDisplay.setVisible(!malformed && !hidden);

        text.setLocation(x3 + 4, 0);
        text.setSize((x4 - 4) - (x3 + 4), h);
        text.setVisible(!malformed && !hidden);

        malformedText.setLocation(x0, 0);
        malformedText.setSize(x4 - x0, h);
        malformedText.setVisible(malformed);

        hiddenText.setLocation(x0, 0);
        hiddenText.setSize(x4 - x0, h);
        hiddenText.setVisible(hidden);

        boolean snapdown = true;
        if (icon != null)
        {
            int nw = iconDisplay.getWidth();
            int nh = iconDisplay.getHeight();
            int s = Math.min(nw, nh);
            if (snapdown)
            {
                int n = 4; while (n < s) n = n * 3/2;
                s = n * 2/3;
                //s = n * 5/6;
            }

            boolean rescale = true;
            if (scaledIcon != null)
            {
                int sw = scaledIcon.getWidth(null);
                int sh = scaledIcon.getHeight(null);
                rescale = sw != s || sh != s;
            }

            boolean rescaling = scaledIcon == icon;
            if (rescale && !rescaling)
            {
                scaledIcon = icon;
                fetcher.scale(icon, s, s, null, 1, this);
            }
        }
    }

//  ---%-@-%---

    public class
    ImageComponent extends JComponent {

        private boolean
        snapdown;

//      -=%=-

        protected void
        paintComponent(Graphics g)
        {
            int w = getWidth(), h = getHeight();
            g.clearRect(0, 0, w, h);

            if (scaledIcon != null)
            {
                int s = Math.min(w, h);
                if (snapdown)
                {
                    int n = 4; while (n < s) n = n * 3/2;
                    s = n * 2/3;
                    //s = n * 5/6;
                }

                int x = (w - s) / 2, y = (h - s) / 2;
                g.drawImage(scaledIcon, x, y, s, s, null);
            }
        }

    }

//  ---%-@-%---

    NotificationComponent(ImageFetcher fetcher)
    {
        this.fetcher = fetcher;

        name = new JLabel();

        type = new JLabel();
        type.setHorizontalAlignment(JLabel.RIGHT);

        iconDisplay = new ImageComponent();
        iconDisplay.snapdown = true;

        text = new JLabel();

        // (悪) I don't set any font by default, so the proportions
        // would be off if we neglected to call setFont from
        // NotificationsWindow. I don't think this is good.

        malformedText = new JLabel(
            JapaneseStrings.get("notifications malformed"),
            JLabel.CENTER);
        hiddenText = new JLabel(
            JapaneseStrings.get("notifications hidden"),
            JLabel.CENTER);

        setLayout(null);
        add(name);
        add(type);
        add(iconDisplay);
        add(text);
        add(malformedText);
        add(hiddenText);
    }

}
