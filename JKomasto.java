
import javax.swing.JPanel;
import javax.swing.JComponent;
import java.awt.Rectangle;
import java.awt.Image;
import java.awt.FontMetrics;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.EventQueue;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.OceanTheme;
import javax.swing.plaf.ColorUIResource;
import javax.swing.UIDefaults;
import java.io.File;
import java.io.IOException;
import cafe.biskuteri.hinoki.Tree;


class
JKomasto
implements ImageAPI2.Listener {

    private LoginWindow
    loginWindow;

    private PostWindow
    autoViewWindow;

    private ImageWindow
    autoMediaWindow;

    private NotificationsWindow
    notificationsWindow;

    private SettingsWindow
    settingsWindow;

    private EmojiPicker
    emojiPicker;

//   -      -%-      - 

    private TimelineWindowUpdater
    windowUpdater;

    private ImageFetcher
    imageFetcher;

    private PriorityExecutor
    imageThreads,
    emojiThreads;

    private DirectoryCache
    emojiCache;

//   -      -%-      -

    private MastodonApi
    api;

    private Settings
    settings;

    private Image
    programIcon;

    private List<ListDetails>
    lists;



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    public void
    finishedLogin(OperationProgress startupProgress)
    {
        windowUpdater = new TimelineWindowUpdater(this);
        startupProgress.markComplete("window updater ready");

        autoViewWindow = new PostWindow(this);
        autoViewWindow.setTitle("Auto view - JKomasto");
        startupProgress.markComplete("auto view ready");
        autoMediaWindow = new ImageWindow(this);
        startupProgress.markComplete("auto media ready");

        TimelineWindow timelineWindow = new TimelineWindow(this);
        timelineWindow.setLocationByPlatform(true);
        timelineWindow.switchToTimeline(TimelineType.HOME);
        startupProgress.markComplete("timeline window ready");

        notificationsWindow = new NotificationsWindow(this);
        startupProgress.markComplete("notifs window ready");

        loginWindow.dispose();
        timelineWindow.setVisible(true);

        emojiPicker.beginLoadingRemoteEmojis();
    }

//   -      -%-      -

    public PostWindow
    getAutoViewWindow() { return autoViewWindow; }

    public ImageWindow
    getAutoMediaWindow() { return autoMediaWindow; }

    public NotificationsWindow
    getNotificationsWindow() { return notificationsWindow; }

    public SettingsWindow
    getSettingsWindow() { return settingsWindow; }

    public EmojiPicker
    getEmojiPicker() { return emojiPicker; }

//   -      -%-      -

    public TimelineWindowUpdater
    getWindowUpdater() { return windowUpdater; }

    public ImageFetcher
    getImageFetcher() { return imageFetcher; }

    public PriorityExecutor
    getImageThreads() { return imageThreads; }

    public PriorityExecutor
    getEmojiThreads() { return emojiThreads; }

//   -      -%-      -

    public MastodonApi
    getMastodonApi() { return api; }

    public Settings
    getSettings() { return settings; }

    public Image
    getProgramIcon() { return programIcon; }

    public List<ListDetails>
    getLists() { return lists; }

//   -      -%-      -

    public void
    populateEmojis(boolean overwrite)
    {
        RequestOutcome r = api.getCustomEmojis();
        if (r.exception != null) return;
        if (r.errorEntity != null) return;

        for (Tree<String> entity: r.entity)
        {
            String shortcode = entity.get("shortcode").value;

            EmojiPicker.Emoji emoji = new EmojiPicker.Emoji();
            emoji.string = ":" + shortcode + ":";
            emoji.label = shortcode;
            emoji.urlr = entity.get("url").value;
            emoji.glyph = null;
            emoji.keywords = new String[] { shortcode };

            if (!emojiPicker.hasEmoji(emoji) || overwrite)
                emojiPicker.addEmoji(emoji);
        }
        emojiPicker.refreshEmojis();
    }


    public void
    populateLists()
    {
        RequestOutcome r = api.getLists();
        if (r.exception != null)
        {
            return;
        }
        else if (r.errorEntity != null)
        {
            return;
        }

        for (Tree<String> entity: r.entity)
        {
            ListDetails list = new ListDetails();
            list.id = entity.get("id").value;
            list.title = entity.get("title").value;
            lists.add(list);
        }
    }

//   -      -%-      -

    public void
    doLayout1(TimelineWindow timelineWindow)
    {
        new Thread() {
            public void
            run()
            {
                timelineWindow.setVisible(true);
                //sleep(50);
                //timelineWindow.toFront();
                //timelineWindow.getToolkit().sync();
                sleep(300);

                Rectangle s1 = timelineWindow.getBounds();
                Rectangle s2 = autoViewWindow.getBounds();
                Rectangle s3 = notificationsWindow.getBounds();

                int cx = s1.x + (s1.width / 2);
                int cy = s1.y + (s1.height / 2);

                int x1 = cx - s1.width * 103/100;
                int x2 = cx + s1.width * 3/100;
                int y1 = s1.y;
                int y2 = cy - s2.height * 104/100;
                int y3 = cy + s2.height * 4/100;

                s2.x = cx - (s2.width / 2);
                s2.y = cy - (s2.height / 2);
                s3.x = cx - (s3.width / 2);
                s3.y = cy - (s3.height / 2);
                /*
                * (注) Editing the values in the bounds of a realised
                * window will change its location without a call to
                * setLocation. We only call setLocation below because
                * that's necessary for unrealised windows.
                *
                * It seems to be buggy behaviour for me (since the
                * values in the bounds objects need to be stable for
                * the animation loop below), so it's not something
                * to rely on. Just, yea.
                */

                autoViewWindow.setLocation(s2.x, s2.y);
                notificationsWindow.setLocation(s3.x, s3.y);
                autoViewWindow.setVisible(true);
                notificationsWindow.setVisible(true);
                //timelineWindow.toFront();
                sleep(650);

                int frames = 12, dur = 300;
                try
                {
                    for (int f = 0; f <= frames; ++f)
                    {
                        autoViewWindow.setLocation(
                            s2.x + (x2 - s2.x) * f/frames,
                            s2.y + (y2 - s2.y) * f/frames
                        );
                        notificationsWindow.setLocation(
                            s3.x + (x2 - s3.x) * f/frames,
                            s3.y + (y3 - s3.y) * f/frames
                        );
                        timelineWindow.setLocation(
                            s1.x + (x1 - s1.x) * f/frames,
                            s1.y + (y1 - s1.y) * f/frames
                        );
                        Thread.sleep(dur / frames);
                    }
                }
                catch (InterruptedException eIt)
                {
                    assert false;
                }

                timelineWindow.toFront();
                timelineWindow.getToolkit().sync();
            }
            private void
            sleep(int ms)
            {
                try { Thread.sleep(ms); }
                catch (InterruptedException eIt) { }
            }
        }.start();
    }

    public void
    doLayout2(TimelineWindow timelineWindow)
    {
        new Thread() {
            public void
            run()
            {
                Toolkit tk = timelineWindow.getToolkit();
                Dimension scsz = tk.getScreenSize();

                timelineWindow.setVisible(true);
                //tk.sync();
                sleep(1000);

                Rectangle s1 = timelineWindow.getBounds();
                Rectangle s2 = autoViewWindow.getBounds();
                Rectangle s3 = notificationsWindow.getBounds();
                int x1 = scsz.width * 2/100;
                int x3 = (scsz.width * 98/100) - s3.width;
                int x2 = ((x3 + x1 + s1.width) - s2.width) / 2;
                int y1 = s1.y;
                int y2 = y1 + (s1.height - s2.height) / 2;
                int y3 = y1 + s1.height - s3.height;

                s2.x = s1.x;
                s2.y = y2;
                s3.x = s1.x;
                s3.y = y3;
                autoViewWindow.setLocation(s2.x, s2.y);
                notificationsWindow.setLocation(s3.x, s3.y);
                autoViewWindow.setVisible(true);
                notificationsWindow.setVisible(true);
                autoViewWindow.toFront();
                timelineWindow.toFront();
                tk.sync();
                sleep(600);

                try
                {
                    int frames = 12, dur = 300;
                    for (int f = 0; f <= frames; ++f)
                    {
                        int x = s1.x + (x1 - s1.x) * f/frames;
                        notificationsWindow.setLocation(x, s3.y);
                        autoViewWindow.setLocation(x, s2.y);
                        timelineWindow.setLocation(x, s1.y);
                        Thread.sleep(dur / frames);
                    }
                }
                catch (InterruptedException eIt)
                {
                    assert false;
                }

                try
                {
                    int frames = 12, dur = 300;
                    //sleep(100);
                    for (int f = 0; f <= frames; ++f)
                    {
                        autoViewWindow.setLocation(
                            x1 + (x2 - x1) * f/frames,
                            y2
                        );
                        Thread.sleep(dur / frames);
                    }
                    //sleep(100);
                    for (int f = 0; f <= frames; ++f)
                    {
                        notificationsWindow.setLocation(
                            x1 + (x3 - x1) * f/frames,
                            y3
                        );
                        Thread.sleep(dur / frames);
                    }
                }
                catch (InterruptedException eIt)
                {
                    assert false;
                }

                timelineWindow.toFront();
            }
            private void
            sleep(int ms)
            {
                try { Thread.sleep(ms); }
                catch (InterruptedException eIt) { }
            }
        }.start();
    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    private static class
    MetalTheme extends OceanTheme {

        private ColorUIResource
        lightPink = new ColorUIResource(246, 240, 240),
        //mildPink  = new ColorUIResource(240, 234, 234),
        mildPink  = new ColorUIResource(242, 240, 240),
        // Our target is really (240, 240, 240), but we should
        // aim up because the timeline window is higher density
        // than most of the system's apps.
        white = new ColorUIResource(250, 250, 250),
        pink = new ColorUIResource(242, 230, 230),
        veryDarkPink = new ColorUIResource(164, 160, 160);

//      -=%=-

        public ColorUIResource
        getPrimary2() { return pink; }

        public ColorUIResource
        getSecondary2() { return white; }

        public ColorUIResource
        getSecondary3() { return mildPink; }

        public ColorUIResource
        getSecondary1() { return veryDarkPink; }

        public ColorUIResource
        getPrimary1() { return veryDarkPink; }

        public void
        addCustomEntriesToTable(UIDefaults table)
        {
            super.addCustomEntriesToTable(table);
            table.put(
                "TabbedPane.tabAreaBackground",
                getPrimary1());
            table.put(
                "TabbedPane.contentAreaColor",
                getSecondary3());
            table.put(
                "TabbedPane.selected",
                getSecondary3());
            table.put(
                "MenuBar.gradient",
                java.util.Arrays.asList(new Object[] {
                    1f, 0f,
                    getSecondary2(),
                    getSecondary3(),
                    getSecondary1()}));
        }

    }

//  ---%-@-%---

    public static void
    main(String... args)
    {
        //System.setProperty("swing.boldMetal", "false");
        MetalLookAndFeel.setCurrentTheme(new MetalTheme());

        System.setProperty("awt.useSystemAAFontSettings","on");
        System.setProperty("swing.aatext", "true");

        Toolkit tk = Toolkit.getDefaultToolkit();
        EventQueue edt = tk.getSystemEventQueue();
        edt.invokeLater(new Runnable() {
            public void
            run()
            {
                JKomasto instance = new JKomasto();
                instance.loginWindow.setVisible(true);
            }
        });
        /*
        * Get into the EDT as soon as possible. ImageFetcher
        * runs on the EDT, so they can fight with the main
        * thread, if you try to stay on the main thread.
        */
    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    public
    JKomasto()
    {
        api = new MastodonApi();

        imageFetcher = new ImageFetcher("JKomasto");
        imageThreads = new PriorityExecutor("JKomasto-Images");
        emojiThreads = new PriorityExecutor("JKomasto-Emojis");

        settings = new Settings();

        ImageAPI2.fetch(
            ImageAPI2.URLs.local("flower"),
            "programIcon", 0, this).run();

        lists = new ArrayList<>();

        try { emojiCache = new DirectoryCache("Custom Emojis"); }
        catch (IOException eIo) { emojiCache = null; }

        emojiPicker = new EmojiPicker(emojiThreads, emojiCache);
        emojiPicker.addEmojisFromCache();

        loginWindow = new LoginWindow(this);
        loginWindow.setLocationByPlatform(true);
        loginWindow.toFront();

        settingsWindow = new SettingsWindow(this);
        /*
        * Putting this here since I'd rather it ready from the
        * start. Not super sure why I don't initialise the
        * rest in the constructor as well. For now I assume
        * it's cause of automatic fetching upon construction.
        * SettingsWindow certainly doesn't do that.
        *
        * Be sure to construct after LoginWindow since the
        * latter is the one that does loading of settings from
        * the cache. I don't do it myself here cause if an
        * error occurs we want it to appear on the login window.
        */
    }

//  -=- --- -%- --- -=-

    public void
    imageReady(String name, Image image)
    {
        if (name.equals("programIcon"))
            programIcon = image;
    }

}



enum
PostVisibility {

    PUBLIC,
    UNLISTED,
    FOLLOWERS,
    MENTIONED

}

enum
TimelineType {

    FEDERATED,
    LOCAL,
    HOME,
    LIST,
    PROFILE,
    NOTIFICATIONS

}

enum
NotificationType {

    MENTION,
    BOOST,
    FAVOURITE,
    FOLLOW,
    FOLLOWREQ,
    POLL,
    ALERT

}



class
TimelinePage {

    public TimelineType
    type;

    public String
    accountNumId, listId;

    public Post[]
    posts;

//  ---%-@-%---

    public
    TimelinePage() { }

    public
    TimelinePage(
        TimelineType type, String accountNumId, String listId,
        Tree<String> entity)
    {
        this.type = type;
        this.accountNumId = accountNumId;
        this.listId = listId;
        /*
        * We cannot derive these from the entity itself.
        * Mastodon only returns an array of posts. So the caller
        * has to tell us what values they made the call for.
        */

        this.posts = new Post[entity.size()];
        for (int o = 0; o < posts.length; ++o) try {
            this.posts[o] = new Post(entity.get(o));
        }
        catch (MalformedEntityException eMe) {
            this.posts[o] = null;
        }
    }

}


class
Notification {

    public NotificationType
    type;

    public String
    id;

    public String
    postId, postText;

    public Account
    actor;

    public Post
    post;

//  ---%-@-%---

    public void
    resolvePostText()
    {
        if (post != null)
        {
            post.resolveApproximateText();
            postText = post.approximateText;
        }
        else postText = null;
    }

//  ---%-@-%---

    public
    Notification() { }

    public
    Notification(Tree<String> entity)
    throws MalformedEntityException
    {
        id = entity.get("id").value;

        String type = entity.get("type").value;
        if (type.equals("favourite"))
            this.type = NotificationType.FAVOURITE;
        else if (type.equals("reblog"))
            this.type = NotificationType.BOOST;
        else if (type.equals("mention"))
            this.type = NotificationType.MENTION;
        else if (type.equals("follow"))
            this.type = NotificationType.FOLLOW;
        else if (type.equals("follow_request"))
            this.type = NotificationType.FOLLOWREQ;
        else if (type.equals("poll"))
            this.type = NotificationType.POLL;
        else
            this.type = NotificationType.ALERT;

        this.actor = new Account(entity.get("account"));

        if (this.type != NotificationType.FOLLOW)
        {
            assert entity.get("status") != null;
            post = new Post(entity.get("status"));
            postId = post.id;
        }
    }

}



class
Post {

    public String
    id,
    uri;

    public Account
    author;

    public PostVisibility
    visibility;

    public String
    language;

    public String
    text,
    approximateText;

    public String
    contentWarning;

    public ZonedDateTime
    dateTime;

    public String
    date, time, relativeTime;

    public boolean
    boosted, favourited, bookmarked;

    public boolean
    inThread, hasReplies;

    public boolean
    edited, filtered;

    public Post
    boostedPost;

    public String
    parentId;

    public Attachment[]
    attachments;

    public String[][]
    emojiUrls;

    public String[][]
    mentions;

    public PostThread
    thread;

    public Poll
    poll;

//   -  -%-  -

    private static final DateTimeFormatter
    DATE_FORMAT = DateTimeFormatter.ofPattern("d LLLL ''uu"),
    TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm");

//  ---%-@-%---

    public void
    resolveApproximateText()
    {
        assert text != null;
        if (approximateText != null) return;

        Tree<String> nodes;
        nodes = RudimentaryHTMLParser.depthlessRead(text);
        if (nodes.size() == 0)
        {
            approximateText = "-";
            return;
        }

        StringBuilder b = new StringBuilder();
        Tree<String> first = nodes.get(0);
        for (Tree<String> node: nodes)
        {
            if (node.key.equals("tag"))
            {
                if (node.get(0).key.equals("br"))
                    b.append("; ");
                if (node.get(0).key.equals("p") && node != first)
                    b.append("; ");
            }
            if (node.key.equals("emoji"))
            {
                b.append(":" + node.value + ":");
            }
            if (node.key.equals("text"))
            {
                b.append(node.value);
            }
        }
        approximateText = b.toString();
    }

    public void
    resolveRelativeTime()
    {
        assert date != null;

        ZonedDateTime now = ZonedDateTime.now();
        long d = ChronoUnit.SECONDS.between(dateTime, now);
        long s = Math.abs(d);
        if (s < 30) relativeTime = "now";
        else if (s < 60) relativeTime = d + "s";
        else if (s < 3600) relativeTime = (d / 60) + "m";
        else if (s < 86400) relativeTime = (d / 3600) + "h";
        else relativeTime = (d / 86400) + "d";
    }

//  ---%-@-%---

    public
    Post() { }

    public
    Post(Tree<String> entity)
    throws MalformedEntityException
    {
        MastodonApi.requireProperties(
            entity,
            "id", "url", "uri", "visibility", "language",
            "created_at", "content", "spoiler_text",
            "in_reply_to_id", "media_attachments", "emojis",
            "reblog", "mentions");
        MastodonApi.eachRequireProperties(
            entity.get("mentions"),
            "url", "id", "acct");

        id = entity.get("id").value;

        uri = entity.get("url").value;
        if (uri == null) uri = entity.get("uri").value;

        author = new Account(entity.get("account"));

        String v = entity.get("visibility").value;
        boolean p = v.equals("public");
        boolean u = v.equals("unlisted");
        boolean f = v.equals("private");
        boolean m = v.equals("direct");
        if (p) visibility = PostVisibility.PUBLIC;
        if (u) visibility = PostVisibility.UNLISTED;
        if (f) visibility = PostVisibility.FOLLOWERS;
        if (m) visibility = PostVisibility.MENTIONED;

        language = entity.get("language").value;

        dateTime =
            ZonedDateTime.parse(entity.get("created_at").value)
                .withZoneSameInstant(ZoneId.systemDefault());
        /*
        date = DATE_FORMAT.format(dateTime);
        time = TIME_FORMAT.format(dateTime);
        */
        String dt = EnglishStrings.get("post date template");
        String tt = EnglishStrings.get("post time template");
        String[] months = EnglishStrings.get("month names").split(",");
        date = dt.replace("<><><>", "" + dateTime.getYear() % 100);
        date = date.replace("<><>", months[dateTime.getMonthValue() - 1]);
        date = date.replace("<>", "" + dateTime.getDayOfMonth());
        int th = dateTime.getHour(), tm = dateTime.getMinute();
        time = tt.replace("<><>", (tm >= 10 ? "" : "0") + tm);
        time = time.replace("<>", (th >= 10 ? "" : "0") + th);

        text = entity.get("content").value;
        String st = entity.get("spoiler_text").value;
        contentWarning = st.trim().isEmpty() ? null : st;

        Tree<String> favourited = entity.get("favourited");
        Tree<String> boosted = entity.get("reblogged");
        Tree<String> bookmarked = entity.get("bookmarked");
        if (favourited != null)
            this.favourited = favourited.value.equals("true");
        if (boosted != null)
            this.boosted = boosted.value.equals("true");
        if (bookmarked != null)
            this.bookmarked = bookmarked.value.equals("true");

        parentId = entity.get("in_reply_to_id").value;
        inThread = parentId != null;
        String nrv = Double.toString(0.0);
        // hinoki.JsonConverter converts all numbers into doubles.
        hasReplies = !entity.get("replies_count").value.equals(nrv);

        Tree<String> edited = entity.get("edited_at");
        if (edited != null)
            this.edited = edited.value != null;

        Tree<String> media = entity.get("media_attachments");
        attachments = new Attachment[media.size()];
        int o = -1; for (Tree<String> media2: media)
        {
            attachments[++o] = new Attachment(media2);
        }

        Tree<String> emojis = entity.get("emojis");
        emojiUrls = new String[emojis.size()][];
        o = -1; for (Tree<String> emoji: emojis)
        {
            String[] mapping = emojiUrls[++o] = new String[2];
            mapping[0] = ":" + emoji.get("shortcode").value + ":";
            mapping[1] = emoji.get("url").value;
            // (未) Also, storage of the resolved
            // images should be within this struct..
        }

        Tree<String> boostedPost = entity.get("reblog");
        if (boostedPost.size() > 0)
            this.boostedPost = new Post(boostedPost);

        Tree<String> mentions = entity.get("mentions");
        this.mentions = new String[mentions.size()][];
        o = -1; for (Tree<String> mention: mentions)
        {
            String[] mapping = this.mentions[++o] = new String[3];
            mapping[0] = mention.get("url").value;
            mapping[1] = mention.get("id").value;  // numId
            mapping[2] = mention.get("acct").value;
        }

        Tree<String> triggeredFilters = entity.get("filtered");
        this.filtered =
            triggeredFilters != null
            && triggeredFilters.size() > 0;

        Tree<String> poll = entity.get("poll");
        if (poll.size() > 0)
            this.poll = new Poll(poll);
    }

}


class
PostThread {

    public List<Post>
    posts;

    public int[]
    parentage;

//  ---%-@-%---

    public
    PostThread() { }

    public
    PostThread(List<Post> posts)
    {
        assert posts != null;

        this.posts = posts;
        resolveParentage();
    }

    public
    PostThread(Tree<String> context)
    throws MalformedEntityException
    {
        this.posts = toList(context);
        resolveParentage();
    }

//   -  -%-  -

    private void
    resolveParentage()
    {
        List<String> ids = new ArrayList<>();
        for (Post post: posts) ids.add(post.id);

        parentage = new int[posts.size()];
        for (int o = 0; o < posts.size(); ++o)
        {
            Post post = posts.get(o);
            parentage[o] = ids.indexOf(post.parentId);
            post.thread = this;
        }
    }

//   -  -%-  -

    public static Tree<String>
    getHighestReachableAncestor(Post post, Tree<String> context)
    {
        // Create minimal entity to represent post.
        Tree<String> placeholder = new Tree<>();
        placeholder.add(new Tree<>("id", post.id));
        placeholder.add(
            new Tree<>("in_reply_to_id", post.parentId));

        // Put all entities in dictionary by ID.
        Map<String, Tree<String>> entities = new HashMap<>();
        entities.put(placeholder.get("id").value, placeholder);
        for (Tree<String> ancestor: context.get("ancestors"))
            entities.put(ancestor.get("id").value, ancestor);

        // Starting from placeholder, repeatedly find the next
        // ancestor, returning the first that can't find.
        Tree<String> cursor = placeholder;
        for (int n = entities.size(); n > 0; --n)
        {
            String parentId = cursor.get("in_reply_to_id").value;
            Tree<String> parent = entities.get(parentId);
            if (parent == null)
                return cursor == placeholder ? null : cursor;
            cursor = parent;
        }
        assert false;
        return null;
    }

//   -  -%-  -

    private static List<Post>
    toList(Tree<String> context)
    throws MalformedEntityException
    {
        assert context != null;
        assert context.get("top") != null;
        assert context.get("descendants") != null;

        List<Post> returnee = new ArrayList<>();

        try { returnee.add(new Post(context.get("top"))); }
        catch (MalformedEntityException eMu) { throw eMu; }

        for (Tree<String> descendant: context.get("descendants"))
            try { returnee.add(new Post(descendant)); }
            catch (MalformedEntityException eMu) { }

        return returnee;
    }

}


class
Account {

    public String
    numId;

    public String
    id,
    name;

    public String
    avatarUrl;

    public Image
    avatar;

    public Object
    avatarResolver;

    public ZonedDateTime
    creationDate;

    public int
    followedCount,
    followerCount;

    public int
    postCount;

    public String[][]
    fields;

    public String
    description;

//  ---%-@-%---

    public
    Account() { }

    public
    Account(Tree<String> entity)
    {
        numId = entity.get("id").value;
        id = entity.get("acct").value;

        String displayName = entity.get("display_name").value;
        String username = entity.get("username").value;
        name = displayName.isEmpty() ? username : displayName;

        avatarUrl = entity.get("avatar").value;

        creationDate =
            ZonedDateTime.parse(entity.get("created_at").value)
                .withZoneSameInstant(ZoneId.systemDefault());

        String c1 = entity.get("following_count").value;
        String c2 = entity.get("followers_count").value;
        String c3 = entity.get("statuses_count").value;
        try {
            followedCount = (int)Double.parseDouble(c1);
            followerCount = (int)Double.parseDouble(c2);
            postCount = (int)Double.parseDouble(c3);
        }
        catch (NumberFormatException eNf) {
            assert false;
        }

        Tree<String> fs = entity.get("fields");
        fields = new String[fs.size()][];
        for (int o = 0; o < fields.length; ++o)
        {
            Tree<String> f = fs.get(o);
            String[] field = fields[o] = new String[3];
            field[0] = f.get("name").value;
            field[1] = f.get("value").value;
            boolean v = f.get("verified_at").value != null;
            field[2] = Boolean.toString(v);
        }

        description = entity.get("note").value;
    }

}


class
Attachment {

    public String
    id;

    public String
    type;

    public String
    url;

    public String
    description;

    public Image
    image;

    public Object
    imageResolver;

    public File
    uploadee;

    public boolean
    descriptionChanged;

//  ---%-@-%---

    public
    Attachment() { }

    public
    Attachment(Tree<String> entity)
    {
        id = entity.get("id").value;
        type = entity.get("type").value;
        description = entity.get("description").value;
        if (description != null && description.trim().isEmpty())
            description = null;

        url = entity.get("remote_url").value;
        if (url == null) url = entity.get("url").value;

        descriptionChanged = false;
    }

}



class
Composition {

    public String
    text,
    contentWarning;

    public PostVisibility
    visibility;

    public String
    language;

    public String
    replyToPostId;

    public Attachment[]
    attachments;

//  ---%-@-%---

    public
    Composition() { }

    public static Composition
    recover(Tree<String> entity)
    {
        assert entity.get("text") != null;

        Composition c = new Composition();
        c.text = entity.get("text").value;
        c.contentWarning = entity.get("spoiler_text").value;
        c.replyToPostId = entity.get("in_reply_to_id").value;

        String visibility = entity.get("visibility").value;
        boolean p = visibility.equals("public");
        boolean u = visibility.equals("unlisted");
        boolean f = visibility.equals("private");
        boolean m = visibility.equals("direct");
        assert p || u || f || m;
        if (p) c.visibility = PostVisibility.PUBLIC;
        if (u) c.visibility = PostVisibility.UNLISTED;
        if (f) c.visibility = PostVisibility.FOLLOWERS;
        if (m) c.visibility = PostVisibility.MENTIONED;

        c.language = entity.get("language").value;

        Tree<String> a = entity.get("media_attachments");
        c.attachments = new Attachment[a.size()];
        for (int o = 0; o < c.attachments.length; ++o)
            c.attachments[o] = new Attachment(a.get(o));

        return c;
    }

    public static Composition
    reply(Post post, String ownId)
    {
        if (post.boostedPost != null) post = post.boostedPost;

        String[] langs = new String[] { "en", "ja", "cmn", "ms" };
        /*
        boolean keep = false;
        if (post.language != null)
            for (String lang: langs)
                keep |= post.language.equals(lang);
        */
        boolean keep = true;

        Composition c = new Composition();
        c.replyToPostId = post.id;
        c.visibility = post.visibility;
        c.contentWarning = post.contentWarning;
        StringBuilder text = new StringBuilder();

        if (!post.author.id.equals(ownId))
        {
            text.append("@" + post.author.id + " ");
            if (post.visibility == PostVisibility.FOLLOWERS)
                c.visibility = PostVisibility.UNLISTED;
                // Default that I prefer.
            keep = false;
        }

        for (String[] mapping: post.mentions)
        {
            String id = mapping[2];
            if (id.equals(ownId)) continue;
            text.append("@" + id + " ");
        }
        // If it becomes a problem, create a set and add the
        // author ID plus mentioned into it, then create mentions
        // from that set.

        c.text = text.toString();
        c.language = keep ? post.language : langs[0];

        c.attachments = new Attachment[0];

        return c;
    }

    public static Composition
    reply(Tree<String> entity, String ownNumId)
    {
        try
        {
            return reply(new Post(entity), ownNumId);
        }
        catch (MalformedEntityException eMe)
        {
            assert false;
            // The UI is supposed to block attempts at
            // replying to a malformed post.
            return null;
        }
    }

}



class
Poll {

    public String
    id;

    public ZonedDateTime
    expiryDateTime;

    public boolean
    multiOption;

    public int[]
    ownVotes;

    public String[]
    options;

    public int[]
    voteCounts;

    public String[][]
    emojiUrls;

//  ---%-@-%---

    Poll() { }

    Poll(Tree<String> entity)
    throws MalformedEntityException
    {
        id = entity.get("id").value;

        String timer = entity.get("expires_at").value;
        if (timer != null)
            expiryDateTime =
                ZonedDateTime.parse(timer)
                    .withZoneSameInstant(ZoneId.systemDefault());
        else
            expiryDateTime = null;

        multiOption = entity.get("multiple").value.equals("true");
        
        this.ownVotes = null;
        boolean voted = entity.get("voted").value.equals("true");
        Tree<String> ownVotes = entity.get("own_votes");
        if (voted && ownVotes != null)
        {
            this.ownVotes = new int[ownVotes.size()];
            int o = -1; for (Tree<String> vote: ownVotes) try
            {
                this.ownVotes[++o] =
                    (int)Double.parseDouble(vote.value) + 1;
            }
            catch (NumberFormatException eNf)
            {
                throw new MalformedEntityException(eNf);
            }
        }

        Tree<String> options = entity.get("options");
        this.options = new String[options.size()];
        this.voteCounts = new int[this.options.length];
        int o = -1; for (Tree<String> option: options) try
        {
            this.options[++o] = option.get("title").value;

            String unparsed = option.get("votes_count").value;
            this.voteCounts[o] = (int)Double.parseDouble(unparsed);
        }
        catch (NumberFormatException eNf)
        {
            throw new MalformedEntityException(eNf);
        }

        Tree<String> emojis = entity.get("emojis");
        emojiUrls = new String[emojis.size()][];
        o = -1; for (Tree<String> emoji: emojis)
        {
            String[] mapping = emojiUrls[++o] = new String[2];
            mapping[0] = ":" + emoji.get("shortcode").value + ":";
            mapping[1] = emoji.get("url").value;
        }
    }

}




class
MalformedEntityException extends Exception {

    public Tree<String>
    entity;

    public String[]
    requiredProperties;

//   -  -%-  -

    public
    MalformedEntityException(
        Tree<String> entity, String[] requiredProperties)
    {
        super("Missing properties");
        this.entity = entity;
        this.requiredProperties = requiredProperties;
    }

    public
    MalformedEntityException(Exception cause)
    {
        super("Bad type within entity", cause);
    }

}



class
ListDetails {

    String
    id, title;

}



class
LoginDetails {

    public String
    accountId, instanceUrl, avatarUrl;

    public String
    clientId, clientSecret;

    public String
    accessToken;

    public Image
    avatar;

}



class
Settings {

    public String[]
    richTextPaneFontNames;

    public PostVisibility
    defaultVisibility;

    public String[]
    speechCommand,
    imageCommand,
    videoCommand,
    audioCommand;

}
