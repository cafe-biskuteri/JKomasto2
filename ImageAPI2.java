
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.AWTEvent;
import java.awt.ActiveEvent;
import java.awt.Toolkit;
import java.awt.EventQueue;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.io.File;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;

class
ImageAPI2
implements PriorityExecutor.PrioritisedRunnable {

    private String
    operation;

    private URL
    url;

    private Image
    unscaledImage;

    private int
    newWidth, newHeight;

    private String
    name;

    private int
    priority;

    private boolean
    notifyThroughEDT;

    private Listener
    listener;

//   -  -%-  -

    private Image
    fetchedImage,
    scaledImage;

    private int
    lastEventId = AWTEvent.RESERVED_ID_MAX;

//  ---%-@-%---

    public void
    run()
    {
        if (operation.equals("fetch")) fetch();
        if (operation.equals("fetchAWT")) fetchAWT();
        if (operation.equals("scale")) scale();
    }

    public int
    getPriority() { return priority; }

//   -  -%-  -

    private void
    fetch()
    {
        try
        {
            fetchedImage = ImageIO.read(url);
            notifyListener();
        }
        catch (IOException eIo)
        {
            fetchedImage = null;
            notifyListener();
        }
    }

    private void
    fetchAWT()
    {
        ImageIcon icon = new ImageIcon(url);
        // Easier than spawning a MediaTracker ourselves.
        fetchedImage = icon.getImage();
        notifyListener();
    }

    private void
    scale()
    {
        scaledImage = ImageOps.areaAveragingScale(
            unscaledImage, newWidth, newHeight);
        notifyListener();
    }

    private void
    notifyListener()
    {
        if (!notifyThroughEDT) new AWTReadyEvent().dispatch();
        else new AWTReadyEvent().post();
    }

//  ---%-@-%---

    public interface
    Listener {

        void
        imageReady(String name, Image image);

    }

    public static abstract class
    URLs {

        public static URL
        local(String name)
        {
            String path = "graphics/" + name + ".png";
            URL url = ImageAPI2.class.getResource(path);
            if (url != null) return url;
        
            // Look for file in working directory as fallback.
            File file = new File(path);
            if (file.exists()) return url = URLs.file(file);
    
            return null;
        }
    
        public static URL
        remote(String urlr)
        {
            try { return new URL(urlr); }
            catch (MalformedURLException eMu) { return null; }
        }
    
        public static Collection<URL>
        locals(String prefix, String extension)
        {
            Collection<URL> returnee = new ArrayList<>();
    
            File dir = new File("./graphics");
            /*
            * Notably won't work if we're inside a JAR file.
            * We fallback by returning an empty list but,
            * maybe we should obsolete this method and
            * have the caller use their own manifest?
            * At the same time, we rely on directories when
            * we want it to be customisable by the user, so
            * supporting specifically directories is better.
            */
            File[] files = dir.listFiles();
            if (files != null) for (File file: files)
            {
                String filename = file.getName();
                if (!filename.startsWith(prefix)) continue;
                if (!filename.endsWith("." + extension)) continue;
    
                returnee.add(URLs.file(file));
            }
    
            return returnee;
        }
    
        public static boolean
        isGIFURL(URL url)
        {
            String path = url.getPath();
    
            int oExt = path.lastIndexOf('.');
            if (oExt == -1) return false;
    
            String ext = path.substring(oExt);
            return ext.equals("gif");
        }

        public static URL
        file(File file)
        {
            try { return file.toURI().toURL(); }
            catch (MalformedURLException eMu) { assert false; }
            return null;
        }

    }

    public static abstract class
    ImageOps {

        public static BufferedImage
        toBufferedImage(Image awtImage)
        {
            int iw = awtImage.getWidth(null);
            int ih = awtImage.getHeight(null);
            assert ih != -1;
    
            BufferedImage returnee = new BufferedImage(
                iw, ih, BufferedImage.TYPE_INT_ARGB);
            Graphics g = returnee.getGraphics();
            g.drawImage(awtImage, 0, 0, null);
            g.dispose();
            return returnee;
        }

        public static Image
        areaAveragingScale(Image image, int w, int h)
        {
            BufferedImage s1 = toBufferedImage(image);
    
            int neededScaleUp = Math.max(
                (w * 3) / s1.getWidth(),
                (h * 3) / s1.getHeight());
            if (neededScaleUp < 1) neededScaleUp = 1;
            BufferedImage s2 = simpleScaleUp(s1, neededScaleUp);
            s1.flush();
    
            BufferedImage s3 = areaAveragingScale(s2, w, h);
            s2.flush();
    
            return s3;
        }
    
        public static BufferedImage
        simpleScaleUp(BufferedImage image, int factor)
        {
            int iw = image.getWidth();
            int ih = image.getHeight();
            assert ih != -1;
            assert image.getType() == BufferedImage.TYPE_INT_ARGB;
    
            BufferedImage buffer = new BufferedImage(
                iw * factor, ih * factor,
                BufferedImage.TYPE_INT_ARGB);
            WritableRaster raster = buffer.getRaster();
            for (int y = 0; y < ih; ++y)
            for (int x = 0; x < iw; ++x)
            {
                int argb = image.getRGB(x, y);
                int[] block = new int[factor * factor];
                Arrays.fill(block, argb);
    
                raster.setDataElements(
                    x * factor, y * factor,
                    factor, factor, block);
            }
    
            return buffer;
        }
    
        public static BufferedImage
        areaAveragingScale(BufferedImage image, int w, int h)
        {
            int sw = image.getWidth(null);
            int sh = image.getHeight(null);
            assert sh != -1;
            assert image.getType() == BufferedImage.TYPE_INT_ARGB;
    
            BufferedImage returnee = new BufferedImage(
                w, h, BufferedImage.TYPE_INT_ARGB);
    
            Rectangle wnd = new Rectangle();
            // Window is a particular larger square whose pixels are
            // being aggregated into a single output pixel.
            for (int y = 0; y < h; ++y)
            for (int x = 0; x < w; ++x)
            {
                int sx1 = x * sw/w;
                int sy1 = y * sh/h;
                int sx2 = (x + 1) * sw/w;
                int sy2 = (y + 1) * sh/h;
                wnd.x = sx1;
                wnd.y = sy1;
                wnd.width = sx2 == sx1 ? 1 : sx2 - sx1;
                wnd.height = sy2 == sy1 ? 1 : sy2 - sy1;
    
                int a = 0, r = 0, g = 0, b = 0, n = 0;
                // n is the number of input pixels aggregated.
                // We're just summing into argb for now, will
                // divide by n after. In integer math we kinda
                // can't risk doing a running average.
                for (int yc = wnd.y; yc < wnd.y + wnd.height; ++yc)
                for (int xc = wnd.x; xc < wnd.x + wnd.width; ++xc)
                {
                    int argb = image.getRGB(xc, yc);
                    a += (argb >> 24) & 0xFF;
                    r += (argb >> 16) & 0xFF;
                    g += (argb >> 8) & 0xFF;
                    b += (argb >> 0) & 0xFF;
                    ++n;
                }
                a /= n; r /= n; g /= n; b /= n;
                int argb = a << 24 | r << 16 | g << 8 | b;
    
                returnee.setRGB(x, y, argb);
            }
    
            return returnee;
        }

        public static BufferedImage
        subimage(Image image, int x, int y, int w, int h)
        {
            BufferedImage returnee = new BufferedImage(
                w, h, BufferedImage.TYPE_INT_ARGB);
            Graphics g = returnee.getGraphics();
            g.drawImage(
                image,
                0, 0, w, h,
                x, y, x + w, y + h, null);
            g.dispose();
            return returnee;
        }

        public static Dimension
        containFit(Dimension returnee, int w, int h, int nw, int nh)
        {
            assert returnee != null;
            if (w == 0 || h == 0) return returnee;
    
            boolean landscape = w > h;
            boolean canHeighten = nh >= (h * nw/w);
            boolean canLengthen = nw >= (w * nh/h);
            boolean snapToWidth =
                (landscape && canHeighten)
                || (!landscape && !canLengthen);
    
            if (snapToWidth)
            {
                returnee.width = nw;
                returnee.height = h * nw/w;
            }
            else
            {
                returnee.height = nh;
                returnee.width = w * nh/h;
            }
    
            return returnee;
        }
    
        public static Dimension
        fillFit(Dimension returnee, int w, int h, int nw, int nh)
        {
            assert returnee != null;
            if (w == 0 || h == 0) return returnee;
    
            if (w > h) {
                returnee.height = nh;
                returnee.width = w * nh/h;
            }
            else {
                returnee.width = nw;
                returnee.height = h * nw/w;
            }
    
            return returnee;
        }
    
        public static Dimension
        containFit(int w, int h, int nw, int nh)
        {
            return containFit(new Dimension(), w, h, nw, nh);
        }
    
        public static Dimension
        fillFit(int w, int h, int nw, int nh)
        {
            return fillFit(new Dimension(), w, h, nw, nh);
        }
        
    }

//   -  -%-  -

    private class
    AWTReadyEvent extends AWTEvent
    implements ActiveEvent {

        public void
        dispatch()
        {
            if (operation.equals("fetch"))
                listener.imageReady(name, fetchedImage);
            if (operation.equals("fetchAWT"))
                listener.imageReady(name, fetchedImage);
            else if (operation.equals("scale"))
                listener.imageReady(name, scaledImage);
        }

//      -=%=-

        AWTReadyEvent()
        {
            super(ImageAPI2.this, ++lastEventId);
            if (lastEventId < AWTEvent.RESERVED_ID_MAX)
                lastEventId = AWTEvent.RESERVED_ID_MAX;
            assert lastEventId > AWTEvent.RESERVED_ID_MAX;
        }

        void
        post()
        {
            Toolkit tk = Toolkit.getDefaultToolkit();
            EventQueue evq = tk.getSystemEventQueue();
            evq.postEvent(this);
            // We're being a 1.1-style event for giggles.
            // We could of course just be a Runnable and
            // go through invokeLater.
        }

    }

//  ---%-@-%---

    private
    ImageAPI2() { }

    public static ImageAPI2
    fetch(URL url, String name, int priority, Listener listener)
    {
        ImageAPI2 returnee = new ImageAPI2();
        returnee.operation =
            URLs.isGIFURL(url) ? "fetchAWT" : "fetch";
        returnee.url = url;
        returnee.name = name;
        returnee.priority = priority;
        returnee.listener = listener;
        returnee.notifyThroughEDT = true;
        return returnee;
    }

    public static ImageAPI2
    fetchNow(URL url, String name, int priority, Listener listener)
    {
        ImageAPI2 returnee = fetch(url, name, priority, listener);
        returnee.notifyThroughEDT = false;
        return returnee;
    }

    public static ImageAPI2
    scale(
        Image unscaledImage, int newWidth, int newHeight,
        String name, int priority, Listener listener)
    {
        ImageAPI2 returnee = new ImageAPI2();
        returnee.operation = "scale";
        returnee.unscaledImage = unscaledImage;
        returnee.newWidth = newWidth;
        returnee.newHeight = newHeight;
        returnee.name = name;
        returnee.priority = priority;
        returnee.notifyThroughEDT = true;
        returnee.listener = listener;
        return returnee;
    }

}
