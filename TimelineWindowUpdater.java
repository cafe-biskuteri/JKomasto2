
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import java.io.StringReader;
import java.io.IOException;
import cafe.biskuteri.hinoki.Tree;
import cafe.biskuteri.hinoki.JsonConverter;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.LineUnavailableException;
import java.awt.Window;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.AWTEvent;
import java.awt.ActiveEvent;
import java.net.URL;

class
TimelineWindowUpdater {

    private JKomasto
    primaire;

    private MastodonApi
    api;

//   -  -%-  -

    private List<TimelineWindow>
    timelineWindows;

    private List<NotificationsWindow>
    notificationWindows;

    private Clip
    notificationSound;

    private Connection
    publicConn,
    userConn;
    //listConn;

//  ---%-@-%---

    public void
    add(TimelineWindow updatee)
    {
        if (!timelineWindows.contains(updatee))
        {
            timelineWindows.add(updatee);

            TimelineType type = updatee.getTimelineType();
            if (type == publicConn.type)
                publicConn.considerRestart();
            if (type == userConn.type)
                userConn.considerRestart();
        }
    }

    public void
    add(NotificationsWindow updatee)
    {
        if (!notificationWindows.contains(updatee))
        {
            notificationWindows.add(updatee);

            userConn.considerRestart();
        }
    }

    public void
    remove(TimelineWindow updatee)
    {
        timelineWindows.remove(updatee);
    }

    public void
    remove(NotificationsWindow updatee)
    {
        notificationWindows.remove(updatee);
    }

    public void
    restart()
    {
        publicConn.restart();
        userConn.restart();
    }

//  ---%-@-%---

    private class
    Connection
    implements
        ServerSideEventsListener,
        Runnable, Thread.UncaughtExceptionHandler {

        private TimelineType
        type;

        private String
        listId;

//       -=-

        private Thread
        thread;

        private StringBuilder
        event, data;

        private int
        lastEventId = AWTEvent.RESERVED_ID_MAX;

//      -=%=-

        public void
        run()
        {
            api.monitorTimeline(type, listId, this);
        }

        public void
        restart()
        {
            if (thread != null) thread.interrupt();

            String name =
                "TimelineWindowUpdater-"
                + type.name().substring(0, 1)
                + type.name().substring(1).toLowerCase();

            thread = new Thread(this);
            thread.setName(name);
            thread.setDaemon(true);

            event = new StringBuilder();
            data = new StringBuilder();

            thread.start();
        }

        public void
        considerRestart()
        {
            boolean needsRestart =
                thread == null
                || thread.getState() == Thread.State.TERMINATED;
            if (needsRestart) restart();
        }

//       -=-

        public void
        lineReceived(String line)
        {
            Thread thread = Thread.currentThread();
            assert !thread.isInterrupted();
            assert thread == Connection.this.thread;

            if (line.isEmpty())
            {
                String event = this.event.toString();
                String data = this.data.toString();
                this.event.delete(0, event.length());
                this.data.delete(0, data.length());
                handleSSE(event, data);
            }
            else if (line.startsWith("data: "))
            {
                data.append(line.substring("data: ".length()));
            }
            else if (line.startsWith("event: "))
            {
                event.append(line.substring("event: ".length()));
            }
            else if (line.startsWith(":")) { }

            /*
            * Note that I ignore https://html.spec.whatwg.org
            * /multipage/server-sent-events.html#dispatchMessage.
            * That is because I am not a browser.
            */
        }

        private void
        handleSSE(String event, String data)
        {
            assert !data.isEmpty();
            if (event.isEmpty()) return;

            boolean newPost = event.equals("update");
            boolean newNotif = event.equals("notification");
            if (!(newPost || newNotif)) return;
            
            if (newNotif)
            {
                assert this == userConn;

                Notification notif = null; try
                {
                    StringReader r = new StringReader(data);
                    Tree<String> entity = JsonConverter.convert(r);
                    r.close();
                    notif = new Notification(entity);
                }
                catch (IOException eIo)
                {
                    assert false;
                    // Honestly I wouldn't assume the SSE stream
                    // will always return a complete JSON document.
                    // But, quick solution that isn't silently ignore..
                }
                catch (MalformedEntityException eMe)
                {
                    assert false;
                    // This should down the thread.
                }

                boolean hide = false;
                String postId = null;
                if (notif.post != null)
                {
                    postId = notif.post.id;
                    if (notif.post.parentId != null)
                        hide = api.isHidden(notif.post.parentId);
                }

                if (hide)
                {
                    api.hidePost(postId);
                }
                else
                {
                    notificationSound.setFramePosition(0);
                    notificationSound.start();
                }
            }

            List<Window> updatees = new ArrayList<>();
            if (newPost)
            for (TimelineWindow w: timelineWindows)
            {
                if (type != w.getTimelineType()) continue;
                updatees.add(w);
            }
            if (newNotif)
            for (NotificationsWindow w: notificationWindows)
            {
                if (type != TimelineType.HOME) break;
                updatees.add(w);
            }

            new Update(this, ++lastEventId, updatees).post();
            if (lastEventId < AWTEvent.RESERVED_ID_MAX)
                lastEventId = AWTEvent.RESERVED_ID_MAX;
            assert lastEventId >= AWTEvent.RESERVED_ID_MAX;
        }

        public void
        uncaughtException(Thread t, Throwable e)
        {
            System.err.print("[JKomasto] Uncaught exception from ");
            System.err.print("timeline monitoring thread.. ");
            System.err.print("Printing for information, ");
            System.err.print("the program is fine.\n");
            e.printStackTrace();
            assert t == this.thread;
            if (thread.isInterrupted()) restart();
        }

        public void
        connectionFailed(IOException eIo)
        {
            System.err.print("[JKomasto] IOException from ");
            System.err.print("timeline monitoring thread.. ");
            System.err.print("Printing for information, ");
            System.err.print("the program is fine.\n");
            eIo.printStackTrace();
            // We should probably restart - but not immediately?
        }

        public void
        requestFailed(int httpCode, Tree<String> json)
        {
            System.err.print("[JKomasto] HTTP error from ");
            System.err.print("timeline monitoring thread.. ");
            System.err.print("Printing for information, ");
            System.err.print("the program is fine.\n");
            System.err.println(json.get("error").value);
            System.err.println("(HTTP code: " + httpCode + ")");
            // Should we try to restart? I can't imagine any
            // recoverable error here though.
        }

    }

    private class
    Update extends AWTEvent
    implements ActiveEvent {

        private List<Window>
        updatees;

//      ---%-@-%---

        public void
        dispatch()
        {
            for (Window w: updatees)
            {
                if (w instanceof NotificationsWindow)
                    ((NotificationsWindow)w).refresh();
                else if (w instanceof TimelineWindow)
                    ((TimelineWindow)w).refresh();
                else
                    assert false;
            }
        }

//      ---%-@-%---

        public
        Update(Object source, int eventId, List<Window> updatees)
        {
            super(source, eventId);
            this.updatees = updatees;
        }

        void
        post()
        {
            Toolkit tk = Toolkit.getDefaultToolkit();
            EventQueue evq = tk.getSystemEventQueue();
            evq.postEvent(this);
        }

    }

//  ---%-@-%---

    TimelineWindowUpdater(JKomasto primaire)
    {
        this.primaire = primaire;
        this.api = primaire.getMastodonApi();

        timelineWindows = new CopyOnWriteArrayList<>();
        notificationWindows = new CopyOnWriteArrayList<>();

        publicConn = new Connection();
        publicConn.type = TimelineType.FEDERATED;
        publicConn.listId = null;
        
        userConn = new Connection();
        userConn.type = TimelineType.HOME;
        userConn.listId = null;
        
        /*
        listConn = new Connection();
        listConn.type = TimelineType.LIST;
        listConn.listId = null;
        Oops... We need a connection for every list that is opened.
        I'm not willing to do that, so we'll disable list updating
        support for now.
        */

        loadNotificationSound();
    }

    void
    loadNotificationSound()
    {
        URL url = getClass().getResource("guitar-12.wav");
        if (url == null) {
            assert false;
        }
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(url));
            notificationSound = clip;
        }
        catch (LineUnavailableException eLu) {
            assert false;
        }
        catch (UnsupportedAudioFileException eUa) {
            assert false;
        }
        catch (IOException eIo) {
            assert false;
        }
        catch (IllegalArgumentException eIa) {
            assert false;
        }
    }

}