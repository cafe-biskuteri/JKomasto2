
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import javax.swing.Box;
import java.awt.Dimension;

interface
Scalable {

    public int
    getScale();

    public void
    setScale(int scale);

}

class
MouseWheelScaler
implements MouseWheelListener {

    private int
    increment, minimum;

//  ---%-@-%---

    public void
    mouseWheelMoved(MouseWheelEvent eWm)
    {
        if (!eWm.isControlDown()) return;
        if (eWm.isShiftDown()) return;
        if (eWm.isAltDown()) return;
        if (eWm.isMetaDown()) return;
        assert eWm.getSource() instanceof Scalable;
        adjustScale(
            (Scalable)eWm.getSource(),
            -eWm.getWheelRotation() * increment,
            minimum
        );
    }

//   -  -%-  -

    public static void
    adjustScale(Scalable s, int adjustment, int minimum)
    {
        int newScale = s.getScale() + adjustment;
        if (newScale < minimum) return;
        s.setScale(newScale);
    }

//  ---%-@-%---

    public
    MouseWheelScaler(int increment, int minimum)
    {
        this.increment = increment;
        this.minimum = minimum;
    }

}

class
AdjustableStrut extends Box.Filler {

    public void
    setSizing(int width, int height)
    {
        changeShape(
            new Dimension(0, 0),
            new Dimension(
                width == 0 ? 1 : width,
                height == 0 ? 1 : height
            ),
            new Dimension(
                width == 0 ? Integer.MAX_VALUE : width,
                height == 0 ? Integer.MAX_VALUE : height
            )
        );
    }

//  ---%-@-%---

    public
    AdjustableStrut()
    {
        super(
            new Dimension(0, 0),
            new Dimension(0, 0),
            new Dimension(0, 0)
        );
        setOpaque(false);
    }

    public
    AdjustableStrut(int width, int height)
    {
        this();
        setSizing(width, height);
    }

}
