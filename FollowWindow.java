
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import java.awt.Image;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import cafe.biskuteri.hinoki.Tree;


class
FollowWindow extends JFrame
implements ImageFetcher.Listener {

    private JKomasto
    primaire;

    private ImageFetcher
    fetcher;

    private MastodonApi
    api;

//   -  -%-  -

    private FollowComponent
    display;

    private List<Account>
    accounts;

    private Image[]
    images;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    showFor(String accountNumId, boolean following)
    {
        display.setCursor(WAIT);

        RequestOutcome r = 
            api.getFollows(accountNumId, following, 300);
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                FollowWindow.this,
                "Failed to fetch follows.."
                + "\n" + r.exception.getMessage()
            );
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                FollowWindow.this,
                "Failed to fetch follows.."
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP code: " + r.httpCode + ")"
            );
        }

        List<Account> accounts = new ArrayList<>();
        for (Tree<String> entity: r.entity)
            accounts.add(new Account(entity));

        if (this.accounts != null)
        {
            for (Account a: this.accounts)
                if (a.avatarResolver == this)
                    a.avatarResolver = null;
        }

        this.accounts = accounts;
        images = new Image[accounts.size()];
        for (int o = 0; o < images.length; ++o)
        {
            Account account = accounts.get(o);
            if (account.avatar != null)
            {
                images[o] = account.avatar;
            }
            else if (account.avatarResolver == null)
            {
                account.avatarResolver = this;
                fetcher.fetch(
                    account.avatarUrl, "" + 0, -1, this);
            }
        }
        display.setImages(images);

        display.setCursor(null);
    }

//   -  -%-  -

    public void
    imageReady(String name, Image image)
    {
        int o; try {
            o = Integer.parseInt(name);
        }
        catch (NumberFormatException eNf) {
            assert false;
            return;
        }
        assert o > 0 && o < accounts.size();

        Account a = accounts.get(o);
        a.avatar = images[o] = image;
        a.avatarResolver = null;

        display.setImages(images);
        // Will this lag us like mad?
    }

    public void
    avatarHovered(int offset)
    {
        if (offset == -1) display.setLabel("");
        else display.setLabel(accounts.get(offset).id);
    }

    public void
    avatarClicked(int offset)
    {
        assert offset != -1;

        ProfileWindow w = new ProfileWindow(primaire);
        w.use(accounts.get(offset));
        w.setLocation(getLocation());
        w.setSize(getSize());
        w.setVisible(true);
    }

//  ---%-@-%---

    FollowWindow(JKomasto primaire)
    {
        this.primaire = primaire;
        api = primaire.getMastodonApi();
        fetcher = primaire.getImageFetcher();

        display = new FollowComponent(this);
        add(display);
    }

}

class
FollowComponent extends JPanel
implements MouseListener, MouseMotionListener {

    private FollowWindow
    primaire;

//   -  -%-  -

    private AvatarGridComponent
    grid;

    private JLabel
    label;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    setImages(Image[] images)
    {
        grid.setImages(images);
        grid.repaint();
    }

    public void
    setLabel(String string)
    {
        label.setText(string);
        label.repaint();
    }

//   -  -%-  -

    public void
    mouseMoved(MouseEvent eM)
    {
        primaire.avatarHovered(grid.getHoveredOffset());
    }

    public void
    mouseClicked(MouseEvent eM)
    {
        setCursor(WAIT);

        int o = grid.getHoveredOffset();
        if (o == -1) return;
        primaire.avatarClicked(o);

        setCursor(null);
    }

    public void
    mouseDragged(MouseEvent eM) { }

    public void
    mousePressed(MouseEvent eM) { }

    public void
    mouseReleased(MouseEvent eM) { }

    public void
    mouseEntered(MouseEvent eM) { }

    public void
    mouseExited(MouseEvent eM) { }

//  ---%-@-%---

    FollowComponent(FollowWindow primaire)
    {
        this.primaire = primaire;

        grid = new AvatarGridComponent();
        grid.addMouseMotionListener(this);
        grid.addMouseListener(this);
        // (悪) No guarantees it will be in order!

        label = new JLabel("", JLabel.CENTER);
        label.setFont(label.getFont().deriveFont(20f));

        setLayout(new BorderLayout());
        add(grid, BorderLayout.CENTER);
        add(label, BorderLayout.SOUTH);

        setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    }

}

class
AvatarGridComponent extends JComponent
implements MouseMotionListener {

    private Image[]
    images = new Image[0];

    private int
    hoveredOffset = -1;

//   -  -%-  -

    private static final int
    AVATAR_WIDTH = 24,
    BORDER_WIDTH = 2,
    CELL_WIDTH = AVATAR_WIDTH + (2 * BORDER_WIDTH),
    CELL_MARGIN = 2,
    CELL_ADVANCE = CELL_MARGIN + CELL_WIDTH;

//  ---%-@-%---

    public void
    setImages(Image[] images)
    {
        if (images == null) images = new Image[0];
        this.images = images;
    }

    public int
    getHoveredOffset() { return hoveredOffset; }

//   -  -%-  -

    protected void
    paintComponent(Graphics g)
    {
        if (images.length == 0) return;

        int cellsPerRow, gridWidth, leftMargin, topMargin;
        cellsPerRow = (getWidth() + CELL_MARGIN) / CELL_ADVANCE;
        if (cellsPerRow > images.length) cellsPerRow = images.length;
        if (cellsPerRow >= 2) --cellsPerRow;
        gridWidth = -CELL_MARGIN + (cellsPerRow * CELL_ADVANCE);
        hoveredOffset = getHoveredOffset(
            cellsPerRow,
            leftMargin = (getWidth() - gridWidth) / 2,
            topMargin = CELL_WIDTH
        );

        g.translate(leftMargin, topMargin);
        for (int o = 0; o <= images.length; ++o)
        {
            boolean finishing = o == images.length;

            if (finishing)
            {
                if (hoveredOffset == -1) break;
                o = hoveredOffset;
            }

            int gridX = 1 + (o % cellsPerRow);
            int gridY = 1 + (o / cellsPerRow);
            Image image = images[o];
            Color shadowColour = new Color(0, 0, 0, 25);
            Color borderColour = Color.BLACK;

            int x = (gridX - 1) * CELL_ADVANCE;
            int y = (gridY - 1) * CELL_ADVANCE;
            int w = CELL_WIDTH;
            int iw = AVATAR_WIDTH;
            if (o == hoveredOffset)
            {
                iw = AVATAR_WIDTH * 2;
                w = CELL_WIDTH * 2;
                x -= CELL_WIDTH * 1/2;
                y -= CELL_WIDTH * 1/2;
            }
            int ix = x + ((w - iw) / 2);
            int iy = y + ((w - iw) / 2);

            g.setColor(shadowColour);
            g.fillRect(x + (w * 1/10), y + (w * 1/10), w, w);
            g.setColor(borderColour);
            g.fillRect(x, y, w, w);
            g.drawImage(image, ix, iy, iw, iw, this);

            if (finishing) break;
        }
    }

    private int
    getHoveredOffset(int cellsPerRow, int leftMargin, int topMargin)
    {
        int hoveredGridX = -1, hoveredGridY = -1;
        Point mousePosition = getMousePosition();
        if (mousePosition == null) return -1;

        int x = mousePosition.x - leftMargin;
        int y = mousePosition.y - topMargin;
        hoveredGridX = 1 + x / CELL_ADVANCE;
        hoveredGridY = 1 + y / CELL_ADVANCE;

        int o = (hoveredGridY - 1) * cellsPerRow;
        o += hoveredGridX - 1;

        if (o < 0) return -1;
        if (o >= images.length) return -1;
        return o;
    }

    public void
    mouseMoved(MouseEvent eM) { repaint(); }

    public void
    mouseDragged(MouseEvent eM) { }

//  ---%-@-%---

    AvatarGridComponent()
    {
        addMouseMotionListener(this);
    }

}
