
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.awt.Graphics;
import java.awt.FontMetrics;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.Cursor;
import java.awt.image.BufferedImage;
import java.awt.font.FontRenderContext;
import java.awt.font.TextAttribute;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleState;
import javax.accessibility.AccessibleStateSet;
import java.io.UnsupportedEncodingException;
import cafe.biskuteri.hinoki.Tree;

class
RichTextPane4 extends JComponent
implements
    MouseListener, MouseMotionListener, KeyListener,
    FocusListener, Accessible, ImageFetcher.Listener,
    ComponentListener {

    private ImageFetcher
    fetcher;

    private List<Font>
    fonts;

    private int
    maxDisplayLines;

    private Tree<String>
    html;

    private Map<String, Image>
    emojis;


//  -=- --- -%- --- -=-


    private List<Element>
    layout;

    private List<Integer>
    lineBoundaries;

    private int
    endingX;

    private int
    startingLine;

    private int
    selStartX, selEndX;

    private boolean
    lineBoundariesInvalid;

//   -      -%-     -

    private Map<String, Image>
    scaledEmojis;

    private AccessibleContext
    access;

    private List<LinkListener>
    linkListeners;


//  -=- --- -%- --- -=-


    private static final Image
    UNRESOLVED_EMOJI = new BufferedImage(
        1, 1, BufferedImage.TYPE_INT_ARGB);



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    public void
    setText(Tree<String> html)
    {
        if (html == null)
        {
            html = new Tree<String>("tag", null);
            html.add(new Tree<String>("html", null));
            html.add(new Tree<String>("children", null));
        }

        this.html = html;
        this.layout.clear();
        endingX = 0;
        startingLine = 1;

        insertIntoLayout(html);
        recalculateLineBoundaries();
        repaint();
        updateAccessibleDescription();
    }

    public void
    setFonts(List<Font> fonts)
    {
        assert !fonts.isEmpty();
        assert !fonts.contains(null);

        Map<TextAttribute, Object> M = new HashMap<>();
        M.put(TextAttribute.LIGATURES, TextAttribute.LIGATURES_ON);
        M.put(TextAttribute.KERNING, TextAttribute.KERNING_ON);

        this.fonts.clear();
        for (Font font: fonts)
        {
            assert font.getSize() > 0;
            this.fonts.add(font.deriveFont(M));
        }

        rescaleEmojis();
        setText(html);
        // The layout has been invalidated,
        // just redo the whole thing again.
    }

    public void
    setFont(Font font)
    {
        List<Font> fonts = new ArrayList<>();
        fonts.add(font);
        setFonts(fonts);
    }

    public void
    setEmojis(Map<String, Image> emojis)
    {
        this.emojis = emojis;
        scaledEmojis.clear();
        rescaleEmojis();
    }

    public void
    setMaxDisplayLines(int maxDisplayLines)
    {
        this.maxDisplayLines = maxDisplayLines;
    }

    public void
    nextPage()
    {
        assert !fonts.isEmpty();
        FontMetrics bfm = getFontMetrics(fonts.get(0));
        int lineHeight = bfm.getAscent() + bfm.getDescent();
        int heightInLines = getHeight() / lineHeight;
        int max = Math.max(lineBoundaries.size() - heightInLines, 1);

        startingLine = Math.min(startingLine + heightInLines, max);
        repaint();
        updateAccessibleDescription();
    }

    public void
    previousPage()
    {
        assert !fonts.isEmpty();
        FontMetrics bfm = getFontMetrics(fonts.get(0));
        int lineHeight = bfm.getAscent() + bfm.getDescent();
        int heightInLines = getHeight() / lineHeight;

        startingLine = Math.max(startingLine - heightInLines, 1);
        repaint();
        updateAccessibleDescription();
    }

//   -      -%-     -

    public void
    recalculateLineBoundaries()
    {
        assert layout != null;

        lineBoundariesInvalid = true;
        if (!isValid()) return;
        int w = getWidth();
        if (w == 0) return;

        selStartX = selEndX = 0;

        int x = 0; for (Element element: layout) {
            element.x = x;
            x += element.xAdvance;
        }
        assert x == endingX;

        lineBoundaries.clear();
        lineBoundaries.add(0);
        Queue<Element> queue = new LinkedList<>(layout);
        while (!queue.isEmpty())
        {
            Element element = queue.peek();
            assertValidElement(element);
            int prevBoundary =
                lineBoundaries.get(lineBoundaries.size() - 1);

            BoundaryPlacementEvaluation eval =
                boundaryPlacementEvaluation(
                    element, prevBoundary, w);
            if (eval.putLater)
            {
                queue.poll();
            }
            else if (eval.putAfter)
            {
                int nextBoundary = element.x + element.xAdvance;
                assert prevBoundary <= nextBoundary;

                lineBoundaries.add(nextBoundary);
                queue.poll();
            }
            else if (eval.putBefore)
            {
                int nextBoundary = element.x;
                assert prevBoundary < nextBoundary;

                lineBoundaries.add(element.x);
                queue.poll();
            }
            else if (eval.putWithin)
            {
                int nextBoundary = nextLineBoundaryWithin(
                    element, prevBoundary, w);
                if (nextBoundary != -1)
                {
                    assert prevBoundary < nextBoundary;
                    lineBoundaries.add(nextBoundary);
                }
                else queue.poll();
            }
            else assert false;
        }
        lineBoundaries.add(0x0FFFFFFF);
        // Be below max value so we can add without overflowing.
        assert lineBoundaries.size() >= 2;

        assert !fonts.isEmpty();
        FontMetrics bfm = getFontMetrics(fonts.get(0));
        int lineHeight = bfm.getAscent() + bfm.getDescent();
        int lines = 1 + lineBoundaries.size() - 2;
        if (lines > maxDisplayLines) lines = maxDisplayLines;
        setPreferredSize(new Dimension(w, lines * lineHeight));

        lineBoundariesInvalid = false;
    }

//     -      -%-      -

    private void
    refreshEmojiNodes()
    {
        assert !fonts.isEmpty();

        for (Element element: layout)
        {
            if (element.image == null) continue;

            if (element.image == UNRESOLVED_EMOJI)
            {
                Image emoji = scaledEmojis.get(element.string);
                if (emoji != null) element.image = emoji;
            }
        }
    }


//  -=- --- -%- --- -=-


    private void
    insertIntoLayout(Tree<String> node)
    {
        assert emojis != null;
        assert scaledEmojis != null;
        assert fonts != null && !fonts.isEmpty();

        if (node.key.equals("text") || node.key.equals("space"))
        {
            List<FontString> strings;
            strings = splitByFont(node.value, fonts);

            for (FontString fontString: strings)
            {
                assert fonts.contains(fontString.font);

                boolean ws1 = true;
                for (char c: fontString.string.toCharArray())
                    if (!(ws1 &= Character.isWhitespace(c)))
                        break;

                Element addee = new Element();
                addee.string = fontString.string;
                addee.link = null;
                addee.font = fontString.font;
                addee.isWhitespace = ws1;
                fillElementXAdvances(addee);
                addee.isWhitespace |= addee.xAdvance == 0;

                assertValidTextElement(addee);

                addee.x = endingX;
                endingX += addee.xAdvance;
                layout.add(addee);
            }
        }
        else if (node.key.equals("emoji"))
        {
            Image emoji = scaledEmojis.get(node.value);
            if (emoji == null) emoji = UNRESOLVED_EMOJI;

            Element addee = new Element();
            addee.string = node.value;
            addee.link = null;
            addee.image = emoji;
            addee.isWhitespace = false;
            addee.font = fonts.get(0);
            fillElementXAdvances(addee);

            addee.x = endingX;
            endingX += addee.xAdvance;
            layout.add(addee);
        }
        else if (node.key.equals("tag"))
        {
            String tagName = node.get(0).key;
            Tree<String> children = node.get("children");
            assert children != null;

            if (tagName.equals("br"))
            {
                Element addee = new Element();
                addee.string = "\n";
                addee.link = null;
                addee.image = null;
                addee.isWhitespace = true;
                addee.font = fonts.get(0);
                fillElementXAdvances(addee);

                addee.x = endingX;
                endingX += addee.xAdvance;
                layout.add(addee);
            }
            else if (tagName.equals("p"))
            {
                Element a1 = new Element();
                Element a2 = new Element();
                a2.string = a1.string = "\n";
                a2.link = a1.link = null;
                a2.image = a1.image = null;
                a2.isWhitespace = a1.isWhitespace = true;
                a2.font = a1.font = fonts.get(0);
                fillElementXAdvances(a1);
                fillElementXAdvances(a2);

                a1.x = endingX;
                endingX += a1.xAdvance;
                a2.x = endingX;
                endingX += a2.xAdvance;
                layout.add(a1);
                layout.add(a2);
            }

            Tree<String> href = node.get("href");
            Tree<String> classes = node.get("class");
            for (Tree<String> child: children)
            {
                int startOffset = layout.size();
                insertIntoLayout(child);
                int endOffset = layout.size() - 1;

                if (href != null)
                {
                    int linkClass = 0;
                    if (classes != null)
                    if (classes.value != null)
                    for (String c: classes.value.split(" "))
                    {
                        if (c.equals("u-url")) linkClass = 1;
                        // Encoding hashtag as 0 for now.
                    }

                    for (int o = startOffset; o <= endOffset; ++o)
                    {
                        Element added = layout.get(o);
                        added.link = href.value;
                        added.linkClass = linkClass;
                    }
                }
            }
        }
        else
        {
            MastodonApi.debugPrint(node);
            assert false;
        }
    }

    private void
    fillElementXAdvances(Element element)
    {
        assert element != null;
        assert element.string != null;
        assert element.font != null;
        FontMetrics fm = getFontMetrics(element.font);

        element.xAdvances = new int[element.string.length()];
        for (int o = 0; o < element.string.length(); ++o)
        {
            String substring = element.string.substring(0, o + 1);
            element.xAdvances[o] = fm.stringWidth(substring);
        }

        if (element.image != null)
        {
            int ow = element.image.getWidth(null);
            int oh = element.image.getHeight(null);
            int nh = fm.getAscent() + fm.getDescent();
            int nw = ow * nh/oh;
            // See #setEmojis.
            if (ow == -1) nw = 0;
            element.xAdvance = nw;
        }
        else if (element.string.equals("\n"))
        {
            element.xAdvance = 0;
            element.xAdvances = new int[] { 0 };
        }
        else
        {
            element.xAdvance = fm.stringWidth(element.string);

            int lo = element.xAdvances.length - 1;
            assert element.xAdvance == element.xAdvances[lo];
        }
    }

    private void
    assertValidElement(Element element)
    {
        assert element.string != null;
        assert element.font != null;
    }

    private void
    assertValidTextElement(Element element)
    {
        assertValidElement(element);
        assert element.image == null;
        assert element.xAdvances.length == element.string.length();

        assert fonts.contains(element.font);
        if (!element.isWhitespace)
            assert element.xAdvance > 0;
    }

//   -      -%-      -

    private List<FontString>
    splitByFont(String string, List<Font> fonts)
    {
        assert string != null && !string.isEmpty();
        assert fonts != null && !fonts.isEmpty();

        List<FontMetrics> fontsMetrics = new ArrayList<>();
        for (Font font: fonts)
            fontsMetrics.add(getFontMetrics(font));

        Font[] topFont = new Font[string.length()];
        determineTopFonts(string, topFont, fontsMetrics);
        
        List<FontString> strings;
        strings = collectByFont(string, topFont);
        strings = resolvePlaceholderFonts(strings);

        return strings;
    }

    private void
    determineTopFonts(
        String string, Font[] topFont,
        List<FontMetrics> fontsMetrics)
    {
        // Determine the appropriate font for each character.

        assert topFont.length == string.length();

        for (int o = 0; o < topFont.length; ++o)
        {
            char c = string.charAt(o);

            if (Character.isWhitespace(c))
            {
                topFont[o] = new Font("null" + o, Font.PLAIN, 0);
            }

            for (int fo = 0; fo < fonts.size(); ++fo)
            {
                Font font = fonts.get(fo);
                if (!font.canDisplay(c)) continue;

                FontMetrics fm = fontsMetrics.get(fo);
                if (fm.charWidth(c) == 0) continue;

                topFont[o] = font;
                break;
            }
        }
    }

    private List<FontString>
    collectByFont(String string, Font[] topFont)
    {
        List<FontString> returnee = new ArrayList<>();
        for (int so = 0, eo = so; so < string.length(); so = eo)
        {
            while (topFont[eo] == topFont[so])
                if (++eo == string.length()) break;

            FontString addee = new FontString();
            addee.font = topFont[so];
            addee.string = string.substring(so, eo);
            returnee.add(addee);
        }
        return returnee;
    }

    private List<FontString>
    resolvePlaceholderFonts(List<FontString> strings)
    {
        List<FontString> returnee = new ArrayList<>();

        String prefix = "";
        for (FontString fontString: strings)
        {
            if (fontString.font == null)
            {
                prefix += fontString.string;
                continue;
            }

            if (fontString.font.getName().startsWith("null"))
                fontString.font = fonts.get(0);

            fontString.string = prefix + fontString.string;

            returnee.add(fontString);
        }
        if (!prefix.isEmpty())
        {
            FontString addee = new FontString();
            addee.string = prefix;
            addee.font = fonts.get(0);
            returnee.add(addee);
        }

        return returnee;
    }

//   -      -%-     -

    private int
    nextLineBoundaryWithin(
        Element element, int prevBoundary, int lineWidth)
    {
        assertValidTextElement(element);
        if (lineWidth < 0) lineWidth = 0;
        // Yes, somehow this happens. I think it's my own layout
        // managers not being careful about this.

        int rx = Math.max(prevBoundary - element.x, 0);
        int nrx = (prevBoundary + lineWidth) - element.x;
        int o = findExceed(element.xAdvances, rx) - 1;
        int no = findExceed(element.xAdvances, nrx) - 1;
        if (o == -1) o = 0;
        if (no == -1) no = 0;
        if (no == element.xAdvances.length)
        {
            assert (prevBoundary + lineWidth) >=
                (element.x + element.xAdvance);
            return -1;
        }
        if (no > o)
        {
            int nx = element.x + element.xAdvances[no];
            assert prevBoundary < nx;
            return nx;
        }
        
        // Minimum advance principle: always advance one printing
        // character's worth on each line, regardless of if that
        // character fits in a line; always advance to at least
        // mno (the offset of the next printing character).
        int mno = findExceed(element.xAdvances, rx);
        if (mno == element.xAdvances.length)
        {
            return -1;
        }
        else
        {
            int mnx = element.x + element.xAdvances[mno];
            assert prevBoundary < mnx;
            return mnx;
        }
    }

//   -      -%-     -

    private void
    rescaleEmojis()
    {
        if (emojis == null) return;
        if (lineBoundariesInvalid) return;

        assert !fonts.isEmpty();
        FontMetrics bfm = getFontMetrics(fonts.get(0));
        int nh = bfm.getAscent() + bfm.getDescent();
        assert nh != 0;

        for (Map.Entry<String, Image> entry: emojis.entrySet())
        {
            Image image = entry.getValue();
            int ow = image.getWidth(null);
            int oh = image.getHeight(null);

            fetcher.scale(
                image, ow * nh/oh, nh,
                entry.getKey(), 0, this);
        }
    }

    public void
    imageReady(String name, Image image)
    {
        scaledEmojis.put(name, image);
        refreshEmojiNodes();
        repaint();
    }

//   -      -%-     -

    protected void
    paintComponent(Graphics g)
    {
        assert layout != null;
        assert !fonts.isEmpty();
        if (lineBoundariesInvalid) return;

        if ("true".equals(System.getProperty("swing.aatext")))
            ((java.awt.Graphics2D)g).setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        Color regColour = g.getColor();
        Color linkColour = Color.GREEN.darker().darker();
        Color selColour = new Color(232, 216, 216, 144);

        FontMetrics bfm = g.getFontMetrics(fonts.get(0));
        int lineHeight = bfm.getAscent() + bfm.getDescent();
        int yOffset = 0;
        if (maxDisplayLines != Integer.MAX_VALUE)
        {
            int maximumHeight =
                lineHeight * Math.min(
                    maxDisplayLines, lineBoundaries.size() - 1);
            yOffset = (getHeight() - maximumHeight) / 2;
        }

        Queue<Element> queue = new LinkedList<>(layout);
        final int ITERATION_LIMIT = 512;
        int iteration = 0;
        int line = startingLine;
        while (!queue.isEmpty())
        {
            assert ++iteration < ITERATION_LIMIT;

            assert line > 0;
            assert line < lineBoundaries.size();
            // sic. First and last line boundaries are sentinels
            // for the following two lines.
            int prevBoundary = lineBoundaries.get(line - 1);
            int nextBoundary = lineBoundaries.get(line);

            Element element = queue.peek();
            assertValidElement(element);

            int rl = line - startingLine;
            int y = (rl * lineHeight) + bfm.getAscent();
            if (rl >= maxDisplayLines) return;
            if (y > getHeight() + bfm.getAscent()) return;
            /*
            * We don't need to render past our bounds, the
            * rest of the queue is further out of bounds, so
            * cease rendering.
            */
            y += yOffset;

            if (element.x + element.xAdvance <= prevBoundary)
            {
                // We are past this element. Skip.
                queue.poll();
                --iteration;
                continue;
            }
            if (element.x >= nextBoundary)
            {
                // We need to seek up to this element. Keep it at
                // front of queue and keep incrementing line.
                ++line;
                continue;
            }

            int rx = Math.max(element.x - prevBoundary, 0);
            if (inSelection(element))
            {
                g.setColor(selColour);  
                if (element.image == null && !element.isWhitespace)
                {
                    int[] advs = element.xAdvances;
                    int last = advs.length - 1;

                    int so; for (so = 0; so < advs.length; ++so)
                    {
                        int adv = advs[so];
                        if (element.x + adv >= selStartX) break;
                    }

                    int eo; for (eo = last; eo >= 0; --eo)
                    {
                        int adv = advs[eo];
                        if (element.x + adv < selEndX) break;
                    }

                    int isx = so == 0 ? 0 : advs[so - 1];
                    int iex = advs[Math.min(eo + 1, last)];
                    int sx = element.x + isx;
                    int ex = element.x + iex;

                    int rsx = Math.max(sx - prevBoundary, 0);
                    int rex = Math.max(ex - prevBoundary, 0);

                    g.fillRect(
                        rsx, y - bfm.getAscent(),
                        rex - rsx, lineHeight
                    );
                }
                else
                {
                    g.fillRect(
                        rx, y - bfm.getAscent(),
                        element.xAdvance, lineHeight
                    );
                }
                g.setColor(regColour);
            }

            if (element.isWhitespace)
            {
                queue.poll();
                continue;
            }

            if (element.image != null)
            {
                assert element.font == fonts.get(0);
                if (element.image != UNRESOLVED_EMOJI)
                {
                    g.drawImage(
                        element.image,
                        rx, y - bfm.getAscent(),
                        element.xAdvance, lineHeight,
                        this
                    );
                }
                queue.poll();
                continue;
            }

            assert !element.isWhitespace;
            assert element.x < nextBoundary;

            String drawee =
                substring(element, prevBoundary, nextBoundary);

            g.setFont(element.font);

            if (element.link != null) g.setColor(linkColour);
            g.drawString(drawee, rx, y);
            if (element.link != null) g.setColor(regColour);

            if (element.string.endsWith(drawee)) queue.poll();
            else ++line;
        }
    }

    private String
    substring(Element element, int prevBoundary, int nextBoundary)
    {
        // Given two (line) boundaries, finds the substring
        // of the given text element that is within them.

        assert prevBoundary < nextBoundary;
        assert element.x < nextBoundary;
        assert element.x + element.xAdvance > prevBoundary;
        assertValidTextElement(element);

        int rx1 = prevBoundary - element.x;
        int rx2 = nextBoundary - element.x;

        int ro1 = findExceed(element.xAdvances, rx1);
        int ro2 = findExceed(element.xAdvances, rx2);

        if (0 > rx1) assert ro1 == 0;
        assert ro2 > ro1;
        assert ro2 > 0;

        return element.string.substring(ro1, ro2);
    }

//   -      -%-     -

    public void
    addLinkListener(LinkListener l) { linkListeners.add(l); }

    public void
    removeLinkListener(LinkListener l) { linkListeners.remove(l); }

//   -      -%-     -

    public void
    mousePressed(MouseEvent eM)
    {
        if (lineBoundariesInvalid) return;

        selEndX = 0;
        selStartX = determineX(eM.getX(), eM.getY());
        requestFocusInWindow();
    }

    public void
    mouseDragged(MouseEvent eM)
    {
        if (lineBoundariesInvalid) return;

        int x = determineX(eM.getX(), eM.getY());

        if (x < selStartX)
        {
            if (selEndX == -1) selEndX = selStartX;
            selStartX = x;
        }
        else selEndX = x;
        repaint();
    }

    public void
    mouseClicked(MouseEvent eM)
    {
        if (lineBoundariesInvalid) return;

        selStartX = selEndX = 0;

        int x = determineX(eM.getX(), eM.getY());
        Element hovered = null;
        for (Element element: layout)
            if (element.x <= x) hovered = element;

        if (hovered != null && hovered.link != null)
            for (LinkListener l: linkListeners)
                l.linkClicked(hovered.link, hovered.linkClass);

        repaint();
    }

    private int
    determineX(int x, int y)
    {
        assert !lineBoundariesInvalid;
        assert lineBoundaries.size() >= 2;
        assert !fonts.isEmpty();

        if (x > getWidth()) x = getWidth();
        if (x < 0) x = 0;

        FontMetrics bfm = getFontMetrics(fonts.get(0));
        // We can cache this from #getFonts if we'd like.
        int lineHeight = bfm.getAscent() + bfm.getDescent();
        int iy = bfm.getAscent();
        int ry; for (ry = iy; ry < getHeight(); ry += lineHeight)
            if (ry >= y) break;

        int rl = (ry - iy) / lineHeight;
        int line = Math.min(
            startingLine + rl,
            lineBoundaries.size() - 1);
        assert line >= 1;
        int prevBoundary = lineBoundaries.get(line - 1);

        return prevBoundary + x;
    }

//   -      -%-     -

    public void
    mouseMoved(MouseEvent eM)
    {
        if (lineBoundariesInvalid) return;

        int x = determineX(eM.getX(), eM.getY());
        Element hovered = null;
        for (Element element: layout)
            if (element.x <= x) hovered = element;

        String tooltip = null;
        Cursor cursor = null;

        if (hovered != null)
        {
            if (hovered.image != null)
            {
                tooltip = hovered.string;
            }
            if (hovered.link != null)
            {
                tooltip = hovered.link;

                for (LinkListener linkListener: linkListeners)
                {
                    Cursor suggestion =
                        linkListener.getCursorForLink(
                            hovered.link,
                            hovered.linkClass);
                    if (suggestion != null) cursor = suggestion;
                }
            }
        }

        setToolTipText(tooltip);
        setCursor(cursor);
    }

//   -      -%-     -

    public void
    keyPressed(KeyEvent eK)
    {
        if (lineBoundariesInvalid) return;

        if (eK.isControlDown())
        {
            if (eK.getKeyCode() == KeyEvent.VK_A) selectAll();
            if (eK.getKeyCode() == KeyEvent.VK_C) copy();
        }
    }

    private void
    selectAll()
    {
        if (lineBoundariesInvalid) return;

        selStartX = 0;
        selEndX = lineBoundaries.get(lineBoundaries.size() - 1);
        repaint();
    }

    private void
    copy()
    {
        assert !lineBoundariesInvalid;
        if (selEndX == 0) return;

        StringBuilder b = new StringBuilder();
        String lastLink = null;
        for (Element element: layout) if (inSelection(element))
        {
            boolean linkedText =
                element.link != null
                && !element.link.contains(element.string);
            // Expensive, but it's for deemed correct behaviour..

            if (element.image != null)
            {
                b.append(element.string);
            }
            else if (linkedText)
            {
                if (element.link != lastLink)
                {
                    b.append(element.link);
                    lastLink = element.link;
                }
            }
            else if (element.isWhitespace)
            {
                b.append(element.string);
            }
            else
            {
                int[] advs = element.xAdvances;
                int last = advs.length - 1;

                int so; for (so = 0; so < advs.length; ++so)
                {
                    int adv = advs[so];
                    if (element.x + adv >= selStartX) break;
                 }

                int eo; for (eo = last; eo >= 0; --eo)
                {
                    int adv = advs[eo];
                    if (element.x + adv < selEndX) break;
                }

                eo = Math.min(eo + 2, advs.length);
                b.append(element.string.substring(so, eo));
            }
        }

        ClipboardApi.serve(b.toString());
    }

    private boolean
    inSelection(Element element)
    {
        if (selEndX == 0) return false;
        return
            element.x + element.xAdvance >= selStartX
            && element.x <= selEndX;
    }

//   -      -%-     -

    public AccessibleContext
    getAccessibleContext() { return access; }

    private void
    updateAccessibleDescription()
    {
        if (lineBoundariesInvalid) return;

        assert !fonts.isEmpty();
        FontMetrics bfm = getFontMetrics(fonts.get(0));
        int lineHeight = bfm.getAscent() + bfm.getDescent();
        assert lineHeight != 0;
        int heightInLines = getHeight() / lineHeight;

        int line = startingLine;
        int end = startingLine + heightInLines;
        end = Math.min(end, lineBoundaries.size() - 1);
        if (line > end)
        {
            access.setAccessibleDescription("");
            return;
        }

        StringBuilder b = new StringBuilder();
        for (Element element: layout)
        {
            // Unlike paintComponent, I won't check for graphical
            // visibility, like a multi-line string being cropped.
            // I'll just print any element that's in range.

            if (element.x > lineBoundaries.get(line)) ++line;
            if (line >= end) break;
            if (element.x < lineBoundaries.get(line - 1)) continue;

            b.append(element.string);
        }
        try {
            access.setAccessibleDescription(
                new String(
                    b.toString().getBytes("US-ASCII"),
                    "US-ASCII"
                )
            );
            b.delete(0, b.length());
        }
        catch (UnsupportedEncodingException eUe) {
            assert false;
        }
    }

//   -      -%-     -

    public void
    focusGained(FocusEvent eF)
    {
        access.firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            null, AccessibleState.FOCUSED
        );
    }

    public void
    focusLost(FocusEvent eF)
    {
        access.firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            AccessibleState.FOCUSED, null);
    }

//   -      -%-     -

    public void
    componentResized(ComponentEvent eC)
    {
        recalculateLineBoundaries();
        repaint();
        updateAccessibleDescription();
    }

//   -      -%-     -

    public void
    componentShown(ComponentEvent eC) { }

    public void
    componentHidden(ComponentEvent eC) { }

    public void
    componentMoved(ComponentEvent eC) { }

    public void
    keyReleased(KeyEvent eK) { }

    public void
    keyTyped(KeyEvent eK) { }

    public void
    mouseReleased(MouseEvent eM) { }

    public void
    mouseEntered(MouseEvent eM) { }

    public void
    mouseExited(MouseEvent eM) { }


//  -=- --- -%- --- -=-


    private static BoundaryPlacementEvaluation
    boundaryPlacementEvaluation(
        Element element, int prevBoundary, int lineWidth)
    {
        BoundaryPlacementEvaluation r =
            new BoundaryPlacementEvaluation();

        /*
        * There are four possible line boundary placements
        * with regards to this element:
        * (1) Emplace a line boundary before this element.
        * (2) Emplace a line boundary right after this element.
        * (3) Ignore this element, emplace one farther down.
        * (4) Emplace one in the middle of this element.
        */

        int rx = Math.max(element.x - prevBoundary, 0);
        boolean
            newline = element.string.equals("\n"),
            image = element.image != null,
            singleChar = element.string.length() == 1,
            longerThanLine = element.xAdvance > lineWidth,
            fits = rx + element.xAdvance <= lineWidth;

        if (element.isWhitespace)
        {
            r.putAfter = newline && element.x != 0;
            r.putLater = !r.putAfter;
        }
        else
        {
            r.putLater =
                fits || (longerThanLine && singleChar);
            r.putWithin =
                !image && longerThanLine && !singleChar;
            r.putBefore =
                !r.putLater && !r.putWithin;
        }

        return r;
    }

//   -      -%-     -

    private static int
    findExceed(int[] values, int max)
    {
        /*
        * Given an array of ascending numbers and a max,
        * returns the offset of the element in the array
        * that crossed the max.
        *
        * Desired behaviour:
        * values[o] > max: o
        * values[all] <= max: values.length
        */

        for (int o = 0; o < values.length; ++o)
            if (values[o] > max)
                return o;

        return values.length;
    }

//   -      -%-     -

    private static void
    debug(String text)
    {
        if (true) return;
        if (false) System.err.print(System.currentTimeMillis() + " ");
        System.err.println(text);
    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    private class
    Access extends AccessibleJComponent {

        public AccessibleStateSet
        getAccessibleStateSet()
        {
            AccessibleStateSet s = super.getAccessibleStateSet();
            s.add(AccessibleState.FOCUSABLE);
            return s;
        }

    }


//  -=- --- -%- --- -=-


    public static interface
    LinkListener {

        void
        linkClicked(String link, int linkClass);

        Cursor
        getCursorForLink(String link, int linkClass);

    }


//  -=- --- -%- --- -=-


    private static class
    Element {

        String
        string;

        String
        link;

        Image
        image;

        boolean
        isWhitespace;

        Font
        font;

        int
        xAdvance;

        int[]
        xAdvances;

        int
        x;

        int
        linkClass;

    }

    private static class
    FontString {

        String
        string;

        Font
        font;

    }

    private static class
    BoundaryPlacementEvaluation {

        boolean
        putBefore,
        putAfter,
        putLater,
        putFirstWithin,
        putNextWithin,
        putWithin;

    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    public static void
    main(String... args)
    {
        ImageFetcher fetcher = new ImageFetcher("imagefetcher");

        RichTextPane4 instance = new RichTextPane4(fetcher);
        List<Font> fonts = new ArrayList<>();
        fonts.add(new Font("VL PGothic", Font.PLAIN, 48));
        fonts.add(new Font("Firefox Emoji", Font.PLAIN, 48));
        instance.setFonts(fonts);

        javax.swing.JFrame mainframe = new javax.swing.JFrame();
        mainframe.add(instance);
        mainframe.setSize(768, 768);
        mainframe.setLocationByPlatform(true);
        mainframe.setVisible(true);
        instance.addKeyListener(new KeyAdapter() {
            public void
            keyPressed(KeyEvent eK)
            {
                int code = eK.getKeyCode();
                int w = mainframe.getWidth();
                int h = mainframe.getHeight();
                if (code == KeyEvent.VK_LEFT) w -= 48;
                if (code == KeyEvent.VK_RIGHT) w += 48;
                mainframe.setSize(w, h);
            }
        });
        instance.setFocusable(true);

        Tree<String> text1 = new Tree<>("text", null);
        Tree<String> emoji = new Tree<>("emoji", null);
        Tree<String> text2 = new Tree<>("text", null);
        text1.value =
            "First, an English sentence."
            + " 次は和文の文字例です。和文の例はホワイトスペースむなし"
            + "のでワードスピリットはずです。"
            + " Then a single ";
        emoji.value = ":kettle:";
        text2.value =
            " emoji.\n-\nThen two new lines."
            + " And a ☕ coffee emoji."
            + " ThenareallylongEnglishwordtoforceittogobeyond"
            + "thewindowwidth.Linkswillbelikethistoo.";
        Tree<String> html = new Tree<>("tag", null);
        html.add(new Tree<String>("p", null));
        Tree<String> children = new Tree<>("children", null);
        children.add(text1);
        children.add(emoji);
        children.add(text2);
        html.add(children);
        instance.setText(html);

        Map<String, Image> emojis = new HashMap<>();
        emojis.put(":kettle:", ImageApi.local("kettle"));
        ImageApi.awaitImage(emojis.get(":kettle:"));
        instance.setEmojis(emojis);
    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    RichTextPane4(ImageFetcher fetcher)
    {
        this.fetcher = fetcher;

        access = new Access();

        fonts = new ArrayList<>();
        fonts.add(new Font("Dialog", Font.PLAIN, 14));

        emojis = new HashMap<>();
        scaledEmojis = new HashMap<>();
        linkListeners = new ArrayList<>();

        maxDisplayLines = Integer.MAX_VALUE;

        layout = new ArrayList<>();
        lineBoundaries = new ArrayList<>();
        lineBoundariesInvalid = true;
        setText(null);

        setOpaque(false);

        setFocusable(true);
        addMouseListener(this);
        addMouseMotionListener(this);
        addKeyListener(this);
        addFocusListener(this);
        addComponentListener(this);
    }

}
