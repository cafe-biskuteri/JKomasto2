
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.MediaTracker;
import java.awt.EventQueue;
import java.awt.AWTEvent;
import java.awt.ActiveEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.io.File;
import java.io.IOException;


class
ImageApi {

    private static MediaTracker
    tracker;

//  ---%-@-%---

    public static Image
    local(String name)
    {
        String path = "graphics/" + name + ".png";
        URL url = ImageApi.class.getResource(path);
        if (url == null) try
        {
            // Look for file in working directory as fallback.
            File file = new File(path);
            if (file.exists()) url = file.toURI().toURL();
        }
        catch (MalformedURLException eMu) { }
        if (url == null) return null;

        Toolkit tk = Toolkit.getDefaultToolkit();
        return tk.createImage(url);
    }

    public static Image
    remote(String urlr)
    throws IOException
    {
        URL url = null;
        try { url = new URL(urlr); }
        catch (MalformedURLException eMu) { }
        if (url == null) return null;

        /*
        if (urlr.endsWith(".gif"))
        {
            Toolkit tk = Toolkit.getDefaultToolkit();
            return tk.createImage(url);
            // Note that this doesn't block. I won't try
            // to, consumers of GIFs should render them
            // directly with image observing or know to
            // await it themselves.
        }
        else
        {
            return ImageIO.read(url);
            // Will block, perhaps past our timeout tolerance,
            // but hopefully reliable. Anyone calling remote
            // should be doing so via ImageFetcher, anyways.
        }
        */
        Toolkit tk = Toolkit.getDefaultToolkit();
        return tk.createImage(url); 
    }

    public static List<String>
    locals(String prefix, String ext)
    {
        List<String> returnee = new ArrayList<>();

        File dir = new File("./graphics");
        /*
        * Notably won't work if we're inside a JAR file.
        * We fallback by returning an empty list but,
        * maybe we should obsolete this method and
        * have the caller use their own manifest?
        * At the same time, we rely on directories when
        * we want it to be customisable by the user, so
        * maybe supporting directories is better.
        */
        File[] files = dir.listFiles();
        if (files != null) for (File file: files)
        {
            String name = file.getName();
            if (!name.startsWith(prefix)) continue;
            if (!name.endsWith("." + ext)) continue;
            int eo = name.length() - ("." + ext).length();
            name = name.substring(0, eo);
            returnee.add(name);
        }

        return returnee;
    }

    public static void
    awaitImage(Image image)
    {
        assert image != null;

        try {
            awaitImage(image, -1);
        }
        catch (TimeoutException eT) {
            assert false;
        }
        catch (IOException eIo) {
            return;
        }
        catch (InterruptedException eIt) {
            return;
        }
    }

    public static void
    awaitImageAwhile(Image image)
    throws TimeoutException, IOException, InterruptedException
    {
        assert image != null;

        awaitImage(image, 250);
    }

    public static Image
    scaleImage(Image image, int w, int h)
    throws IOException, InterruptedException
    {
        assert w > 0 && h > 0;

        awaitImage(image);
        return areaAveragingScale(image, w, h);
    }

    public static Dimension
    containFit(Dimension returnee, int w, int h, int nw, int nh)
    {
        assert returnee != null;
        if (w == 0 || h == 0) return returnee;

        boolean landscape = w > h;
        boolean canHeighten = nh >= (h * nw/w);
        boolean canLengthen = nw >= (w * nh/h);
        boolean snapToWidth =
            (landscape && canHeighten)
            || (!landscape && !canLengthen);

        if (snapToWidth)
        {
            returnee.width = nw;
            returnee.height = h * nw/w;
        }
        else
        {
            returnee.height = nh;
            returnee.width = w * nh/h;
        }

        return returnee;
    }

    public static Dimension
    containFit(int w, int h, int nw, int nh)
    {
        return containFit(new Dimension(), w, h, nw, nh);
    }

    public static Dimension
    fillFit(Dimension returnee, int w, int h, int nw, int nh)
    {
        assert returnee != null;
        if (w == 0 || h == 0) return returnee;

        if (w > h) {
            returnee.height = nh;
            returnee.width = w * nh/h;
        }
        else {
            returnee.width = nw;
            returnee.height = h * nw/w;
        }

        return returnee;
    }

    public static Dimension
    fillFit(int w, int h, int nw, int nh)
    {
        return fillFit(new Dimension(), w, h, nw, nh);
    }

    public static String
    debugString(Image image, Image scaledImage)
    {
        return
            defaultString(image)
            + ", " + defaultString(scaledImage);
    }

//   -  -%-  -

    private static String
    defaultString(Object o)
    {
        if (o == null) return "null";
        return
            o.getClass().getName() + "#"
            + Integer.toHexString(o.hashCode());
    }

    private static void
    awaitImage(Image image, int duration)
    throws TimeoutException, IOException, InterruptedException
    {
        int id = 16 + (int)(Thread.currentThread().getId() >> 36);

        tracker.addImage(image, id);
        try
        {
            long start = System.currentTimeMillis();
            if (duration == -1) tracker.waitForID(id);
            else tracker.waitForID(id, duration);
            long end = System.currentTimeMillis();
            if (duration != -1)
                assert (end - start) <= duration * 14/10;
            /*
            * This can trigger false if the process was stopped
            * for a while then resumed.
            */

            if (tracker.isErrorID(id))
                throw new IOException("Error in loading image");

            if (image.getHeight(null) == -1)
            {
                assert duration != -1;
                // I don't expect the blocking waitForID to return
                // an image that didn't error but is not loaded.
                throw new TimeoutException(
                    "Timed out in awaiting image");
            }
        }
        finally
        {
            tracker.removeImage(image, id);
            // The documentation was a bit confusing about this
            // but yes, you have to pass image specifically.
        }
    }

    private static Image
    areaAveragingScale(Image image, int w, int h)
    {
        BufferedImage s1 = toBufferedImage(image);

        int neededScaleUp = Math.max(
            (w * 3) / s1.getWidth(),
            (h * 3) / s1.getHeight());
        if (neededScaleUp < 1) neededScaleUp = 1;
        BufferedImage s2 = simpleScaleUp(s1, neededScaleUp);
        s1.flush();

        BufferedImage s3 = areaAveragingScale(s2, w, h);
        s2.flush();

        return s3;
    }

    private static BufferedImage
    toBufferedImage(Image image)
    {
        int iw = image.getWidth(null);
        int ih = image.getHeight(null);
        assert ih != -1;

        BufferedImage returnee = new BufferedImage(
            iw, ih, BufferedImage.TYPE_INT_ARGB);
        Graphics g = returnee.getGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();

        return returnee;
    }

    private static BufferedImage
    simpleScaleUp(BufferedImage image, int factor)
    {
        int iw = image.getWidth();
        int ih = image.getHeight();
        assert ih != -1;
        assert image.getType() == BufferedImage.TYPE_INT_ARGB;

        BufferedImage buffer = new BufferedImage(
            iw * factor, ih * factor,
            BufferedImage.TYPE_INT_ARGB);
        WritableRaster raster = buffer.getRaster();
        for (int y = 0; y < ih; ++y)
        for (int x = 0; x < iw; ++x)
        {
            int argb = image.getRGB(x, y);
            int[] block = new int[factor * factor];
            Arrays.fill(block, argb);

            raster.setDataElements(
                x * factor, y * factor,
                factor, factor, block);
        }

        return buffer;
    }

    private static BufferedImage
    areaAveragingScale(BufferedImage image, int w, int h)
    {
        int sw = image.getWidth(null);
        int sh = image.getHeight(null);
        assert sh != -1;
        assert image.getType() == BufferedImage.TYPE_INT_ARGB;

        BufferedImage returnee = new BufferedImage(
            w, h, BufferedImage.TYPE_INT_ARGB);

        Rectangle wnd = new Rectangle();
        for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x)
        {
            int sx1 = x * sw/w;
            int sy1 = y * sh/h;
            int sx2 = (x + 1) * sw/w;
            int sy2 = (y + 1) * sh/h;
            //window(wnd, sx, sy, sw, sh, factor);
            wnd.x = sx1;
            wnd.y = sy1;
            wnd.width = sx2 == sx1 ? 1 : sx2 - sx1;
            wnd.height = sy2 == sy1 ? 1 : sy2 - sy1;

            int a = 0, r = 0, g = 0, b = 0, n = 0;
            for (int yc = wnd.y; yc < wnd.y + wnd.height; ++yc)
            for (int xc = wnd.x; xc < wnd.x + wnd.width; ++xc)
            {
                int argb = image.getRGB(xc, yc);
                a += (argb >> 24) & 0xFF;
                r += (argb >> 16) & 0xFF;
                g += (argb >> 8) & 0xFF;
                b += (argb >> 0) & 0xFF;
                ++n;
            }
            a /= n; r /= n; g /= n; b /= n;
            int argb = a << 24 | r << 16 | g << 8 | b;

            returnee.setRGB(x, y, argb);
        }

        return returnee;
    }

    private static void
    window(Rectangle rect, int x, int y, int w, int h, int factor)
    {
        rect.x = (x >= factor) ? (x - factor) : 0;
        rect.y = (y >= factor) ? (y - factor) : 0;
        rect.width = (x + factor < w) ? factor * 2: (w - rect.x);
        rect.height = (y + factor < h) ? factor * 2: (h - rect.y);
    }

//  ---%-@-%---

    static {
        Component dummyComponent = new Container();
        tracker = new MediaTracker(dummyComponent);
        // ImageIcon does this, so.
    }

    private
    ImageApi() { }

}


class
ImageFetcher
implements ThreadFactory {

    //private File
    //downloads;

    private PriorityBlockingQueue<Request>
    pending;

    private ThreadPoolExecutor
    pool;

//     -  -%-  -

    private int
    lastThreadId = 0;

    private int
    lastEventId = AWTEvent.RESERVED_ID_MAX;

//  ---%-@-%---

    public void
    fetch(
        String urlr,
        String name, int priority, Listener listener)
    {
        assert urlr != null;
        assert listener != null;

        Request request = new Request();
        request.urlr = urlr;
        request.type = 1;
        request.retries = 0;
        request.uuid = generateUUID();
        request.name = name;
        request.priority = priority;
        request.listener = listener;
        pending.add(request);

        respawnThreads();
    }

    public void
    scale(
        Image original, int width, int height,
        String name, int priority, Listener listener)
    {
        assert original != null;
        assert width > 0 && height > 0;

        Request request = new Request();
        request.original = original;
        request.scaleWidth = width;
        request.scaleHeight = height;
        request.type = 2;
        request.retries = 0;
        request.uuid = generateUUID();
        request.name = name;
        request.priority = priority;
        request.listener = listener;
        pending.add(request);

        respawnThreads();
    }

    public void
    halt()
    {
        pool.shutdown();
    }

//   -  -%-  -

    public Thread
    newThread(Runnable runnable)
    {
        Thread returnee = new Thread(runnable);
        returnee.setDaemon(true);

        String suffix = "" + ++lastThreadId;
        returnee.setName("ImageFetcher-" + suffix);

        return returnee;
    }

    private void
    respawnThreads()
    {
        int n = pool.getMaximumPoolSize() - pool.getPoolSize();
        while (n-- > 0) pool.execute(new RequestHandler());
    }

    private void
    completeRequest(Request request)
    throws TimeoutException, IOException, InterruptedException
    {
        if (request.type == 1)
        {
            if (request.original == null)
                request.original = ImageApi.remote(request.urlr);
            assert request.original != null;

            ImageApi.awaitImageAwhile(request.original);
            assert request.original.getHeight(null) != -1;
            request.retries = 0;
            new ReadyEvent(request).post();
        }
        else if (request.type == 2)
        {
            if (request.scaled == null)
                request.scaled = ImageApi.scaleImage(
                    request.original,
                    request.scaleWidth, request.scaleHeight);
            assert request.scaled != null;

            ImageApi.awaitImageAwhile(request.scaled);
            assert request.scaled.getHeight(null) != -1;
            request.retries = 0;
            new ReadyEvent(request).post();
        }
        else assert false;
    }

    private String
    generateUUID()
    {
        return
            "" + this.hashCode()
            + getClass().hashCode()
            + System.currentTimeMillis()
            + (int)(Short.MAX_VALUE * Math.random());
        /*
        * This should be unlikely enough to ever collide,
        * especially with sequential processing and
        * deletion after finish, giving a temporal window.
        * util.UUID is a modern convenient alternative.
        */
    }

    private Image
    toBufferedImage(Image scaled)
    {
        assert scaled.getHeight(null) != -1;

        BufferedImage returnee = new BufferedImage(
            scaled.getWidth(null), scaled.getHeight(null),
            BufferedImage.TYPE_INT_ARGB
        );
        
        try
        {
            Graphics g = returnee.getGraphics();
            g.drawImage(scaled, 0, 0, null);
            g.dispose();
        }
        catch (ClassCastException eCc)
        {
            boolean ignore = false;

            StackTraceElement[] trace = eCc.getStackTrace();
            String usualCulprit = "convertToRGB";
            for (int o = 0; o < 5 && !ignore; ++o)
                if (trace[o].getMethodName().equals(usualCulprit))
                    ignore = true;

            if (!ignore) throw eCc;
        }

        return returnee;
    }

//  ---%-@-%---

    public static class
    Request
    implements Comparable<Request> {

        String
        urlr;

        Image
        original;

        int
        scaleWidth, scaleHeight;

        Image
        scaled;

        int
        type, retries;

        String
        uuid, name;

        int
        priority;

        Listener
        listener;

        public int
        compareTo(Request request)
        {
            if (this.priority == request.priority)
                return request.retries - this.retries;
            else
                return request.priority - this.priority;
            /*
            * The one with a lower comparator value will be
            * prioritised higher.
            */
        }

        public String
        toString()
        {
            StringBuilder b = new StringBuilder();
            b.append(getClass().getName() + "[");
            b.append("urlr=" + urlr);
            b.append(",original=" + original);
            b.append(",scaleWidth=" + scaleWidth);
            b.append(",scaleHeight=" + scaleHeight);
            b.append(",scaled=" + scaled);
            b.append(",retries=" + retries);
            b.append(",uuid=" + uuid);
            b.append(",name=" + name);
            b.append(",priority=" + priority);
            b.append(",listener=");
            b.append(listener.getClass().getName() + "#");
            b.append(Integer.toHexString(listener.hashCode()));
            b.append("]");
            return b.toString();
        }

    }

    public interface
    Listener {

        void
        imageReady(String name, Image image);

    }

//   -  -%-  -

    private class
    RequestHandler
    implements Runnable {

        public void
        run()
        {
            Request request = null;
            while (true) try
            {
                request = pending.take();
                completeRequest(request);
            }
            catch (TimeoutException eT)
            {
                retryIfUnder(request, 15);
                continue;
            }
            catch (MalformedURLException eMu)
            {
                eMu.printStackTrace();
                assert false;
                continue;
            }
            catch (IOException eIo)
            {
                retryIfUnder(request, 5);
                continue;
            }
            catch (InterruptedException eIt)
            {
                break;
            }
        }

        private void
        retryIfUnder(Request request, int retries)
        {
            if (request.retries < retries)
            {
                ++request.retries;
                pending.add(request);
            }
        }

    }

    private class
    ReadyEvent extends AWTEvent
    implements ActiveEvent {

        Request
        request;

//      -=%=-

        public void
        dispatch()
        {
            Image image = null;
            if (request.scaled != null)
                image = request.scaled;
            else if (request.original != null)
                image = request.original;
            else
                assert false;

            request.listener.imageReady(request.name, image);
        }

//      -=%=-

        ReadyEvent(Request request)
        {
            super(ImageFetcher.this, ++lastEventId);
            if (lastEventId < AWTEvent.RESERVED_ID_MAX)
                lastEventId = AWTEvent.RESERVED_ID_MAX;
            assert lastEventId > AWTEvent.RESERVED_ID_MAX;
            this.request = request;
        }

        void
        post()
        {
            Toolkit tk = Toolkit.getDefaultToolkit();
            EventQueue evq = tk.getSystemEventQueue();
            evq.postEvent(this);
        }

    }

//  ---%-@-%---

    public
    ImageFetcher(File downloads)
    {
        //this.downloads = downloads;
        initCommon();
    }

    public
    ImageFetcher(String name)
    {
        /*
        String tmpPath = System.getProperty("java.io.tmpdir");
        File downloads = new File(tmpPath, name);
        downloads.mkdir();
        downloads.deleteOnExit();
        this.downloads = downloads;
        */
        initCommon();
    }

    private void
    initCommon()
    {
        pending = new PriorityBlockingQueue<>();

        pool = (ThreadPoolExecutor)Executors
            .newFixedThreadPool(4, this);
        respawnThreads();
    }

}
