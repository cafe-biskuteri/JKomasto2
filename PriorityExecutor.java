
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.Collection;
import java.util.ArrayList;

class
PriorityExecutor
implements ThreadFactory {

    private String
    name;

//   -  -%-  -

    private PriorityBlockingQueue<PrioritisedRunnable>
    pending;

    private ThreadPoolExecutor
    executor;

    private int
    lastThreadIndex = 0;

    private Collection<Thread>
    threads;

//  ---%-@-%---

    public void
    execute(PrioritisedRunnable runnable)
    {
        pending.add(runnable);
    }

    public boolean
    idle()
    {
        boolean idle = pending.isEmpty();
        for (Thread thread: threads)
        {
            idle &= thread.getState() == Thread.State.WAITING;
            // This is QueueClearer's idle thread state.
        }
        return idle;
    }

//   -  -%-  -

    public Thread
    newThread(Runnable r)
    {
        Thread returnee = new Thread(r);
        returnee.setDaemon(true);
        returnee.setName(name + "-" + ++lastThreadIndex);
        threads.add(returnee);
        return returnee;
    }

//  ---%-@-%---

    public interface
    PrioritisedRunnable
    extends Runnable, Comparable<PrioritisedRunnable> {

        int
        getPriority();

        default int
        compareTo(PrioritisedRunnable other)
        {
            return other.getPriority() - this.getPriority();
        }

    }

//   -  -%-  -

    private class
    QueueClearer
    implements Runnable {

        public void
        run()
        {
            while (true) try { pending.take().run(); }
            catch (InterruptedException eIt) { break; }
        }

    }

//  ---%-@-%---

    public
    PriorityExecutor(String name)
    {
        this.name = name;

        pending = new PriorityBlockingQueue<>();

        threads = new ArrayList<>(4);

        executor = (ThreadPoolExecutor)Executors
            .newFixedThreadPool(4, this);
        executor.execute(new QueueClearer());
        executor.execute(new QueueClearer());
        executor.execute(new QueueClearer());
        executor.execute(new QueueClearer());
    }

}
