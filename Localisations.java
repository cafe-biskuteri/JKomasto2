
import java.util.Map;
import java.util.HashMap;
import javax.swing.JFrame;

class
EnglishStrings {

    private static Map<String, String>
    strings;

//  ---%-@-%---

    public static String
    get(String key)
    {
        return strings.getOrDefault(key, " - ? - ");
    }

//  ---%-@-%---

    static {
        strings = new HashMap<>();

        strings.put(
            "have instance conn",
            "Have connection to server"
        );
        strings.put("have app cred", "Have app credentials");
        strings.put("have acc token", "Have access token");
        strings.put("have account details", "Have account details");
        strings.put("have lists", "Have custom lists");
        strings.put("have emojis", "Have custom emojis");
        strings.put(
            "window updater ready",
            "Window-updating thread ready");
        strings.put(
            "auto view ready", "Auto post view ready");
        strings.put(
            "auto media ready", "Auto media view ready");
        strings.put(
            "timeline window ready",
            "First timeline window ready");
        strings.put(
            "notifs window ready", "Notifications window ready");

        strings.put("login title", "JKomasto - Login");
        strings.put("enter instance", "Enter an instance URL!");
        strings.put("connect", "Connect");
        strings.put(
            "paste auth code", "Paste the authorisation code!");
        strings.put("login", "Login");
        strings.put(
            "try auto login", "Automatically login if possible");

        strings.put(
            "instance conn fail",
            "Tried to connect to URL, failed..");
        strings.put(
            "app cred fail",
            "Tried to get app credentials, failed.."
        );
        strings.put(
            "acc token fail",
            "Tried to get acc token, failed.."
        );
        strings.put(
            "account details fail",
            "Tried to get account details, failed.."
        );
        strings.put(
            "cache load login fail",
            "We couldn't get login details from the cache.."
        );
        strings.put(
            "cache save login fail",
            "We couldn't save login details into the cache.."
        );
        strings.put(
            "cache load settings fail",
            "We couldn't get settings from the cache.."
        );
        strings.put(
            "cache save settings fail",
            "We couldn't save settings into the cache.."
        );
        strings.put(
            "acc token renewal instr",
            "We will need you to login through a web browser,\n"
            + "and they will give you an authorisation code\n"
            + "that you will paste here. Sorry..!"
        );
        strings.put(
            "acc token renewal title",
            "Authorisation code renewal"
        );
        strings.put(
            "acc token renewal ask copy",
            "\nWe cannot use Desktop.browse(URI) on your\n"
            + "computer.. You'll have to open your web\n"
            + "browser yourself, and copy this URL in."
        );

    }
    static {
        strings.put("timeline fetch fail", "Failed to fetch page..");
        strings.put(
            "timeline fetch next page empty",
            "The instance said there were no posts after this page."
        );
        strings.put("timeline title template", "<> timeline");
        strings.put("list timeline title template", "'<>' list");
        strings.put("account timeline title template", "<>");
        strings.put("boosted by template", "boosted by <>");

        strings.put("federated in title", "Federated");
        strings.put("local in title", "Local");
        strings.put("home in title", "Home");

        strings.put("program menu", "Program");
        strings.put("open home", "Open home timeline");
        strings.put("open federated", "Open federated timeline");
        strings.put("open notifications", "Open notifications");
        strings.put("open own profile", "Open own profile");
        strings.put("open other profile", "Open profile..");
        strings.put("restart updaters", "Restart server listener");
        strings.put("create post", "Create a post");
        strings.put("open auto view", "Open auto post view");
        strings.put("open auto media", "Open auto media view");
        strings.put("open emojis", "Open emoji picker");
        strings.put("character count", "Character count of post body");
        strings.put("open list template", "Open '<>' list");
        strings.put("open settings", "Open settings");

        strings.put("timeline menu", "Timeline");
        strings.put("flip newest", "Flip to newest post");
        strings.put("flip older", "Flip to older page");
        strings.put("flip newer", "Flip to newer page");
        strings.put("use chrono", "Display chronologically");
        strings.put("use categories", "Display categorically");

        strings.put(
            "open profile msg",
            "Whose account do you want to see?\n"
            + "Type an account name with the instance,\n"
            + "or a display name if you can't remember."
        );
        strings.put("open profile title", "Profile search");
        strings.put(
            "open profile lookup fail",
            "Tried to fetch accounts, but it failed.."
        );
        strings.put(
            "open profile no results",
            "There were no results from the query.. ☹️"
        );
        strings.put(
            "open profile pick from results",
            "Maybe one of these?"
        );
        strings.put("open profile results title", "Search results");
        strings.put("timeline hidden post", "Post hidden");
        strings.put("timeline malformed post", "Post malformed");
        strings.put("confirm quit title", "Quit JKomasto?");
        strings.put(
            "confirm quit",
            "You are closing the last timeline window.\n"
            + "Are you sure you'd like to quit JKomasto?\n"
            + "\n"
            + "If you meant to just put the window away,\n"
            + "then you should minimize it."
        );
        strings.put("confirm quit no", "Cancel");
        strings.put("confirm quit yes", "Quit");
    }
    static {
        strings.put("post date template", "<> <><> <><><>");
        strings.put(
            "month names",
            "January,February,March,April,May,June,July,"
            + "August,September,October,November,December"
        );
        strings.put("post time template", "<>:<><>");
    }
    static {
        strings.put("notifications title", "Notifications");
        strings.put("notifications mention", "mentioned");
        strings.put("notifications boost", "boosted");
        strings.put("notifications favourite", "favourited");
        strings.put("notifications follow", "followed");
        strings.put("notifications followreq", "req. follow");
        strings.put("notifications poll", "poll ended");
        strings.put("notifications alert", "posted");
        strings.put(
            "notifications malformed notification",
            "Malformed notification"
        );
        strings.put(
            "notifications hidden notification",
            "Notification hidden"
        );
    }
    static {
        strings.put("emojis title", "Emoji picker");
        strings.put("emojis search", "Search/filter:");
    }
    static {
        strings.put("profile account id", "Account ID");
        strings.put("profile display name", "Display name");
        strings.put("profile followed", "Followed & 'ers");
        strings.put("profile history", "Posts & since");
        strings.put("profile see posts", "Browse posts");
        strings.put("profile see followers", "Browse followers");
        strings.put("profile followed template", "<> & <>");
        strings.put("profile history template", "<> & <><> <><><>");
        strings.put("follower count grades", "A ton,Many,Some");
        strings.put("post count grades", "~,K,00");
    }
    static {
        strings.put("post media title", "Media");
        strings.put(
            "post confirm delete",
            "Are you sure you'd like to delete this post?\n"
        );
        strings.put(
            "post confirm redraft",
            "Are you sure you'd like to delete this post?\n"
            + "You are redrafting, so a composition window\n"
            + "should open with its contents filled."
        );
        strings.put("post confirm delete title", "Confirm delete");
        strings.put("post confirm delete yes", "Delete");
        strings.put("post confirm delete no", "Cancel");
        strings.put("post open replies", "Browse thread");
        strings.put("post open poll", "View poll");
        strings.put("post copy id", "Copy post ID");
        strings.put("post copy link", "Copy post link");
        strings.put("post delete", "Delete post");
        strings.put("post redraft", "Delete and redraft post");
        strings.put("post bookmark", "Bookmark post");
        strings.put("post unbookmark", "Unbookmark post");
    }
    static {
        strings.put("compose title", "Submit a new post");
        strings.put("compose text tab", "Text");
        strings.put("compose media tab", "Media");
        strings.put("english", "English");
        strings.put("japanese", "Japanese");
        strings.put("mandarin", "Mandarin");
        strings.put("malay", "Malay");
        // We need to figure out ISO 643 codes apparently.
        // Doesn't seem trivial.. Thinking of asking util.Locale about it.
        strings.put("compose language", "Language");
        strings.put("compose visibility", "Visibility");
        strings.put("compose public", "Public");
        strings.put("compose unlisted", "Unlisted");
        strings.put("compose followers", "Followers");
        strings.put("compose mentioned", "Mentioned");
        strings.put("undo", "Undo");
        strings.put("redo", "Redo");
        strings.put("compose reply field", "In reply to");
        strings.put("compose cw field", "Content warning");
        strings.put("compose submit", "Submit");
        strings.put(
            "compose attachment preview title",
            "Attachment preview"
        );
        strings.put("compose delete", "Delete");
        strings.put("compose revert", "Revert");
        strings.put("compose add tooltip", "Add file to upload");
        strings.put(
            "compose delete tooltip",
            "Delete selected attachment"
        );
        strings.put(
            "compose revert tooltip",
            "Revert changes to selected attachment"
        );
        strings.put(
            "compose move up tooltip",
            "Move selected attachment up"
        );
        strings.put(
            "compose move down tooltip",
            "Move selected attachment down"
        );
        strings.put(
            "ordinals",
            "first,second,third,fourth,fifth,"
            + "sixth,seventh,eighth,nineth,tenth"
        );
        strings.put(
            "compose attachment tooltip template",
            "<> attachment"
        );
        strings.put("compose description field", "Description");
        strings.put("confirm discard title", "Discard big composition?");
        strings.put(
            "confirm discard",
            "Closing the compose window discards the\n"
            + "composition, so what you wrote will be lost.\n"
            + "Are you sure you'd like to do this?"
        );
        strings.put("confirm discard no", "Cancel");
        strings.put("confirm discard yes", "Discard");
        strings.put("copy", "Copy");
        strings.put("cut", "Cut");
        strings.put("paste", "Paste");
        strings.put(
            "compose no description",
            "One of your attachments has no desc-.\n"
            + "ription. Would you like to submit anyways?"
        );
        strings.put(
            "compose no description title",
            "Attachment without description?"
        );
        strings.put("confirm no description no", "Cancel");
        strings.put("confirm no description yes", "Submit");
    }
    static {
        strings.put("image scale", "Show scaled to window");
        strings.put("image unscale", "Show unscaled");
        strings.put(
            "image none",
            "(There are no images being displayed.)"
        );
        strings.put("image type template", "Media of type <>.");
        strings.put("image show desc", "Show description");
        strings.put("image hide desc", "Show image");
    }
    static {
        strings.put("settings title", "Settings");
        strings.put("settings speech title", "Speech synthesis");
        strings.put("settings image title", "Image");
        strings.put("settings video title", "Video");
        strings.put("settings audio title", "Audio");
        strings.put("settings font names", "Post view font faces");
        strings.put("settings default visibility",
            "Default composition visibility");
    }

    private
    EnglishStrings() { }

}

class
JapaneseStrings {

    private static Map<String, String>
    strings;

//  ---%-@-%---

    public static String
    get(String key)
    {
        return strings.getOrDefault(key, " - ? - ");
    }

//  ---%-@-%---

    static {
        strings = new HashMap<>();

        strings.put("have instance conn","インスタンスに接続した");
        strings.put("have app cred", "アプリ届けが揃える");
        strings.put("have acc token", "アクセス証明が揃える");
        strings.put("have account details", "アカウント情報が揃える");
        strings.put("have lists", "リスト集が揃える");
        strings.put("have emojis", "サーバーの絵文字が揃える");
        strings.put("window updater ready", "更新監視が揃える");
        strings.put("auto view ready", "自動投稿表示が揃える");
        strings.put("auto media ready", "自動添付表示が揃える");
        strings.put(
            "timeline window ready",
            "初回ﾀｲﾑﾗｲﾝｳｨﾝﾄﾞｳが揃える");
        strings.put("notifs window ready", "通知ウィンドウが揃える");

        strings.put("login title", "JKomasto - ログイン");
        strings.put("enter instance", "ｲﾝｽﾀﾝｽﾘﾝｸを頂戴");
        strings.put("connect", "接続 (C)");
        strings.put("paste auth code", "認定コードを貼付…!");
        strings.put("login", "ログイン (L)");
        strings.put("try auto login", "できたら自動ログインします (A)");

        strings.put("instance conn fail", "ｲﾝｽﾀﾝｽに接続に失敗…");
        strings.put("app cred fail", "アプリ届けを受信に失敗…");
        strings.put("acc token fail", "アクセス証明を受信に失敗…");
        strings.put("account details fail", "アカウント情報を受信に失敗…");
        strings.put(
            "cache load login fail",
            "キャッシュからログイン情報の引き出しに失敗…"
        );
        strings.put(
            "cache save login fail",
            "キャッシュにログイン情報の書き込みに失敗…"
        );
        strings.put(
            "cache load settings fail",
            "キャッシュから独自設定を引き出しに失敗…"
        );
        strings.put(
            "cache save settings fail",
            "キャッシュに独自設定を書き込みに失敗…"
        );
        strings.put(
            "acc token renewal instr",
            "ウェブブラウザで特別ログインをして、産出した\n"
            + "認定コードを以下の入力欄に貼り付く必要が"
            + "あります。\nしばらくご迷惑をかけましては"
            + "感謝いたします…");
        strings.put("acc token renewal title", "認定コード更新");
        strings.put(
            "acc token renewal ask copy",
            "\nDesktop.browse(URI)する事ができませんため、\n"
            + "以下に現したリンクを写してウェブブラウザに\n"
            + "自分入力と接続する必要があります。しばらく\n"
            + "ご迷惑をかけましては感謝いたします…"
        );
    }
    static {
        strings.put(
            "timeline fetch fail",
            "タイムラインページを受信に失敗..");
        strings.put(
            "timeline fetch next page empty",
            "サーバーにはこれからの投稿がないようです。");
        strings.put("timeline title template", "<>タイムライン");
        strings.put("list timeline title template", "『<>』組");
        strings.put("account timeline title template", "<>");
        strings.put("boosted by template", "広告, <>より");
        strings.put("federated in title", "連合");
        strings.put("local in title", "インスタンス内");
        strings.put("home in title", "ホーム");

        strings.put("program menu", "プログラム (P)");
        strings.put("open home", "ホームタイムラインを見る");
        strings.put("open federated", "連合タイムラインを見る");
        strings.put("open notifications", "通知を見る");
        strings.put("open own profile", "自分の紹介を見る");
        strings.put("open other profile", "紹介を見る…");
        strings.put("restart updaters", "更新監視を起こし直す");
        strings.put("create post", "投稿を作り出す");
        strings.put("open auto view", "自動投稿表示を見る");
        strings.put("open auto media", "自動添付表示を見る");
        strings.put("open emojis", "絵文字入力を見る");
        strings.put("character count", "内容の文字数");
        strings.put("open list template", "『<>』組を見る");
        strings.put("open settings", "独自設定を見る");

        strings.put("timeline menu", "タイムライン (T)");
        strings.put("flip newest", "最新投稿に見る");
        strings.put("flip older", "前のページに進む");
        strings.put("flip newer", "後のページに進む");
        strings.put("use chrono", "発生順で見る (H)");
        strings.put("use categories", "類合わせで見る (A)");

        strings.put(
            "open profile msg",
            "どちらさんの紹介を見せて頂きますか。\n"
            + "インスタンス加えの宛名、或いは\n"
            + "より短いの呼び名を入力して下さい。");
        strings.put(
            "open profile title",
            "紹介向きのアカウント検索");
        strings.put(
            "open profile lookup fail",
            "アカウントの検索中に失敗..");
        strings.put(
            "open profile no results",
            "検索候補はないようです。 ☹️");
        strings.put(
            "open profile pick from results",
            "お探しのアカウントは以下に表しますか。");
        strings.put(
            "open profile results title",
            "紹介向きのアカウントの検索候補");
        strings.put("timeline hidden post", "隠した投稿");
        strings.put("timeline malformed post", "無効な投稿");
    }
    static {
        strings.put("post date template", "<>日<><>月<><><>年");
        strings.put("month names", "1,2,3,4,5,6,7,8,9,10,11,12");
        strings.put("post time template", "<>:<><>");
    }
    static {
        strings.put("notifications title", "通知");
        strings.put("notifications mention", "呼ばれ");
        strings.put("notifications boost", "広告し");
        strings.put("notifications favourite", "気入");
        strings.put("notifications follow", "予約");
        strings.put("notifications followreq", "申請");
        strings.put("notifications poll", "終了");
        strings.put("notifications alert", "更新");
        strings.put("notifications malformed", "壊れた通知");
        strings.put("notifications hidden", "隠した通知");
    }
    static {
        strings.put("emojis title", "絵文字選択");
        strings.put("emojis search", "条件で検索");
    }
    static {
        strings.put("profile account id", "宛名");
        strings.put("profile display name", "呼び名");
        strings.put("profile followed", "フォロー中/ワー");
        strings.put("profile history", "投稿数、登録月");
        strings.put("profile see posts", "投稿を見る");
        strings.put("profile see followers", "フォロワーを見る");
        strings.put("profile followed template", "<> と <>");
        strings.put("profile history template", "<> と <><>月<><><>年");
        strings.put("follower count grades", "群衆,多数,幾らか");
        strings.put("post count grades", "約,千,百");
    }
    static {
        strings.put("post media title", "添付");
        strings.put(
            "post confirm delete",
            "投稿を本当に消去しますか？\n");
        strings.put(
            "post confirm redraft",
            "投稿を本当に消去しますか？\n"
            + "投稿し直すため、書いたものが\n"
            + "写してあるウィンドウが現れます。\n");
        strings.put("post confirm delete title", "消去を了承");
        strings.put("post confirm delete yes", "消去");
        strings.put("post confirm delete no", " 放って置く");
        strings.put("post open replies", "スレを見る");
        strings.put("post open poll", "アンケートを見る");
        strings.put("post copy id", "投稿識別子を写す");
        strings.put("post copy link", "投稿リンクを写す");
        strings.put("post delete", "投稿を消去する");
        strings.put("post redraft", "投稿を書き直す");
        strings.put("post bookmark", "投稿に栞を付く");
        strings.put("post unbookmark", "投稿の栞を外す");
    }
    static {
        strings.put("compose title", "投稿を作り出す");
        strings.put("compose text tab", "文章");
        strings.put("compose media tab", "添付");
        strings.put("english", "英語");
        strings.put("japanese", "日本語");
        strings.put("mandarin", "華語");
        strings.put("malay", "馬来語");
        strings.put("compose language", "言語");
        strings.put("compose visibility", "口外範囲");
        strings.put("compose public", "公共");
        strings.put("compose unlisted", "フォローワー向け");
        strings.put("compose followers", "フォローワーのみ");
        strings.put("compose mentioned", "相手のみ");
        strings.put("undo", "取り返す");
        strings.put("redo", "やり直す");
        strings.put("compose reply field", "返信の当て");
        strings.put("compose cw field", "注意文");
        strings.put("compose submit", "差す");
        strings.put("compose attachment preview title", "添付下見");
        strings.put("compose delete", "除く");
        strings.put("compose revert", "復元");
        strings.put("compose add tooltip",
            "アップロード向けのファイルを付く");
        strings.put("compose delete tooltip", "選んだ添付を除く");
        strings.put("compose revert tooltip",
            "変更を捨てる、選んだ添付の");
        strings.put(
            "compose move up tooltip",
            "先頭へ一歩にずらす、選んだ添付を");
        strings.put(
            "compose move down tooltip",
            "後ろへ一歩にずらす、選んだ添付を");
        strings.put(
            "ordinals",
            "第一,第二,第三,第四,第五,第六,第七,第八,第九,第十");
        strings.put("compose attachment tooltip template", "添付<>");
        strings.put("compose description field", "叙述");
        strings.put("confirm discard title", "捨てを了承");
        strings.put(
            "confirm discard",
            "ウィンドウを閉たら、作りかけた\n"
            + "投稿が滅び、取り返しはできません。\n"
            + "それでもよろしいですか？");
        strings.put("confirm discard no", "放って置く");
        strings.put("confirm discard yes", "捨てる");
        strings.put("copy", "写す");
        strings.put("cut", "切り取り");
        strings.put("paste", "写しを付く");
        strings.put("compose no description title", "叙述が欠ける");
        strings.put(
            "compose no description",
            "叙述が付いてない添付が残ります。\n"
            + "それでも差しますか？");
        strings.put("confirm no description no", "作りに戻る");
        strings.put("confirm no description yes", "差す");
    }
    static {
        strings.put("image scale", "ウィンドウ幅に拡大");
        strings.put("image unscale", "拡大しない");
        strings.put("image none", "(見える画像がないです)");
        strings.put("image type template", "タイプが<>であります。");
        strings.put("image show desc", "叙述を見る");
        strings.put("image hide desc", "画像を見る");
    }
    static {
        strings.put("settings title", "独自設定");
        strings.put("settings speech title", "読み上げの合成");
        strings.put("settings image title", "画像添付の覧");
        strings.put("settings video title", "動画添付の覧");
        strings.put("settings audio title", "音声添付の覧");
        strings.put("settings font names", "投稿表示の書体名");
        strings.put("settings default visibility", "初期の口外範囲");
    }

    private
    JapaneseStrings() { }

}
