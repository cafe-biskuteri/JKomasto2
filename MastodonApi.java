
import cafe.biskuteri.hinoki.Tree;
import cafe.biskuteri.hinoki.JsonConverter;
import cafe.biskuteri.hinoki.DSVTokeniser;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.SocketTimeoutException;
import java.io.Reader;
import java.io.Writer;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;

class
MastodonApi {

    private String
    instanceUrl;

    private Tree<String>
    appCredentials,
    accessToken,
    accountDetails;

    private List<String>
    hiddenPostIds = new LinkedList<>();

//  -=- --- -%- --- -=-

    private static final String
    SCOPES = "read+write";



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    public String
    getInstanceUrl() { return instanceUrl; }

    public Tree<String>
    getAppCredentials() { return appCredentials; }

    public Tree<String>
    getAccessToken() { return accessToken; }

    public Tree<String>
    getAccountDetails() { return accountDetails; }

    public LoginDetails
    getLogin()
    {
        LoginDetails returnee = new LoginDetails();
        returnee.instanceUrl = instanceUrl;
        returnee.accessToken = accessToken.get("access_token").value;
        returnee.clientId = appCredentials.get("client_id").value;
        returnee.clientSecret
            = appCredentials.get("client_secret").value;
        returnee.accountId = accountDetails.get("acct").value;
        returnee.avatarUrl = accountDetails.get("avatar").value;
        return returnee;
    }

    public void
    setInstanceUrl(String a) { instanceUrl = a; }

    public void
    setAppCredentials(Tree<String> a) { appCredentials = a; }

    public void
    setAccessToken(Tree<String> a) { accessToken = a; }

    public void
    setAccountDetails(Tree<String> a) { accountDetails = a; }

    public void
    setLogin(LoginDetails login)
    {
        assert login.instanceUrl != null;
        assert login.accessToken != null;

        setInstanceUrl(login.instanceUrl);

        Tree<String> a = new Tree<>();
        a.add(new Tree<String>("client_id", login.clientId));
        a.add(new Tree<String>("client_secret", login.clientSecret));
        setAppCredentials(a);

        a = new Tree<>();
        a.add(new Tree<String>("access_token", login.accessToken));
        setAccessToken(a);
    }

//   -      -%-      -

    public RequestOutcome
    getAppCredentialsAnew()
    {
        RequestOutcome returnee = new RequestOutcome();

        try
        {
            URL endpoint = new URL(instanceUrl + "/api/v1/apps");
            HttpURLConnection conn = cast(endpoint.openConnection());
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.connect();

            Writer output = owriter(conn.getOutputStream());
            output.write("client_name=JKomasto");
            output.write("&redirect_uris=urn:ietf:wg:oauth:2.0:oob");
            output.write("&scopes=" + SCOPES);
            output.close();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

    public URI
    getAuthorisationURL()
    {
        assert instanceUrl != null;
        assert appCredentials != null;
        String clientId = appCredentials.get("client_id").value;

        try
        {
            StringBuilder b = new StringBuilder();
            b.append(instanceUrl);
            b.append("/oauth/authorize");
            // Be careful of the spelling!!
            b.append("?response_type=code");
            b.append("&redirect_uri=urn:ietf:wg:oauth:2.0:oob");
            b.append("&scope=" + SCOPES);
            b.append("&client_id=" + clientId);

            return new URI(b.toString());
        }
        catch (URISyntaxException eUs) { assert false; return null; }
    }

    public RequestOutcome
    getAccessTokenAnew(String authorisationCode)
    {
        assert instanceUrl != null;
        assert appCredentials != null;

        String id = appCredentials.get("client_id").value;
        String secret = appCredentials.get("client_secret").value;
        RequestOutcome returnee = new RequestOutcome();

        try
        {
            URL endpoint = new URL(instanceUrl + "/oauth/token");
            HttpURLConnection conn = cast(endpoint.openConnection());
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.connect();

            Writer output = owriter(conn.getOutputStream());
            output.write("client_id=" + id);
            output.write("&client_secret=" + secret);
            output.write("&redirect_uri=urn:ietf:wg:oauth:2.0:oob");
            output.write("&grant_type=authorization_code");
            output.write("&scope=" + SCOPES);
            output.write("&code=" + authorisationCode);
            output.close();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

    public RequestOutcome
    getAccountDetailsAnew()
    {
        assert accessToken != null;

        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        try
        {
            String s = "/api/v1/accounts/verify_credentials";
            URL endpoint = new URL(instanceUrl + s);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s2 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s2);
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    getTimelinePage(
        TimelineType type,
        int count, String maxId, String minId,
        String accountId, String listId)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        assert !(accountId != null && listId != null);

        String url = instanceUrl + "/api/v1";
        if (accountId != null)
        {
            url += "/accounts/" + accountId + "/statuses";
        }
        else if (listId != null)
        {
            url += "/timelines/list/" + listId;
        }
        else switch (type)
        {
            case FEDERATED:
            case LOCAL: url += "/timelines/public"; break;
            case HOME: url += "/timelines/home"; break;
            default: assert false;
        }
        url += "?limit=" + count;
        if (maxId != null) url += "&max_id=" + maxId;
        if (minId != null) url += "&min_id=" + minId;
        // This is a GET endpoint, it rejects receiving
        // query params through the body.

        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn =
                cast(endpoint.openConnection());
            String s2 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s2);
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

    public RequestOutcome
    getLists()
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String url = instanceUrl + "/api/v1/lists";
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s = "Bearer " + token;
            conn.setRequestProperty("Authorization", s);
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    setPostFavourited(String postId, boolean favourited)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String s1 = "/api/v1/statuses/" + postId;
        String s2 = favourited ? "/favourite" : "/unfavourite";
        String url = instanceUrl + s1 + s2;
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn =
                cast(endpoint.openConnection());
            String s3 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s3);
            conn.setRequestMethod("POST");
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

    public RequestOutcome
    setPostBoosted(String postId, boolean boosted)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String s1 = "/api/v1/statuses/" + postId;
        String s2 = boosted ? "/reblog" : "/unreblog";
        String url = instanceUrl + s1 + s2;
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s3 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s3);
            conn.setRequestMethod("POST");
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

    public RequestOutcome
    setPostBookmarked(String postId, boolean bookmarked)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String s1 = "/api/v1/statuses/" + postId;
        String s2 = bookmarked ? "/bookmark" : "/unbookmark";
        String url = instanceUrl + s1 + s2;
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s3 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s3);
            conn.setRequestMethod("POST");
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }


//   -      -%-      -

    public RequestOutcome
    submit(
        String text, PostVisibility visibility,
        String replyTo, String contentWarning,
        String[] mediaIDs, String language)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String visibilityParam = "direct";
        switch (visibility)
        {
            case PUBLIC: visibilityParam = "public"; break;
            case UNLISTED: visibilityParam = "unlisted"; break;
            case FOLLOWERS: visibilityParam = "private"; break;
            case MENTIONED: visibilityParam = "direct"; break;
            default: assert false;
        }

        String url = instanceUrl + "/api/v1/statuses";
        try
        {
            text = encode(text);
            contentWarning = encode(contentWarning);

            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s1 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s1);
            String s2 =
                Integer.toString(text.hashCode())
                + System.currentTimeMillis() / 1000;
            conn.setRequestProperty("Idempotency-Key", s2);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.connect();

            Writer output = owriter(conn.getOutputStream());
            output.write("status=" + text);
            output.write("&visibility=" + visibilityParam);
            if (replyTo != null) {
                output.write("&in_reply_to_id=" + replyTo);
            }
            if (contentWarning != null) {
                output.write("&spoiler_text=" + contentWarning);
            }
            for (String mediaID: mediaIDs) {
                output.write("&media_ids[]=" + mediaID);
            }
            output.write("&language=" + language);

            output.close();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    deletePost(String postId)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String url = instanceUrl + "/api/v1/statuses/" + postId;
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s1 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s1);
            conn.setRequestMethod("DELETE");
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    getSpecificPost(String postId)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String url = instanceUrl + "/api/v1/statuses/" + postId;
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn =
                cast(endpoint.openConnection());
            String s1 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s1);
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

    public RequestOutcome
    getPostContext(String postId)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String s1 = instanceUrl + "/api/v1/statuses/";
        String s2 = postId + "/context";
        String url = s1 + s2;
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s3 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s3);
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    getAccounts(String query)
    {
        assert query != null;

        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String url = instanceUrl + "/api/v1/accounts/search";
        url += "?q=" + encode(query);

        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s1 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s1);
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    getFollows(String numId, boolean following, int limit)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String url = instanceUrl + "/api/v1/accounts/" + numId;
        url += following ? "/following" : "/followers";
        url += "?limit=" + limit;
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s2 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s2);
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    getCustomEmojis()
    {
        RequestOutcome returnee = new RequestOutcome();

        String url = instanceUrl + "/api/v1/custom_emojis";
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    uploadFile(File file, String description)
    {
        assert file != null;
        assert file.canRead();

        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String bct =
            "multipart/form-data; "
            + "boundary=\"JKomastoFileUpload\"";
        String fsb = "--JKomastoFileUpload\r\n";
        String feb = "\r\n--JKomastoFileUpload--\r\n";
        String fcd =
            "Content-Disposition: form-data; "
            + "name=\"file\"; "
            + "filename=\"" + file.getName() + "\"\r\n";
        String fct = "Content-Type: image/png\r\n\r\n";
        // Um, not all uploads are pngs. Not that we have to be
        // accurate, the server shouldn't trust us anyways...
        int contentLength = 0;
        contentLength += fsb.length();
        contentLength += feb.length();
        contentLength += fcd.length();
        contentLength += fct.length();
        contentLength += file.length();
        /*
        * (知) This was an absurdity to debug. Contrary to
        * cURL, Java sets default values for some headers,
        * some of which are restricted, meaning you can't
        * arbitrarily change them. Content-Length is one
        * of them, set to 2^14-1 bytes. I'm pretty sure
        * the file I was uploading was under this, but
        * anyways one of the two parties was stopping me
        * from finishing transferring my form data.
        *
        * They didn't mention this in the Javadocs.
        * I noticed HttpURLConnection#setChunkedStreamingMode
        * and #setFixedLengthStreamingMode by accident.
        * Turns out, the latter is how I do what cURL and
        * Firefox are doing - precalculate the exact size
        * of the body and set the content length to it.
        * Unfortunately, this is not flexible, we have to
        * be exact. Thankfully, my answers pass..
        *
        * On the other side, Mastodon is obtuse as usual.
        * They had code that basically throws a generic 500
        * upon any sort of error from their library[1]. What
        * problem the library had with my requests, I could
        * never know. There is an undocumented requirement
        * that you must put a filename in the content
        * disposition. That one I found by guessing.
        *
        * I solved this with the help of -Djavax.net.debug,
        * which revealed to me how my headers and body
        * differed from cURL and Firefox. If this issue
        * happens again, I advise giving up.
        *
        * [1] app/controllers/api/v1/media_controller.rb
        * #create. 3 March 2022
        */

        String url = instanceUrl + "/api/v1/media/";
        try
        {
            String s1 = "";
            if (description != null)
                s1 = "?description=" + encode(description);

            URL endpoint = new URL(url + s1);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s2 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s2);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setFixedLengthStreamingMode(contentLength);
            conn.setRequestProperty("Content-Type", bct);
            conn.setRequestProperty("Accept", "*/*");
            conn.connect();

            OutputStream ostream = conn.getOutputStream();
            InputStream istream = new FileInputStream(file);

            ostream.write(fsb.getBytes());
            ostream.write(fcd.getBytes());
            ostream.write(fct.getBytes());

            byte[] buffer = new byte[32768];
            int n; while ((n = istream.read(buffer)) != -1)
                ostream.write(buffer, 0, n);

            ostream.write(feb.getBytes());
            istream.close();
            ostream.close();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

    public RequestOutcome
    downloadFile(String urlr, File destination)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        try
        {
            URL url = new URL(urlr);
            HttpURLConnection conn = cast(url.openConnection());
            String s = "Bearer " + token;
            conn.setRequestProperty("Authorization", s);
            conn.connect();

            OutputStream ostream = new FileOutputStream(destination);
            InputStream istream = conn.getInputStream();

            int c; while ((c = istream.read()) != -1)
                ostream.write(c);

            istream.close();
            ostream.close();

            if (conn.getResponseCode() >= 300)
                doStandardJsonReturn(conn, returnee);
            else
                returnee.entity = new Tree<String>();
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
            /*
            * I don't know how to distinguish between
            * IOException from the file vs. the connection,
            * but let's conceptualise the destination file
            * as a remote place being connected to.
            */
        }

        return returnee;
    }

    public RequestOutcome
    editAttachment(String id, String description)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String url = instanceUrl + "/api/v1/media/" + id;
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s1 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s1);
            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.connect();

            Writer output = owriter(conn.getOutputStream());
            output.write("description=" + encode(description));
            output.close();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    voteInPoll(String id, int[] indexes)
    {
        assert id != null;
        assert indexes != null;
        assert indexes.length > 0;

        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        StringBuilder params = new StringBuilder();
        for (int index: indexes)
        {
            if (index != indexes[0]) params.append("&");
            params.append("choices[]=" + (index - 1));
        }

        String url = instanceUrl + "/api/v1/polls/" + id + "/votes";
        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s1 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s1);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.connect();

            Writer output = owriter(conn.getOutputStream());
            output.write(params.toString());
            // No need to encode, it's likely valid UTF-8.
            output.close();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

//   -      -%-      -

    public RequestOutcome
    getNotifications(int count, String maxId, String minId)
    {
        String token = accessToken.get("access_token").value;
        RequestOutcome returnee = new RequestOutcome();

        String url = instanceUrl + "/api/v1/notifications";
        url += "?limit=" + count;
        if (maxId != null) url += "&max_id=" + maxId;
        if (minId != null) url += "&min_id=" + minId;

        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn = cast(endpoint.openConnection());
            String s1 = "Bearer " + token;
            conn.setRequestProperty("Authorization", s1);
            conn.connect();

            doStandardJsonReturn(conn, returnee);
        }
        catch (IOException eIo)
        {
            returnee.exception = eIo;
        }

        return returnee;
    }

    public void
    monitorTimeline(
        TimelineType type, String listId,
        ServerSideEventsListener handler)
    {
        String token = accessToken.get("access_token").value;

        String url = instanceUrl + "/api/v1/streaming";
        switch (type)
        {
            case FEDERATED: url += "/public"; break;
            case LOCAL: url += "/public/local"; break;
            case HOME: url += "/user"; break;
            case LIST: url += "/list?list=" + listId; break;
            default: assert false;
        }

        try
        {
            URL endpoint = new URL(url);
            HttpURLConnection conn =
                cast(endpoint.openConnection());
            String s = "Bearer " + token;
            conn.setRequestProperty("Authorization", s);
            conn.setReadTimeout(500);
            conn.connect();

            int httpCode = conn.getResponseCode();
            if (httpCode >= 300)
            {
                Reader input = ireader(conn.getErrorStream());
                Tree<String> errorEntity =
                    JsonConverter.convert(input);
                input.close();
                handler.requestFailed(httpCode, errorEntity);
                return;
            }

            Reader input = ireader(conn.getInputStream());
            BufferedReader br = new BufferedReader(input);
            while (true) try
            {
                if (Thread.interrupted()) break;
                String line = br.readLine();
                if (Thread.interrupted()) break;
                if (line != null) handler.lineReceived(line);
            }
            catch (SocketTimeoutException eSt) { }
        }
        catch (IOException eIo)
        {
            handler.connectionFailed(eIo);
        }
    }

//   -      -%-      -

    public void
    hidePost(String postId)
    {
        assert postId != null;
        orderedSetAdd(hiddenPostIds, postId);
    }

    public void
    unhidePost(String postId)
    {
        assert postId != null;
        hiddenPostIds.remove(postId);
        // Hopefully String#equals will work here.
        // Like I mean, I think Object#equals just does ==, which
        // is why this works for arbitrary objects. But if they did
        // implement #equals then this can be a bit expensive.
    }

    public boolean
    isHidden(String postId)
    {
        for (String hiddenPostId: hiddenPostIds)
        {
            int test = postId.compareTo(hiddenPostId);
            if (test == 0) return true;
            if (test > 0) return false;
        }
        return false;
    }


//  -=- --- -%- --- -=-


    public static void
    debugPrint(Tree<String> tree)
    {
        debugPrint(tree, "");
    }

    public static void
    debugPrint(Tree<String> tree, String prefix)
    {
        System.err.print(prefix);
        System.err.print(deescape(tree.key));
        System.err.print(": ");
        System.err.println(deescape(tree.value));
        for (Tree<String> child: tree)
            debugPrint(child, prefix + "    ");
    }

//   -      -%-     -

    public static void
    requireProperties(Tree<String> entity, String... props)
    throws MalformedEntityException
    {
        for (String property: props)
            if (entity.get(property) == null)
                throw new MalformedEntityException(entity, props);
    }

    public static void
    eachRequireProperties(Tree<String> container, String... props)
    throws MalformedEntityException
    {
        for (Tree<String> entity: container)
            requireProperties(entity, props);
    }


//  -=- --- -%- --- -=-


    private static void
    doStandardJsonReturn(
        HttpURLConnection conn, RequestOutcome outcome)
    throws IOException
    {
        outcome.httpCode = conn.getResponseCode();
        if (outcome.httpCode >= 300)
        {
            Reader input = ireader(conn.getErrorStream());
            outcome.errorEntity = JsonConverter.convert(input);
            input.close();
        }
        else
        {
            Reader input = ireader(conn.getInputStream());
            outcome.entity = JsonConverter.convert(input);
            input.close();
        }
    }

    private static void
    wrapResponseInTree(
        HttpURLConnection conn, RequestOutcome outcome)
    throws IOException
    {
        outcome.httpCode = conn.getResponseCode();
        if (outcome.httpCode >= 300)
        {
            Reader input = ireader(conn.getErrorStream());
            outcome.errorEntity = fromPlain(input);
            input.close();
        }
        else
        {
            Reader input = ireader(conn.getInputStream());
            outcome.entity = fromPlain(input);
            input.close();
        }
    }

    private static Tree<String>
    fromPlain(Reader r)
    throws IOException
    {
        StringBuilder b = new StringBuilder();
        int c; while ((c = r.read()) != -1) b.append((char)c);
    
        Tree<String> leaf = new Tree<String>();
        leaf.key = "body";
        leaf.value = b.toString();
        Tree<String> doc = new Tree<String>();
        doc.add(leaf);
        return doc;
    }

//   -      -%-      - 

    private static <E extends Comparable<E>> void
    orderedSetAdd(List<E> list, E element)
    {
        ListIterator<E> it = list.listIterator();
        while (it.hasNext())
        {
            int test = element.compareTo(it.next());
            if (test > 0)
            {
                it.previous();
                it.add(element);
                break;
            }
            if (test == 0) break;
        }
        if (!it.hasNext())
        {
            list.add(element);
        }
    }

//   -      -%-      -

    private static String
    deescape(String string)
    {
        if (string == null) return string;
        string = string.replaceAll("\n", "\\\\n");
        return string;
    }

    private static String
    encode(String s)
    {
        try {
            if (s == null) return null;
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException eUe) {
            assert false;
            return null;
        }
    }

    private static HttpURLConnection
    cast(URLConnection conn)
    {
        return (HttpURLConnection)conn;
    }

    private static InputStreamReader
    ireader(InputStream is)
    throws IOException
    {
        assert is != null;
        return new InputStreamReader(is);
    }

    private static OutputStreamWriter
    owriter(OutputStream os)
    throws IOException
    {
        assert os != null;
        return new OutputStreamWriter(os);
    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    public static List<LoginDetails>
    getLoginsFromCache()
    throws IOException
    {
        FileReader r = new FileReader(getCachePath());
        DSVTokeniser.Options o = new DSVTokeniser.Options();

        List<LoginDetails> returnee = new LinkedList<>();
        LoginDetails buildee = new LoginDetails();
        while (true)
        {
            Tree<String> row = DSVTokeniser.tokenise(r, o);
            boolean eof = row.get(0).value.equals(o.endOfStreamValue);
            boolean eos = row.get(0).value.equals("%");
            if (eof || eos)
            {
                boolean accept = true;
                accept &= buildee.instanceUrl != null;
                accept &= buildee.accessToken != null;
                if (!accept)
                    throw new IOException("cache invalid format");

                returnee.add(buildee);
                buildee = new LoginDetails();

                if (eof) break;
            }
            else if (buildee.instanceUrl == null)
            {
                if (row.size() < 2)
                    throw new IOException("cache invalid format");

                buildee.instanceUrl = row.get(0).value;
                buildee.accessToken = row.get(1).value;
            }
            else if (buildee.clientId == null)
            {
                if (row.size() < 2)
                    throw new IOException("cache invalid format");

                buildee.clientId = row.get(0).value;
                buildee.clientSecret = row.get(1).value;
            }
            else if (buildee.accountId == null)
            {
                if (row.size() < 2)
                    throw new IOException("cache invalid format");

                buildee.accountId = row.get(0).value;
                buildee.avatarUrl = row.get(1).value;
            }
            else continue;
            // Additional rows, probably newer version of file. Ignore.
        }
        r.close();

        return returnee;
    }

    public static void
    saveLoginsIntoCache(List<LoginDetails> logins)
    throws IOException
    {
        assert logins != null;

        FileWriter w = new FileWriter(getCachePath());

        LoginDetails first = logins.size() == 0 ? null : logins.get(0);
        for (LoginDetails login: logins)
        {
            if (login != first) w.write("%\n");

            assert login.instanceUrl != null;
            assert login.accessToken != null;
            assert login.clientId != null;
            assert login.clientSecret != null;
            assert login.accountId != null;
            assert login.avatarUrl != null;

            w.write(login.instanceUrl.replaceAll(":", "\\\\:") + ":");
            w.write(login.accessToken + "\n");

            w.write(login.clientId.replaceAll(":", "\\\\:") + ":");
            w.write(login.clientSecret.replaceAll(":", "\\\\:") + "\n");

            w.write(login.accountId.replaceAll(":", "\\\\:") + ":");
            w.write(login.avatarUrl.replaceAll(":", "\\\\:") + "\n");
        }

        w.close();
    }


//  -=- --- -%- --- -=-


    private static String
    getCachePath()
    {
        String userHome = System.getProperty("user.home");
        String osName = System.getProperty("os.name");
        boolean isWindows = osName.contains("Windows");
        boolean isUnix = !isWindows;
        // We assume. If you're running JKomasto in classic Mac OS
        // for some reason, you should probably edit the code..
        String configDir = isWindows ? "AppData/Local" : ".config";
        String filename = "jkomasto.logins.dsv";
        String path = userHome + "/" + configDir + "/" + filename;
        return path;
    }

}
