
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.Box;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;
import javax.swing.border.Border;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JFileChooser;
import javax.swing.ImageIcon;
import javax.swing.MenuElement;
import javax.swing.GroupLayout;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoManager;
import javax.swing.event.UndoableEditListener;
import javax.swing.event.UndoableEditEvent;
import java.nio.file.Files;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.Cursor;
import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.AWTKeyStroke;
import java.awt.KeyboardFocusManager;
import java.io.File;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.FlavorEvent;

import cafe.biskuteri.hinoki.Tree;
import java.io.IOException;

class
ComposeWindow extends JFrame
implements Scalable, WindowListener {

    private JKomasto
    primaire;

    private MastodonApi
    api;

    private ImageFetcher
    fetcher;

    private PostVisibility
    defaultVisibility;

//   -  -%-  -

    private Composition
    composition;

    private ComposeComponent
    contentsDisplay;

    private AttachmentsComponent
    attachmentsDisplay;

    private JTabbedPane
    tabs;

    private int
    scale;

    private String
    submitWarning;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    use(Composition composition)
    {
        assert composition != null;
        /*
        * A malformed composition doesn't make sense - we saw
        * that one way that could happen is if a reply was
        * requested on a malformed post, but in that case the
        * caller should decide what to do, which can be passing
        * us a blank composition.
        */

        synchronized (this)
        {
            this.composition = composition;
            syncDisplayToComposition();
        }
    }

    public void
    newComposition()
    {
        synchronized (this)
        {
            composition = new Composition();
            composition.text = "";
            composition.visibility = defaultVisibility;
            composition.language =
                new String[] { "en", "ja", "cmn", "ms" }[0];
            composition.replyToPostId = null;
            composition.contentWarning = null;
            composition.attachments = new Attachment[0];
            syncDisplayToComposition();
        }
    }

    public void
    submit()
    {
        synchronized (this)
        {
            syncCompositionToDisplay();

            if (composition.replyToPostId != null)
                assert !composition.replyToPostId.trim().isEmpty();
            if (composition.contentWarning != null)
                assert !composition.contentWarning.trim().isEmpty();

            if (contentsDisplay.getSubmitWarning().contains("U"))
            {
                String[] s = new String[] {
                    JapaneseStrings
                        .get("confirm no description yes"),
                    JapaneseStrings
                        .get("confirm no description no"),
                    JapaneseStrings
                        .get("compose no description"),
                    JapaneseStrings
                        .get("compose no description title")
                };
                int response = JOptionPane.showOptionDialog(
                    this,
                    s[2], s[3],
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null,
                    new String[] { s[0], s[1] }, s[0]);
                if (response != 0) return;
            }

            for (int o = 0; o < tabs.getTabCount(); ++o)
                tabs.setEnabledAt(o, false);
            tabs.setCursor(WAIT);

            tabs.setSelectedComponent(contentsDisplay);
            contentsDisplay.setSubmitting(true);
            tabs.paintImmediately(tabs.getBounds());

            boolean uploadsOkay = true;
            for (Attachment a: composition.attachments)
            {
                //if (a.descriptionChanged)
                    //uploadsOkay &= uploadNewDescription(a);

                if (a.descriptionChanged)
                    uploadsOkay &= reupload(a);
                /*
                * The endpoint for editing an uploaded media
                * is broken at the moment, so go around by just
                * downloading and reuploading. Hopefully this
                * is sufficiently robust and we don't have to
                * disable the attachments' tab's description field.
                */

                if (a.id == null)
                    uploadsOkay &= upload(a);

                if (!uploadsOkay) break;
            }

            if (!uploadsOkay)
            {
                contentsDisplay.setSubmitting(false);
                for (int o = 0; o < tabs.getTabCount(); ++o)
                    tabs.setEnabledAt(o, true);
                tabs.setCursor(null);
                return;
            }

            int attachmentCount = composition.attachments.length;
            String[] mediaIDs = new String[attachmentCount];
            for (int o = 0; o < mediaIDs.length; ++o)
                mediaIDs[o] = composition.attachments[o].id;

            RequestOutcome r = api.submit(
                composition.text, composition.visibility,
                composition.replyToPostId,
                composition.contentWarning,
                mediaIDs, composition.language);
            if (r.exception != null)
            {
                JOptionPane.showMessageDialog(
                    ComposeWindow.this,
                    "Tried to submit post, failed..."
                    + "\n" + r.exception.getMessage());
            }
            else if (r.errorEntity != null)
            {
                JOptionPane.showMessageDialog(
                    ComposeWindow.this,
                    "Tried to submit post, failed..."
                    + "\n" + r.errorEntity.get("error").value
                    + "\n(HTTP error code: " + r.httpCode + ")");
            }
            else
            {
                newComposition();
            }

            contentsDisplay.setSubmitting(false);
            for (int o = 0; o < tabs.getTabCount(); ++o)
                tabs.setEnabledAt(o, true);
            tabs.setCursor(null);
        }
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        contentsDisplay.setScale(scale);
        attachmentsDisplay.setScale(scale);

        String fname = "VL PGothic";
        int fsz = 13 * scale/20;
        Font f = new Font(fname, Font.BOLD, fsz);
        tabs.setFont(f);

        boolean isDisposed = !isDisplayable();
        int w = 360 * scale/20;
        int h = 270 * scale/20;
        contentsDisplay.setPreferredSize(new Dimension(w, h));
        attachmentsDisplay.setPreferredSize(new Dimension(w, h));
        pack(); if (isDisposed) dispose();
    }

//   -  -%-  -

    private void
    syncDisplayToComposition()
    {
        ComposeComponent d1 = contentsDisplay;
        d1.setText(composition.text);
        d1.setReplyToPostId(composition.replyToPostId);
        d1.setVisibility(composition.visibility);
        d1.setLanguage(composition.language);
        d1.setContentWarning(composition.contentWarning);
        d1.resetFocus();

        AttachmentsComponent d2 = attachmentsDisplay;
        d2.setAttachments(composition.attachments);
    }

    private void
    syncCompositionToDisplay()
    {
        Composition c = composition;

        ComposeComponent d1 = contentsDisplay;
        c.text = d1.getText();
        c.visibility = d1.getVisibility();
        c.language = d1.getLanguage();
        c.replyToPostId = nonEmpty(d1.getReplyToPostId());
        c.contentWarning = nonEmpty(d1.getContentWarning());

        AttachmentsComponent d2 = attachmentsDisplay;
        c.attachments = d2.getAttachments();
    }

    public void
    openEmojiPicker()
    {
        EmojiPicker w = primaire.getEmojiPicker();
        w.setListener(contentsDisplay);
        if (!w.isVisible())
        {
            w.setLocation(getX() + (getWidth() * 9/8), getY());
            w.setVisible(true);
        }
    }

    private boolean
    uploadNewDescription(Attachment a)
    {
        RequestOutcome r = api.editAttachment(a.id, a.description);
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                ComposeWindow.this,
                "Tried to update attachment"
                + " description, failed..."
                + "\n" + r.exception.getMessage());
            return false;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                ComposeWindow.this,
                "Tried to update attachment"
                + " description, failed..."
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP code: " + r.httpCode + ")");
            return false;
        }
        else return true;
    }

    private boolean
    reupload(Attachment a)
    {
        File destination = null; try
        {
            destination =
                File.createTempFile("JKomasto-reupload-", null);
        }
        catch (IOException eIo)
        {
            JOptionPane.showMessageDialog(
                ComposeWindow.this,
                "Couldn't create destination for downloading "
                + " attachment for reupload...");
            return false;
        }

        RequestOutcome r = api.downloadFile(a.url, destination);
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                ComposeWindow.this,
                "Tried to download existing attachment"
                + " for reupload, failed..."
                + "\n" + r.exception.getMessage());
            return false;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                ComposeWindow.this,
                "Tried to download existing attachment"
                + " for reupload, failed..."
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP code: " + r.httpCode + ")");
            return false;
        }
        else
        {
            a.uploadee = destination;
            boolean uploaded = upload(a);
            destination.delete();
            return uploaded;
        }
    }

    private boolean
    upload(Attachment a)
    {
        RequestOutcome r =
            api.uploadFile(a.uploadee, a.description);
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                ComposeWindow.this,
                "Tried to upload attachment, failed..."
                + "\n" + r.exception.getMessage());
            return false;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                ComposeWindow.this,
                "Tried to upload attachment, failed..."
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP code: " + r.httpCode + ")");
            return false;
        }
        else
        {
            a.id = r.entity.get("id").value;
            return true;
        }
    }

    public void
    attachmentCountChanged(int newCount)
    {
        if (tabs == null) return;
        // We can't initialise the tabs before the displays, and
        // AttachmentsDisplay calls this on construction.

        String s = JapaneseStrings.get("compose media tab");
        tabs.setTitleAt(
            1, s + (newCount == 0 ? "" : "(" + newCount + ")"));

        submitWarning =
            (newCount > 0 ? "A" : "")
            + (submitWarning.contains("U") ? "U" : "");
        contentsDisplay.setSubmitWarning(submitWarning);
    }

    public void
    attachmentDescriptionsChanged(boolean allFilled)
    {
        submitWarning =
            (submitWarning.contains("A") ? "A" : "")
            + (!allFilled ? "U" : "");
        contentsDisplay.setSubmitWarning(submitWarning);
    }

    public void
    windowClosing(WindowEvent eW)
    {
        syncCompositionToDisplay();

        int totalTextCount = 0;
        totalTextCount += composition.text.length();
        for (Attachment attachment: composition.attachments)
            if (attachment.description != null)
                totalTextCount += attachment.description.length();

        if (totalTextCount > 50)
        {
            String[] options = new String[] {
                JapaneseStrings.get("confirm discard yes"),
                JapaneseStrings.get("confirm discard no")
            };
            int response = JOptionPane.showOptionDialog(
                this,
                JapaneseStrings.get("confirm discard"),
                JapaneseStrings.get("confirm discard title"),
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options, options[1]
            );
            if (response != 0) return;
        }

        dispose();
    }

    public ImageWindow
    newImageWindow()
    {
        return new ImageWindow(primaire);
    }

    public void
    windowOpened(WindowEvent eW) { }

    public void
    windowClosed(WindowEvent eW) { }

    public void
    windowActivated(WindowEvent eW) { }

    public void
    windowDeactivated(WindowEvent eW) { }

    public void
    windowIconified(WindowEvent eW) { }

    public void
    windowDeiconified(WindowEvent eW) { }

//   -  -%-  -

    private static String
    nonEmpty(String s)
    {
        if (s.trim().isEmpty()) return null;
        return s;
    }

//  ---%-@-%---

    ComposeWindow(JKomasto primaire)
    {
        super(JapaneseStrings.get("compose title"));
        this.primaire = primaire;
        Settings settings = primaire.getSettings();
        api = primaire.getMastodonApi();
        fetcher = primaire.getImageFetcher();

        defaultVisibility = settings.defaultVisibility;

        String key = "TabbedPane.tabAreaBackground";
        Color bg = javax.swing.UIManager.getColor(key);
        String[] s = new String[] {
            JapaneseStrings.get("compose text tab"),
            JapaneseStrings.get("compose media tab")
        };

        submitWarning = "";

        contentsDisplay = new ComposeComponent(this);
        attachmentsDisplay = new AttachmentsComponent(this);
        addFocusTraversalKeys(contentsDisplay);
        addFocusTraversalKeys(attachmentsDisplay);

        tabs = new JTabbedPane();
        tabs.addTab(s[0], contentsDisplay);
        tabs.addTab(s[1], attachmentsDisplay);

        setBackground(bg);
        setIconImage(primaire.getProgramIcon());
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        setContentPane(tabs);
        newComposition();
        setScale(20);

        addWindowListener(this);
        addMouseWheelListener(new MouseWheelScaler(2, 10));
        addKeyListener(new SharedKeyShortcuts(primaire, true));
        /*
        * This addition was necessary before, because there was no
        * way to escape a JTextArea's focus by keyboard on my KDE 3
        * desktop (Ctrl-Tab which is AWT's substitution was mapped
        * to desktop switching). Now it no longer is, but I'll keep this
        * anyways.
        */
    }

//   -  -%-  -

    private static void
    addFocusTraversalKeys(Container c)
    {
        final int FW =
            KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS;

        AWTKeyStroke esc =
            AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ESCAPE, 0);

        Set<AWTKeyStroke> keystrokes = new HashSet<>();
        keystrokes.addAll(c.getFocusTraversalKeys(FW));
        keystrokes.add(esc);
        c.setFocusTraversalKeys(FW, keystrokes);
    }

}



class
ComposeComponent extends JPanel
implements
    ActionListener, CaretListener, KeyListener,
    Scalable, EmojiPicker.Listener {

    private ComposeWindow
    primaire;

//   -  -%-  -

    private JTextArea
    text;

    private JTextField
    reply, contentWarning;

    private JLabel
    replyLabel, contentWarningLabel;

    private JButton
    openEmojiPicker;

    private JLabel
    textLength;

    private JComboBox<String>
    visibility,
    language;

    private JLabel
    visibilityLabel,
    languageLabel,
    submitWarning;

    private JButton
    submit;

    private JScrollPane
    scroll;

    private JPanel
    top;

    private UndoManager
    textUndos;

    private JMenuItem
    undo,
    redo;

    private TextActionPopupMenu
    textActionPopup1,
    textActionPopup2;

    private int
    scale;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    setText(String text)
    {
        this.text.setText(text);
        textUndos.discardAllEdits();
    }

    public void
    setReplyToPostId(String postId)
    {
        this.reply.setText(postId);
    }

    public void
    setVisibility(PostVisibility visibility)
    {
        switch (visibility)
        {
            case PUBLIC:
                this.visibility.setSelectedIndex(0); break;
            case UNLISTED:
                this.visibility.setSelectedIndex(1); break;
            case FOLLOWERS:
                this.visibility.setSelectedIndex(2); break;
            case MENTIONED:
                this.visibility.setSelectedIndex(3); break;
            default:
                assert false;
        }
    }

    public void
    setLanguage(String language)
    {
        if (language.equals("en"))
            this.language.setSelectedIndex(0);
        else if (language.equals("ja"))
            this.language.setSelectedIndex(1);
        else if (language.equals("cmn"))
            this.language.setSelectedIndex(2);
        else if (language.equals("ms"))
            this.language.setSelectedIndex(3);
        else
            this.language.setSelectedItem(language);
    }

    public void
    setContentWarning(String contentWarning)
    {
        this.contentWarning.setText(contentWarning);
    }

    public void
    setSubmitWarning(String submitWarning)
    {
        this.submitWarning.setText(submitWarning);
    }

    public String
    getSubmitWarning()
    {
        return submitWarning.getText();
    }

    public String
    getText()
    {
        return text.getText();
    }

    public String
    getReplyToPostId()
    {
        return reply.getText();
    }

    public String
    getContentWarning()
    {
        return contentWarning.getText();
    }

    public PostVisibility
    getVisibility()
    {
        int offset = visibility.getSelectedIndex();
        assert offset != -1;
        assert offset < 4;
        return new PostVisibility[] {
            PostVisibility.PUBLIC,
            PostVisibility.UNLISTED,
            PostVisibility.FOLLOWERS,
            PostVisibility.MENTIONED
        }[offset];
    }

    public String
    getLanguage()
    {
        int offset = language.getSelectedIndex();
        if (offset != -1 && offset < 4)
            return new String[] { "en", "ja", "cmn", "ms" }[offset];
        else
            return (String)language.getSelectedItem();
    }

    public void
    setSubmitting(boolean submitting)
    {
        text.setEnabled(!submitting);
        language.setEnabled(!submitting);
        visibility.setEnabled(!submitting);
        submit.setEnabled(!submitting);
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        String fname = "VL PGothic";
        Font labels = new Font(fname, Font.BOLD, 13 * scale/20);
        Font fields = new Font(fname, Font.PLAIN, 13 * scale/20);
        Font textarea = new Font(fname, Font.PLAIN, 15 * scale/20);
        Font submit = new Font(fname, Font.BOLD, 13 * scale/20);

        replyLabel.setFont(labels);
        contentWarningLabel.setFont(labels);
        openEmojiPicker.setFont(labels);
        textLength.setFont(labels);
        visibility.setFont(fields);
        language.setFont(fields);
        this.submit.setFont(submit);
        visibilityLabel.setFont(labels);
        languageLabel.setFont(labels);
        submitWarning.setFont(labels);

        textActionPopup1.setFont(labels);
        textActionPopup2.setFont(labels);

        reply.setFont(fields);
        contentWarning.setFont(fields);
        text.setFont(textarea);

        int u1 = 8 * scale/20;
        int u2 = 4 * scale/20;
        Border b1 = BorderFactory.createEmptyBorder(u1, u1, u1, u1);
        Border b2 = BorderFactory.createEmptyBorder(u2, u2, u2, u2);
        Border b3 = BorderFactory.createLineBorder(Color.GRAY);
        Border bc = BorderFactory.createCompoundBorder(b3, b2);
        text.setBorder(b2);
        scroll.setViewportBorder(b3);
        setBorder(b1);

        int mx = 2 * scale/20;
        int my = 4 * scale/20;
        openEmojiPicker.setMargin(new Insets(mx, my, mx, my));
        
        ((GridLayout)top.getLayout()).setHgap(8 * scale/20);
        ((BorderLayout)getLayout()).setVgap(8 * scale/20);
    }

    public void
    resetFocus()
    {
        text.requestFocusInWindow();
    }

//   -  -%-  -

    private void
    updateTextLength()
    {
        int length = text.getText().length();
        /*
        * The web interface doesn't do this expensive thing.
        * It has an upwards counter, incremented by I'm not
        * sure what. Presumably they have some control over
        * the text input. I'd rather not, cause I use a
        * Japanese IME, I'm going to see how laggy this is.
        * It raises our app's system requirements, but, I was
        * going to transition it to multithreading anyways,
        * I don't think we're going to be very cheap.. Which
        * sucks, but the Mastodon API is not helping us here.
        */
        textLength.setText(Integer.toString(length));
        /*
        * Another thing I could do is temporarily move the
        * caret to the end and then find its position, then
        * seek back. Not sure how much that would help, but
        * if this is too laggy, that's what I'd try next.
        */
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();

        setCursor(WAIT);
        if (src == submit) primaire.submit();
        if (src == undo && textUndos.canUndo()) textUndos.undo();
        if (src == redo && textUndos.canRedo()) textUndos.redo();
        if (src == openEmojiPicker) primaire.openEmojiPicker();
        setCursor(null);
    }

    public void
    caretUpdate(CaretEvent eCa)
    {
        setCursor(WAIT);
        updateTextLength();
        setCursor(null);
    }

    public void
    keyPressed(KeyEvent eK)
    {
        assert eK.getSource() == text;

        boolean ctrl = eK.isControlDown();
        boolean shift = eK.isShiftDown();
        boolean z = eK.getKeyCode() == KeyEvent.VK_Z;
        boolean y = eK.getKeyCode() == KeyEvent.VK_Y;

        if ((ctrl && shift && z) || (ctrl && y))
        {
            setCursor(WAIT);
            if (textUndos.canRedo()) textUndos.redo();
            setCursor(null);
        }
        else if (ctrl && z)
        {
            setCursor(WAIT);
            if (textUndos.canUndo()) textUndos.undo();
            setCursor(null);
        }

        updateTextLength();
    }

    public void
    emojiPicked(EmojiPicker.Emoji emoji)
    {
        setCursor(WAIT);
        text.replaceSelection(emoji.string);
        setCursor(null);
    }

    public void
    keyReleased(KeyEvent eK) { }

    public void
    keyTyped(KeyEvent eK) { }

//  ---%-@-%---

    ComposeComponent(ComposeWindow primaire)
    {
        this.primaire = primaire;

        textUndos = new UndoManager();
        undo = new JMenuItem(JapaneseStrings.get("undo"));
        redo = new JMenuItem(JapaneseStrings.get("redo"));
        undo.addActionListener(this);
        redo.addActionListener(this);

        textActionPopup1 = new TextActionPopupMenu();
        textActionPopup2 = new TextActionPopupMenu();
        textActionPopup2.insert(new JSeparator(), 0);
        textActionPopup2.insert(redo, 0);
        textActionPopup2.insert(undo, 0);

        String s1 = JapaneseStrings.get("compose reply field");
        reply = new JTextField();
        replyLabel = new JLabel(s1 + ": ");
        replyLabel.setLabelFor(reply);
        reply.addMouseListener(textActionPopup1);
        reply.addKeyListener(textActionPopup1);

        String s2 = JapaneseStrings.get("compose cw field");
        contentWarning = new JTextField();
        contentWarningLabel = new JLabel(s2 + ": ");
        contentWarningLabel.setLabelFor(contentWarning);
        contentWarning.addMouseListener(textActionPopup1);
        contentWarning.addKeyListener(textActionPopup1);

        top = new JPanel();
        top.setOpaque(false);
        top.setLayout(new GridLayout(2, 2));
        top.add(replyLabel);
        top.add(reply);
        top.add(contentWarningLabel);
        top.add(contentWarning);

        String s3 = JapaneseStrings.get("open emojis");
        openEmojiPicker = new JButton("☕");
        openEmojiPicker.setToolTipText(s3);
        openEmojiPicker.addActionListener(this);

        String s4 = JapaneseStrings.get("character count");
        textLength = new JLabel("0", JLabel.CENTER);
        textLength.setToolTipText(s4);

        String[] options = new String[] {
            JapaneseStrings.get("compose public"),
            JapaneseStrings.get("compose unlisted"),
            JapaneseStrings.get("compose followers"),
            JapaneseStrings.get("compose mentioned")
        };
        visibility = new JComboBox<>(options);
        //visibility.getAccessibleContext()
            //.setAccessibleName("Select post visibility");
        /*
        * Don't bother. JComboBox's accessibility is bugged.
        * The AccessibleContext seems to have everything for a
        * combobox, but even going through items in the popup cuts off.
        */
        visibility.setForeground(Color.BLACK);
        visibilityLabel = new JLabel(
            JapaneseStrings.get("compose visibility") + ":",
            JLabel.RIGHT
        );
        visibilityLabel.setLabelFor(visibility);
        Dimension msz = Box.createGlue().getMaximumSize();
        visibilityLabel.setMaximumSize(msz);

        submit = new JButton(JapaneseStrings.get("compose submit"));
        submit.addActionListener(this);
        Insets submitMargin = submit.getMargin();
        submitMargin.left = submitMargin.left * 1/2;
        submitMargin.right = submitMargin.right * 1/2;
        submit.setMargin(submitMargin);

        options = new String[] {
            JapaneseStrings.get("english"),
            JapaneseStrings.get("japanese"),
            JapaneseStrings.get("mandarin"),
            JapaneseStrings.get("malay")
        };
        language = new JComboBox<>(options);
        language.setEditable(true);
        {
            String key = "TabbedPane.contentAreaColor";
            Color bg = javax.swing.UIManager.getColor(key);
            JTextField field = null;
            JButton button = null;
            for (Component c: language.getComponents()) {
                if (c instanceof JTextField) field = (JTextField)c;
                if (c instanceof JButton) button = (JButton)c;
            }
            button.setEnabled(false);
            field.setForeground(Color.BLACK);
            field.setBackground(new Color(bg.getRGB()));
            button.setEnabled(true);
            /*
            * (知) The nightmare is over. I fought for 2 days straight
            * trying to win the task of changing the background
            * colour of an editable JComboBox.
            *
            * The problem lies in two areas. One, if you were keh poh
            * and fetched your desired colour using UIManager,
            * chances are you will get a ColorUIResource.
            * plaf.basic.BasicTextUI is setup as a property listener
            * where when you setText on the JTextField, it will check
            * if the background colour is a ColorUIResource, and
            * if so reset the background colour to the L&F's default.
            * (I don't know what is the purpose of that behaviour.)
            * Above, I have played defense by setting the background
            * to a non-UIResource Color copy.
            *
            * Two, the combo box's button is also listening to the
            * combo box's background colour. I don't know how
            * setting the text field's background corresponds to the
            * combo box's itself, but somehow it does, the button is
            * also initially affected by our field.setBackground. I
            * work around this by disabling the button temporarily
            * and reenabling it, which seems to work.
            *
            * These are hacks on undocumented details, but they left
            * me no choice. I wasn't trying to do something complex,
            * I just wanted to have the background colour be the same
            * as non-editable comboboxes, and same with the hack in
            * AttachmentComponent, I just wanted to add a button.
            * They should've published ways.
            */
        }
        languageLabel = new JLabel(
            JapaneseStrings.get("compose language") + ":",
            JLabel.RIGHT
        );
        languageLabel.setLabelFor(language);
        languageLabel.setMaximumSize(msz);

        submitWarning = new JLabel("");
        // Not accessible atm. I'm planning on conveying the warnings
        // to the accessible description of the submit button instead.

        JPanel bottom = new JPanel();
        bottom.setOpaque(false);
        GroupLayout layout = new GroupLayout(bottom);
        layout.setHorizontalGroup(
            layout.createSequentialGroup()
                .addGroup(
                    layout.createParallelGroup()
                        .addComponent(openEmojiPicker)
                        .addComponent(textLength))
                .addGroup(
                    layout.createParallelGroup()
                        .addComponent(languageLabel)
                        .addComponent(visibilityLabel))
                .addGroup(
                    layout.createParallelGroup()
                        .addComponent(language)
                        .addComponent(visibility))
                .addGroup(
                    layout.createParallelGroup()
                        .addComponent(submitWarning)
                        .addComponent(submit))
        );
        final GroupLayout.Alignment CENTRE =
             GroupLayout.Alignment.CENTER;
        layout.setVerticalGroup(
            layout.createSequentialGroup()
                .addGroup(
                    layout.createParallelGroup(CENTRE)
                        .addComponent(openEmojiPicker)
                        .addComponent(languageLabel)
                        .addComponent(language)
                        .addComponent(submitWarning))
                .addGroup(
                    layout.createParallelGroup(CENTRE)
                        .addComponent(textLength)
                        .addComponent(visibilityLabel)
                        .addComponent(visibility)
                        .addComponent(submit))
        );
        layout.linkSize(openEmojiPicker, textLength);
        layout.setAutoCreateGaps(true);
        bottom.setLayout(layout);

        text = new JTextArea();
        text.setLineWrap(true);
        text.setWrapStyleWord(true);
        text.getAccessibleContext().setAccessibleName("Post text");
        text.addCaretListener(this);
        text.addMouseListener(textActionPopup2);
        text.addKeyListener(textActionPopup2);
        text.addKeyListener(this);
        disableTabs(text);
        text.getDocument().addUndoableEditListener(textUndos);

        scroll = new JScrollPane(
            text,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setBorder(null);

        setLayout(new BorderLayout());
        add(top, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);
        add(bottom, BorderLayout.SOUTH);
    }

    public static void
    disableTabs(JTextArea textArea)
    {
        textArea.setFocusTraversalKeys(
            KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS,
            null);
        textArea.setFocusTraversalKeys(
            KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
            null);
        /*
        * By default JTextAreas will set their own traversal
        * keys because they want to allow tabs within the text.
        * For our use case that isn't needed (the web UI also
        * bans tabs within the text components), so we'll cancel
        * that, so that tabbing to change focus will work again.
        */
    }

}

class
AttachmentsComponent extends JPanel
implements
    ActionListener, KeyListener,
    Scalable, ComponentListener, FlavorListener {

    private ComposeWindow
    primaire;

//   -  -%-  -

    private List<Attachment>
    working;

    private JPanel
    selections;

    private JToggleButton
    attachment1,
    attachment2,
    attachment3,
    attachment4,
    selected,
    last;

    private JButton
    add,
    pasteFromClipboard;

    private JButton
    delete,
    revert;

    private JButton
    moveUp,
    moveDown;

    private JLabel
    descriptionLabel;

    private JTextArea
    description;

    private JScrollPane
    scroll;

    private AdjustableStrut
    strut;

    private Box
    centre;

    private JFileChooser
    chooser;

    private UndoManager
    descriptionUndos;

    private TextActionPopupMenu
    textActionPopup;

    private boolean
    pastingFromClipboard;

    private int
    scale;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    setAttachments(Attachment[] n)
    {
        assert n != null;

        working.clear();
        select(null);

        addAll(working, n);
        announceCountChanged();

        syncSelectionsToWorking();
        if (last != null) select(last);
    }

    public Attachment[]
    getAttachments()
    {
        return working.toArray(new Attachment[0]);
    }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        String fname = "VL PGothic";
        Font labels = new Font(fname, Font.BOLD, 13 * scale/20);
        Font textarea = new Font(fname, Font.PLAIN, 15 * scale/20);

        attachment1.setFont(labels);
        attachment2.setFont(labels);
        attachment3.setFont(labels);
        attachment4.setFont(labels);
        descriptionLabel.setFont(labels);
        add.setFont(labels);
        delete.setFont(labels);
        revert.setFont(labels);
        moveUp.setFont(labels);
        moveDown.setFont(labels);

        textActionPopup.setFont(labels);

        description.setFont(textarea);

        int bs = 32 * scale/20;
        Dimension bsz = new Dimension(bs, bs);
        add.setPreferredSize(bsz);
        attachment1.setPreferredSize(bsz);
        attachment2.setPreferredSize(bsz);
        attachment3.setPreferredSize(bsz);
        attachment4.setPreferredSize(bsz);

        int u1 = 8 * scale/20;
        Insets bm = new Insets(u1, u1, u1, u1);
        add.setMargin(bm);
        attachment1.setMargin(bm);
        attachment2.setMargin(bm);
        attachment3.setMargin(bm);
        attachment4.setMargin(bm);

        strut.setSizing(8 * scale/20, 0);

        int u2 = 4 * scale/20;
        int u3 = 14 * scale/20;
        Border
            b1 = BorderFactory.createEmptyBorder(u1, u1, u1, u1),
            b2 = BorderFactory.createEmptyBorder(u2, u2, u2, u2),
            b3 = BorderFactory.createLineBorder(Color.GRAY),
            b4 = BorderFactory.createEmptyBorder(u2, u1, u1, u1);
            //b5 = BorderFactory.createEtchedBorder();
        description.setBorder(b2);
        scroll.setViewportBorder(b3);
        centre.setBorder(b4);
        this.setBorder(b1);

        ((FlowLayout)selections.getLayout()).setHgap(4 * scale/20);
        ((BorderLayout)getLayout()).setHgap(8 * scale/20);
        ((BorderLayout)getLayout()).setVgap(8 * scale/20);
    }

    public int
    getScale() { return scale; }

//   -  -%-  -

    private void
    select(JToggleButton button)
    {
        if (selected != null) selected.setSelected(false);

        if (button == null)
        {
            description.setText("");
            descriptionUndos.discardAllEdits();

            selected = null;
            delete.setEnabled(false);
            revert.setEnabled(false);
            moveUp.setEnabled(false);
            moveDown.setEnabled(false);
            description.setEnabled(false);
        }
        else
        {
            int offset =
                offsetOf(selections.getComponents(), button);
            assert offset != -1;
            assert offset < working.size();

            Attachment a = working.get(offset);
            description.setText(a.description);
            descriptionUndos.discardAllEdits();

            (selected = button).setSelected(true);
            delete.setEnabled(true);
            revert.setEnabled(true);
            moveUp.setEnabled(selected != attachment1);
            moveDown.setEnabled(selected != last);
            description.setEnabled(true);
        }
    }

    private void
    syncSelectionsToWorking()
    {
        selections.removeAll();
        last = null;
        if (working.size() > 0)
        {
            selections.add(attachment1);
            Image i = working.get(0).image;
            ImageIcon ic = i == null ? null : new ImageIcon(i);
            attachment1.setIcon(ic);
            last = attachment1;
        }
        if (working.size() > 1)
        {
            selections.add(attachment2);
            Image i = working.get(1).image;
            ImageIcon ic = i == null ? null : new ImageIcon(i);
            attachment2.setIcon(ic);
            last = attachment2;
        }
        if (working.size() > 2)
        {
            selections.add(attachment3);
            Image i = working.get(2).image;
            ImageIcon ic = i == null ? null : new ImageIcon(i);
            attachment3.setIcon(ic);
            last = attachment3;
        }
        if (working.size() > 3)
        {
            selections.add(attachment4);
            Image i = working.get(3).image;
            ImageIcon ic = i == null ? null : new ImageIcon(i);
            attachment4.setIcon(ic);
            last = attachment4;
        }
        if (working.size() < 4) selections.add(add);

        selections.repaint();
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();
        boolean isFromSelections =
            selections.isAncestorOf((Component)src);

        setCursor(WAIT);

        if (src == add)
        {
            File addee = null;

            pastingFromClipboard = false;
            int r = chooser.showOpenDialog(this);
            if (r == JFileChooser.APPROVE_OPTION)
            {
                addee = chooser.getSelectedFile();
            }
            else if (pastingFromClipboard) try
            {
                addee =
                    ClipboardApi.saveClipboardImageToTempfile();
                assert addee != null;
                pastingFromClipboard = false;
            }
            catch (IOException eIo)
            {
                eIo.printStackTrace();
                assert false;
            }

            if (addee != null)
            {
                Attachment a = attachmentFromFile(addee);
                saveSelected();
                announceDescriptionsChanged();

                working.add(a);
                announceCountChanged();

                syncSelectionsToWorking();
                assert last != null; select(last);
            }
        }

        if (src == delete)
        {
            assert selected != null;
            int offset =
                offsetOf(selections.getComponents(), selected);
            assert offset != -1;
            assert offset < working.size();

            working.remove(offset);
            announceCountChanged();

            syncSelectionsToWorking();
            int firstOffset = offset == 0 ? 0 : offset - 1;
            JToggleButton first = (JToggleButton)(
                last == null
                    ? null
                    : selections.getComponent(firstOffset));
            select(first);
        }

        if (src != add && isFromSelections)
        {
            assert src instanceof JToggleButton;
            if (src == selected)
            {
                JToggleButton button = (JToggleButton)src;
                int offset =
                    offsetOf(selections.getComponents(), button);
                assert offset != -1;
                assert offset < working.size();
                preview(working.get(offset));
            }
            else
            {
                saveSelected();
                announceDescriptionsChanged();
                select((JToggleButton)src);
            }
        }

        if (src == revert)
        {
            while (descriptionUndos.canUndo())
                descriptionUndos.undo();
            announceDescriptionsChanged();
        }

        if (src == moveUp)
        {
            assert selected != null;
            assert selected != attachment1;
            saveSelected();
            announceDescriptionsChanged();

            int offset =
                offsetOf(selections.getComponents(), selected);
            assert offset != -1;
            assert offset > 0;
            Attachment t = working.get(offset);
            working.set(offset, working.get(offset - 1));
            working.set(offset - 1, t);

            syncSelectionsToWorking();
            // Now that we've swapped the attachments in
            // working, the JToggleButton corresponding to
            // the attachment we selected is now different.
            offset = offset - 1;
            select((JToggleButton)selections.getComponent(offset));
        }
        if (src == moveDown)
        {
            assert selected != null;
            assert selected != last;
            saveSelected();
            announceDescriptionsChanged();

            int offset =
                offsetOf(selections.getComponents(), selected);
            assert offset != -1;
            assert offset < working.size() - 1;
            Attachment t = working.get(offset);
            working.set(offset, working.get(offset + 1));
            working.set(offset + 1, t);

            syncSelectionsToWorking();
            offset = offset + 1;
            select((JToggleButton)selections.getComponent(offset));
        }

        if (src == pasteFromClipboard)
        {
            pastingFromClipboard = true;
            chooser.cancelSelection();
        }

        setCursor(null);
    }

    private void
    saveSelected()
    {
        if (selected == null) return;

        int offset =
            offsetOf(selections.getComponents(), selected);
        assert offset != -1;

        Attachment a = working.get(offset);
        String description = this.description.getText();
        a.descriptionChanged =
            a.id != null && !description.equals(a.description);
        a.description =
            description.trim().isEmpty() ? null : description;
    }

    private void
    preview(Attachment a)
    {
        Attachment[] attachments = getAttachments();
        int offset = offsetOf(attachments, a);
        assert offset != -1;
        assert offset < attachments.length;

        ImageWindow w = primaire.newImageWindow();
        // Shouldn't we be signaling ComposeWindow to do
        // this instead? They would probably have to do
        // syncCompositionToDisplay first, but it would work.
        w.use(attachments, false);
        w.toImage(offset);
        w.setIconImage(primaire.getIconImage());
        w.setTitle(
            JapaneseStrings.get("compose attachment preview title")
            + " - JKomasto"
        );
        w.setVisible(true);
    }

    private void
    announceCountChanged()
    {
        primaire.attachmentCountChanged(working.size());
    }

    private void
    announceDescriptionsChanged()
    {
        boolean allFilled = true;
        for (Attachment a: working)
            allFilled &= a.description != null;
        primaire.attachmentDescriptionsChanged(allFilled);
    }

    public void
    componentHidden(ComponentEvent eC)
    {
        // Triggered when tab is changed.
        setCursor(WAIT);
        saveSelected();
        announceDescriptionsChanged();
        setCursor(null);
    }

    public void
    keyPressed(KeyEvent eK)
    {
        assert eK.getSource() == description;

        boolean ctrl = eK.isControlDown();
        boolean shift = eK.isShiftDown();
        boolean z = eK.getKeyCode() == KeyEvent.VK_Z;
        boolean y = eK.getKeyCode() == KeyEvent.VK_Y;

        if ((ctrl && shift && z) || (ctrl && y))
        {
            setCursor(WAIT);
            if (descriptionUndos.canRedo()) descriptionUndos.redo();
            setCursor(null);
        }
        else if (ctrl && z)
        {
            setCursor(WAIT);
            if (descriptionUndos.canUndo()) descriptionUndos.undo();
            setCursor(null);
        }
    }

    public void
    flavorsChanged(FlavorEvent eFl)
    {
        setCursor(WAIT);

        Clipboard cb = getToolkit().getSystemClipboard();
        boolean i = ClipboardApi.chooseImageFlavour(cb) != null;
        pasteFromClipboard.setEnabled(i);

        setCursor(null);
    }

    public void
    keyReleased(KeyEvent eK) { }

    public void
    keyTyped(KeyEvent eK) { }

    public void
    componentShown(ComponentEvent eC) { }

    public void
    componentMoved(ComponentEvent eC) { }

    public void
    componentResized(ComponentEvent eC) { }

//   -  -%-  -

    private static Attachment
    attachmentFromFile(File f)
    {
        String mime, mimePrimary; try {
            mime = Files.probeContentType(f.toPath());
            mimePrimary = mime != null ? mime.split("/")[0] : null;
            // Too lazy to instantiate a javax.activation.MimeType.
        }
        catch (IOException eIo) {
            mime = null;
            mimePrimary = null;
        }

        Attachment returnee = new Attachment();
        returnee.id = null;
        returnee.type = "unknown";
        returnee.url = null;
        returnee.description = null;
        returnee.image = null;
        returnee.uploadee = f;

        if (mimePrimary != null && mimePrimary.equals("image"))
        {
            returnee.type = "image";

            String urlr = f.toURI().toString();
            try {
                returnee.image = ImageApi.remote(urlr);
            }
            catch (IOException eIo) {
                returnee.image = null;
            }
            // This usage of ImageApi#remote is deprecated.

            Object comment =
                returnee.image.getProperty("comment", null);
            boolean hasComment =
                comment != null
                && comment != Image.UndefinedProperty;
            // I much prefer that the image properties have
            // loaded by now, that we don't have to
            // asynchronously read them later.

            if (hasComment)
                returnee.description = (String)comment;
        }

        return returnee;
    }

    private static <T> int
    offsetOf(T[] array, T item)
    {
        for (int o = 0; o < array.length; ++o)
            if (array[o] == item) return o;
        return -1;
    }

    private static <T> void
    addAll(List<T> list, T[] array)
    {
        for (T item: array) list.add(item);
    }

//  ---%-@-%---

    AttachmentsComponent(ComposeWindow primaire)
    {
        this.primaire = primaire;

        String[] strings = new String[] {
            JapaneseStrings.get("compose add tooltip"),
            JapaneseStrings.get("compose delete"),
            JapaneseStrings.get("compose revert"),
            JapaneseStrings.get("compose delete tooltip"),
            JapaneseStrings.get("compose revert tooltip"),
            JapaneseStrings.get("compose move up tooltip"),
            JapaneseStrings.get("compose move down tooltip"),
            JapaneseStrings.get("compose description field")
        };
        String[] s1 = JapaneseStrings.get("ordinals").split(",");
        for (int o = 0; o < s1.length; ++o)
            s1[o] = capitalise(s1[o]);
        String s2 = JapaneseStrings
            .get("compose attachments tooltip template");

        textActionPopup = new TextActionPopupMenu();

        chooser = new JFileChooser();
        chooser.setDragEnabled(true);
        // Lets you drag into the filename field, pasting a URL.

        pasteFromClipboard = new JButton("Paste from clipboard");
        pasteFromClipboard.addActionListener(this);
        Clipboard cb = getToolkit().getSystemClipboard();
        cb.addFlavorListener(this);
        boolean i = ClipboardApi.chooseImageFlavour(cb) != null;
        pasteFromClipboard.setEnabled(i);

        Box customButtonRow = Box.createHorizontalBox();
        customButtonRow.add(pasteFromClipboard);
        customButtonRow.add(Box.createHorizontalGlue());
        BorderLayout bl = (BorderLayout)chooser.getLayout();
        Container bottomControls = (Container)
            bl.getLayoutComponent(BorderLayout.SOUTH);
        Container usualButtonRow = (Container)
            bottomControls.getComponent(3);
        usualButtonRow.setLayout(new GridLayout(1, 2, 6, 0));
        usualButtonRow.setMaximumSize(
            usualButtonRow.getPreferredSize());
        customButtonRow.add(usualButtonRow);
        customButtonRow.setBorder(
            BorderFactory.createEmptyBorder(4, 0, 0, 0));
        bottomControls.add(customButtonRow);
        /*
        * Hacking on the Metal JFileChooser - will brick if a file
        * chooser of different layout is used.
        *
        * This file chooser has a BorderLayout where the bottom
        * component is a vertical box containing the filename row,
        * filetype row, blank space, then a button area.
        *
        * The button area has an odd layout where it gives each
        * button the same size, which is useful, then it adds a
        * vertical margin at the top, pinning the buttons right
        * under it. That's very uncooperative with what I've been
        * trying, so I'll replace that layout with something more
        * amenable to my goal.
        */

        add = new JButton("+");
        add.setToolTipText(strings[0]);
        add.addActionListener(this);
        attachment1 = new JToggleButton("1");
        attachment2 = new JToggleButton("2");
        attachment3 = new JToggleButton("3");
        attachment4 = new JToggleButton("4");
        attachment1.setToolTipText(s2.replace("<>", s1[0]));
        attachment2.setToolTipText(s2.replace("<>", s1[1]));
        attachment3.setToolTipText(s2.replace("<>", s1[2]));
        attachment4.setToolTipText(s2.replace("<>", s1[3]));
        attachment1.addActionListener(this);
        attachment2.addActionListener(this);
        attachment3.addActionListener(this);
        attachment4.addActionListener(this);

        selections = new JPanel();
        selections.setOpaque(false);
        selections.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        working = new ArrayList<Attachment>();

        Box top = Box.createHorizontalBox();
        top.add(selections);
        top.add(Box.createGlue());

        delete = new JButton(strings[1]);
        revert = new JButton(strings[2]);
        moveUp = new JButton("←");
        moveDown = new JButton("→");
        delete.setToolTipText(strings[3]);
        revert.setToolTipText(strings[4]);
        moveUp.setToolTipText(strings[5]);
        moveDown.setToolTipText(strings[6]);
        delete.setEnabled(false);
        revert.setEnabled(false);
        moveUp.setEnabled(false);
        moveDown.setEnabled(false);
        delete.addActionListener(this);
        revert.addActionListener(this);
        moveUp.addActionListener(this);
        moveDown.addActionListener(this);

        strut = new AdjustableStrut();
        Box bottom = Box.createHorizontalBox();
        bottom.add(moveUp);
        bottom.add(moveDown);
        bottom.add(strut);
        bottom.add(delete);
        bottom.add(Box.createGlue());
        bottom.add(revert);

        description = new JTextArea();
        description.setLineWrap(true);
        description.setWrapStyleWord(true);
        description.setEnabled(false);
        description.addMouseListener(textActionPopup);
        description.addKeyListener(textActionPopup);
        description.addKeyListener(this);
        ComposeComponent.disableTabs(description);
        descriptionLabel = new JLabel(strings[7]);
        descriptionLabel.setLabelFor(description);
        descriptionUndos = new UndoManager();
        description.getDocument()
            .addUndoableEditListener(descriptionUndos);

        scroll = new JScrollPane(
            description,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setBorder(null);

        syncSelectionsToWorking();

        JPanel row1 = new JPanel();
        row1.setOpaque(false);
        row1.setLayout(new BorderLayout());
        row1.add(descriptionLabel, BorderLayout.NORTH);
        row1.add(scroll, BorderLayout.CENTER);

        centre = Box.createVerticalBox();
        centre.add(row1);

        setLayout(new BorderLayout());
        add(centre, BorderLayout.CENTER);
        add(top, BorderLayout.NORTH);
        add(bottom, BorderLayout.SOUTH);

        this.addComponentListener(this);
    }

//   -  -%-  -

    private static String
    capitalise(String s)
    {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

}

class
TextActionPopupMenu extends JPopupMenu
implements MouseListener, KeyListener, ActionListener {

    private JMenuItem
    copy,
    cut,
    paste;

    private long
    pressed;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    setFont(Font font)
    {
        super.setFont(font);
        for (MenuElement mse: getSubElements())
            setFont(mse, font);
    }

//   -  -%-  -

    public void
    mousePressed(MouseEvent eM)
    {
        assert eM.getSource() instanceof JTextComponent;
        JTextComponent src = (JTextComponent)eM.getSource();

        if (eM.isPopupTrigger())
        {
            Cursor prev = src.getCursor();
            src.setCursor(WAIT);
            show(src, eM.getX(), eM.getY());
            src.setCursor(prev);
        }
    }

    public void
    mouseReleased(MouseEvent eM)
    {
        if (eM.getClickCount() == 0) setVisible(false);
    }

    public void
    keyPressed(KeyEvent eK)
    {
        Component src = (Component)eK.getSource();
        assert src instanceof JTextComponent;

        if (eK.getKeyCode() == KeyEvent.VK_CONTEXT_MENU)
        {
            Cursor prev = src.getCursor();
            src.setCursor(WAIT);
            show(src, 4, 4);
            src.setCursor(prev);
        }
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        assert getInvoker() instanceof JTextComponent;
        JTextComponent inv = (JTextComponent)getInvoker();

        setCursor(WAIT);
        if (eA.getSource() == copy) inv.copy();
        if (eA.getSource() == cut) inv.cut();
        if (eA.getSource() == paste) inv.paste();
        setCursor(null);
    }

    public void
    mouseClicked(MouseEvent eM) { }

    public void
    mouseEntered(MouseEvent eM) { }

    public void
    mouseExited(MouseEvent eM) { }

    public void
    keyReleased(KeyEvent eK) { }

    public void
    keyTyped(KeyEvent eK) { }

//   -  -%-  -

    private static void
    setFont(MenuElement me, Font font)
    {
        me.getComponent().setFont(font);
        for (MenuElement mse: me.getSubElements())
            setFont(mse, font);
    }

//  ---%-@-%---

    public
    TextActionPopupMenu()
    {
        copy = new JMenuItem(JapaneseStrings.get("copy"));
        cut = new JMenuItem(JapaneseStrings.get("cut"));
        paste = new JMenuItem(JapaneseStrings.get("paste"));
        copy.addActionListener(this);
        cut.addActionListener(this);
        paste.addActionListener(this);

        add(cut);
        add(copy);
        add(paste);
    }

}

