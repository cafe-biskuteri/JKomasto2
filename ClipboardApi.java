
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;

import java.awt.MediaTracker;

class
ClipboardApi
implements Transferable, ClipboardOwner {

    private static final ClipboardApi
    instance = new ClipboardApi();

    private static String
    string;

//  ---%-@-%---

    public static void
    serve(String string)
    {
        assert string != null;
        instance.string = string;
        Toolkit tk = Toolkit.getDefaultToolkit();
        Clipboard cb = tk.getSystemClipboard();
        cb.setContents(instance, instance);
    }

    public static File
    saveClipboardImageToTempfile()
    throws IOException
    {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Clipboard cb = tk.getSystemClipboard();
        DataFlavor imageFlavour = chooseImageFlavour(cb);
        if (imageFlavour == null) return null;

        String ext = "." + imageFlavour.getSubType();
        File f = File.createTempFile(
            "JKomasto-", "-clipboard" + ext
            /*
            * Files.probeContentType is an utter troll. On my
            * system, it basically just looks at the file extension,
            * doesn't try to look inside the file. What was the
            * point.. Anyways that's why I have to do this,
            * giving the flavour's subtype as an extension.
            */
        );

        f.deleteOnExit();
        try
        {
            if (imageFlavour == DataFlavor.imageFlavor)
            {
                Image i = (Image)cb.getData(imageFlavour);

                BufferedImage bi = new BufferedImage(
                    i.getWidth(null), i.getHeight(null),
                    BufferedImage.TYPE_INT_ARGB
                );
                bi.getGraphics().drawImage(i, 0, 0, null);

                ImageIO.write(bi, "png", f);
            }
            else
            {
                FileOutputStream out = new FileOutputStream(f);
                InputStream in = (InputStream)cb.getData(imageFlavour);
                int c; while ((c = in.read()) != -1) out.write(c);
                in.close();
                out.close();
            }
        }
        catch (UnsupportedFlavorException eUf)
        {
            assert false;
            /*
            * If this happens I'm guessing it'd be from the
            * clipboard data changing in between our choosing a
            * flavour and our trying to read data. I wanted to use
            * a Transferable to maybe avoid this, but that costs
            * extra columns in source text, so decided not to.
            */
        }

        return f;
    }

    public static DataFlavor
    chooseImageFlavour(Clipboard cb)
    {
        //DataFlavor pngFlavour = null;
        DataFlavor gifFlavour = null;
        DataFlavor jpegFlavour = null;
        DataFlavor lastImageFlavour = null;
        for (DataFlavor f: cb.getAvailableDataFlavors())
        {
            if (!f.isRepresentationClassInputStream()) continue;
            if (!f.getPrimaryType().equals("image")) continue;

            lastImageFlavour = f;
            //if (f.getSubType().equals("png")) pngFlavour = f;
            if (f.getSubType().equals("gif")) gifFlavour = f;
            if (f.getSubType().equals("jpeg")) jpegFlavour = f;
        }
        DataFlavor chosenFlavour = lastImageFlavour;
        if (jpegFlavour != null) chosenFlavour = jpegFlavour;
        if (gifFlavour != null) chosenFlavour = gifFlavour;
        //if (pngFlavour != null) chosenFlavour = pngFlavour;
        /*
        * When a clipboard image has a moderate amount of colours
        * or complexity, trying to get a png hangs, with "owner
        * timed out" for Clipboard#getData.
        *
        * I tried to work around this by taking the Image DataFlavor
        * instead, which I could write into the file as png using
        * ImageIO. However, getData also hangs for that for
        * relevant images. I tried to take clipboard ownership of
        * the transferable, getContents also hangs.
        *
        * It's safe to assume that images with transparency are
        * right out under Java's clipboard API at the moment, at
        * least on my system. jpeg and gif work fine, but they lack
        * transparency.
        */
        
        return chosenFlavour;
    }

//   -  -%-  -

    public String
    getTransferData(DataFlavor flavour)
    {
        assert flavour == DataFlavor.stringFlavor;
        return string;
    }

    public DataFlavor[]
    getTransferDataFlavors()
    {
        return new DataFlavor[] { DataFlavor.stringFlavor };
        /*
        * We could support javaJVMLocalObjectMimeType to transfer
        * structs from RichTextPane4 to the compose window. But
        * the present copy which serialises to a string is good
        * enough, it's not like the compose window can render the
        * custom emojis, it'll need a string anyways.
        */
    }

    public boolean
    isDataFlavorSupported(DataFlavor flavour)
    {
        return flavour == DataFlavor.stringFlavor;
    }

    public void
    lostOwnership(Clipboard clipboard, Transferable contents) { }

}
