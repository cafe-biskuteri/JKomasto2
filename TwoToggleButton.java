
import javax.swing.AbstractButton;
import javax.swing.DefaultButtonModel;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import java.util.List;
import java.util.ArrayList;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Shape;
import java.awt.Rectangle;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import java.util.concurrent.TimeoutException;
import java.io.IOException;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleState;
import javax.accessibility.AccessibleRole;
import javax.accessibility.AccessibleAction;

import javax.swing.JFrame;

class
TwoToggleButton extends AbstractButton
implements
    KeyListener, MouseListener, FocusListener, Accessible,
    ImageAPI2.Listener {

    private PriorityExecutor
    imageAPIExecutor;

    private String
    primaryName,
    secondaryName;

//   -  -%-  -

    private boolean
    primaryToggled,
    secondaryToggled;

    private Image
    primaryToggledIcon,
    secondaryToggledIcon,
    primaryUntoggledIcon,
    secondaryUntoggledIcon;

    private AccessibleContext
    access;

//   -  -%-  -

    private static Image
    buttonUp,
    buttonDown,
    selectedOverlay,
    disabledOverlay;

//  ---%-@-%---

    public void
    setPrimaryToggled(boolean n) { primaryToggled = n; }

    public void
    setSecondaryToggled(boolean n) { secondaryToggled = n; }

    public boolean
    getPrimaryToggled() { return primaryToggled; }

    public boolean
    getSecondaryToggled() { return secondaryToggled; }

    public void
    togglePrimary()
    {
        setPrimaryToggled(!primaryToggled);
        announce(primaryName, primaryToggled);
    }

    public void
    toggleSecondary()
    {
        setSecondaryToggled(!secondaryToggled);
        announce(secondaryName, secondaryToggled);
    }

//   -  -%-  -

    private void
    announce(String name, boolean toggled)
    {
        String command = name + (toggled ? "On" : "Off");
        getModel().setActionCommand(command);
        doClick();

        /*
        * We need to implement AccessibleValue then announce
        * a change in that here. Changing the name or
        * description is inappropriate because those are
        * reserved for whoever constructs us.
        */
    }


    protected void
    paintComponent(Graphics g)
    {
        if (getModel().isArmed())
            g.drawImage(buttonDown, 0, 0, this);
        else
            g.drawImage(buttonUp, 0, 0, this);

        if (secondaryToggled)
            g.drawImage(secondaryToggledIcon, 0, 0, this);
        else
            g.drawImage(secondaryUntoggledIcon, 0, 0, this);
        if (primaryToggled)
            g.drawImage(primaryToggledIcon, 0, 0, this);
        else
            g.drawImage(primaryUntoggledIcon, 0, 0, this);

        if (!isEnabled())
            g.drawImage(disabledOverlay, 0, 0, this);
        if (isFocusOwner())
            g.drawImage(selectedOverlay, 0, 0, this);
    }


    public void
    mousePressed(MouseEvent eM)
    {
        ButtonModel model = getModel();
        model.setArmed(true);
        model.setPressed(true);
    }

    public void
    mouseReleased(MouseEvent eM)
    {
        ButtonModel model = getModel();
        if (model.isArmed())
        {
            boolean shift = eM.isShiftDown();
            boolean prim = eM.getButton() == MouseEvent.BUTTON1;
            boolean secon = eM.getButton() == MouseEvent.BUTTON3;
            secon |= shift && prim;

            if (secon) toggleSecondary();
            else if (prim) togglePrimary();

            requestFocusInWindow();
        }
        model.setArmed(false);
        model.setPressed(false);
    }

    public void
    mouseEntered(MouseEvent eM)
    {
        ButtonModel model = getModel();
        model.setRollover(true);
        model.setArmed(model.isPressed());
    }

    public void
    mouseExited(MouseEvent eM)
    {
        ButtonModel model = getModel();
        model.setRollover(false);
        model.setArmed(false);
    }

    public void
    keyPressed(KeyEvent eK)
    {
        boolean shift = eK.isShiftDown();
        boolean prim = eK.getKeyCode() == KeyEvent.VK_SPACE;
        boolean secon = eK.getKeyCode() == KeyEvent.VK_ENTER;
        secon |= shift && prim;

        if (secon) toggleSecondary();
        else if (prim) togglePrimary();

        requestFocusInWindow();
    }

    public void
    focusGained(FocusEvent eF)
    {
        access.firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            null, AccessibleState.FOCUSED);
        repaint();
    }

    public void
    focusLost(FocusEvent eF)
    {
        access.firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            AccessibleState.FOCUSED, null);
        repaint();
    }

    public AccessibleContext
    getAccessibleContext() { return access; }

    public boolean
    imageUpdate(Image i, int f, int x, int y, int w, int h)
    {
        // Copied from RoundButton, I don't want it to
        // cause any shenanigans here either.
        if ((f & (ALLBITS|FRAMEBITS)) != 0)
        {
            repaint();
            return f != ALLBITS;
        }
        return ((f & (ERROR|ABORT)) == 0);
    }


    public void
    mouseClicked(MouseEvent eM) { }

    public void
    keyTyped(KeyEvent eK) { }

    public void
    keyReleased(KeyEvent eK) { }

//  ---%-@-%---

    private class
    Access extends AccessibleAbstractButton
    implements AccessibleAction {

        public AccessibleRole
        getAccessibleRole() { return AccessibleRole.PUSH_BUTTON; }

        public AccessibleAction
        getAccessibleAction() { return this; }

        public int
        getAccessibleActionCount() { return 2; }

        public String
        getAccessibleActionDescription(int o)
        {
            if (o == 1) return secondaryName;
            else return primaryName;
        }

        public boolean
        doAccessibleAction(int o)
        {
            if (o == 1) toggleSecondary();
            else togglePrimary();
            return true;
        }

    }

//  ---%-@-%---

    TwoToggleButton(
        String primaryName, String secondaryName)
    {
        assert primaryName != null;
        assert secondaryName != null;
        assert !primaryName.equals(secondaryName);
        this.primaryName = primaryName;
        this.secondaryName = secondaryName;

        if (buttonUp == null) loadCommonImages();

        access = new Access();

        primaryToggled = false;
        secondaryToggled = false;

        setModel(new DefaultButtonModel());
        setFocusable(true);
        setOpaque(false);

        int w = buttonUp.getWidth(null);
        int h = buttonUp.getHeight(null);
        setPreferredSize(new Dimension(w, h));
        loadSpecificImages();
        // Notably we only do this once, and without scaling.

        this.addKeyListener(this);
        this.addMouseListener(this);
        this.addFocusListener(this);
    }

//   -  -%-  -

    private void
    loadSpecificImages()
    {
        String[] names = new String[] {
            primaryName + "Toggled",
            primaryName + "Untoggled",
            secondaryName + "Toggled",
            secondaryName + "Untoggled" };

        for (String name: names) ImageAPI2
            .fetchNow(
                ImageAPI2.URLs.local(name),
                "specific:" + name, 0, this)
            .run();
    }

    private void
    loadCommonImages()
    {
        String[] names = new String[] {
            "buttonUp",
            "buttonDown",
            "disabledOverlay",
            "selectedOverlay" };

        for (String name: names) ImageAPI2
            .fetchNow(
                ImageAPI2.URLs.local(name),
                "common:" + name, 0, this)
            .run();
    }

    public void
    imageReady(String name, Image image)
    {
        if (name.startsWith("specific:"))
        {
            name = name.substring("specific:".length());
            if (name.equals(primaryName + "Toggled"))
                primaryToggledIcon = image;
            if (name.equals(primaryName + "Untoggled"))
                primaryUntoggledIcon = image;
            if (name.equals(secondaryName + "Toggled"))
                secondaryToggledIcon = image;
            if (name.equals(secondaryName + "Untoggled"))
                secondaryUntoggledIcon = image;
        }
        if (name.startsWith("common:"))
        {
            name = name.substring("common:".length());
            if (name.equals("buttonUp"))
                buttonUp = image;
            if (name.equals("buttonDown"))
                buttonDown = image;
            if (name.equals("disabledOverlay"))
                disabledOverlay = image;
            if (name.equals("selectedOverlay"))
                selectedOverlay = image;
        }
    }

}


class
RoundButton extends AbstractButton
implements
    KeyListener, MouseListener, FocusListener,
    Accessible, ImageFetcher.Listener, ImageAPI2.Listener {

    private ImageFetcher
    fetcher;

    private Image
    image;

    private boolean
    gif;

//   -  -%-  -

    private Image
    scaledImage;

    private AccessibleContext
    access;

//   -  -%-  -

    private static Image
    buttonUp,
    buttonDown,
    selectedOverlay,
    disabledOverlay,
    loadingOverlay;

    private static Shape
    roundClip;

//  ---%-@-%---

    public void
    setImage(Image image, boolean gif)
    {
        if (scaledImage != null && scaledImage != image)
            scaledImage.flush();

        this.image = image;
        scaledImage = null;
        this.gif = gif;
        considerRescale();
        setToolTipText(
            ImageApi.debugString(this.image, scaledImage));
        repaint();
    }

//     -    -%-     -

    private void
    considerRescale()
    {
        if (image != null)
        {
            if (!gif)
            {
                Rectangle b = roundClip.getBounds();
                assert b.width > 0 && b.height > 0;

                Dimension n = ImageApi.fillFit(
                    image.getWidth(null),
                    image.getHeight(null),
                    b.width, b.height);
                /*
                * When this goes down into paintComponent to
                * be fill-fitted again, it should result
                * in the exact same dimensions. That fillFit
                * in paintComponent is really for cases where
                * we use the image as scaledImage.
                */

                String name = "" + image.hashCode();
                fetcher.scale(
                    image, n.width, n.height,
                    name, 2, this);
            }
            else
            {
                scaledImage = image;
            }
        }
    }

    public void
    imageReady(String name, Image image)
    {
        if (name.startsWith("common:"))
        {
            name = name.substring("common:".length());
            if (name.equals("buttonUp"))
                buttonUp = image;
            if (name.equals("buttonDown"))
                buttonDown = image;
            if (name.equals("disabledOverlay"))
                disabledOverlay = image;
            if (name.equals("selectedOverlay"))
                selectedOverlay = image;
            if (name.equals("loadingOverlay"))
                loadingOverlay = image;
            return;
        }

        if (RoundButton.this.image == null) return;
        String expected = "" + RoundButton.this.image.hashCode();
        if (!name.equals(expected)) return;

        scaledImage = image;
        setToolTipText(ImageApi
            .debugString(this.image, scaledImage));
        repaint();
    }

    protected void
    paintComponent(Graphics g)
    {
        Rectangle b = roundClip.getBounds();

        if (getModel().isArmed())
            g.drawImage(buttonDown, 0, 0, null);
        else
            g.drawImage(buttonUp, 0, 0, null);

        if (!isEnabled())
            g.drawImage(disabledOverlay, 0, 0, null);
        if (isFocusOwner())
            g.drawImage(selectedOverlay, 0, 0, null);

        if (image != null && scaledImage == null)
            g.drawImage(loadingOverlay, 0, 0, null);

        if (scaledImage != null)
        {
            if (!prepareImage(scaledImage, null))
                g.drawImage(loadingOverlay, 0, 0, null);

            Dimension n = ImageApi.fillFit(
                scaledImage.getWidth(null),
                scaledImage.getHeight(null),
                b.width, b.height);
            int x = b.x + (b.width - n.width) / 2;
            int y = b.y + (b.height - n.height) / 2;
            Shape defaultClip = g.getClip();
            g.setClip(roundClip);
            g.drawImage(
                scaledImage, x, y, n.width, n.height,
                gif ? this : null);
            g.setClip(defaultClip);
        }
    }

    public void
    keyPressed(KeyEvent eK)
    {
        if (eK.getKeyCode() != KeyEvent.VK_SPACE) return;
        doClick();
    }

    public void
    mousePressed(MouseEvent eM)
    {
        getModel().setArmed(true);
        getModel().setPressed(true);
    }

    public void
    mouseReleased(MouseEvent eM)
    {
        if (getModel().isArmed())
        {
            doClick();
        }
        getModel().setArmed(false);
        getModel().setPressed(false);
    }

    public void
    mouseEntered(MouseEvent eM)
    {
        getModel().setRollover(true);
        getModel().setArmed(getModel().isPressed());
    }

    public void
    mouseExited(MouseEvent eM)
    {
        getModel().setRollover(false);
        getModel().setArmed(false);
    }


    public void
    focusGained(FocusEvent eF)
    {
        access.firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            null, AccessibleState.FOCUSED
        );
        repaint();
    }

    public void
    focusLost(FocusEvent eF)
    {
        access.firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            AccessibleState.FOCUSED, null
        );
        repaint();
    }

    public AccessibleContext
    getAccessibleContext() { return access; }

    public boolean
    imageUpdate(Image i, int f, int x, int y, int w, int h)
    {
        /*
        * Revert AbstractButton's override, we need to be
        * repainted when we're rendering a GIF. I am not
        * going to bother setting the current scaledImage
        * as the icon, given ImageIcon's blocking behaviour.
        */
        if ((f & (ALLBITS|FRAMEBITS)) != 0) {
            repaint();
            return f != ALLBITS;
        }
        return ((f & (ERROR|ABORT)) == 0);
    }


    public void
    mouseClicked(MouseEvent eM) { }

    public void
    keyReleased(KeyEvent eK) { }

    public void
    keyTyped(KeyEvent eK) { }

//  ---%-@-%---

    private class
    Access extends AccessibleAbstractButton
    implements AccessibleAction {

        public AccessibleRole
        getAccessibleRole() { return AccessibleRole.PUSH_BUTTON; }

        public AccessibleAction
        getAccessibleAction() { return this; }

        public int
        getAccessibleActionCount() { return 1; }

        public String
        getAccessibleActionDescription(int o)
        {
            return getAccessibleDescription();
        }

        public boolean
        doAccessibleAction(int o)
        {
            //doClick();
            return true;
        }

    }

//  ---%-@-%---

    RoundButton(ImageFetcher fetcher)
    {
        assert fetcher != null;
        this.fetcher = fetcher;

        access = new Access();

        if (buttonUp == null) loadCommonImages();
        
        setModel(new DefaultButtonModel());
        setFocusable(true);
        setOpaque(false);
        setIcon(null);

        int w = buttonUp.getWidth(null);
        int h = buttonUp.getHeight(null);
        setPreferredSize(new Dimension(w, h));

        this.addKeyListener(this);
        this.addMouseListener(this);
        this.addFocusListener(this);
    }

//   -  -%-  -

    private void
    loadCommonImages()
    {
        String[] names = new String[] {
            "buttonUp", "buttonDown",
            "disabledOverlay", "selectedOverlay",
            "loadingOverlay" };

        for (String name: names) ImageAPI2
            .fetchNow(
                ImageAPI2.URLs.local(name),
                "common:" + name, 0, this)
            .run();

        int radius = 6;
        roundClip = new Ellipse2D.Float(
            radius,
            radius,
            buttonUp.getWidth(null) - (2 * radius),
            buttonUp.getHeight(null) - (2 * radius));
    }

}


class
TwoToggleButtonTest {

    public static void
    main(String... args)
    {
        if (args.length != 2)
        {
            String err = "Please give two toggle names.";
            System.err.println(err);
            System.exit(1);
        }

        String p = args[0], s = args[1];
        TwoToggleButton t1 = new TwoToggleButton(p, s);
        TwoToggleButton t2 = new TwoToggleButton(p, s);
        TwoToggleButton t3 = new TwoToggleButton(p, s);

        JFrame mainframe = new JFrame("Two-toggle button test");
        mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainframe.setLocationByPlatform(true);
        mainframe.add(t1, BorderLayout.WEST);
        mainframe.add(t2);
        mainframe.add(t3, BorderLayout.EAST);
        mainframe.pack();
        t2.setEnabled(false);
        t3.requestFocusInWindow();
        mainframe.setVisible(true);
    }

}
