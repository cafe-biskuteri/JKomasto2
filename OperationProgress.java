
import java.util.LinkedHashMap;
import java.util.Collection;
import java.util.HashSet;

class
OperationProgress {

    private LinkedHashMap<String, Boolean>
    steps;

    private Collection<Listener>
    listeners;

//  ---%-@-%---

    public void
    add(String stepName)
    {
        steps.put(stepName, false);
    }

    public boolean
    get(String stepName) { return steps.get(stepName); }

    public void
    markComplete(String stepName)
    {
        steps.put(stepName, true);
        for (Listener listener: listeners)
            listener.stepComplete(this, stepName);
    }

    public void
    reset()
    {
        for (String stepName: steps.keySet())
            steps.put(stepName, false);
    }

    public void
    addListener(Listener l) { listeners.add(l); }

    public void
    removeListener(Listener l) { listeners.remove(l); }

//  ---%-@-%---

    public static interface
    Listener {

        public void
        stepComplete(OperationProgress progress, String stepName);
        
    }

//  ---%-@-%---

    public
    OperationProgress()
    {
        steps = new LinkedHashMap<>();
        listeners = new HashSet<>();
    }

}