
import cafe.biskuteri.hinoki.Tree;
import java.io.IOException;

interface
ServerSideEventsListener {

    void
    connectionFailed(IOException eIo);

    void
    requestFailed(int httpCode, Tree<String> json);

    void
    lineReceived(String line);

}

class
RequestOutcome {

    Exception
    exception;

    int
    httpCode;

    Tree<String>
    errorEntity;

    Tree<String>
    entity;

}