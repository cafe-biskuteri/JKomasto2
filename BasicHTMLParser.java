
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import cafe.biskuteri.hinoki.Tree;
import cafe.biskuteri.hinoki.PatternParser;

public interface
BasicHTMLParser {

    public static Tree<String>
    parse(String html)
    throws InvalidHTMLException
    {
        Tree<String> document = new Tree<>("html", html);
        document = separateTagBoundaries(document);
        document = recogniseTaglikes(document);
        document = separateWhitespaces(document);
        document = recogniseTags(document);
        document = evaluateHtmlEscapes(document);
        document = separateShortcodes(document);
        document = hierarchise(document);
        return document;
    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    private static Tree<String>
    separateTagBoundaries(Tree<String> document)
    {
        assert document.key.equals("html");
        assert document.size() == 0;
        assert document.value != null;
        
        List<PatternParser.Pattern> patterns = new ArrayList<>();
        patterns.add(new TagBoundariesPattern());

        Tree<String> returnee = new Tree<>("tokens", null);
        returnee.children.addAll(PatternParser
            .parse(document.value, patterns));

        return returnee;
    }
    
    private static Tree<String>
    recogniseTaglikes(Tree<String> document)
    {
        assert document.key.equals("tokens");
        assert document.size() >= 0;
        assert document.value == null;
        
        Tree<String> returnee = new Tree<>("tokens", null);
        
        boolean inTaglike = false;
        Tree<String> taglike = null;
        for (Tree<String> token: document)
        if (inTaglike)
        {
            if (token.key.equals("tag boundary"))
            if (token.value.equals(">"))
            {
                inTaglike = false;
                taglike.add(token);
                returnee.add(taglike);
                taglike = null;
                continue;
            }
                
            taglike.add(token);
        }
        else
        {
            if (token.key.equals("tag boundary"))
            if (token.value.equals("<"))
            {
                inTaglike = true;
                taglike = new Tree<>("taglike", null);
                taglike.add(token);
                continue;
            }
            
            returnee.add(token);
        }
        
        if (inTaglike)
        {
            inTaglike = false;
            returnee.children.addAll(taglike.children);
            taglike = null;
        }
        
        return returnee;
    }

    private static Tree<String>
    separateWhitespaces(Tree<String> document)
    {
        Tree<String> returnee = new Tree<>("tokens", null);

        List<PatternParser.Pattern> forTags = new ArrayList<>();
        List<PatternParser.Pattern> forText = new ArrayList<>();
        forTags.add(new WhitespacePattern(true));
        forText.add(new WhitespacePattern(false));

        for (Tree<String> token: document)
        if (token.key.equals("taglike"))
        {
            Tree<String> addee = new Tree<>("taglike", null);

            for (Tree<String> subtoken: token)
            if (subtoken.key.equals("text"))
                addee.children.addAll(PatternParser
                    .parse(subtoken.value, forTags));
            else addee.add(subtoken);

            returnee.add(addee);
        }
        else if (token.key.equals("text"))
        {
            returnee.children.addAll(PatternParser
                .parse(token.value, forText));
        }
        else assert false;

        return returnee;
    }

    private static Tree<String>
    recogniseTags(Tree<String> document)
    {
        Tree<String> returnee = new Tree<>("tokens", null);
        
        for (Tree<String> token: document)
        if (token.key.equals("taglike"))
        {
            assert token.size() >= 2;
            int lo = token.size() - 1;
            assert token.get(0).key.equals("tag boundary");
            assert token.get(0).value.equals("<");
            assert token.get(lo).key.equals("tag boundary");
            assert token.get(lo).value.equals(">");
            
            Tree<String> tag = new Tree<>("tag", null);
            int o; for (o = 1; o < lo; ++o)
            if (o == 1)
            {
                if (token.get(o).key.equals("space")) break;
                
                String tagName = token.get(o).value;
                assert tagName.equals(tagName.trim());

                boolean valid =
                    Character.isLetter(tagName.charAt(0))
                    || tagName.charAt(0) == '/';
                if (!valid) break;
                
                tag.add(new Tree<>(tagName, null));
            }
            else
            {
                if (token.get(o).key.equals("space")) continue;
                if (token.get(o).key.equals("tag boundary")) break;
                
                String attr = token.get(o).value;
                assert attr.equals(attr.trim());

                int oEq = attr.indexOf('=');
                String name =
                    oEq == -1 ? attr : attr.substring(0, oEq);
                String value =
                    oEq == -1 ? null : attr.substring(oEq + 1);
                if (value != null)
                {
                    boolean quoted =
                        value.startsWith("\"")
                        && value.endsWith("\"");
                    if (quoted) value = value
                        .substring(1, value.length() - 1);
                }
                if (value != null && value.isEmpty()) value = null;
                // This ordering actually has consequences.
                // Nulling for empty values occurs after dequoting.
                
                tag.add(new Tree<>(name, value));
            }

            if (o == lo)
            {
                tag.value = derecognise(token).value;
                returnee.add(tag);
            }
            else returnee.add(derecognise(token));
        }
        else returnee.add(token);
        
        return returnee;
    }
        
    private static Tree<String>
    evaluateHtmlEscapes(Tree<String> document)
    {
        for (Tree<String> token: document)
        {
            token.value = evaluateHtmlEscapes(token.value);
            for (Tree<String> subtoken: token)
            {
                subtoken.key =
                    evaluateHtmlEscapes(subtoken.key);
                subtoken.value =
                    evaluateHtmlEscapes(subtoken.value);
            }
        }
    
        return document;
    }
    
    private static Tree<String>
    separateShortcodes(Tree<String> document)
    {
        Tree<String> returnee = new Tree<>("tokens", null);
        
        List<PatternParser.Pattern> patterns = new ArrayList<>();
        patterns.add(new ShortcodePattern());
        
        for (Tree<String> token: document)
        if (token.key.equals("text"))
        {
            returnee.children.addAll(PatternParser
                .parse(token.value, patterns));
        }
        else returnee.add(token);
        
        return returnee;
    }
    
    private static Tree<String>
    hierarchise(Tree<String> document)
    {
        assert document.key.equals("tokens");
        assert document.value == null;
        assert document.size() >= 0;
        
        Tree<String> root = new Tree<String>();
        root.key = "tag";
        root.add(new Tree<>("html", null));
        root.add(new Tree<>("children", null));
        
        Stack<Tree<String>> parents = new Stack<>();
        parents.push(root);
        for (Tree<String> node: document)
        if (node.key.equals("tag"))
        {
            assert node.size() > 0;
            assert node.get("children") == null;
            
            String tagName = node.get(0).key;
            tagName = tagName.toLowerCase();
            node.add(new Tree<>("children", null));
    
            boolean selfClosing = node.get("/") != null;
            selfClosing |= tagName.equals("br");
            selfClosing |= tagName.equals("!doctype");
            selfClosing |= tagName.equals("meta");
            selfClosing |= tagName.equals("!--");
            if (selfClosing)
            {
                // Even if this node is a tag, it won't have any
                // contents, so add it straight into current tag.
                Tree<String> currentTag = parents.peek();
                currentTag.get("children").add(node);
                continue;
            }
    
            boolean isClosing = tagName.startsWith("/");
            if (isClosing)
            {
                // This closing tag is meant to finish the
                // current tag, pushing it into the grandparent.
                
                assert parents.size() > 1;
                Tree<String> parent = parents.pop();
                Tree<String> grandparent = parents.peek();
    
                // One second, quick check.
                String pTagName = parent.get(0).key;
                pTagName = pTagName.toLowerCase();
                if (!tagName.equals("/" + pTagName))
                {
                    // This closing tag doesn't match the current
                    // tag - it's misplaced. Derecognise it as our
                    // error handling measure.
                    parent.get("children").add(derecognise(node));
                    parents.push(parent);
                    continue;
                }
                
                grandparent.get("children").add(parent);
                continue;
            }
            
            // Does not close. We're entering this tag now.
            parents.push(node);
        }
        else
        {
            Tree<String> currentTag = parents.peek();
            currentTag.get("children").add(node);
        }

        assert parents.size() == 1;
        return parents.pop();
    }
    
//   -      -%-      -

    private static String
    evaluateHtmlEscapes(String string)
    {
        if (string == null) return string;

        StringBuilder whole = new StringBuilder();
        StringBuilder part = new StringBuilder();
        boolean inEscape = false;
        for (char c: string.toCharArray())
        {
            if (inEscape && c == ';')
            {
                part.append(c);
                inEscape = false;
                String v = part.toString();
                part.setLength(0);
                if (v.equals("&lt;")) part.append('<');
                if (v.equals("&gt;")) part.append('>');
                if (v.equals("&amp;")) part.append('&');
                if (v.equals("&quot;")) part.append('"');
                if (v.equals("&apos;")) part.append('\'');
                if (v.equals("&#39;")) part.append('\'');
            }
            else if (!inEscape && c == '&')
            {
                String v = part.toString();
                part.setLength(0);
                if (!v.isEmpty()) whole.append(v);
                part.append(c);
                inEscape = true;
            }
            else
            {
                part.append(c);
            }
        }
        /*
        * It's not clear to me now why it does this partitioning
        * of buffers, with part added to whole only upon another
        * escape starting. It seems like part is having dual usage
        * as buffer for both non-escape and escape parts but,
        * wouldn't straightforward appending work fine. Won't touch
        * it though.
        */
        String v = part.toString();
        part.setLength(0);
        if (!v.isEmpty()) whole.append(v);
        return whole.toString();
    }
    
    private static Tree<String>
    derecognise(Tree<String> compositeToken)
    {
        StringBuilder b = new StringBuilder();
        for (Tree<String> token: compositeToken)
            b.append(token.value);
    
        return new Tree<>("text", b.toString());
    }
    
    
    
// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    static class
    TagBoundariesPattern implements PatternParser.Pattern {
        
        private String
        string;

        private int[]
        quoteRegions;

//      -=%=-

        public String
        patternName() { return "tag boundary"; }
        
        public int
        patternRecognise(String string, int so)
        {
            if (string != this.string) computeQuoteRegions(string);

            char c = string.charAt(so);
            boolean boundary = c == '<' || c == '>';
            boolean quoted = quoteRegions[so] > 0;

            if (boundary && !quoted) return so + 1;
            return -1;
        }

//       -=-

        private void
        computeQuoteRegions(String string)
        {
            this.string = string;

            quoteRegions = new int[string.length()];
            boolean inTag = false;
            int curr = 0;
            for (int o = 0; o < string.length(); ++o)
            if (curr == 2)
            {
                char c = string.charAt(o);
                quoteRegions[o] = 2;
                if (c == '"') curr = 0;
            }
            else if (curr == 1)
            {
                char c = string.charAt(o);
                quoteRegions[o] = 1;
                if (c == '\'') curr = 0;
            }
            else
            {
                char c = string.charAt(o);
                if (c == '<') inTag = true;
                if (c == '>') inTag = false;
                if (c == '"' && inTag) curr = 2;
                if (c == '\'' && inTag) curr = 1;
                quoteRegions[o] = curr;
            }
        }
        
    }
    
    static class
    WhitespacePattern implements PatternParser.Pattern {

        private boolean
        respectQuotes;

//       -=-

        private String
        string;

        private int[]
        quoteRegions;

//      -=%=-

        public String
        patternName() { return "space"; }
        
        public int
        patternRecognise(String string, int so)
        {
            if (string != this.string) computeQuoteRegions(string);

            char c = string.charAt(so);
            boolean space = Character.isWhitespace(c);
            boolean quoted = quoteRegions[so] > 0;
            if (!space || quoted) return -1;

            int eo; for (eo = so + 1; eo < string.length(); ++eo)
            if (!Character.isWhitespace(string.charAt(eo))) break;

            return eo;
        }

//       -=-

        private void
        computeQuoteRegions(String string)
        {
            this.string = string;

            quoteRegions = new int[string.length()];

            if (respectQuotes)
            for (int o = 0, curr = 0; o < string.length(); ++o)
            if (curr == 2)
            {
                char c = string.charAt(o);
                quoteRegions[o] = 2;
                if (c == '"') curr = 0;
            }
            else if (curr == 1)
            {
                char c = string.charAt(o);
                quoteRegions[o] = 1;
                if (c == '\'') curr = 0;
            }
            else
            {
                char c = string.charAt(o);
                if (c == '"') curr = 2;
                if (c == '\'') curr = 1;
                quoteRegions[o] = curr;
            }
        }

//      -=%=-

        WhitespacePattern(boolean respectQuotes)
        {
            this.respectQuotes = respectQuotes;
        }
        
    }
    
    class
    ShortcodePattern implements PatternParser.Pattern {
        
        public String
        patternName() { return "emoji"; }
        
        public int
        patternRecognise(String string, int so)
        {
            if (string.charAt(so) != ':') return -1;

            for (int eo = so + 1; eo < string.length(); ++eo)
            {
                char c = string.charAt(eo);
                if (Character.isWhitespace(c)) return -1;
                if (c == ':') return eo + 1;
            }
            return -1;
        }
    }

}

class
InvalidHTMLException extends Exception {

    InvalidHTMLException(String message) { super(message); }

}
