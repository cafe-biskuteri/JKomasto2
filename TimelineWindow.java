
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuBar;
import javax.swing.ButtonGroup;
import javax.swing.MenuElement;
import javax.swing.JSeparator;
import javax.swing.JRadioButton;
import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.Cursor;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.KeyboardFocusManager;
import java.awt.AWTKeyStroke;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Comparator;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleAction;
import javax.accessibility.AccessibleText;
import javax.accessibility.AccessibleState;
import java.net.URL;

import cafe.biskuteri.hinoki.Tree;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeParseException;
import javax.accessibility.AccessibleRole;
import javax.accessibility.AccessibleStateSet;
import java.io.UnsupportedEncodingException;

class
TimelineWindow extends JFrame
implements
    Scalable, ActionListener, WindowListener,
    ImageFetcher.Listener {

    private JKomasto
    primaire;

    private MastodonApi
    api;

    private TimelineWindowUpdater
    windowUpdater;

    private ImageFetcher
    fetcher;

    private volatile TimelinePage
    page;

//  -=- --- -%- --- -=-

    private final Map<Post, Integer>
    categories;

    private TimelineComponent
    display;

    private JMenuItem
    openHome,
    openMessages,
    openLocal,
    openFederated,
    openNotifications,
    openOwnProfile,
    openProfile,
    restartUpdaters,
    createPost,
    openAutoMediaView,
    openAutoPostView,
    openSettings;

    private final List<JMenuItem>
    openList;

    private JMenuItem
    flipToNewestPost,
    useChronological,
    useCategories;

    private volatile int
    currentOperationId;

    private boolean
    showingLatest,
    showingLast;

    private String
    sBoost;

    private int
    scale;

    private Image
    scaledPattern;

    private boolean
    rescaling;

//  -=- --- -%- --- -=-

    private static Image
    pattern;

    private static final int
    PREVIEW_COUNT = TimelineComponent.PREVIEW_COUNT;

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);


// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    public void
    use(TimelinePage newPage, boolean isLatest, boolean isLast)
    {
        assert newPage != null;
        /*
        * Our code promises that a timeline page cannot be
        * malformed. The entity for it is a container of post
        * entities. If one of the post entities is malformed,
        * that offset within the page is null, but the page
        * itself isn't.
        */

        int operationId = ++currentOperationId;

        display.setCursor(WAIT);

        categories.clear();
        categories.put(null, 1);
        for (Post post: newPage.posts)
        {
            if (post == null) continue;

            boolean isBoost = post.boostedPost != null;

            boolean inConversation = false;
            boolean inChain = false;
            for (Post p: newPage.posts)
            {
                if (p == null) continue;

                boolean isReply = post.id.equals(p.parentId);
                boolean isParent = p.id.equals(post.parentId);
                if (isReply || isParent)
                {
                    inChain = post.author.id.equals(p.author.id);
                    inConversation = !inChain;
                }

                if (isReply || isParent) break;
            }

            if (inConversation) categories.put(post, 3);
            else if (inChain) categories.put(post, 2);
            else if (isBoost) categories.put(post, 0);
            else categories.put(post, 1);
        }

        if (useCategories.isSelected())
            Arrays.sort(
                newPage.posts,
                new Comparator<Post>() {
                    public int
                    compare(Post a, Post b)
                    {
                        int f1 =
                            -categories.get(a)
                                .compareTo(categories.get(b));
                        int f2 = -a.author.id.compareTo(b.author.id);
                        int f3 = -a.dateTime.compareTo(b.dateTime);

                        if (f1 != 0) return f1;

                        if (categories.get(a) >= 2) return f3;
                        else return f2 != 0 ? f2 : f3;
                    }
                }
            );
        /*
        * This mutates page.posts to the category sorting,
        * if we switch back to chronological mode isn't it
        * fair to sort it back, or to use a separate list for
        * sorted posts? (Not a big deal though, right now
        * we demand the user reload the page after
        * changing the sorting.)
        */

        if (currentOperationId != operationId) return;

        page = newPage;
        showingLatest = isLatest;
        showingLast = isLast;
        syncDisplayToPage();

        if (currentOperationId != operationId) return;

        currentOperationId = 0;
        display.setCursor(null);
        display.changeBanner();
    }

    public String
    summary(Post p)
    {
        if (p.boostedPost != null)
            return summary(p.boostedPost);

        p.resolveApproximateText();
        String s = p.approximateText;
        if (p.contentWarning != null) s = p.contentWarning;
        return s.length() > 16 ? s.substring(0, 16) : s;
    }

    public void
    showLatestPage()
    {
        assert page != null;
        assert page.posts != null;

        int operationId = ++currentOperationId;

        display.setCursor(WAIT);

        RequestOutcome r = api.getTimelinePage(
            page.type,
            PREVIEW_COUNT, null, null,
            page.accountNumId, page.listId);

        if (currentOperationId != operationId) return;

        if (r.entity == null)
        {
            genericRequestFailed(r, "timeline fetch fail");
            display.setCursor(null);
            return;
        }

        boolean isLast = r.entity.size() < PREVIEW_COUNT;
        TimelinePage newPage = new TimelinePage(
            page.type, page.accountNumId, page.listId,
            r.entity);

        use(newPage, true, isLast);
    }

    public void
    showNextPage()
    {
        assert page != null;
        assert page.posts != null;
        assert page.posts.length == PREVIEW_COUNT;

        int operationId = ++currentOperationId;

        display.setCursor(WAIT);

        String oldestId = page.posts[0].id;
        for (Post post: page.posts)
            if (post.id.compareTo(oldestId) < 0)
                oldestId = post.id;

        RequestOutcome r = api.getTimelinePage(
            page.type,
            PREVIEW_COUNT, oldestId, null,
            page.accountNumId, page.listId);

        if (currentOperationId != operationId) return;

        if (r.entity == null)
        {
            genericRequestFailed(r, "timeline fetch fail");
            display.setCursor(null);
            return;
        }
        if (r.entity.size() == 0)
        {
            use(this.page, showingLatest, true);
            display.setCursor(null);
            return;
        }

        boolean isLast = r.entity.size() < PREVIEW_COUNT;
        TimelinePage newPage = new TimelinePage(
            page.type, page.accountNumId, page.listId,
            r.entity);

        use(newPage, false, isLast);
    }

    public void
    showPreviousPage()
    {
        assert page != null;
        assert page.posts != null;

        if (page.posts.length == 0)
        {
            showLatestPage();
            return;
        }

        int operationId = ++currentOperationId;

        display.setCursor(WAIT);

        String latestId = page.posts[0].id;
        for (Post post: page.posts)
            if (post.id.compareTo(latestId) > 0)
                latestId = post.id;

        RequestOutcome r = api.getTimelinePage(
            page.type,
            PREVIEW_COUNT, null, latestId,
            page.accountNumId, page.listId);

        if (currentOperationId != operationId) return;

        if (r.entity == null)
        {
            genericRequestFailed(r, "timeline fetch fail");
            display.setCursor(null);
            return;
        }
        if (r.entity.size() == 0)
        {
            use(this.page, true, showingLast);
            display.setCursor(null);
            return;
        }
        if (r.entity.size() < PREVIEW_COUNT)
        {
            showLatestPage();
            return;
        }

        boolean isLatest = r.entity.size() < PREVIEW_COUNT;
        boolean isLast = r.entity.size() < PREVIEW_COUNT;
        TimelinePage newPage = new TimelinePage(
            page.type, page.accountNumId, page.listId,
            r.entity);

        use(newPage, isLatest, isLast);
    }

    public void
    switchToTimeline(TimelineType type)
    {
        assert type != TimelineType.LIST;
        assert type != TimelineType.PROFILE;

        int operationId = ++currentOperationId;

        display.setCursor(WAIT);

        TimelinePage newPage = new TimelinePage();
        newPage.type = type;
        newPage.accountNumId = null;
        newPage.listId = null;
        newPage.posts = new Post[0];

        if (currentOperationId != operationId) return;

        page = newPage;
        showLatestPage();

        String t = JapaneseStrings.get("timeline title template");
        String s = null; switch (newPage.type)
        {
            case FEDERATED: s = "federated"; break;
            case LOCAL: s = "local"; break;
            case HOME: s = "home"; break;
            default: assert false;
        }
        String n = JapaneseStrings.get(s + " in title");
        assert t.indexOf("<>") != -1;
        if (t.indexOf("<>") > 0) n = n.toLowerCase();
        setTitle(t.replace("<>", n) + " - JKomasto");

        String f = newPage.type.toString().toLowerCase();
        Image bg = ImageApi.local(f);
        if (bg != null) ImageApi.awaitImage(bg);
        display.setBackgroundImage(bg);
        /*
        * (注) Java's image renderer draws images
        * with transparency darker than GIMP does.
        * Overcompensate in lightening.
        */
    }

    public void
    switchToList(int offset)
    {
        List<ListDetails> lists = primaire.getLists();
        assert openList.size() == lists.size();
        assert offset > -1 && offset < lists.size();
        ListDetails list = lists.get(offset);

        int operationId = ++currentOperationId;

        display.setCursor(WAIT);

        TimelinePage newPage = new TimelinePage();
        newPage.type = TimelineType.LIST;
        newPage.listId = list.id;
        newPage.posts = new Post[0];

        if (currentOperationId != operationId) return;

        page = newPage;
        showLatestPage();

        String t = JapaneseStrings.get(
            "list timeline title template");
        assert t.indexOf("<>") != -1;
        setTitle(t.replace("<>", list.title) + " - JKomasto");

        Image bg = ImageApi.local("list");
        if (bg != null) ImageApi.awaitImage(bg);
        display.setBackgroundImage(bg);
    }

    public void
    switchToAuthorPosts(String authorNumId)
    {
        assert authorNumId != null;

        int operationId = ++currentOperationId;

        display.setCursor(WAIT);

        TimelinePage newPage = new TimelinePage();
        newPage.type = TimelineType.PROFILE;
        newPage.accountNumId = authorNumId;
        newPage.listId = null;
        newPage.posts = new Post[0];

        if (currentOperationId != operationId) return;

        page = newPage;
        showLatestPage();

        String t = JapaneseStrings.get(
            "account timeline title template");
        assert t.indexOf("<>") != -1;
        setTitle(t.replace("<>", authorNumId) + " - JKomasto");

        Image bg = ImageApi.local("profile");
        if (bg != null) ImageApi.awaitImage(bg);
        display.setBackgroundImage(bg);
    }

    public TimelineType
    getTimelineType() { return this.page.type; }

    public void
    openOwnProfile()
    {
        display.setCursor(WAIT);

        Tree<String> accountDetails = api.getAccountDetails();
        ProfileWindow w = new ProfileWindow(primaire);
        w.use(new Account(accountDetails));
        w.setLocationByPlatform(true);
        w.setVisible(true);

        display.setCursor(null);
    }

    public void
    openProfile()
    {
        display.setCursor(WAIT);

        String query = JOptionPane.showInputDialog(
            this,
            JapaneseStrings.get("open profile msg"),
            JapaneseStrings.get("open profile title"),
            JOptionPane.PLAIN_MESSAGE
        );
        if (query == null) return;
        if (query.startsWith("@")) query = query.substring(1);

        RequestOutcome r = api.getAccounts(query);
        if (r.entity == null)
        {
            genericRequestFailed(r, "open profile lookup fail");
            display.setCursor(null);
            return;
        }
        if (r.entity.size() == 0)
        {
            JOptionPane.showMessageDialog(
                this,
                JapaneseStrings.get("open profile no results"));
            display.setCursor(null);
            return;
        }

        Tree<String> openee = null;

        List<Object> message = new ArrayList<>();
        message.add(
            JapaneseStrings.get("open profile pick from results"));
        ButtonGroup exclGroup = new ButtonGroup();
        for (Tree<String> account: r.entity)
        {
            String name = account.get("display_name").value;
            String acct = account.get("acct").value;

            if (acct.equals(query)) {
                openee = account;
                break;
            }

            JRadioButton choice = new JRadioButton();
            choice.setText(name + " (" + acct + ")");
            exclGroup.add(choice);
            message.add(choice);
        }

        if (openee == null)
        {
            int response = JOptionPane.showConfirmDialog(
                this,
                message.toArray(),
                JapaneseStrings.get("open profile results title"),
                JOptionPane.OK_CANCEL_OPTION
            );
            if (response == JOptionPane.OK_OPTION)
            for (int o = 1; o < message.size(); ++o)
            {
                // Find selected radio button.
                JRadioButton b = (JRadioButton)message.get(o);
                if (exclGroup.isSelected(b.getModel())) {
                    openee = r.entity.get(o - 1);
                    break;
                }
            }
        }

        if (openee == null)
        {
            display.setCursor(null);
            return;
        }

        ProfileWindow w = new ProfileWindow(primaire);
        w.use(new Account(openee));
        w.setLocationByPlatform(true);
        w.setVisible(true);

        display.setCursor(null);
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        display.setScale(scale);

        String fname = "VL PGothic";
        int fsz = 14 * scale/20;
        Font menu = new Font(fname, Font.PLAIN, fsz * 14/12);
        setFont(getJMenuBar(), menu);

        getJMenuBar().setBorder(null);

        int sw = getGraphicsConfiguration().getBounds().width;
        boolean maximised = getWidth() == sw;
        if (!maximised)
        {
            Dimension lsz =
                display.getLayout().preferredLayoutSize(display);

            int bw = 180 + (18 * PREVIEW_COUNT);
            Dimension sz = new Dimension();
            sz.width = bw * scale/20;
            sz.height = lsz.height;
            boolean disposed = !isDisplayable();

            display.setPreferredSize(sz);
            pack(); if (disposed) dispose();
        }

        considerRescale();
    }


//   -      -%-      -

    public Image
    getPattern() { return pattern; }


//  -=- --- -%- --- -=-


    public void
    imageReady(String name, Image image)
    {
        if (name.equals("p"))
        {
            pattern = image;
            considerRescale();
        }
        if (name.equals("ps"))
        {
            scaledPattern = image;
            getJMenuBar().repaint();
            display.repaint();
        }
    }

    private void
    considerRescale()
    {
        if (pattern != null)
        {
            int nw = pattern.getWidth(null) * scale/20;
            int nh = pattern.getHeight(null) * scale/20;
            boolean rescale = true;
            if (scaledPattern != null)
            {
                rescale =
                    scaledPattern.getWidth(null) != nw
                    || scaledPattern.getHeight(null) != nw;
            }

            if (rescale && !rescaling)
            {
                rescaling = true;
                fetcher.scale(pattern, nw, nh, "ps", 1, this);
            }
        }
    }

//   -      -%-      -

    private void
    genericRequestFailed(RequestOutcome r, String msgKey)
    {
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                TimelineWindow.this,
                JapaneseStrings.get(msgKey)
                + "\n" + wrap(r.exception.getMessage(), 80)
            );
        }
        else
        {
            JOptionPane.showMessageDialog(
                TimelineWindow.this,
                JapaneseStrings.get(msgKey)
                + "\n" + wrap(r.errorEntity.get("error").value, 80)
                + "\n(HTTP code: " + r.httpCode + ")"
            );
        }
    }

//   -      -%-      -

    private void
    syncDisplayToPage()
    {
        List<PostPreviewComponent> previews;
        List<SeparatorComponent> separators;
        previews = display.getPostPreviews();
        separators = display.getSeparators();

        int available = page.posts.length;
        int max = previews.size();
        assert available <= max;

        int b1 = -1, b2 = -1, b3 = -1;
        for (int o = 0; o < available; ++o)
        {
            PostPreviewComponent preview = previews.get(o);
            Post post = page.posts[o];
            AccessibleContext a = preview.getAccessibleContext();

            a.setAccessibleName((o + 1) + "");

            if (useCategories.isSelected())
            {
                int category = categories.get(post);
                if (category == 3) b1 = o;
                if (category == 2) b2 = o;
                if (category == 1) b3 = o;
            }

            if (post == null)
            {
                preview.showMalformedPlaceholder();
                a.setAccessibleDescription(
                    JapaneseStrings.get("timeline malformed post"));
                continue;
            }
            else if (api.isHidden(post.id))
            {
                preview.showHiddenPlaceholder();
                a.setAccessibleDescription(
                    JapaneseStrings.get("timeline hidden post"));
                continue;
            }

            String booster, author;
            if (post.boostedPost == null)
            {
                booster = null;
                author = post.author.name;
                preview.setTopLeft(author);
            }
            else
            {
                booster = post.author.name;
                post = post.boostedPost;
                author = post.author.name;
                assert sBoost.indexOf("<>") != -1;
                String s = sBoost.replace("<>", booster);
                preview.setTopLeft(s);
            }

            String flags = "";
            if (post.visibility == PostVisibility.FOLLOWERS)
                flags += "F";
            if (post.inThread) flags += "t";
            if (post.hasReplies) flags += "r";
            if (post.poll != null) flags += "p";
            if (post.attachments.length > 0) flags += "a";
            post.resolveRelativeTime();
            preview.setTopRight(flags + " " + post.relativeTime);

            post.resolveApproximateText();
            preview.setBottom(
                post.contentWarning != null
                    ? "(" + post.contentWarning + ")"
                    : post.approximateText
            );

            StringBuilder b = new StringBuilder();
            b.append(
                booster != null
                    ? "Boosted by " + booster
                    : "Post by " + author
            );
            if (post.attachments.length > 0) {
                b.append(", has attachment");
            }
            b.append(
                post.contentWarning != null
                    ? ", C W, " + post.contentWarning
                    : ", " + post.approximateText
            );
            b.append(", " + post.relativeTime + " ago");
            if (post.visibility == PostVisibility.FOLLOWERS) {
                b.append(", followers only");
            }
            if (post.inThread) b.append(", within thread");
            if (post.hasReplies) b.append(", has replies");
            b.append(".");
            // (来) We may want to localise these.
            String s = limitToASCII(b.toString());
            b.delete(0, b.length());
            a.setAccessibleDescription(s);

            preview.showPost();
        }
        for (int o = available; o < max; ++o)
        {
            PostPreviewComponent preview = previews.get(o);
            AccessibleContext a = preview.getAccessibleContext();

            preview.reset();
            a.setAccessibleName((o + 1) + "");
            a.setAccessibleDescription("Empty slot, no post.");

            preview.showPost();
        }

        for (int o = 0; o < separators.size(); ++o)
        {
            SeparatorComponent separator = separators.get(o);
            separator.setType(0);
            if (o == b1) separator.setType(1);
            if (o == b2) separator.setType(1);
            if (o == b3) separator.setType(1);
            separator.repaint();
        }

        AccessibleContext a = display.getAccessibleContext();
        a.setAccessibleDescription(available + " posts in page");

        display.setNextPageAvailable(!showingLast);
        display.setPreviousPageAvailable(true);
        display.resetFocus();
    }

    public void
    refresh()
    {
        if (currentOperationId != 0) return;

        if (showingLatest)
        {
            showLatestPage();
        }
        else
        {
            //showNextPage();
            //showPreviousPage();
            /*
            * The Mastodon API provides no means of
            * refreshing the timeline in place.
            */
            /*
            * Commenting out cause it's wayy too laggy to
            * be something called automatically, by
            * TimelineWindowUpdater in our case.
            *
            * Technically, if we can fetch the previous and
            * next page asynchronously, we can update ourselves
            * once those are ready.
            */
        }
    }

    public void
    previewSelected(int index)
    {
        Post[] posts = page.posts;
        if (index > posts.length) return;
        Post post = posts[index - 1];
        if (post.boostedPost != null) post = post.boostedPost;
        boolean hidden = api.isHidden(post.id);

        PostWindow w1 = primaire.getAutoViewWindow();
        ImageWindow w2 = primaire.getAutoMediaWindow();
        if (w1.isDisplayable())
            w1.use(post, hidden);
        if (w2.isDisplayable())
            w2.use(post.attachments, hidden);
    }

    public void
    previewOpened(int index)
    {
        Post[] posts = page.posts;
        if (index > posts.length) return;
        Post post = posts[index - 1];
        if (post.boostedPost != null) post = post.boostedPost;

        PostWindow w = new PostWindow(primaire);
        w.use(post, false);
        w.setLocation(getX() + getWidth() + 10, getY());
        w.setVisible(true);
    }

    public void
    previewHidden(int index)
    {
        Post[] posts = page.posts;
        if (index > posts.length) return;
        Post post = posts[index - 1];
        if (post.boostedPost != null) post = post.boostedPost;

        List<PostPreviewComponent> previews
            = display.getPostPreviews();
        PostPreviewComponent preview = previews.get(index - 1);

        if (api.isHidden(post.id))
        {
            api.unhidePost(post.id);
            preview.showPost();
        }
        else
        {
            api.hidePost(post.id);
            preview.showHiddenPlaceholder();
        }
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();

        display.setCursor(WAIT);

        if (src == openHome)
        {
            switchToTimeline(TimelineType.HOME);
        }
        if (src == openFederated)
        {
            switchToTimeline(TimelineType.FEDERATED);
        }
        if (openList.contains(src))
        {
            switchToList(openList.indexOf(src));
        }
        if (src == openLocal)
        {
            switchToTimeline(TimelineType.LOCAL);
        }
        if (src == openOwnProfile)
        {
            openOwnProfile();
        }
        if (src == openProfile)
        {
            openProfile();
        }
        if (src == restartUpdaters)
        {
            primaire.getWindowUpdater().restart();
        }
        if (src == createPost)
        {
            ComposeWindow w = new ComposeWindow(primaire);
            w.setLocationRelativeTo(this);
            w.setVisible(true);
        }
        if (src == openAutoMediaView)
        {
            ImageWindow w = primaire.getAutoMediaWindow();
            if (!w.isVisible())
            {
                int bottom = getY() + getHeight();
                w.setLocation(getX(), bottom + 10);
                w.setVisible(true);
            }
            else w.toFront();
        }
        if (src == openAutoPostView)
        {
            PostWindow w = primaire.getAutoViewWindow();
            if (!w.isVisible())
            {
                int right = getX() + getWidth();
                w.setLocation(right + 10, getY());
                w.setVisible(true);
            }
            else w.toFront();
            /*
            * (知) I'd like to note that making this request
            * doesn't unminimise windows. A more forced way is
            * to set it invisible then visible again, but that's
            * no guarantee either. I doubt the AWT API is going
            * to have something that lets us unminimise windows,
            * I anticipate that'd be something the window system
            * decides. Cause like, if you think of Show Desktop
            * toggles that minimise all windows, or an older X
            * desktop that iconifies windows, you can see how
            * unminimising yourself is going to be disruptive.
            */
        }
        if (src == openNotifications)
        {
            Window w = primaire.getNotificationsWindow();
            if (!w.isVisible())
            {
                w.setLocationByPlatform(true);
                w.setVisible(true);
            }
            else w.toFront();
        }
        if (src == openSettings)
        {
            Window w = primaire.getSettingsWindow();
            if (!w.isVisible())
            {
                w.setLocationByPlatform(true);
                w.setVisible(true);
            }
            else w.toFront();
        }
        if (src == flipToNewestPost)
        {
            showLatestPage();
        }

        display.setCursor(null);
    }

    public void
    windowOpened(WindowEvent eW)
    {
        windowUpdater.add(this);

        pack();
        /*
        * Crude fix to the layout bug. Since we apparently already
        * have a listener to window opens, just pack again to the
        * preferred size, to counter the mysterious size down.
        *
        * The mysterious size down is, only occuring on my TDE
        * system it seems, where we are initially given the
        * preferred size for display but a subsequent call gives
        * us 2 less width and 4 less height. I don't know what is
        * the cause of that, stack trace gives no clue. Reading
        * the code of BorderLayout, I thought that maybe it's
        * from the parent suddenly acquiring insets, but one
        * listener I set up said that we weren't having any
        * change in sets. We have to assume it's coming from
        * the window manager or something.
        */
    }

    public void
    windowClosed(WindowEvent eW)
    {
        windowUpdater.remove(this);
    }

    public void
    windowClosing(WindowEvent eW) { }

    public void
    windowDeiconified(WindowEvent eW) { }

    public void
    windowIconified(WindowEvent eW) { }

    public void
    windowActivated(WindowEvent eW) { }

    public void
    windowDeactivated(WindowEvent eW) { }


//  -=- --- -%- --- -=-


    private static void
    setFont(MenuElement me, Font font)
    {
        me.getComponent().setFont(font);
        for (MenuElement mse: me.getSubElements())
            setFont(mse, font);
    }

    private static String
    limitToASCII(String s)
    {
        /*
        * (知) A bit of a fatal flaw in Java's accessibility
        * framework. D-Bus has an assertion enabled in
        * production that crashes the VM when D-Bus is
        * given invalid UTF-8 characters. Meanwhile Java
        * only supports giving data to the accessibility
        * bus as UTF-16 strings, so.. I had to restrict
        * us to the ASCII set here to stay clear of that
        * assertion. I can't really do anything about it,
        * it's not like I can stuff UTF-8 chars into a
        * Java string.
        *
        * My getting the screenreader announcements to
        * work on this page has taken me literally a
        * whole midnight when I should've been sleeping.
        * I hope it's satisfactory..!
        */
        try
        {
            return new String(s.getBytes("US-ASCII"), "US-ASCII");
        }
        catch (UnsupportedEncodingException eUe)
        {
            assert false;
            return null;
        }
    }

    private static String
    wrap(String text, int lineLength)
    {
        StringBuilder returnee = new StringBuilder();

        int wo = 0, o, rem = lineLength;
        for (o = 0; o <= text.length(); ++o)
        {
            boolean whitespace = o == text.length();
            if (o < text.length())
            {
                char c = text.charAt(o);
                whitespace |= Character.isWhitespace(c);
            }
            if (whitespace)
            {
                String addee = text.substring(wo, o);
                boolean fits = rem > addee.length();
                if (!fits) {
                    returnee.append("\n");
                    rem = lineLength;
                }
                returnee.append(addee);
                rem -= addee.length();
                wo = o;
            }
        }

        return returnee.toString();
    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    TimelineWindow(JKomasto primaire)
    {
        this.primaire = primaire;
        api = primaire.getMastodonApi();
        windowUpdater = primaire.getWindowUpdater();
        fetcher = primaire.getImageFetcher();

        fetcher.fetch(
            getClass().getResource("graphics/timelineWindow.png")
                .toString(),
            "p", 2, this);

        sBoost = JapaneseStrings.get("boosted by template");
        String[] strings = new String[] {
            JapaneseStrings.get("open home"),
            JapaneseStrings.get("open federated"),
            JapaneseStrings.get("open list template"),
            JapaneseStrings.get("open notifications"),
            JapaneseStrings.get("open own profile"),
            JapaneseStrings.get("open other profile"),
            JapaneseStrings.get("restart updaters"),
            JapaneseStrings.get("create post"),
            JapaneseStrings.get("open auto media"),
            JapaneseStrings.get("open auto view"),
            JapaneseStrings.get("open settings"),
            JapaneseStrings.get("flip newest"),
            JapaneseStrings.get("use chrono"),
            JapaneseStrings.get("use categories")
        };

        openHome = new JMenuItem(strings[0]);
        openFederated = new JMenuItem(strings[1]);
        openNotifications = new JMenuItem(strings[3]);
        openOwnProfile = new JMenuItem(strings[4]);
        openProfile = new JMenuItem(strings[5]);
        restartUpdaters = new JMenuItem(strings[6]);
        createPost = new JMenuItem(strings[7]);
        openAutoMediaView = new JMenuItem(strings[8]);
        openAutoPostView = new JMenuItem(strings[9]);
        openSettings = new JMenuItem(strings[10]);
        openHome.addActionListener(this);
        openFederated.addActionListener(this);
        openNotifications.addActionListener(this);
        openOwnProfile.addActionListener(this);
        openProfile.addActionListener(this);
        restartUpdaters.addActionListener(this);
        createPost.addActionListener(this);
        openAutoMediaView.addActionListener(this);
        openAutoPostView.addActionListener(this);
        openSettings.addActionListener(this);

        openList = new ArrayList<>();
        for (ListDetails list: primaire.getLists())
        {
            String text = strings[2].replace("<>", list.title);
            JMenuItem openList = new JMenuItem(text);
            openList.addActionListener(this);
            this.openList.add(openList);
        }

        flipToNewestPost = new JMenuItem(strings[11]);
        flipToNewestPost.addActionListener(this);
        ButtonGroup g1 = new ButtonGroup();
        useChronological = new JCheckBoxMenuItem(strings[12]);
        useCategories = new JCheckBoxMenuItem(strings[13]);
        useCategories.setSelected(true);
        g1.add(useChronological);
        g1.add(useCategories);
        useChronological.setMnemonic(KeyEvent.VK_H);
        useCategories.setMnemonic(KeyEvent.VK_A);

        String s1 = JapaneseStrings.get("program menu");
        JMenu programMenu = new JMenu(s1);
        programMenu.setMnemonic(KeyEvent.VK_P);
        programMenu.add(openHome);
        programMenu.add(openFederated);
        if (!openList.isEmpty())
        {
            for (JMenuItem openList: this.openList)
                programMenu.add(openList);
        }
        programMenu.add(openNotifications);
        programMenu.add(openOwnProfile);
        programMenu.add(openProfile);
        programMenu.add(new JSeparator());
        programMenu.add(restartUpdaters);
        programMenu.add(openSettings);
        programMenu.add(new JSeparator());
        programMenu.add(createPost);
        programMenu.add(new JSeparator());
        programMenu.add(openAutoMediaView);
        programMenu.add(openAutoPostView);
        String s2 = JapaneseStrings.get("timeline menu");
        JMenu timelineMenu = new JMenu(s2);
        timelineMenu.setMnemonic(KeyEvent.VK_T);
        timelineMenu.add(flipToNewestPost);
        timelineMenu.add(new JSeparator());
        timelineMenu.add(useChronological);
        timelineMenu.add(useCategories);
        JMenuBar menuBar = new JMenuBar() {
            protected void
            paintComponent(Graphics g)
            {
                Image pattern = getPattern();
                if (pattern == null) return;
                int pw = pattern.getWidth(null);
                int ph = pattern.getHeight(null);
                for (int y = 0; y < getHeight(); y += ph)
                for (int x = 0; x < getWidth(); x += pw)
                    g.drawImage(pattern, x, y, null);
            }
        };
        menuBar.setOpaque(false);
        menuBar.add(programMenu);
        menuBar.add(timelineMenu);
        setJMenuBar(menuBar);

        page = new TimelinePage();
        page.posts = new Post[0];
        categories = new HashMap<>();

        display = new TimelineComponent(this, fetcher);
        display.setNextPageAvailable(false);
        display.setPreviousPageAvailable(false);
        setContentPane(display);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setIconImage(primaire.getProgramIcon());
        setScale(22);

        addWindowListener(this);
        addMouseWheelListener(new MouseWheelScaler(1, 6));
        addKeyListener(new SharedKeyShortcuts(primaire, true));
    }

}



class
TimelineComponent extends JPanel
implements
    Scalable,
    KeyListener, MouseListener, ActionListener,
    FocusListener, ImageFetcher.Listener {

    private TimelineWindow
    primaire;

    private ImageFetcher
    fetcher;


//  -=- --- -%- --- -=-


    private JButton
    next, prev;

    private final List<PostPreviewComponent>
    postPreviews;

    private final List<SeparatorComponent>
    separators;

    private boolean
    hoverSelect;

    private Image
    backgroundImage, scaledBackgroundImage;

    private Banner
    bannerDisplay;

    private Image[]
    banners;

    private int[]
    bannerPriorities;

    private int
    totalBannerPriority;

    private int
    scale;

    private int
    vgap;

    private JComponent
    bottom,
    centre;

    private AdjustableStrut
    strut1, strut2;

    private boolean
    hidingTop;


//  -=- --- -%- --- -=-


    static final int
    PREVIEW_COUNT = 7;

    private static final Cursor
    HAND = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR),
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    public List<PostPreviewComponent>
    getPostPreviews() { return postPreviews; }

    public List<SeparatorComponent>
    getSeparators() { return separators; }

    public void
    setNextPageAvailable(boolean n) { next.setEnabled(n); }

    public void
    setPreviousPageAvailable(boolean n) { prev.setEnabled(n); }

    public void
    setHoverSelect(boolean n) { hoverSelect = n; }

    public void
    setBackgroundImage(Image image)
    {
        if (scaledBackgroundImage != null &&
                scaledBackgroundImage != backgroundImage)
            scaledBackgroundImage.flush();

        backgroundImage = image;
        scaledBackgroundImage = null;
        repaint();
        /*
        * Are the timeline components constantly repainting?
        * Previously we didn't repaint here, so the bottom of
        * the window retained the previous background image.
        * But it's supposed to be whole window doing that, if
        * we didn't repaint. But the new background image took
        * effect behind the timeline components. Logs aren't
        * suggesting that, though.
        */
    }

    public void
    resetFocus()
    {
        postPreviews.get(0).requestFocusInWindow();
    }

//   -      -%-      -

    public void
    changeBanner()
    {
        if (banners.length == 0)
        {
            bannerDisplay.setImage(null);
            bannerDisplay.repaint();
            return;
        }

        double roll = Math.random();
        double cursor = 0.0;
        int o; for (o = 0; o < banners.length; ++o)
        {
            int priority = bannerPriorities[o];
            double chance = (double)priority / totalBannerPriority;

            boolean chosen = roll <= (cursor += chance);
            if (chosen) break;
        }

        bannerDisplay.setImage(banners[o]);
        bannerDisplay.repaint();

        int decrease = bannerPriorities[o] / 2;
        bannerPriorities[o] -= decrease;
        totalBannerPriority -= decrease;
    }

//   -      -%-      -

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        for (PostPreviewComponent c: postPreviews)
            c.setScale(scale);

        this.vgap = 3 * scale/20;

        int BOLD_ITALIC = Font.BOLD | Font.ITALIC;
        String fname = "VL PGothic";
        int fsz = 14 * scale/20;
        Font labels = new Font(fname, Font.ITALIC, fsz);
        next.setFont(labels);
        prev.setFont(labels);

        int xm1 = 10 * scale/20;
        int ym1 = 8 * scale/20;
        Border b1 =
            BorderFactory.createEmptyBorder(ym1, xm1, ym1, xm1);
        setBorder(b1);

        Border b2 =
            BorderFactory.createEmptyBorder(ym1, 0, 0, 0);
        bottom.setBorder(b2);

        int xm2 = 10 * scale/20;
        String key = "TabbedPane.tabAreaBackground";
        Color c = javax.swing.UIManager.getColor(key);
        Border b3 =
            BorderFactory.createEmptyBorder(ym1, xm2, ym1, xm2);
        Border b4 =
            BorderFactory.createMatteBorder(1, 1, 1, 1, c);
        Border b5 =
            BorderFactory.createCompoundBorder(b4, b3);
        centre.setBorder(b5);

        int ym2 = 3 * scale/20;
        Border b6 =
            BorderFactory.createEmptyBorder(ym2, 0, ym2, 0);
        bannerDisplay.setBorder(b6);

        strut1.setSizing(xm1, 0);
        strut2.setSizing(xm1, 0);

        doLayout();
    }


//  -=- --- -%- --- -=-


    private boolean
    bannersLoaded()
    {
        for (Image banner: banners)
            if (banner == null) return false;
            // banners is an array, an offset can be set.
        return true;
    }

    public void
    imageReady(String name, Image image)
    {
        if (name.startsWith("b")) try
        {
            int o = Integer.parseInt(name.substring(1));
            banners[o] = image;
            if (bannersLoaded()) changeBanner();
        }
        catch (NumberFormatException eNf)
        {
            assert false;
        }
    }

    protected void
    paintComponent(Graphics g)
    {
        g.setColor(getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());

        paintPattern(g);
        paintBackground(g);
        paintFocus(g);
    }

    protected void
    paintPattern(Graphics g)
    {
        Image pattern = primaire.getPattern();
        if (pattern == null) return;

        int pw = pattern.getWidth(null);
        int ph = pattern.getHeight(null);
        int sy = -primaire.getJMenuBar().getHeight();
        for (int y = sy; y < getHeight(); y += ph)
        for (int x = 0; x < getWidth(); x += pw)
            g.drawImage(pattern, x, y, null);

        g.clearRect(
            centre.getX(), centre.getY(),
            centre.getWidth(), centre.getHeight()
        );
    }

    protected void
    paintBackground(Graphics g)
    {
        if (backgroundImage == null) return;
        int w = getWidth(), h = getHeight();

        int b = h * 5 / 10;
        int iw = backgroundImage.getWidth(null);
        int ih = backgroundImage.getHeight(null);
        if (ih > iw) {
            ih = ih * b / iw;
            iw = b;
        }
        else {
            iw = iw * b / ih;
            ih = b;
        }
        int x = w - iw, y = h - ih;
        g.drawImage(backgroundImage, x, y, iw, ih, this);
    }

    protected void
    paintFocus(Graphics g)
    {
        //String key = "TabbedPane.contentAreaColor";
        //g.setColor(javax.swing.UIManager.getColor(key));
        g.setColor(new Color(0, 0, 0, 25));
        for (PostPreviewComponent p: postPreviews)
        {
            if (!p.hasFocus()) continue;
            int s1 = p.getHeight() * 3/32;
            int s2 = p.getHeight() * 1/32;
            int x = centre.getX() + p.getX() - s1;
            int w = p.getWidth() + s1 + s1;
            int y = centre.getY() + p.getY() - s2;
            int h = p.getHeight() + s2 + s2;
            g.fillRect(x, y, w, h);
        }
    }


    private void
    open(PostPreviewComponent p)
    {
        int o = postPreviews.indexOf(p);
        assert o != -1;
        primaire.previewOpened(1 + o);
    }

    private void
    select(PostPreviewComponent p)
    {
        int o = postPreviews.indexOf(p);
        assert o != -1;
        primaire.previewSelected(1 + o);

        repaint();
        p.getAccessibleContext().firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            null, AccessibleState.SELECTED
        );
    }

    private void
    hide(PostPreviewComponent p)
    {
        int o = postPreviews.indexOf(p);
        assert o != -1;
        primaire.previewHidden(1 + o);
    }

    private void
    deselect(PostPreviewComponent p)
    {
        repaint();
        p.setScale(this.scale);
        p.getAccessibleContext().firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            AccessibleState.SELECTED, null
        );
    }


    public void
    focusGained(FocusEvent eF)
    {
        Object src = eF.getSource();
        assert src instanceof PostPreviewComponent;
        PostPreviewComponent p = (PostPreviewComponent)src;

        setCursor(WAIT);

        select(p);
        p.getAccessibleContext().firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            null, AccessibleState.FOCUSED
        );
        /*
        * Docs sort of lied... They did settle everything
        * for us, except for this, we have to listen to
        * and fire this ourselves... And like, it has to
        * be enabled in the accessible context's state set,
        * presumably FOCUSABLE is on by default but SELECTED
        * isn't. Or maybe it is but the screen reader won't
        * announce if I don't tell them about focuses..
        */

        setCursor(null);
    }

    public void
    focusLost(FocusEvent eF)
    {
        Object src = eF.getSource();
        assert src instanceof PostPreviewComponent;
        PostPreviewComponent p = (PostPreviewComponent)src;

        setCursor(WAIT);

        deselect(p);
        p.getAccessibleContext().firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            AccessibleState.FOCUSED, null
        );

        setCursor(null);
    }


    public void
    mouseClicked(MouseEvent eM)
    {
        Object src = eM.getSource();
        if (src instanceof PostPreviewComponent)
        {
            PostPreviewComponent p = (PostPreviewComponent)src;

            setCursor(WAIT);

            if (eM.getButton() == MouseEvent.BUTTON3)
            {
                hide(p);
            }
            else if (eM.getClickCount() == 2)
            {
                open(p);
            }

            select(p);
            p.requestFocusInWindow();

            setCursor(null);
        }
        else if (src instanceof Banner)
        {
            changeBanner();
        }
        else assert false;
    }

    public void
    mouseEntered(MouseEvent eM)
    {
        Object src = eM.getSource();
        if (src instanceof PostPreviewComponent)
        {
            if (hoverSelect) mouseClicked(eM);
            /*
            * This appears to be slower when we are hover selecting
            * from another window. I don't think this was the case
            * in the past. If it's not a new thing, then maybe our
            * DE is firing mouse move events less for windows that
            * aren't the focus owner, in which case this is fine.
            */
        }
    }

    public void
    mouseExited(MouseEvent eM)
    {
        Object src = eM.getSource();
        if (src instanceof PostPreviewComponent)
        {
            PostPreviewComponent p = (PostPreviewComponent)src;

            setCursor(WAIT);

            deselect(p);

            setCursor(null);
        }
    }


    public void
    keyPressed(KeyEvent eK)
    {
        setCursor(WAIT);

        Object src = eK.getSource();
        assert src instanceof PostPreviewComponent;
        PostPreviewComponent p = (PostPreviewComponent)src;
        int code = eK.getKeyCode();

        if (code == KeyEvent.VK_ENTER)
        {
            open(p);
        }
        if (code == KeyEvent.VK_AT)
        {
            for (PostPreviewComponent p2: postPreviews)
                p2.setTopHidden(true);
        }

        setCursor(null);
    }

    public void
    keyReleased(KeyEvent eK)
    {
        setCursor(WAIT);

        int code = eK.getKeyCode();

        if (code == KeyEvent.VK_AT)
        {
            for (PostPreviewComponent p2: postPreviews)
                p2.setTopHidden(false);
        }

        setCursor(null);
    }

    public void
    keyTyped(KeyEvent eK)
    {
        setCursor(WAIT);

        Object src = eK.getSource();
        assert src instanceof PostPreviewComponent;
        PostPreviewComponent p = (PostPreviewComponent)src;
        char key = eK.getKeyChar();

        if (key == 'h' || key == 'H')
        {
            hide(p);
        }

        setCursor(null);
    }


    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();

        setCursor(WAIT);
        if (src == next) primaire.showNextPage();
        else if (src == prev) primaire.showPreviousPage();
        setCursor(null);
    }


    public void
    mousePressed(MouseEvent eM) { }

    public void
    mouseReleased(MouseEvent eM) { }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    private static class
    Banner extends JComponent
    implements ImageFetcher.Listener {

        private Image
        image, scaledImage;

        private ImageFetcher
        fetcher;

//        -=%=-

        public void
        setImage(Image image)
        {
            this.image = image;
            scaledImage = null;
            considerRescale();
            setToolTipText(
                ImageApi.debugString(this.image, scaledImage));
            repaint();
        }

//         -=-

        public void
        imageReady(String name, Image image)
        {
            scaledImage = image;
            setToolTipText(
                ImageApi.debugString(this.image, scaledImage));
            repaint();
        }

        private void
        considerRescale()
        {
            assert fetcher != null;
            if (image == null) return;
            if (!isDisplayable()) return;

            Insets insets = getInsets();
            int dx1 = insets.left;
            int dx2 = getWidth() - insets.right;
            int dy1 = insets.top;
            int dy2 = getHeight() - insets.bottom;

            Dimension nsz = ImageApi.fillFit(
                image.getWidth(null),
                image.getHeight(null),
                dx2 - dx1, dy2 - dy1);

            boolean rescale = true;
            if (scaledImage != null)
            {
                int sw = scaledImage.getWidth(null);
                int sh = scaledImage.getHeight(null);
                rescale = sw != nsz.width || sh != nsz.height;
            }
            if (!rescale) return;

            boolean rescaling = scaledImage == image;
            if (rescaling) return;

            fetcher.scale(
                image, nsz.width, nsz.height,
                null, 1, this);
            scaledImage = image;
        }

        protected void
        paintComponent(Graphics g)
        {
            int w = getWidth(), h = getHeight();
            Insets insets = getInsets();
            int dx1 = insets.left;
            int dx2 = w - insets.right;
            int dy1 = insets.top;
            int dy2 = h - insets.bottom;

            if (scaledImage != null)
            {
                int iw = scaledImage.getWidth(null);
                int ih = scaledImage.getHeight(null);

                if (ih != (dy2 - dy1)) return;
                // Assume a rescale is going to happen.

                int x = (w - iw) / 2;
                g.drawImage(scaledImage, x, dy1, iw, ih, null);
                g.clearRect(0, dy1, insets.left, (dy2 - dy1));
                g.clearRect(dx2, dy1, w - dx2, (dy2 - dy1));
                // Better idea than setting clip or doing
                // 8-argument drawImage, i think.
            }
        }

        public void
        doLayout()
        {
            considerRescale();
        }

//        -=%=-

        Banner(ImageFetcher fetcher)
        {
            this.fetcher = fetcher;
            Banner.this.setCursor(HAND);
        }

    }



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



    TimelineComponent(TimelineWindow primaire, ImageFetcher fetcher)
    {
        this.primaire = primaire;
        this.fetcher = fetcher;

        postPreviews = new ArrayList<>(PREVIEW_COUNT);
        separators = new ArrayList<>(PREVIEW_COUNT - 1);
        hoverSelect = true;

        prev = new JButton("< prev");
        next = new JButton("next >");
        prev.setEnabled(false);
        next.setEnabled(false);
        prev.setToolTipText(JapaneseStrings.get("flip newer"));
        next.setToolTipText(JapaneseStrings.get("flip older"));
        prev.addActionListener(this);
        next.addActionListener(this);

        List<String> names = ImageApi.locals("banner", "png");
        banners = new Image[names.size()];
        bannerPriorities = new int[names.size()];
        totalBannerPriority = 0;
        for (int o = 0; o < names.size(); ++o)
        {
            String path = "graphics/" + names.get(o) + ".png";
            URL url = getClass().getResource(path);
            fetcher.fetch(url.toString(), "b" + o, 2, this);
            bannerPriorities[o] = 2 << 16;
            totalBannerPriority += bannerPriorities[o];
        }
        bannerDisplay = new Banner(fetcher);
        bannerDisplay.addMouseListener(this);
        bannerDisplay.setImage(null);

        bottom = new Box(BoxLayout.X_AXIS);
        //bottom.add(Box.createGlue());
        bottom.add(bannerDisplay);
        bottom.add(strut1 = new AdjustableStrut());
        bottom.add(prev);
        bottom.add(strut2 = new AdjustableStrut());
        bottom.add(next);

        centre = new JPanel();
        centre.setOpaque(false);
        centre.setLayout(new LayoutManager() {

            public void
            addLayoutComponent(String name, Component c) { }

            public void
            removeLayoutComponent(Component c) { }

            public Dimension
            minimumLayoutSize(Container p)
            {
                return new Dimension(0, 0);
            }

            public Dimension
            preferredLayoutSize(Container p)
            {
                int w = 0, h = 0;
                for (Component c: p.getComponents())
                {
                    Dimension csz = c.getPreferredSize();
                    Dimension cmsz = c.getMaximumSize();
                    int cw = Math.min(csz.width, cmsz.width);
                    int ch = Math.min(csz.height, cmsz.height);

                    w = Math.max(w, cw);
                    h += ch;
                    h += vgap;
                }
                h -= vgap;

                Insets insets = p.getInsets();
                w += insets.left + insets.right;
                h += insets.top + insets.bottom;

                return new Dimension(w, h);
            }

            public void
            layoutContainer(Container p)
            {
                Insets insets = p.getInsets();
                int w = p.getWidth(), h = p.getHeight();
                int ph =
                    p.getPreferredSize().height
                    - insets.top - insets.bottom;

                int bottom = h - insets.bottom;
                int cw = w - insets.left - insets.right;
                int x = insets.left;

                int y = Math.max(insets.top, (h - ph) / 2);
                for (Component c: p.getComponents())
                {
                    Dimension csz = c.getPreferredSize();
                    Dimension cmsz = c.getMaximumSize();
                    int ch = Math.min(csz.height, cmsz.height);

                    boolean hide = y > bottom;
                    if (hide) ch = 0;

                    c.setBounds(x, y, cw, ch);
                    y += ch + vgap;
                }
            }
        });
        // We might be able to use GridBagLayout instead, since our
        // components aren't changing. If it's really the case that
        // we need our own layout manager though, then break it out
        // into a new class, since multiple windows need it.
        for (int n = PREVIEW_COUNT; n > 0; --n)
        {
            PostPreviewComponent c = new PostPreviewComponent();
            c.addMouseListener(this);
            c.addFocusListener(this);
            c.addKeyListener(this);
            postPreviews.add(c);
            SeparatorComponent s = new SeparatorComponent();
            separators.add(s);
            centre.add(c);
            centre.add(s);
        }
        Component[] added = centre.getComponents();
        centre.remove(added[added.length - 1]);

        setLayout(new BorderLayout(0, 0));
        add(centre, BorderLayout.CENTER);
        add(bottom, BorderLayout.SOUTH);

        addFocusTraversalKeys(this);
    }


//  -=- --- -%- --- -=-


    private static void
    addFocusTraversalKeys(Container c)
    {
        final int FW =
            KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS;
        final int BW =
            KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS;

        final AWTKeyStroke UP =
            AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_UP, 0);
        final AWTKeyStroke DOWN =
            AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_DOWN, 0);

        Set<AWTKeyStroke> keystrokes = new HashSet<>();
        keystrokes.addAll(c.getFocusTraversalKeys(FW));
        keystrokes.add(DOWN);
        c.setFocusTraversalKeys(FW, keystrokes);

        keystrokes = new HashSet<>();
        keystrokes.addAll(c.getFocusTraversalKeys(BW));
        keystrokes.add(UP);
        c.setFocusTraversalKeys(BW, keystrokes);
    }

}


class
SeparatorComponent extends JComponent {

    private int
    type;

//  ---%-@-%---

    public void
    setType(int type) { this.type = type; }

//   -  -%-  -

    protected void
    paintComponent(Graphics g)
    {
        int w = getWidth(), h = getHeight();
        int y = h / 2;
        int x1 = w * 1/20;
        int x2 = w * 10/20;
        int x3 = w * 10/20;
        int x4 = w * 19/20;

        String key = "TabbedPane.contentAreaColor";
        Color co = javax.swing.UIManager.getColor(key);
        Color c = new Color(
            (co.getRed() * 216/255),
            (co.getGreen() * 216/255),
            (co.getBlue() * 216/255)
        );

        if (type != 0)
        {
            x2 = w * 9/20;
            x3 = w * 11/20;
        }

        g.setColor(c);
        g.fillRect(x1, y, x2 - x1, 1);
        g.fillRect(x3, y, x4 - x3, 1);
        g.setColor(c.brighter());
        g.fillRect(x1, y + 1, x2 - x1, 1);
        g.fillRect(x3, y + 1, x4 - x3, 1);

        if (type != 0)
        {
            int[] xs = {
                (w / 2) - 13, (w / 2) - 1, (w / 2) + 11,
                (w / 2) - 12, (w / 2) - 0, (w / 2) + 12
            };
            g.setColor(c.brighter());
            for (int x: xs) g.drawOval(x - 3, y - 2, 6, 6);
            g.setColor(c);
            for (int x: xs) g.drawOval(x - 3, y - 3, 6, 6);
        }
    }

//  ---%-@-%---

    SeparatorComponent()
    {
        setPreferredSize(new Dimension(0, 8));
        setType(0);
    }

}


class
PostPreviewComponent extends JComponent
implements Scalable, Accessible {

    private JLabel
    topLeft, topRight;

    private JLabel
    bottom;

    private JLabel
    malformedText, hiddenText;

    private JPanel
    standard, malformed, hidden;

    private AccessibleContext
    access;

    private int
    scale;

//  ---%-@-%---

    public void
    setTopLeft(String text) { topLeft.setText(text); }

    public void
    setTopRight(String text) { topRight.setText(text); }

    public void
    setBottom(String text) { bottom.setText(text); }

    public void
    reset()
    {
        setTopLeft(" ");
        setTopRight(" ");
        setBottom(" ");
    }

    public void
    showPost()
    {
        removeAll();
        add(this.standard, BorderLayout.CENTER);
        revalidate();
    }

    public void
    showMalformedPlaceholder()
    {
        this.malformed.setPreferredSize(getSize());
        removeAll();
        add(this.malformed, BorderLayout.CENTER);
        revalidate();
    }

    public void
    showHiddenPlaceholder()
    {
        this.hidden.setPreferredSize(getSize());
        removeAll();
        add(this.hidden, BorderLayout.CENTER);
        revalidate();
    }

    public void
    setTopHidden(boolean hidden)
    {
        Color transparent = new Color(0, 0, 0, 0);
        topLeft.setForeground(hidden ? transparent : null);
        topRight.setForeground(hidden ? transparent : null);
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        String fname = "VL PGothic";
        int fsz = 14 * scale/20;
        Font small = new Font(fname, Font.PLAIN, fsz);
        Font large = new Font(fname, Font.PLAIN, fsz * 14/12);
        topLeft.setFont(small);
        topRight.setFont(small);
        bottom.setFont(large);
        hiddenText.setFont(large);
    }

//   -    -%-     -

    public AccessibleContext
    getAccessibleContext() { return access; }

//  ---%-@-%---

    private class
    Access extends AccessibleJComponent
    implements AccessibleAction {

        public AccessibleRole
        getAccessibleRole() { return AccessibleRole.LIST_ITEM; }

        public int
        getAccessibleChildrenCount() { return 0; }

        public AccessibleAction
        getAccessibleAction() { return this; }

        public int
        getAccessibleActionCount() { return 1; }

        public String
        getAccessibleActionDescription(int o)
        {
            return "Open post in new window";
        }

        public AccessibleStateSet
        getAccessibleStateSet()
        {
            AccessibleStateSet s = super.getAccessibleStateSet();
            s.add(AccessibleState.SELECTABLE);
            return s;
        }

        public boolean
        doAccessibleAction(int o)
        {
            KeyEvent eK = new KeyEvent(
                PostPreviewComponent.this, KeyEvent.KEY_FIRST, 0,
                0, KeyEvent.VK_ENTER, KeyEvent.CHAR_UNDEFINED
            );
            for (KeyListener listener: getKeyListeners())
            {
                listener.keyPressed(eK);
            };
            return getKeyListeners().length > 0;
        }

    }

//  ---%-@-%---

    public
    PostPreviewComponent()
    {
        access = new Access();

        topLeft = new JLabel();
        topLeft.setOpaque(false);

        topRight = new JLabel();
        topRight.setHorizontalAlignment(JLabel.RIGHT);
        topRight.setOpaque(false);

        JPanel top = new JPanel();
        top.setOpaque(false);
        top.setLayout(new BorderLayout());
        top.add(topRight, BorderLayout.EAST);
        top.add(topLeft, BorderLayout.CENTER);

        bottom = new JLabel();
        bottom.setOpaque(false);

        standard = new JPanel();
        standard.setOpaque(false);
        standard.setLayout(new BorderLayout());
        standard.add(top, BorderLayout.NORTH);
        standard.add(bottom);

        String s1 = JapaneseStrings.get("timeline malformed post");
        malformedText = new JLabel(s1, JLabel.CENTER);
        malformedText.setOpaque(false);

        String s2 = JapaneseStrings.get("timeline hidden post");
        hiddenText = new JLabel(s2, JLabel.CENTER);
        hiddenText.setOpaque(false);

        malformed = new JPanel();
        malformed.setOpaque(false);
        malformed.setLayout(new BorderLayout());
        malformed.add(malformedText, BorderLayout.CENTER);

        hidden = new JPanel();
        hidden.setOpaque(false);
        hidden.setLayout(new BorderLayout());
        hidden.add(hiddenText, BorderLayout.CENTER);

        setLayout(new BorderLayout());

        setFocusable(true);
        setOpaque(false);
        reset();
        showPost();
    }

}
