
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.JComponent;
import javax.swing.Box;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.Component;
import java.awt.Image;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import java.io.File;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleAction;
import javax.accessibility.AccessibleText;
import javax.accessibility.AccessibleState;

import cafe.biskuteri.hinoki.Tree;
import java.net.URI;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ArrayList;
import javax.accessibility.AccessibleRole;
import java.io.UnsupportedEncodingException;


class
LoginWindow extends JFrame
implements
    Scalable, ImageFetcher.Listener,
    OperationProgress.Listener {

    private JKomasto
    primaire;

    private MastodonApi
    api;

    private ImageFetcher
    fetcher;

//   -  -%-  -

    private LoginComponent
    display;

    private OperationProgress
    startupProgress;

    private String
    sConn, sCred, sToken, sAcc, sLists, sEmojis;

    private List<LoginDetails>
    logins;

    private int
    scale;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    useCache(int loginId)
    {
        assert logins != null;
        assert !logins.isEmpty();
        assert loginId > 0 && loginId <= logins.size();

        api.setLogin(logins.get(loginId - 1));
        display.setInstanceUrl(api.getInstanceUrl());
        
        startupProgress.reset();
        startupProgress.markComplete("have app cred");
        startupProgress.markComplete("have acc token");
        display.setCursor(WAIT);
        display.setShowingProgress(true);

        RequestOutcome r = api.getAccountDetailsAnew();
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("account details fail")
                + "\n" + r.exception.getMessage()
            );

            display.setShowingProgress(false);
            display.setCursor(null);
            return;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("account details fail")
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP error code: " + r.httpCode + ")"
            );

            display.setShowingProgress(false);
            display.setCursor(null);
            return;
        }
        else
        {
            api.setAccountDetails(r.entity);
            startupProgress.markComplete("have instance conn");
            startupProgress.markComplete("have account details");
        }

        try
        {
            logins.set(loginId - 1, api.getLogin());
            api.saveLoginsIntoCache(logins);
        }
        catch (IOException eIo)
        {
            JOptionPane.showMessageDialog(
                this,
                JapaneseStrings.get("cache save login fail")
                + "\n" + eIo.getClass()
                + ": " + eIo.getMessage());
        }

        primaire.populateLists();
        startupProgress.markComplete("have lists");

        primaire.populateEmojis(false);
        startupProgress.markComplete("have emojis");

        primaire.finishedLogin(startupProgress);
        display.setCursor(null);
        display.setShowingProgress(false);
    }

    public void
    useInstanceUrl()
    {
        String url = display.getInstanceUrl();
        if (url.trim().isEmpty())
        {
            // Should we show an error dialog..?
            display.setInstanceUrl("");
            return;
        }
        if (!hasProtocol(url))
        {
            url = "https://" + url;
            display.setInstanceUrl(url);
            display.paintImmediately(display.getBounds(null));
        }

        startupProgress.reset();
        display.setCursor(WAIT);
        display.setShowingProgress(true);
        api.setInstanceUrl(url);

        RequestOutcome r = api.getAppCredentialsAnew();
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("app cred fail")
                + "\n" + r.exception.getMessage()
            );

            display.setShowingProgress(false);
            display.setCursor(null);
            return;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("app cred fail")
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP error code: " + r.httpCode + ")"
            );

            display.setShowingProgress(false);
            display.setCursor(null);
            return;
        }
        else
        {
            api.setAppCredentials(r.entity);
            startupProgress.markComplete("have instance conn");
            startupProgress.markComplete("have app cred");
        }

        URI uri = api.getAuthorisationURL();

        boolean s1 = Desktop.isDesktopSupported();
        Desktop d = s1 ? Desktop.getDesktop() : null;
        boolean s2 = s1 && d.isSupported(Desktop.Action.BROWSE);
        if (s2)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("acc token renewal instr"),
                JapaneseStrings.get("acc token renewal title"),
                JOptionPane.INFORMATION_MESSAGE
            );
            try { d.browse(uri); }
            catch (IOException eIo) { s2 = false; }
        }
        if (!s2)
        {
            JTextField field = new JTextField();
            field.setText(uri.toString());
            field.setPreferredSize(new Dimension(120, 32));
            field.selectAll();

            JOptionPane.showMessageDialog(
                LoginWindow.this,
                new Object[] {
                    JapaneseStrings.get("acc token renewal instr"),
                    JapaneseStrings.get("acc token renewal ask copy"),
                    field
                },
                JapaneseStrings.get("acc token renewal title"),
                JOptionPane.INFORMATION_MESSAGE
            );
        }

        display.setShowingProgress(false);
        display.setCursor(null);
        display.receiveAuthorisationCode();
    }

    public void
    useAuthorisationCode()
    {
        String code = display.getAuthorisationCode();

        display.setCursor(WAIT);
        display.setShowingProgress(true);

        RequestOutcome r = api.getAccessTokenAnew(code);
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("acc token fail")
                + "\n" + r.exception.getMessage()
            );

            display.setShowingProgress(false);
            display.setCursor(null);
            return;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("acc token fail")
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP error code: " + r.httpCode + ")"
            );

            display.setShowingProgress(false);
            display.setCursor(null);
            return;
        }
        else
        {
            api.setAccessToken(r.entity);
            startupProgress.markComplete("have acc token");
        }

        r = api.getAccountDetailsAnew();
        if (r.exception != null)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("account details fail")
                + "\n" + r.exception.getMessage()
            );

            display.setShowingProgress(false);
            display.setCursor(null);
            return;
        }
        else if (r.errorEntity != null)
        {
            JOptionPane.showMessageDialog(
                LoginWindow.this,
                JapaneseStrings.get("account details fail")
                + "\n" + r.errorEntity.get("error").value
                + "\n(HTTP error code: " + r.httpCode + ")"
            );

            display.setShowingProgress(false);
            display.setCursor(null);
            return;
        }
        else
        {
            api.setAccountDetails(r.entity);
            startupProgress.markComplete("have account details");
        }

        try
        {
            if (logins == null) logins = new ArrayList<>();
            else assert !logins.isEmpty();

            LoginDetails current = api.getLogin();
            int mo; for (mo = 0; mo < logins.size(); ++mo)
            {
                LoginDetails login = logins.get(mo);
                boolean m1 =
                    login.accountId.equals(current.accountId);
                boolean m2 = 
                    login.instanceUrl
                        .equals(current.instanceUrl);
                if (m1 && m2) break;
            }
            if (mo < logins.size()) logins.set(mo, current);
            else logins.add(current);

            api.saveLoginsIntoCache(logins);
        }
        catch (IOException eIo)
        {
            JOptionPane.showMessageDialog(
                this,
                JapaneseStrings.get("cache save login fail")
                + "\n" + eIo.getClass()
                + ": " + eIo.getMessage());
        }

        primaire.populateLists();
        startupProgress.markComplete("have lists");
        
        primaire.populateEmojis(false);
        startupProgress.markComplete("have emojis");
        
        primaire.finishedLogin(startupProgress);
        display.setCursor(null);
        display.setShowingProgress(false);
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;
        boolean isDisposed = !isDisplayable();

        display.setScale(scale);

        int w = 280 * scale/20;
        int h = 360 * scale/20;
        display.setPreferredSize(new Dimension(w, h));
        pack(); if (isDisposed) dispose();
    }

//   -  -%-  -

    public void
    imageReady(String name, Image image)
    {
        String[] fields = name.split(":");
        assert fields.length == 2;
        try
        {
            int hashCode = Integer.parseInt(fields[0]);
            int o = Integer.parseInt(fields[1]);
            if (hashCode != logins.hashCode()) return;
            LoginDetails login = logins.get(o);
            login.avatar = image;
            boolean gif = login.avatarUrl.endsWith(".gif");
            display.setAvatar(o, login.avatar, gif);
        }
        catch (NumberFormatException eNf)
        {
            assert false;
        }
    }

    public void
    stepComplete(OperationProgress progress, String stepName)
    {
        assert progress == this.startupProgress;
        final String[] STEP_NAMES = new String[] {
            "have instance conn",
            "have app cred",
            "have acc token",
            "have account details",
            "have lists",
            "have emojis",
            "window updater ready",
            "auto view ready",
            "auto media ready",
            "timeline window ready",
            "notifs window ready" };

        StringBuilder b = new StringBuilder();
        for (String stepName2: STEP_NAMES)
        {
            String stepDesc = JapaneseStrings.get(stepName2);
            char symbol = startupProgress.get(stepName2) ? '✓' : '✕';
            b.append(symbol + " " + stepDesc + "\n");
        }
        display.setText(b.toString());

        int line; for (line = 1; line <= STEP_NAMES.length; ++line)
            if (stepName.equals(STEP_NAMES[line - 1])) break;
        display.setLine(line);

        display.paintImmediately(display.getBounds());
    }

//   -  -%-  -

    public static boolean
    hasProtocol(String url) { return url.matches("^.+://.*"); }

//  ---%-@-%---

    LoginWindow(JKomasto primaire)
    {
        this.primaire = primaire;
        api = primaire.getMastodonApi();
        fetcher = primaire.getImageFetcher();

        startupProgress = new OperationProgress();
        final String[] STEP_NAMES = new String[] {
            "have instance conn",
            "have app cred",
            "have acc token",
            "have account details",
            "have lists",
            "have emojis",
            "window updater ready",
            "auto view ready",
            "auto media ready",
            "timeline window ready",
            "notifs window ready" };
        for (String stepName: STEP_NAMES) startupProgress.add(stepName);
        startupProgress.addListener(this);

        setTitle(JapaneseStrings.get("login title"));

        display = new LoginComponent(this, fetcher);
        display.setMayAutoLogin(false);
        add(display);

        setIconImage(primaire.getProgramIcon());
        setLocationByPlatform(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setScale(20);

        addKeyListener(new SharedKeyShortcuts(primaire, true));
        addMouseWheelListener(new MouseWheelScaler(2, 10));

        try
        {
            logins = api.getLoginsFromCache();
            int o = 0; for (LoginDetails login: logins)
            {
                String name = logins.hashCode() + ":" + o;
                fetcher.fetch(login.avatarUrl, name, 1, this);
                ++o;
            }
            display.setMayAutoLogin(true);
            display.setAutoLogins(logins);
        }
        catch (FileNotFoundException eNf)
        {
            logins = null;
        }
        catch (IOException eIo)
        {
            JOptionPane.showMessageDialog(
                this,
                JapaneseStrings.get("cache load login fail")
                + "\n" + eIo.getClass() + ": " + eIo.getMessage()
            );
            logins = null;
            /*
            * We need to warn the user that it is strongly advised
            * that they quit the application and fix the problem,
            * assuming they edited that cache file. Because if they
            * do a new login now, it will overwrite the old
            * erroneous cache file.
            */
            /*
            * Are you sure you meant to show a message dialog
            * to an unconstructed and unshown us? We should
            * be a valid JComponent by now but, where would
            * the message even pop up.
            */
        }

        Settings settings = primaire.getSettings();
        try
        {
            SettingsApi.loadSettingsFromCache(settings);
        }
        catch (FileNotFoundException eNf)
        {
            SettingsApi.setDefaultSettings(settings);
        }
        catch (IOException eIo)
        {
            JOptionPane.showMessageDialog(
                this,
                JapaneseStrings.get("cache load settings fail")
                + "\n" + eIo.getClass() + ": " + eIo.getMessage()
            );
            SettingsApi.setDefaultSettings(settings);
            /*
            * Again, we should warn the user since if they open
            * the settings dialog it'll come up with the defaults,
            * and if they accept it'll overwrite their current
            * settings.
            */
        }
    }
}

class
LoginComponent extends JPanel
implements
    Scalable,
    ActionListener, FocusListener,
    MouseListener, KeyListener {

    private LoginWindow
    primaire;

    private ImageFetcher
    fetcher;

//   -  -%-  -

    private JTextArea
    statusDisplay;

    private JTextField
    instanceUrlField,
    authorisationCodeField;

    private JLabel
    instanceUrlLabel,
    authorisationCodeLabel;

    private JButton
    instanceUrlButton,
    authorisationCodeButton;

    private JPanel
    labelArea,
    forField,
    manualLoginPanel,
    optionsPanel,
    bottomPanel;

    private Box
    autoLoginPanel;

    private JLabel
    statusDisplay2;

    private JCheckBox
    autoLoginToggle;

    private boolean
    mayAutoLogin;

    private int
    scale;

    private Image
    backgroundImage;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public String
    getInstanceUrl()
    {
        return instanceUrlField.getText();
    }

    public void
    setInstanceUrl(String url)
    {
        instanceUrlField.setText(url);
    }

    public String
    getAuthorisationCode()
    {
        return authorisationCodeField.getText();
    }

    public void
    setText(String status)
    {
        statusDisplay.setText(status);
    }

    public void
    setAutoLoginToggled(boolean a)
    {
        autoLoginToggle.setSelected(a);
    }

    public boolean
    isAutoLoginToggled()
    {
        return autoLoginToggle.isSelected();
    }

    public void
    setMayAutoLogin(boolean a)
    {
        mayAutoLogin = a;
        if (isAncestorOf(instanceUrlField))
            autoLoginToggle.setEnabled(mayAutoLogin);
    }

    public void
    receiveInstanceUrl()
    {
        labelArea.remove(authorisationCodeLabel);
        forField.remove(authorisationCodeButton);
        manualLoginPanel.remove(authorisationCodeField);
        labelArea.add(instanceUrlLabel, BorderLayout.NORTH);
        forField.add(instanceUrlButton, BorderLayout.EAST);
        manualLoginPanel.add(instanceUrlField);
        revalidate();
        autoLoginToggle.setEnabled(mayAutoLogin);
    }

    public void
    receiveAuthorisationCode()
    {
        labelArea.remove(instanceUrlLabel);
        forField.remove(instanceUrlButton);
        manualLoginPanel.remove(instanceUrlField);
        labelArea.add(authorisationCodeLabel, BorderLayout.NORTH);
        forField.add(authorisationCodeButton, BorderLayout.EAST);
        manualLoginPanel.add(authorisationCodeField);
        revalidate();
        autoLoginToggle.setEnabled(false);
    }

    public void
    setAutoLogins(List<LoginDetails> logins)
    {
        autoLoginPanel.removeAll();
        autoLoginPanel.add(Box.createVerticalGlue());
        for (LoginDetails login: logins)
        {
            AutoLoginComponent c =
                new AutoLoginComponent(fetcher);
            c.setName(login.accountId);
            c.setInstance(login.instanceUrl);
            c.setFont(getFont());
            boolean gif = login.avatarUrl.endsWith(".gif");
            c.setAvatar(login.avatar, gif);
            c.setBorderMode(1);
            c.addFocusListener(this);
            c.addKeyListener(this);
            c.addMouseListener(this);
            c.setScale(getScale());

            try
            {
                String s =
                    login.accountId + " at "
                    + login.instanceUrl.replaceAll("^https://", "");
                s = new String(s.getBytes("US-ASCII"), "US-ASCII");
                c.getAccessibleContext().setAccessibleName(s);
            }
            catch (UnsupportedEncodingException eUe)
            {
                assert false;
            }

            autoLoginPanel.add(c);
        }
        autoLoginPanel.add(Box.createVerticalGlue());
    }

    public void
    setAvatar(int o, Image avatar, boolean gif)
    {
        AutoLoginComponent target = null;

        int o2 = -1;
        for (Component c: autoLoginPanel.getComponents())
            if (c instanceof AutoLoginComponent)
                if (++o2 == o) {
                    target = (AutoLoginComponent)c;
                    break;
                }

        assert target != null;
        target.setAvatar(avatar, gif);
    }

    public void
    setShowingProgress(boolean n)
    {
        if (n)
            ((CardLayout)bottomPanel.getLayout())
                .last(bottomPanel);
        else
            ((CardLayout)bottomPanel.getLayout())
                .first(bottomPanel);
    }

    public void
    setLine(int line)
    {
        if (line > 0)
        {
            String[] lines = statusDisplay.getText().split("\n");
            assert line <= lines.length;
            statusDisplay2.setText(lines[line - 1]);
            /*
            * I'm unable to get the accessibility API to announce
            * these changes, it seems. Cause, the main way is to
            * focus the component, but my methods that cause the
            * status display to be shown go on to do work, hogging
            * the EDT, so I think the status display isn't gaining
            * focus. I tried other ways like forcing the focus to
            * fall somehow (no programmatic way to do so provided)
            * or firing some kind of property change, those didn't
            * go either.
            *
            * I'm getting tired of these APIs that don't have clear
            * support of something and I'm thrashing about trying to
            * get something done. Technically this live region isn't
            * needed, as the auto login component itself is described
            * and the login promptly completes anyways, so I'm
            * stopping here. But I wish there was a way to create a
            * live region, in this framework that doesn't seem to
            * have it. Maybe it involves not being on the EDT and
            * busywaiting for focus to be transferred to something.
            */
        }
    }

//   -  -%-  -

    public void
    actionPerformed(ActionEvent eA)
    {
        if (eA.getSource() == instanceUrlButton)
        {
            if (isAutoLoginToggled()) primaire.useCache(1);
            else primaire.useInstanceUrl();
        }
        if (eA.getSource() == authorisationCodeButton)
            primaire.useAuthorisationCode();
    }

    public int
    getLoginId(AutoLoginComponent a)
    {
        int returnee = -1, id = 0;
        Component[] components = autoLoginPanel.getComponents();
        for (Component c: autoLoginPanel.getComponents())
        {
            if (!(c instanceof AutoLoginComponent)) continue;
            ++id;
            if (c == a) { returnee = id; break; }
        }
        assert returnee != -1;
        return returnee;
    }

    public void
    focusGained(FocusEvent eF)
    {
        assert eF.getSource() instanceof AutoLoginComponent;
        AutoLoginComponent a =
            (AutoLoginComponent)eF.getSource();

        a.setBorderMode(2);
        a.getAccessibleContext().firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            null, AccessibleState.FOCUSED
        );
    }

    public void
    focusLost(FocusEvent eF)
    {
        assert eF.getSource() instanceof AutoLoginComponent;
        AutoLoginComponent a =
            (AutoLoginComponent)eF.getSource();

        a.setBorderMode(1);
        a.getAccessibleContext().firePropertyChange(
            AccessibleContext.ACCESSIBLE_STATE_PROPERTY,
            AccessibleState.FOCUSED, null
        );
    }

    public void
    keyPressed(KeyEvent eK)
    {
        assert eK.getSource() instanceof AutoLoginComponent;
        AutoLoginComponent a =
            (AutoLoginComponent)eK.getSource();
        int code = eK.getKeyCode();

        setCursor(WAIT);

        if (code == KeyEvent.VK_SPACE
            || code == KeyEvent.VK_ENTER)
        {
            a.setBorderMode(3);
            primaire.useCache(getLoginId(a));
            a.setBorderMode(1);
        }

        setCursor(null);
    }

    public void
    mouseClicked(MouseEvent eM)
    {
        assert eM.getSource() instanceof AutoLoginComponent;
        AutoLoginComponent a =
            (AutoLoginComponent)eM.getSource();

        setCursor(WAIT);

        // We're blocking the EDT soon so don't request focus and
        // wait for the redraw. Set the borders now.
        for (Component c: autoLoginPanel.getComponents())
        {
            if (!(c instanceof AutoLoginComponent)) continue;
            AutoLoginComponent a2 = (AutoLoginComponent)c;
            a2.setBorderMode(a2 == a ? 3 : 1);
        }

        primaire.useCache(getLoginId(a));
        a.setBorderMode(1);

        setCursor(null);
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        for (Component c: autoLoginPanel.getComponents())
        {
            if (!(c instanceof Scalable)) continue;
            ((Scalable)c).setScale(scale);
        }

        int bw = 14 * scale/20;
        Border b1, b2, bi, bo, be;
        b1 = BorderFactory.createEtchedBorder();
        b2 = BorderFactory.createEmptyBorder(bw, bw, bw, bw);
        bi = BorderFactory.createCompoundBorder(b1, b2);
        bo = BorderFactory.createEmptyBorder(bw, bw, bw, bw);
        be = BorderFactory.createEmptyBorder();

        String fname = "VL PGothic";
        int fsz = 14 * scale/20;
        Font small = new Font(fname, Font.PLAIN, fsz);
        Font medium = new Font(fname, Font.PLAIN, fsz * 14/12);
        Font large = new Font(fname, Font.PLAIN, fsz * 16/12);

        ((BorderLayout)manualLoginPanel.getLayout())
            .setVgap(4 * scale/20);

        instanceUrlField.setFont(small);
        instanceUrlLabel.setFont(small);
        instanceUrlButton.setFont(small);

        authorisationCodeField.setFont(small);
        authorisationCodeLabel.setFont(small);
        authorisationCodeButton.setFont(small);

        autoLoginToggle.setBorder(be);
        autoLoginToggle.setFont(small);

        optionsPanel.setBorder(bi);

        statusDisplay.setBorder(bi);
        statusDisplay.setFont(large);
        statusDisplay2.setFont(medium);

        //((BorderLayout)getLayout()).setHgap(8 * scale/20);
        setBorder(bo);
    }

    protected void
    paintComponent(Graphics g)
    {
        if (backgroundImage != null)
        {
            int pw = backgroundImage.getWidth(null);
            int ph = backgroundImage.getHeight(null);
            int ox = -(getWidth() % pw) / 2;
            int oy = -(getHeight() % ph) / 2;
            if (pw != -1)
            {
                for (int y = oy; y < getHeight(); y += ph)
                for (int x = ox; x < getWidth(); x += pw)
                    g.drawImage(backgroundImage, x, y, null);
            }
        }

        super.paintComponent(g);
    }


    public void
    mousePressed(MouseEvent eM) { }

    public void
    mouseReleased(MouseEvent eM) { }

    public void
    mouseEntered(MouseEvent eM) { }

    public void
    mouseExited(MouseEvent eM) { }

    public void
    keyReleased(KeyEvent eK) { }

    public void
    keyTyped(KeyEvent eK) { }

//  ---%-@-%---

    LoginComponent(LoginWindow primaire, ImageFetcher fetcher)
    {
        this.primaire = primaire;
        this.fetcher = fetcher;

        mayAutoLogin = true;
        setOpaque(false);

        backgroundImage = ImageApi.local("loginWindow");
        ImageApi.awaitImage(backgroundImage);

        String[] s = new String[] {
            JapaneseStrings.get("enter instance"),
            JapaneseStrings.get("connect"),
            JapaneseStrings.get("paste auth code"),
            JapaneseStrings.get("login"),
            JapaneseStrings.get("try auto login") };

        instanceUrlField = new JTextField();
        instanceUrlLabel = new JLabel(s[0]);
        instanceUrlLabel.setLabelFor(instanceUrlField);
        instanceUrlButton = new JButton(s[1]);
        instanceUrlButton.setMnemonic(KeyEvent.VK_C);
        instanceUrlButton.addActionListener(this);

        authorisationCodeField = new JPasswordField();
        authorisationCodeLabel = new JLabel(s[2]);
        authorisationCodeLabel.setLabelFor(authorisationCodeField);
        authorisationCodeButton = new JButton(s[3]);
        authorisationCodeButton.setMnemonic(KeyEvent.VK_L);
        authorisationCodeButton.addActionListener(this);

        labelArea = new JPanel();
        labelArea.setOpaque(false);
        labelArea.setLayout(new BorderLayout());

        forField = new JPanel();
        forField.setOpaque(false);
        forField.setLayout(new BorderLayout());
        forField.add(labelArea, BorderLayout.WEST);

        autoLoginToggle = new JCheckBox(s[4]);
        autoLoginToggle.setMnemonic(KeyEvent.VK_A);
        autoLoginToggle.setEnabled(mayAutoLogin);

        optionsPanel = new JPanel();
        optionsPanel.setOpaque(false);
        optionsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        optionsPanel.add(autoLoginToggle);

        manualLoginPanel = new JPanel();
        manualLoginPanel.setOpaque(false);
        manualLoginPanel.setLayout(new BorderLayout());
        manualLoginPanel.add(instanceUrlField);
        manualLoginPanel.add(forField, BorderLayout.SOUTH);
        manualLoginPanel.getAccessibleContext()
            .setAccessibleName("Login to a new instance");

        autoLoginPanel = Box.createVerticalBox();
        autoLoginPanel.getAccessibleContext()
            .setAccessibleName("Auto login list");

        statusDisplay = new JTextArea();
        statusDisplay.setEditable(false);
        statusDisplay.setBackground(null);

        statusDisplay2 = new JLabel();

        bottomPanel = new JPanel();
        bottomPanel.setOpaque(false);
        bottomPanel.setLayout(new CardLayout());
        bottomPanel.add(manualLoginPanel);
        bottomPanel.add(statusDisplay2);

        receiveInstanceUrl();

        setLayout(new BorderLayout());
        add(autoLoginPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
        //add(optionsPanel, BorderLayout.CENTER);
        //add(statusDisplay, BorderLayout.SOUTH);
    }

}

class
AutoLoginComponent extends JComponent
implements Scalable, Accessible, ImageFetcher.Listener {

    private ImageFetcher
    fetcher;

    private Image
    avatar;

    private boolean
    gif;

//   -  -%-  -

    private JLabel
    name, instance;

    private Image
    scaledAvatar;

    private ImageComponent
    avatarDisplay;

    private Access
    access;

    private int
    scale;

    private int
    mode;

//  ---%-@%---

    public void
    setName(String n) { name.setText(n); }

    public void
    setInstance(String n) { instance.setText(n); }

    public void
    setAvatar(Image avatar, boolean gif)
    {
        if (scaledAvatar != null && scaledAvatar != this.avatar)
            scaledAvatar.flush();

        this.avatar = avatar;
        scaledAvatar = null;
        this.gif = gif;
        avatarDisplay.considerRescale();
        setToolTipText(
            ImageApi.debugString(this.avatar, scaledAvatar));
        avatarDisplay.repaint();
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        String fname = "VL PGothic";
        int fsz = 15 * scale/20;
        Font large = new Font(fname, Font.PLAIN, fsz * 14/12);
        Font small = new Font(fname, Font.PLAIN, fsz);

        name.setFont(large);
        instance.setFont(small);

        int lineHeight = getFontMetrics(small).getHeight();
        int hgap = lineHeight * 1/2;
        int ah = lineHeight * 3;
        ((BorderLayout)getLayout()).setHgap(hgap);
        avatarDisplay.setPreferredSize(new Dimension(ah, ah));

        int mw = Integer.MAX_VALUE;
        int mh = getPreferredSize().height;
        setMaximumSize(new Dimension(mw, mh));

        setBorderMode(mode);
    }

    public void
    setBorderMode(int mode)
    {
        this.mode = mode;

        int u1 = ((BorderLayout)getLayout()).getHgap();
        int u2 = u1 / 2;

        Border bevel =
            BorderFactory.createEmptyBorder(2, 2, 2, 2);
        if (mode == 2)
            bevel = BorderFactory.createRaisedBevelBorder();
        if (mode == 3)
            bevel = BorderFactory.createLoweredBevelBorder();

        Border padding =
            BorderFactory.createEmptyBorder(0, u1, 0, 0);
        Border margin =
            BorderFactory.createEmptyBorder(u2, 0, u2, 0);

        Border b1 =
            BorderFactory.createCompoundBorder(bevel, padding);
        Border b2 =
            BorderFactory.createCompoundBorder(margin, b1);
        setBorder(b2);
    }

    public AccessibleContext
    getAccessibleContext() { return access; }

//   -  -%-  -

    public void
    imageReady(String name, Image image)
    {
        scaledAvatar = image;
        avatarDisplay.repaint();
    }

//  ---%-@-%---

    private class
    ImageComponent extends JComponent {

        protected void
        paintComponent(Graphics g)
        {
            if (scaledAvatar != null)
            {
                int w = getWidth(), h = getHeight();
                g.drawImage(
                    scaledAvatar, 0, 0, w, h,
                    gif ? this : null);
            }
        }

        protected void
        considerRescale()
        {
            if (!isDisplayable()) return;

            if (avatar != null)
            {
                if (!gif)
                {
                    int w = getWidth(), h = getHeight();
                    assert w > 0 && h > 0;
    
                    boolean rescale = true;
                    if (scaledAvatar != null)
                    {
                        int sw = scaledAvatar.getWidth(null);
                        int sh = scaledAvatar.getHeight(null);
                        rescale = sw != w || sh != h;
                    }
    
                    boolean rescaling = scaledAvatar == avatar;
                    if (rescale && !rescaling)
                    {
                        scaledAvatar = avatar;
                        fetcher.scale(
                            avatar, w, h,
                            null, 1, AutoLoginComponent.this);
                    }
                }
                else
                {
                    scaledAvatar = avatar;
                }
            }
        }

    }

    private class
    Access extends AccessibleJComponent
    implements AccessibleAction {

        public AccessibleRole
        getAccessibleRole() { return AccessibleRole.LIST_ITEM; }

        public int
        getAccessibleChildrenCount() { return 0; }

        public AccessibleAction
        getAccessibleAction() { return this; }

        public int
        getAccessibleActionCount() { return 1; }

        public String
        getAccessibleActionDescription(int o)
        {
            return "Log into this account";
        }

        public boolean
        doAccessibleAction(int o)
        {
            KeyEvent eK = new KeyEvent(
                AutoLoginComponent.this, KeyEvent.KEY_FIRST, 0,
                0, KeyEvent.VK_ENTER, KeyEvent.CHAR_UNDEFINED
            );
            for (KeyListener listener: getKeyListeners())
            {
                listener.keyPressed(eK);
            };
            return getKeyListeners().length > 0;
        }

    }

//  ---%-@%---

    AutoLoginComponent(ImageFetcher fetcher)
    {
        this.fetcher = fetcher;

        access = new Access();

        name = new JLabel();
        instance = new JLabel();

        Box right = Box.createVerticalBox();
        right.add(Box.createVerticalGlue());
        right.add(name);
        right.add(instance);
        right.add(Box.createVerticalGlue());

        avatarDisplay = new ImageComponent();
        
        setLayout(new BorderLayout());
        add(right, BorderLayout.CENTER);
        add(avatarDisplay, BorderLayout.WEST);

        setFocusable(true);
    }

}
