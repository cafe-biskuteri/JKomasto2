
import javax.swing.JFrame;
import javax.swing.JComponent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JScrollPane;
import javax.swing.Scrollable;
import javax.swing.JComboBox;
import javax.swing.border.Border;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;

class
SettingsWindow extends JFrame
implements Scalable, WindowListener {

    private JKomasto
    primaire;

    private Settings
    settings;

//   -  -%-  -

    private SettingsComponent
    display;

    private int
    scale;

//  ---%-@-%---

    public void
    syncDisplayToSettings()
    {
        assert settings != null;
        display.setDefaultVisibility(settings.defaultVisibility);
        display.setSpeechPaneCommand(settings.speechCommand);
        display.setImagePaneCommand(settings.imageCommand);
        display.setVideoPaneCommand(settings.videoCommand);
        display.setAudioPaneCommand(settings.audioCommand);
        display.setTextPaneFontNames(
            settings.richTextPaneFontNames);
    }

    public void
    syncSettingsToDisplay()
    {
        
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;
        boolean disposed = !isDisplayable();

        display.setScale(scale);
        pack(); if (disposed) dispose();
    }

    public void
    windowOpened(WindowEvent eW) { setScale(this.scale); }

    public void
    windowClosed(WindowEvent eW) { }

    public void
    windowClosing(WindowEvent eW) { }

    public void
    windowActivated(WindowEvent eW) { }

    public void
    windowDeactivated(WindowEvent eW) { }

    public void
    windowIconified(WindowEvent eW) { }

    public void
    windowDeiconified(WindowEvent eW) { }

//  ---%-@-%---

    SettingsWindow(JKomasto primaire)
    {
        setTitle(JapaneseStrings.get("settings title"));

        this.primaire = primaire;
        settings = primaire.getSettings();

        display = new SettingsComponent(this);
        setContentPane(display);

        /*
        String key = "TabbedPane.tabAreaBackground";
        Color bg = javax.swing.UIManager.getColor(key);
        setBackground(bg);
        */

        setIconImage(primaire.getProgramIcon());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        setScale(20);
        addMouseWheelListener(new MouseWheelScaler(1, 6));
        addWindowListener(this);

        syncDisplayToSettings();
    }

}

class
SettingsComponent extends JComponent {

    private SettingsWindow
    primaire;

    private JTabbedPane
    tabs;

    private GeneralOptionsComponent
    generalOptions;

    private ExternalCommandsComponent
    externalCommands;

    private TextPaneOptionsComponent
    textPaneOptions;

//  ---%-@-%---

    public void
    setDefaultVisibility(PostVisibility visibility)
    {
        generalOptions.setDefaultVisibility(visibility);
    }

    public void
    setSpeechPaneCommand(String[] command)
    {
        externalCommands.setSpeechCommand(command);
    }

    public void
    setImagePaneCommand(String[] command)
    {
        externalCommands.setImageCommand(command);
    }

    public void
    setVideoPaneCommand(String[] command)
    {
        externalCommands.setVideoCommand(command);
    }

    public void
    setAudioPaneCommand(String[] command)
    {
        externalCommands.setAudioCommand(command);
    }

    public void
    setTextPaneFontNames(String[] fontNames)
    {
        textPaneOptions.setFontNames(fontNames);
    }

//   -  -%-  -

    public void
    setScale(int scale)
    {
        generalOptions.setScale(scale);
        externalCommands.setScale(scale);
        textPaneOptions.setScale(scale);

        int w = 280 * scale/20;
        int h = 128 * scale/20;
        generalOptions.setPreferredSize(new Dimension(w, h));

        h = 192 * scale/20;
        externalCommands.setPreferredSize(new Dimension(w, h));

        h = 160 * scale/20;
        textPaneOptions.setPreferredSize(new Dimension(w, h));

        int xm = 10 * scale/20;
        int ym = 6 * scale/20;
        setBorder(BorderFactory.createEmptyBorder(ym, xm, ym, xm));
    }

//  ---%-@-%---

    SettingsComponent(SettingsWindow primaire)
    {
        this.primaire = primaire;

        String s1 = "General options";
        String s2 = "External commands";
        String s3 = "Text pane options";
        Border b1 = BorderFactory.createEtchedBorder();
        Border b2 = BorderFactory.createTitledBorder(b1, s1);
        Border b3 = BorderFactory.createTitledBorder(b1, s2);
        Border b4 = BorderFactory.createTitledBorder(b1, s3);

        generalOptions = new GeneralOptionsComponent(primaire);
        generalOptions.setBorder(b2);

        externalCommands =
            new ExternalCommandsComponent(primaire);
        externalCommands.setBorder(b3);
        
        textPaneOptions = new TextPaneOptionsComponent(primaire);
        textPaneOptions.setBorder(b4);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(generalOptions);
        add(Box.createVerticalStrut(4));
        add(externalCommands);
        add(Box.createVerticalStrut(4));
        add(textPaneOptions);
    }

}

class
GeneralOptionsComponent extends JComponent {

    private SettingsWindow
    primaire;

//   -  -%-  -

    private JLabel
    defaultVisibilityLabel;

    private JComboBox<String>
    defaultVisibility;

    private int
    ym, xm;

//  ---%-@-%---

    public PostVisibility
    getDefaultVisibility()
    {
        String[] options = new String[] {
            JapaneseStrings.get("compose public"),
            JapaneseStrings.get("compose unlisted"),
            JapaneseStrings.get("compose followers"),
            JapaneseStrings.get("compose mentioned")
        };
        String s = (String)defaultVisibility.getSelectedItem();
        if (s.equals(options[0])) return PostVisibility.PUBLIC;
        if (s.equals(options[1])) return PostVisibility.UNLISTED;
        if (s.equals(options[2])) return PostVisibility.FOLLOWERS;
        if (s.equals(options[3])) return PostVisibility.MENTIONED;
        assert false;
        return null;
    }

    public void
    setDefaultVisibility(PostVisibility visibility)
    {
        String s = null;
        if (visibility == PostVisibility.PUBLIC)
            s = JapaneseStrings.get("compose public");
        if (visibility == PostVisibility.UNLISTED)
            s = JapaneseStrings.get("compose unlisted");
        if (visibility == PostVisibility.FOLLOWERS)
            s = JapaneseStrings.get("compose followers");
        if (visibility == PostVisibility.MENTIONED)
            s = JapaneseStrings.get("compose mentioned");
        assert s != null;
        defaultVisibility.setSelectedItem(s);
    }
    
//   -  -%-  -

    public void
    setScale(int scale)
    {
        String fname = "VL PGothic";
        int fsz = 14 * scale/20;
        Font font = new Font(fname, Font.PLAIN, fsz);

        defaultVisibilityLabel.setFont(font);
        defaultVisibility.setFont(font);

        xm = 10 * scale/20;
        ym = 6 * scale/20;
    }

    public void
    doLayout()
    {
        int w = getWidth(), h = getHeight();
        Insets insets = getInsets();

        FontMetrics fm = getFontMetrics(
            defaultVisibility.getFont());
        int lh = fm.getHeight() * 3/2;

        int xs = insets.left + xm;
        int xe = w - insets.right - xm;
        int x1 = xs + (xe - xs) * 1/4;
        int x2 = x1 + 8;

        int ys = insets.top + ym;
        int ye = h - insets.bottom - ym;
        int y1 = ys + lh;

        defaultVisibilityLabel.setBounds(xs, ys, x1 - xs, y1 - ys);
        defaultVisibility.setBounds(x2, ys, xe - x2, y1 - ys);
    }

//  ---%-@-%---

    GeneralOptionsComponent(SettingsWindow primaire)
    {
        this.primaire = primaire;

        String[] strings = new String[] {
            JapaneseStrings.get("settings default visibility")
        };
        String[] options = new String[] {
            JapaneseStrings.get("compose public"),
            JapaneseStrings.get("compose unlisted"),
            JapaneseStrings.get("compose followers"),
            JapaneseStrings.get("compose mentioned")
        };

        defaultVisibilityLabel = new JLabel(strings[0]);

        defaultVisibility = new JComboBox<>(options);
        defaultVisibilityLabel.setLabelFor(defaultVisibility);

        setLayout(null);
        add(defaultVisibilityLabel);
        add(defaultVisibility);
    }

}

class
ExternalCommandsComponent extends JComponent {

    private SettingsWindow
    primaire;

//   -  -%-  -

    private JLabel
    speechLabel,
    imageLabel,
    videoLabel,
    audioLabel;

    private FieldListComponent
    speechFields,
    imageFields,
    videoFields,
    audioFields;

    private int
    xm, ym;

//  ---%-@-%---

    public void
    setSpeechCommand(String[] command)
    {
        speechFields.setFields(command);
    }

    public void
    setImageCommand(String[] command)
    {
        imageFields.setFields(command);
    }

    public void
    setVideoCommand(String[] command)
    {
        videoFields.setFields(command);
    }

    public void
    setAudioCommand(String[] command)
    {
        audioFields.setFields(command);
    }

//   -  -%-  -

    public void
    setScale(int scale)
    {
        speechFields.setScale(scale);
        imageFields.setScale(scale);
        videoFields.setScale(scale);
        audioFields.setScale(scale);

        String fname = "VL PGothic";
        int fsz = 14 * scale/20;
        Font font = new Font(fname, Font.PLAIN, fsz);

        speechLabel.setFont(font);
        imageLabel.setFont(font);
        videoLabel.setFont(font);
        audioLabel.setFont(font);

        xm = 8 * scale/20;
        ym = 0 * scale/20;
        doLayout();
    }

    public void
    doLayout()
    {
        int w = getWidth(), h = getHeight();
        Insets insets = getInsets();

        FontMetrics fm = getFontMetrics(speechLabel.getFont());
        int lm = max(new int[] {
            fm.stringWidth(speechLabel.getText()),
            fm.stringWidth(imageLabel.getText()),
            fm.stringWidth(videoLabel.getText()),
            fm.stringWidth(audioLabel.getText()) });

        int xs = insets.left + xm;
        int xe = w - insets.right - xm;
        int x1 = xs + lm + 8;

        int ys = insets.top + ym;
        int ye = h - insets.bottom - ym;
        int y1 = ys + (ye - ys) * 1/4;
        int y2 = y1 + (ye - ys) * 1/4;
        int y3 = y2 + (ye - ys) * 1/4;

        speechLabel.setBounds(xs, ys, x1 - xs, y1 - ys);
        imageLabel.setBounds(xs, y1, x1 - xs, y2 - y1);
        videoLabel.setBounds(xs, y2, x1 - xs, y3 - y2);
        audioLabel.setBounds(xs, y3, x1 - xs, ye - y3);

        speechFields.setBounds(x1, ys, xe - x1, y1 - ys);
        imageFields.setBounds(x1, y1, xe - x1, y2 - y1);
        videoFields.setBounds(x1, y2, xe - x1, y3 - y2);
        audioFields.setBounds(x1, y3, xe - x1, ye - y3);
    }

//   -  -%-  -

    private static int
    max(int[] numbers)
    {
        int max = numbers[0];
        for (int n: numbers) if (n > max) max = n;
        return max;
    }

//  ---%-@-%---

    ExternalCommandsComponent(SettingsWindow primaire)
    {
        this.primaire = primaire;

        String[] strings = new String[] {
            JapaneseStrings.get("settings speech title"),
            JapaneseStrings.get("settings image title"),
            JapaneseStrings.get("settings video title"),
            JapaneseStrings.get("settings audio title"),
        };

        speechLabel = new JLabel(strings[0]);
        imageLabel = new JLabel(strings[1]);
        videoLabel = new JLabel(strings[2]);
        audioLabel = new JLabel(strings[3]);
        
        speechFields = new FieldListComponent();
        imageFields = new FieldListComponent();
        videoFields = new FieldListComponent();
        audioFields = new FieldListComponent();
    
        setLayout(null);
        add(speechLabel);
        add(speechFields);
        add(imageLabel);
        add(imageFields);
        add(videoLabel);
        add(videoFields);
        add(audioLabel);
        add(audioFields);
    }

}

class
TextPaneOptionsComponent extends JComponent {

    private SettingsWindow
    primaire;

//   -  -%-  -

    private JLabel
    fontNamesLabel;

    private FieldListComponent
    fontNames;

    private JPanel
    keep1;

//  ---%-@-%---

    public void
    setFontNames(String[] fontNames)
    {
        this.fontNames.setFields(fontNames);
    }

    public String[]
    getFontNames() { return fontNames.getFields(); }

//   -  -%-  -

    public void
    setScale(int scale)
    {
        String fname = "VL PGothic";
        int fsz = 14 * scale/20;
        Font font = new Font(fname, Font.PLAIN, fsz);

        fontNamesLabel.setFont(font);
        keep1.setMaximumSize(new Dimension(
            Short.MAX_VALUE,
            keep1.getPreferredSize().height));

        fontNames.setScale(scale);
    }

//  ---%-@-%---

    TextPaneOptionsComponent(SettingsWindow primaire)
    {
        this.primaire = primaire;

        String[] strings = new String[] {
            JapaneseStrings.get("settings font names")
        };

        fontNamesLabel = new JLabel(strings[0]);
        keep1 = new JPanel();
        keep1.add(fontNamesLabel);
        
        fontNames = new FieldListComponent();

        JScrollPane scroll = new JScrollPane(
            fontNames,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setBorder(null);
    
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(keep1);
        add(scroll);
    }

}

class
FieldListComponent extends JComponent
implements ActionListener, FocusListener, Scrollable {

    private JButton
    add;

    private int
    scale;

//  ---%-@-%---

    public void
    setFields(String[] fields)
    {
        removeAll();
        for (String field: fields)
        {
            JButton addee = new JButton(field);
            addee.setMargin(new Insets(0, 0, 0, 0));
            addee.addActionListener(this);
            add(addee);
        }
        add(add);

        setScale(scale);
        repaint();
    }

    public String[]
    getFields()
    {
        settleFields();
        cullFields();

        String[] returnee = new String[getFieldCount()];

        int o = -1; for (Component c: getComponents())
        {
            if (c == add) continue;
            assert c instanceof JButton;

            returnee[++o] = ((JButton)c).getText();
        }

        return returnee;
    }

    public int
    getFieldCount()
    {
        int count = 0;
        for (Component c: getComponents())
            if (c instanceof JButton)
                ++count;
        return count;
    }

//   -  -%-  -

    private void
    addField()
    {
        int o = getComponentCount() - 1;
        assert getComponent(o) == add;

        JTextField addee = new JTextField("");
        addee.addFocusListener(this);

        add(addee, o);
        setScale(scale);
        addee.requestFocusInWindow();
    }

    private void
    settleFields()
    {
        for (int o = 0; o < getComponentCount(); ++o)
        {
            Component c = getComponent(o);
            if (c == add) continue;
            if (!(c instanceof JTextField)) continue;

            String text = ((JTextField)c).getText();
            JButton addee = new JButton(text);
            addee.setMargin(new Insets(0, 0, 0, 0));
            addee.addActionListener(this);

            remove(o);
            add(addee, o);
        }

        setScale(scale);
        repaint();
    }

    private void
    unsettleField(JButton b)
    {
        int o; for (o = 0; o < getComponentCount(); ++o)
            if (getComponent(o) == b) break;
        assert o != getComponentCount();

        String text = b.getText();
        JTextField addee = new JTextField(text);
        addee.addFocusListener(this);
        
        remove(o);
        add(addee, o);
        setScale(scale);
        repaint();
        addee.requestFocusInWindow();
    }

    private void
    cullFields()
    {
        for (int o = getComponentCount() - 1; o >= 0; --o)
        {
            Component c = getComponent(o);
            if (c == add) continue;
            if (!(c instanceof JButton)) continue;

            String text = ((JButton)c).getText();
            if (text.trim().isEmpty()) remove(o);
        }

        repaint();
    }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        String fname = "VL PGothic";
        int fsz = 14 * scale/20;
        Font font = new Font(fname, Font.PLAIN, fsz);

        ((FlowLayout)getLayout()).setHgap(4 * scale/20);

        FontMetrics fm = getFontMetrics(font);
        int lh = fm.getHeight() * 5/4;
        int p = fsz * 2/14;

        for (Component c: getComponents())
        {
            if (c == add)
            {
                int BOLD_ITALIC = Font.BOLD | Font.ITALIC;
                c.setFont(font.deriveFont(BOLD_ITALIC));
                add.setMargin(new Insets(0, 0, 0, p));
            }
            else if (c instanceof JButton)
            {
                c.setFont(font);
            }
            else if (c instanceof JTextField)
            {
                c.setFont(font);
                c.setMinimumSize(new Dimension(lh * 2, lh));
                c.setPreferredSize(c.getMinimumSize());
            }
        }
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();
        if (src == add) addField();
        else if (src instanceof JButton)
            unsettleField((JButton)src);
    }

    public void
    focusLost(FocusEvent eF)
    {
        settleFields();
        cullFields();
    }

    public boolean
    getScrollableTracksViewportHeight() { return false; }
    
    public boolean
    getScrollableTracksViewportWidth() { return true; }
    
    public Dimension
    getPreferredScrollableViewportSize()
    {
        return getPreferredSize();
    }

    public Dimension
    getPreferredSize()
    {
        // Attempt to predict how much space FlowLayout would use.
        int w = getWidth();
        int hgap = ((FlowLayout)getLayout()).getHgap();
        int vgap = ((FlowLayout)getLayout()).getVgap();

        int h = 0;
        int rw = w;
        int lh = 0;
        for (Component c: getComponents())
        {
            Dimension csz = c.getPreferredSize();
            
            rw -= hgap + csz.width;
            if (rw < 0) {
                h += vgap + lh;
                rw = w - csz.width;
                lh = csz.height;
            }
            else {
                if (csz.height > lh) lh = csz.height;
            }
        }
        h += vgap + lh + vgap;

        return new Dimension(w, h);
    }
    
    public int
    getScrollableBlockIncrement(
        Rectangle visibleRect, int orientation, int direction)
    {
        return visibleRect.height / 2;
    }
    
    public int
    getScrollableUnitIncrement(
        Rectangle visibleRect, int orientation, int direction)
    {
        return visibleRect.height / 8;
    }

    public void
    focusGained(FocusEvent eF) { }

//  ---%-@-%---

    FieldListComponent()
    {
        add = new JButton("+");
        add.setMargin(new Insets(0, 0, 0, 0));
        add.addActionListener(this);

        setLayout(new FlowLayout());
        add(add);
    }

}
