
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Graphics;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Shape;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.Insets;
import java.awt.geom.Ellipse2D;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.util.List;
import java.util.ArrayList;
import cafe.biskuteri.hinoki.Tree;

class
ProfileWindow extends JFrame
implements Scalable, ComponentListener, ImageFetcher.Listener {

    private JKomasto
    primaire;

    private MastodonApi
    api;

    private ImageFetcher
    fetcher;

    private Account
    account;

//   -  -%-  -

    private ProfileComponent
    display;

    private int
    scale;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    use(Account account)
    {
        if (this.account != null)
        {
            if (this.account.avatarResolver == this)
                this.account.avatarResolver = null;
        }

        this.account = account;

        boolean gif = account.avatarUrl.endsWith(".gif");
        display.setAvatar(account.avatar, gif);
        if (account.avatar == null
            && account.avatarResolver == null)
        {
            String name = "" + account.hashCode();
            account.avatarResolver = this;
            fetcher.fetch(account.avatarUrl, name, 4, this);
        }

        display.setAccountID(account.id);
        display.setDisplayName(account.name);

        String t1, t3; String[] t2, t4, t5;
        t1 = JapaneseStrings.get("profile followed template");
        t2 = JapaneseStrings.get("follower count grades").split(",");
        t3 = JapaneseStrings.get("profile history template");
        t4 = JapaneseStrings.get("post count grades").split(",");
        t5 = JapaneseStrings.get("month names").split(",");

        int n1 = account.followedCount;
        int n2 = account.followerCount;
        String sn1 = (n1 > 500) ? t2[0] : (n1 > 100) ? t2[1] : t2[2];
        String sn2 = (n2 > 500) ? t2[0] : (n2 > 100) ? t2[1] : t2[2];
        if (t1.indexOf("<>") > 0) sn1 = sn1.toLowerCase();
        if (t1.indexOf("<><>") > 0) sn2 = sn2.toLowerCase();
        t1 = t1.replaceAll("<><>", sn2).replaceAll("<>", sn1);
        display.setFollowedAndFollowers(t1);

        int n3 = account.postCount;
        String sn3;
        if (n3 >= 1000) sn3 = t4[0] + (n3 / 1000) + t4[1];
        else if (n3 >= 300) sn3 = t4[0] + (n3 / 100) + t4[2];
        else sn3 = Integer.toString(n3);
        String sn4 = t5[account.creationDate.getMonthValue() - 1];
        String sn5 = "" + account.creationDate.getYear();
        t3 = t3.replaceAll("<><><>", sn5);
        t3 = t3.replaceAll("<><>", sn4);
        t3 = t3.replaceAll("<>", sn3);
        display.setHistory(t3);

        for (int i = 1; i <= 4; ++i)
        {
            if (i > account.fields.length)
            {
                display.setField(i, "", null);
                continue;
            }
            String n = account.fields[i - 1][0];
            String v = account.fields[i - 1][1];
            try
            {
                display.setField(i, n, BasicHTMLParser.parse(v));
            }
            catch (InvalidHTMLException eIh)
            {
                String s = "(invalid HTML)";
                display.setField(i, n, new Tree<>("text", s));
            }
        }

        String desc = account.description;
        try
        {
            display.setDescription(BasicHTMLParser.parse(desc));
        }
        catch (InvalidHTMLException eIh)
        {
            String s = "(invalid HTML)";
            display.setDescription(new Tree<String>("text", s));
        }

        setTitle(account.name + " - JKomasto");
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;
        boolean isDisposed = !isDisplayable();

        int w = 512 * scale/20;
        int h = 420 * scale/20;
        display.setPreferredSize(new Dimension(w, h));
        pack(); if (isDisposed) dispose();

        display.setScale(scale);
    }

//   -  -%-  -

    public void
    seePosts()
    {
        display.setCursor(WAIT);

        TimelineWindow w = new TimelineWindow(primaire);
        w.switchToAuthorPosts(account.numId);
        w.setLocationRelativeTo(this);
        w.setVisible(true);

        display.setCursor(null);
    }

    public void
    seeFollowers()
    {
        display.setCursor(WAIT);

        FollowWindow w = new FollowWindow(primaire);
        w.showFor(account.numId, false);
        w.setTitle("Followers of " + account.id);
        w.setLocation(this.getLocation());
        w.setSize(this.getSize());
        w.setVisible(true);

        display.setCursor(null);
    }

    public void
    componentShown(ComponentEvent eC)
    {
        display.resetFocus();
    }

    public void
    imageReady(String name, Image image)
    {
        if (name.equals("" + account.hashCode()))
        {
            account.avatar = image;
            account.avatarResolver = null;

            boolean gif = account.avatarUrl.endsWith(".gif");
            display.setAvatar(image, gif);
        }
    }


    public void
    componentResized(ComponentEvent eC) { }

    public void
    componentMoved(ComponentEvent eC) { }

    public void
    componentHidden(ComponentEvent eC) { }

//  ---%-@-%---

    ProfileWindow(JKomasto primaire)
    {
        super("Profile window - JKomasto");
        this.primaire = primaire;
        api = primaire.getMastodonApi();
        fetcher = primaire.getImageFetcher();

        display = new ProfileComponent(this, fetcher);
        add(display);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setIconImage(primaire.getProgramIcon());
        setScale(20);

        this.addComponentListener(this);
        addMouseWheelListener(new MouseWheelScaler(1, 10));
        addKeyListener(new SharedKeyShortcuts(primaire, true));
    }

}

class
ProfileComponent extends JPanel
implements Scalable, ActionListener, ImageFetcher.Listener {

    private ProfileWindow
    primaire;

    private ImageFetcher
    fetcher;

    private Image
    avatar;

    private boolean
    gif;

//   -  -%-  -

    private Image
    scaledAvatar;

    private JLabel
    accountId,
    displayName,
    followed,
    history;

    private RichTextPane4
    field1,
    field2,
    field3,
    field4;

    private JLabel
    accountIdLabel,
    displayNameLabel,
    followedLabel,
    historyLabel,
    field1Label,
    field2Label,
    field3Label,
    field4Label;

    private RichTextPane4
    description;

    private JScrollPane
    scroll;

    private JButton
    seePosts,
    seeFollowers;

    private Point[]
    distances;

    private int
    scale;

//   -  -%-  -

    private static final Cursor
    WAIT = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

//  ---%-@-%---

    public void
    setAvatar(Image avatar, boolean gif)
    {
        if (scaledAvatar != null && scaledAvatar != avatar)
            scaledAvatar.flush();

        this.avatar = avatar;
        scaledAvatar = null;
        this.gif = gif;
        considerRescale();
        repaint();
    }

    public void
    setAccountID(String id)
    {
        accountId.setText(id);
    }

    public void
    setDisplayName(String name)
    {
        displayName.setText(name);
    }

    public void
    setFollowedAndFollowers(String text)
    {
        followed.setText(text);
    }

    public void
    setHistory(String text)
    {
        history.setText(text);
    }

    public void
    setField(int index, String name, Tree<String> value)
    {
        assert index >= 1 && index <= 4;
        JLabel label = null;
        RichTextPane4 field = null;
        switch (index)
        {
            case 1: label = field1Label; field = field1; break;
            case 2: label = field2Label; field = field2; break;
            case 3: label = field3Label; field = field3; break;
            case 4: label = field4Label; field = field4; break;
        }
        label.setText(name);
        field.setText(value);
    }

    public void
    setDescription(Tree<String> html)
    {
        description.setText(html);
    }

    public int
    getScale() { return scale; }

    public void
    setScale(int scale)
    {
        this.scale = scale;

        String fname = "VL Gothic";
        int fsz = 14 * scale/20;
        Font f1 = new Font(fname, Font.PLAIN, fsz);
        Font f2 = new Font(fname, Font.PLAIN, fsz * 13/14);

        accountIdLabel.setFont(f1);
        displayNameLabel.setFont(f1);
        followedLabel.setFont(f2);
        historyLabel.setFont(f1);
        field1Label.setFont(f1);
        field2Label.setFont(f1);
        field3Label.setFont(f1);
        field4Label.setFont(f1);
        accountId.setFont(f1);
        displayName.setFont(f1);
        followed.setFont(f1);
        history.setFont(f1);
        field1.setFont(f2);
        field2.setFont(f2);
        field3.setFont(f2);
        field4.setFont(f2);

        description.setFont(f2);

        seePosts.setFont(f1);
        seeFollowers.setFont(f1);

        scroll.getVerticalScrollBar().setUnitIncrement(fsz);
    }

    public void
    resetFocus()
    {
        seePosts.requestFocusInWindow();
    }

//   -  -%-  -

    public void
    imageReady(String name, Image image)
    {
        this.scaledAvatar = image;
        repaint();
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();

        setCursor(WAIT);
        if (src == seePosts) primaire.seePosts();
        if (src == seeFollowers) primaire.seeFollowers();
        setCursor(null);
    }

    protected void
    paintComponent(Graphics g)
    {
        g.clearRect(0, 0, getWidth(), getHeight());

        int w = getWidth(), h = getHeight();

        if (scaledAvatar != null)
        {
            int aw = w * 4/10;
            int ah = aw;
            int ax = (w - aw) / 2;
            int ay = 10;
            int acx = ax + (aw / 2);
            int acy = ay + (ah / 2);
            Shape defaultClip = g.getClip();
            g.setClip(new Ellipse2D.Float(ax, ay, aw, ah));
            g.drawImage(
                scaledAvatar, ax, ay, aw, ah,
                gif ? this : null);
            g.setClip(defaultClip);
        }

        Point[] d = distances;
        g.setColor(new Color(0, 0, 0, 50));
        g.fillRect(0, d[2 - 1].y, d[2 - 1].x, 1);
        g.fillRect(0, d[5 - 1].y, d[5 - 1].x, 1);
        g.fillRect(0, d[8 - 1].y, d[8 - 1].x, 1);
        g.fillRect(0, d[11 - 1].y, d[11 - 1].x, 1);
        g.fillRect(d[14 - 1].x, d[14 - 1].y, w, 1);
        g.fillRect(d[17 - 1].x, d[17 - 1].y, w, 1);
        g.fillRect(d[20 - 1].x, d[20 - 1].y, w, 1);
        g.fillRect(d[23 - 1].x, d[23 - 1].y, w, 1);
        g.setColor(new Color(0, 0, 0, 15));
        g.fillRect(0, d[2 - 1].y + 1, d[2 - 1].x, 1);
        g.fillRect(0, d[5 - 1].y + 1, d[5 - 1].x, 1);
        g.fillRect(0, d[8 - 1].y + 1, d[8 - 1].x, 1);
        g.fillRect(0, d[11 - 1].y + 1, d[11 - 1].x, 1);
        g.fillRect(d[14 - 1].x, d[14 - 1].y + 1, w, 1);
        g.fillRect(d[17 - 1].x, d[17 - 1].y + 1, w, 1);
        g.fillRect(d[20 - 1].x, d[20 - 1].y + 1, w, 1);
        g.fillRect(d[23 - 1].x, d[23 - 1].y + 1, w, 1);
    }

    public void
    doLayout()
    {
        final double TAU = 2 * Math.PI;
        int w = getWidth(), h = getHeight();
        int aw = w * 4/10;
        int ah = aw;
        int ax = (w - aw) / 2;
        int ay = 10 * scale/20;
        int acx = ax + (aw / 2);
        int acy = ay + (ah / 2);

        Point[] d = new Point[24];
        for (int o = 0; o < 24; ++o) d[o] = new Point();

        int xr = aw * 58/100, yr = ah * 53/100;

        d[1 - 1].x = acx + (int)(xr * Math.cos(TAU * (225 + 7)/360));
        d[2 - 1].x = acx + (int)(yr * Math.cos(TAU * (225 + 0)/360));
        d[3 - 1].x = acx + (int)(xr * Math.cos(TAU * (225 - 7)/360));
        d[4 - 1].x = acx + (int)(xr * Math.cos(TAU * (195 + 5)/360));
        d[5 - 1].x = acx + (int)(yr * Math.cos(TAU * (195 + 0)/360));
        d[6 - 1].x = acx + (int)(xr * Math.cos(TAU * (195 - 5)/360));
        d[7 - 1].x = acx + (int)(xr * Math.cos(TAU * (165 + 5)/360));
        d[8 - 1].x = acx + (int)(yr * Math.cos(TAU * (165 + 0)/360));
        d[9 - 1].x = acx + (int)(xr * Math.cos(TAU * (165 - 5)/360));
        d[10 - 1].x = acx + (int)(xr * Math.cos(TAU * (135 + 7)/360));
        d[11 - 1].x = acx + (int)(yr * Math.cos(TAU * (135 + 0)/360));
        d[12 - 1].x = acx + (int)(xr * Math.cos(TAU * (135 - 7)/360));

        d[13 - 1].x = acx + (int)(xr * Math.cos(TAU * (-45 - 7)/360));
        d[14 - 1].x = acx + (int)(yr * Math.cos(TAU * (-45 + 0)/360));
        d[15 - 1].x = acx + (int)(xr * Math.cos(TAU * (-45 + 7)/360));
        d[16 - 1].x = acx + (int)(xr * Math.cos(TAU * (-15 - 5)/360));
        d[17 - 1].x = acx + (int)(yr * Math.cos(TAU * (-15 + 0)/360));
        d[18 - 1].x = acx + (int)(xr * Math.cos(TAU * (-15 + 5)/360));
        d[19 - 1].x = acx + (int)(xr * Math.cos(TAU * (15 - 5)/360));
        d[20 - 1].x = acx + (int)(yr * Math.cos(TAU * (15 + 0)/360));
        d[21 - 1].x = acx + (int)(xr * Math.cos(TAU * (15 + 5)/360));
        d[22 - 1].x = acx + (int)(xr * Math.cos(TAU * (45 - 7)/360));
        d[23 - 1].x = acx + (int)(yr * Math.cos(TAU * (45 + 0)/360));
        d[24 - 1].x = acx + (int)(xr * Math.cos(TAU * (45 + 7)/360));

        d[1 - 1].y = acy + (int)(yr * Math.sin(TAU * (225 + 7)/360));
        d[2 - 1].y = acy + (int)(yr * Math.sin(TAU * (225 + 0)/360));
        d[3 - 1].y = acy + (int)(yr * Math.sin(TAU * (225 - 7)/360));
        d[4 - 1].y = acy + (int)(yr * Math.sin(TAU * (195 + 5)/360));
        d[5 - 1].y = acy + (int)(yr * Math.sin(TAU * (195 + 0)/360));
        d[6 - 1].y = acy + (int)(yr * Math.sin(TAU * (195 - 5)/360));
        d[7 - 1].y = acy + (int)(yr * Math.sin(TAU * (165 + 5)/360));
        d[8 - 1].y = acy + (int)(yr * Math.sin(TAU * (165 + 0)/360));
        d[9 - 1].y = acy + (int)(yr * Math.sin(TAU * (165 - 5)/360));
        d[10 - 1].y = acy + (int)(yr * Math.sin(TAU * (135 + 7)/360));
        d[11 - 1].y = acy + (int)(yr * Math.sin(TAU * (135 + 0)/360));
        d[12 - 1].y = acy + (int)(yr * Math.sin(TAU * (135 - 7)/360));

        d[13 - 1].y = acy + (int)(yr * Math.sin(TAU * (-45 - 7)/360));
        d[14 - 1].y = acy + (int)(yr * Math.sin(TAU * (-45 + 0)/360));
        d[15 - 1].y = acy + (int)(yr * Math.sin(TAU * (-45 + 7)/360));
        d[16 - 1].y = acy + (int)(yr * Math.sin(TAU * (-15 - 5)/360));
        d[17 - 1].y = acy + (int)(yr * Math.sin(TAU * (-15 + 0)/360));
        d[18 - 1].y = acy + (int)(yr * Math.sin(TAU * (-15 + 5)/360));
        d[19 - 1].y = acy + (int)(yr * Math.sin(TAU * (15 - 5)/360));
        d[20 - 1].y = acy + (int)(yr * Math.sin(TAU * (15 + 0)/360));
        d[21 - 1].y = acy + (int)(yr * Math.sin(TAU * (15 + 5)/360));
        d[22 - 1].y = acy + (int)(yr * Math.sin(TAU * (45 - 7)/360));
        d[23 - 1].y = acy + (int)(yr * Math.sin(TAU * (45 + 0)/360));
        d[24 - 1].y = acy + (int)(yr * Math.sin(TAU * (45 + 7)/360));

        Font f1 = accountIdLabel.getFont();
        Font f2 = field1.getFont();
        FontMetrics fm1 = getFontMetrics(f1);
        FontMetrics fm2 = getFontMetrics(f2);
        int lh1 = fm1.getAscent() + fm1.getDescent();
        int lh2 = fm2.getAscent() + fm2.getDescent();

        accountIdLabel.setLocation(0, d[1 - 1].y - lh1/2);
        displayNameLabel.setLocation(0, d[4 - 1].y - lh1/2);
        followedLabel.setLocation(0, d[7 - 1].y - lh2/2);
        historyLabel.setLocation(0, d[10 - 1].y - lh1/2);
        accountIdLabel.setSize(d[1 - 1].x - 0, lh1);
        displayNameLabel.setSize(d[4 - 1].x - 0, lh1);
        followedLabel.setSize(d[7 - 1].x - 0, lh2);
        historyLabel.setSize(d[10 - 1].x - 0, lh1);

        field1Label.setLocation(d[13 - 1].x, d[13 - 1].y - lh1/2);
        field2Label.setLocation(d[16 - 1].x, d[16 - 1].y - lh1/2);
        field3Label.setLocation(d[19 - 1].x, d[19 - 1].y - lh1/2);
        field4Label.setLocation(d[22 - 1].x, d[22 - 1].y - lh1/2);
        field1Label.setSize(w - d[13 - 1].x, lh1);
        field2Label.setSize(w - d[16 - 1].x, lh1);
        field3Label.setSize(w - d[19 - 1].x, lh1);
        field4Label.setSize(w - d[22 - 1].x, lh1);

        accountId.setLocation(0, d[3 - 1].y - lh1/2);
        displayName.setLocation(0, d[6 - 1].y - lh1/2);
        followed.setLocation(0, d[9 - 1].y - lh1/2);
        history.setLocation(0, d[12 - 1].y - lh1/2);
        accountId.setSize(d[3 - 1].x - 0, lh1);
        displayName.setSize(d[6 - 1].x , lh1);
        followed.setSize(d[9 - 1].x , lh1);
        history.setSize(d[12 - 1].x , lh1);

        field1.setLocation(d[15 - 1].x, d[15 - 1].y - lh2/2);
        field2.setLocation(d[18 - 1].x, d[18 - 1].y - lh2/2);
        field3.setLocation(d[21 - 1].x, d[21 - 1].y - lh2/2);
        field4.setLocation(d[24 - 1].x, d[24 - 1].y - lh2/2);
        field1.setSize(w - d[15 - 1].x, lh2);
        field2.setSize(w - d[18 - 1].x, lh2);
        field3.setSize(w - d[21 - 1].x, lh2);
        field4.setSize(w - d[24 - 1].x, lh2);

        this.distances = d;

        int m = 10 * scale/20;
        int bw = (w - (4 * m)) / 4;
        int bh = seePosts.getPreferredSize().height;
        bh = bh * Math.max(scale, 20)/20;
        int by = h - m - bh;
        seePosts.setLocation(m, by);
        seePosts.setSize(bw, bh);
        seeFollowers.setLocation(m + bw + m, by);
        seeFollowers.setSize(bw, bh);

        scroll.setLocation(m, (ay + ah) + m);
        scroll.setSize(w - (2 * m), seePosts.getY() - m - scroll.getY());
    }

    private void
    considerRescale()
    {
        if (avatar != null)
        {
            if (!gif)
            {
                int aw = getWidth() * 4/10;
                int ah = aw;

                boolean rescale = true;
                if (scaledAvatar != null)
                {
                    int sw = scaledAvatar.getWidth(null);
                    int sh = scaledAvatar.getHeight(null);
                    rescale = sw != aw || sh != ah;
                }

                boolean rescaling = scaledAvatar == avatar;
                if (rescale && !rescaling)
                {
                    scaledAvatar = avatar;
                    fetcher.scale(
                        avatar, aw, ah,
                        null, 1, this);
                }
            }
            else
            {
                scaledAvatar = avatar;
            }
        }
    }

//  ---%-@-%---

    ProfileComponent(
        ProfileWindow primaire, ImageFetcher fetcher)
    {
        this.primaire = primaire;
        this.fetcher = fetcher;

        String[] s = new String[] {
            JapaneseStrings.get("profile account id"),
            JapaneseStrings.get("profile display name"),
            JapaneseStrings.get("profile followed"),
            JapaneseStrings.get("profile history"),
            JapaneseStrings.get("profile see posts"),
            JapaneseStrings.get("profile see followers")
        };

        int a = JLabel.RIGHT;
        accountIdLabel = new JLabel(s[0], a);
        displayNameLabel = new JLabel(s[1], a);
        followedLabel = new JLabel(s[2], a);
        historyLabel = new JLabel(s[3], a);
        field1Label = new JLabel("");
        field2Label = new JLabel("");
        field3Label = new JLabel("");
        field4Label = new JLabel("");

        accountId = new JLabel("", a);
        displayName = new JLabel("", a);
        followed = new JLabel("", a);
        history = new JLabel("", a);
        field1 = new RichTextPane4(fetcher);
        field2 = new RichTextPane4(fetcher);
        field3 = new RichTextPane4(fetcher);
        field4 = new RichTextPane4(fetcher);
        /*
        field1.setMultiline(false);
        field2.setMultiline(false);
        field3.setMultiline(false);
        field4.setMultiline(false);
        */

        description = new RichTextPane4(fetcher);

        scroll = new JScrollPane(
            description,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );
        scroll.setBorder(null);

        seePosts = new JButton(s[4]);
        seeFollowers = new JButton(s[5]);
        Insets m1 = seePosts.getMargin();
        Insets m2 = seeFollowers.getMargin();
        m1.left = m1.right = m2.left = m2.right = 1;
        seePosts.setMargin(m1);
        seeFollowers.setMargin(m1);
        seePosts.addActionListener(this);
        seeFollowers.addActionListener(this);

        setLayout(null);
        add(accountIdLabel);
        add(accountId);
        add(displayNameLabel);
        add(displayName);
        add(followedLabel);
        add(followed);
        add(historyLabel);
        add(history);
        add(field1Label);
        add(field1);
        add(field2Label);
        add(field2);
        add(field3Label);
        add(field3);
        add(field4Label);
        add(field4);
        add(scroll);
        add(seePosts);
        add(seeFollowers);
    }

}
